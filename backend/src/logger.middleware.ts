import {
  NestMiddleware,
  Injectable,
  ForbiddenException,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response } from 'express';

/** The AuthMiddleware is used to
 * (1) read the request header bearer token/user access token
 * (2) decrypt the access token to get the user object
 */
@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor() {}

  async use(req: Request | any, res: Response, next: () => void) {
    const bearerHeader = req.headers.authorization;
    // const accessToken = bearerHeader && bearerHeader.split(' ')[1];
    const accessToken = bearerHeader
      ? bearerHeader.replace('Bear', '').trim()
      : null;
    if (!bearerHeader || !accessToken) {
      throw new HttpException(
        'Please authentication ',
        HttpStatus.UNAUTHORIZED,
      );
    }
    next();
  }
}
