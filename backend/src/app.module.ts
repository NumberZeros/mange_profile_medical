import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { configService } from './configuration';
import { RootModule } from './containers/modules.root';
import { AppLoggerMiddleware } from './services/packages/middleware';

@Module({
  imports: [
    TypeOrmModule.forRootAsync(
      /* ------------------------------ Apply heroku ------------------------------ */
      // process.env.NODE_ENV == 'production'
      //   ? {
      //       useFactory: () => ({
      //         url: process.env.DATABASE_URL,
      //         type: 'postgres',
      //         entities: [join(__dirname, '**', '*.entity.{ts,js}')],
      //         synchronize: true,
      //         ssl: {
      //           rejectUnauthorized: false,
      //         },
      //       }),
      //     }
      //   :
      /* ----------------------------------- End ---------------------------------- */
      {
        useFactory: async () =>
          Object.assign(await configService.getTypeOrmConfig(), {
            autoLoadEntities: true,
          }),
      },
    ),
    // process.env.TYPE == 'apis'
    //   ? GraphQLModule.forRoot({
    //       autoSchemaFile: true,
    //       playground: true,
    //       introspection: true,
    //       context: ({ req }) => ({ ...req }),
    //     })
    //   : ScheduleModule.forRoot(),
    GraphQLModule.forRoot({
      autoSchemaFile: true,
      playground: true,
      introspection: true,
      context: ({ req }) => ({ ...req }),
    }),
    ScheduleModule.forRoot(),
    RootModule,
  ],
})
export class AppModule implements NestModule {
  constructor(private connection: Connection) {}
  configure(consumer: MiddlewareConsumer): void {
    consumer.apply(AppLoggerMiddleware).forRoutes('*');
  }
}
