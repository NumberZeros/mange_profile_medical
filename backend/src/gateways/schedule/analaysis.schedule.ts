import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import * as moment from 'moment';
import { AnalysisEntity } from 'src/entities/analysis.entity';
import { AnalaysisServices } from 'src/services/reports/analaysis/analaysis.service';
import { PatientServices } from 'src/services/core/patients/patient.service';
import { DoctorServices } from 'src/services/core/users/doctor.service';

@Injectable()
export class AnalaysisSchedule {
  private readonly logger = new Logger(AnalaysisSchedule.name);
  constructor(
    private patientServies: PatientServices,
    private doctorServices: DoctorServices,
    private analaysisServices: AnalaysisServices,
  ) {}
  calculationDateOfWeek = (now: Date) => {
    const startWeek = moment(now).startOf('week').utc().toDate();
    const endWeek = moment(now).endOf('week').utc().toDate();
    const listDateOfWeek = [];
    let countDate = startWeek;
    while (countDate.valueOf() <= endWeek.valueOf()) {
      listDateOfWeek.push(moment(countDate).format('DD/MM/YYYY'));
      countDate = moment(countDate).add(1, 'days').toDate();
    }
    return { listDateOfWeek, startWeek, endWeek };
  };

  updateChart = async ({ user, startWeek, endWeek, listDateOfWeek }) => {
    try {
      let foundUser = await this.analaysisServices.findOne({
        user,
        startweek: moment(startWeek).format('DD/MM/YYYY'),
      });
      if (!foundUser) {
        const analaysis = Object.assign(new AnalysisEntity(), {
          user,
          startweek: moment(startWeek).format('DD/MM/YYYY'),
        });
        foundUser = await this.analaysisServices.post(analaysis);
      }

      const foundDoctor =
        user !== 'admin' ? await this.doctorServices.findUserID(user) : null;

      const { totalPatientNow, totalPatient, totalPatientRiskAssessment } =
        await this.patientServies.calculationPatient(
          startWeek,
          endWeek,
          user !== 'admin' ? foundDoctor.id : null,
        );

      const {
        dataPatientNegative,
        dataTablePatientContacDescription,
        dataTablePatientCreated,
      } = await this.patientServies.getDataTableNegativeAndContact({
        startWeek,
        endWeek,
        listDateOfWeek,
        doctorid: user !== 'admin' ? foundDoctor.id : null,
      });
      const data = Object.assign(new AnalysisEntity(), {
        ...foundUser,
        totalPatient,
        totalPatientNow,
        totalPatientRiskAssessment,
        dataTablePatientCreated: JSON.stringify(dataTablePatientCreated),
        dataTablePatientNegative: JSON.stringify(dataPatientNegative),
        dataTablePatientContacDescription: JSON.stringify(
          dataTablePatientContacDescription,
        ),
      });
      console.table(dataTablePatientCreated);
      // console.table(dataPatientNegative);
      // console.table(dataTablePatientContacDescription);

      this.analaysisServices.post(data);
    } catch (error) {
      console.log(error);
    }
  };

  @Cron('0 1 * * * *')
  // @Cron('0 * * * * *')
  async AnalaysisForAdmin(): Promise<void> {
    try {
      const { listDateOfWeek, startWeek, endWeek } = this.calculationDateOfWeek(
        new Date(),
      );
      this.updateChart({
        user: 'admin',
        startWeek,
        endWeek,
        listDateOfWeek,
      });
    } catch (error) {
      this.logger.error(`ERR AnalaysisForAdmin ${error}`);
    }
  }

  @Cron('0 30 * * * *')
  // @Cron('1 * * * * *')
  async AnalaysisForDoctor(): Promise<void> {
    try {
      const { listDateOfWeek, startWeek, endWeek } = this.calculationDateOfWeek(
        new Date(),
      );
      const listDoctor = await this.doctorServices.findAllDoctorForSchedule();
      for (const doctor of listDoctor) {
        this.updateChart({
          user: doctor.user.id,
          startWeek,
          endWeek,
          listDateOfWeek,
        });
      }
    } catch (error) {
      this.logger.error(`ERR AnalaysisForDoctor ${error}`);
    }
  }
}
