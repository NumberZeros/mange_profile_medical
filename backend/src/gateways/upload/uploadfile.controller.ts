import {
  Controller,
  HttpException,
  HttpStatus,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { EGender, ERoles } from 'src/entities/users.entity';
import { AddressServices } from 'src/services/core/addresses/address.service';
import { CategoryServices } from 'src/services/core/categories/category.service';
import { PathogenicServices } from 'src/services/core/pathogenices/pathogenic.service';
import { PatientServices } from 'src/services/core/patients/patient.service';
import { FilesServices } from 'src/services/core/upload/file.service';
import { DoctorServices } from 'src/services/core/users/doctor.service';
import { UserServices } from 'src/services/core/users/user.service';
import * as _ from 'lodash';
import { ETransfer } from 'src/entities/patient.entity';
import * as moment from 'moment';
import { MedicalServices } from 'src/services/core/medicals/medical.service';
import {
  ECAttribute,
  EColor,
  ERiskAssesment,
} from 'src/entities/medical.entity';
import { DEFAULT_DATE } from 'src/services/packages';
import { GroupServices } from 'src/services/core/groups/group.service';
import { LogMedicalServices } from 'src/services/core/log_medicals/logs.service';
import { LogMedicalEntity } from 'src/entities/log_medical.entity';
import { PatientStatusServices } from 'src/services/core/patient_status/patitent_status.service';
@Controller('graphql/upload')
export class UploadController {
  constructor(
    private fileServies: FilesServices,
    private doctorServies: DoctorServices,
    private userServices: UserServices,
    private addressServices: AddressServices,
    private patientServies: PatientServices,
    private pathogenicServies: PathogenicServices,
    private categoryServies: CategoryServices,
    private medicalServices: MedicalServices,
    private groupServices: GroupServices,
    private logServices: LogMedicalServices,
    private statusServices: PatientStatusServices,
  ) {}

  @Post('doctors')
  @UseInterceptors(FileInterceptor('file'))
  async uploadDotor(@UploadedFile() file: Express.Multer.File) {
    try {
      const fileName = file.originalname;
      const isValid = await this.fileServies.checkFileNameFormat({
        fileName: fileName.toUpperCase(),
        type: 'DOCTOR',
      });
      if (!isValid)
        throw new HttpException(
          `Tên file chưa đúng với định dạng. Vui lòng sửa lại theo format "DOCTOR 20210802 1000001.csv"`,
          HttpStatus.BAD_REQUEST,
        );
      const data = await file.buffer.toString('utf-8');
      const convertData = await this.fileServies.convertCSV(data, false);
      if (!convertData.success)
        throw new HttpException(
          `Vui lòng nhập đúng dữ liệu với format"`,
          HttpStatus.BAD_REQUEST,
        );
      const listUser = [];
      for (const item of convertData.data) {
        const foundEmail = await this.userServices.findEmail(item['EMAIL']);
        if (foundEmail) {
          continue;
        }

        const foundPhone = await this.userServices.findPhone(
          `(+84) ${item['DIEN THOAI']}`,
        );
        if (foundPhone) {
          continue;
        }

        try {
          const dataUser = {
            lastname: item['HO VA TEN LOT'],
            firstname: item['TEN'],
            email: item['EMAIL'],
            phone: `(+84) ${item['DIEN THOAI']}`,
            dob:
              item['NGAY THANG NAM SINH'] !== ''
                ? moment(item['NGAY THANG NAM SINH']).toDate()
                : DEFAULT_DATE,
            gender:
              item['GIOI TINH'].toUpperCase() === 'NAM'
                ? EGender.male
                : EGender.female,
            certificate: item['CHUNG CHI HANH NGHE'],
          };
          const externalid = fileName.replace('doctor', '').trim();
          const user = await this.userServices.intergateCSV(
            {
              ...dataUser,
              isverified: true,
              roles: ERoles.doctor,
              password: dataUser.email,
            },
            externalid,
          );

          await this.addressServices.post(
            {
              isverified: false,
              name: item['DIA CHI'] !== '' ? item['DIA CHI'] : '',
              ward: item['XA/PHUONG'] !== '' ? item['XA/PHUONG'] : '',
              province: item['QUAN/HUYEN'] !== '' ? item['QUAN/HUYEN'] : '',
              city: item['TINH/TP'] !== '' ? item['TINH/TP'] : '',
              user: user.id,
            },
            user,
          );
          const group = await this.groupServices.findName('');
          await this.doctorServies.integrateCSV(
            { certificate: dataUser.certificate, externalid },
            { user, groups: [group] },
          );
          listUser.push(dataUser);
        } catch (error) {}
      }
      return { success: true, data: listUser };
    } catch (error) {
      throw new HttpException(error, HttpStatus.FORBIDDEN);
    }
  }

  @Post('patients')
  @UseInterceptors(FileInterceptor('file'))
  async uploadPatients(@UploadedFile() file: Express.Multer.File) {
    try {
      const fileName = file.originalname;
      const isValid = await this.fileServies.checkFileNameFormat({
        fileName: fileName.toUpperCase(),
        type: 'PATIENT',
      });
      if (!isValid)
        throw new HttpException(
          `Tên file chưa đúng với định dạng. Vui lòng sửa lại theo format "PATIENT 20210802 1000001.csv"`,
          HttpStatus.BAD_REQUEST,
        );
      const data = await file.buffer.toString('utf-8');

      const convertData = await this.fileServies.convertCSV(data, false);
      if (!convertData.success)
        throw new HttpException(
          `Vui lòng nhập đúng dữ liệu với format"`,
          HttpStatus.BAD_REQUEST,
        );

      const listPatients = [];

      for (const item of convertData.data) {
        // console.log(`Integration ${fileName}`, item);
        try {
          if (
            !item['HO VA TEN LOT'] ||
            item['HO VA TEN LOT'] === '' ||
            !item['TEN'] ||
            item['TEN'] === '' ||
            !item['DIEN THOAI'] ||
            item['DIEN THOAI'] === '' ||
            !item['NGAY THANG NAM SINH'] ||
            item['NGAY THANG NAM SINH'] === ''
          ) {
            // console.log('Data not valid', item);
            continue;
          }

          const foundPatient = await this.patientServies.findUniqPatient({
            firstname: item['TEN'],
            lastname: item['HO VA TEN LOT'],
            phone: `(+84) ${item['DIEN THOAI']}`,
            dob: item['NGAY THANG NAM SINH'],
          });

          if (foundPatient) {
            // console.log(`Found patient`, foundPatient);
            continue;
          }

          try {
            const dataPatient = {
              lastname: item['HO VA TEN LOT'],
              firstname: item['TEN'],
              localcode: item['MA BENH NHAN CUA PHUONG'],
              otherid: item['ID KHAC'],
              gender:
                item['GIOI TINH'].toUpperCase() === 'NAM'
                  ? EGender.male
                  : EGender.female,
              phone: `(+84) ${item['DIEN THOAI']}`,
              dob:
                item['NGAY THANG NAM SINH'] !== ''
                  ? moment(item['NGAY THANG NAM SINH']).toDate()
                  : DEFAULT_DATE,
              quantityfamily:
                item['SO NGUOI SONG CHUNG'] !== ''
                  ? item['SO NGUOI SONG CHUNG']
                  : 0,
              doctorid:
                item['EMAIL BAC SI'] !== ''
                  ? item['EMAIL BAC SI']
                  : 'macdinh@gmail.com',
              firstdatecovid:
                item['NGAY DAU TIEN CÓ XET NGHIEM PRC'] !== ''
                  ? moment(item['NGAY DAU TIEN CÓ XET NGHIEM PRC']).toDate()
                  : DEFAULT_DATE,
              pathogenicdate:
                item['NGAY CO TRIEU CHUNG DAU TIEN'] === ''
                  ? moment(item['NGAY CO TRIEU CHUNG DAU TIEN']).toDate()
                  : DEFAULT_DATE,
              pathogenicname: item['TRIEU CHUNG DAU TIEN'],
              otherreason: item['YEU TO NGUY CO KHAC'],
              beforesick: item['BENH MAN TINH TRUOC DAY'],
              notesatussick: item['TINH TRANG BENH MAN TINH HIEN TAI'],
              quantittyvacin: item['SO LAN TIEM VACCINE'],
              source: item['NOI CHUYEN VIEN'],
              name: item['DIA CHI'],
              ward: item['XA/PHUONG'],
              province: item['QUAN/HUYEN'],
              city: item['TINH/TP'],
            };
            const foundDoctor = await this.doctorServies.findEmail(
              dataPatient.doctorid,
            );
            // const foundCategory = await this.categoryServies.findName(
            //   dataPatient.category,
            // );
            // if (!foundCategory) {
            //   console.error('Patient Integration not found category');
            // }
            // const listPathogenic = [];
            // const listPathogenicNotFound = [];

            // if (item['YEU TO NGUY CO'] && item['YEU TO NGUY CO'] !== '') {
            //   for (const pathogenicItem of item['YEU TO NGUY CO'].split(';')) {
            //     const foudPathogenic = await this.pathogenicServies.findName(
            //       pathogenicItem.trim(),
            //     );
            //     if (!foudPathogenic)
            //       listPathogenicNotFound.push(pathogenicItem.trim());
            //     else listPathogenic.push(foudPathogenic);
            //   }

            //   if (listPathogenicNotFound.length > 0) {
            //     console.error(
            //       'Patient Integration not found pathogenic',
            //       listPathogenicNotFound,
            //     );
            //   }
            // }
            const externalid = fileName.replace('patient', '').trim();
            let dataSource;
            switch (dataPatient.source) {
              case 'Bệnh viện/Phòng khám':
                dataSource = ETransfer.hopital;
                break;
              case 'Y tế địa phương':
                dataSource = ETransfer.healthfacilities;
                break;
              case 'F1 đang theo dõi':
                dataSource = ETransfer.followed;
                break;
              case 'Tự đăng kí':
                dataSource = ETransfer.single;
                break;

              default:
                dataSource = ETransfer.other;
                break;
            }

            const patient = await this.patientServies.integrateCSV(
              {
                ...dataPatient,
                source: dataSource,
                quantittyvacin: parseFloat(dataPatient.quantittyvacin),
              },
              {
                externalid,
                isverified: false,
                doctor: foundDoctor
                  ? foundDoctor
                  : await this.doctorServies.findEmail('macdinh@gmail.com'),
                category: await this.categoryServies.findName('Không'),
                pathogenic: [await this.pathogenicServies.findName('Không')],
                sharedoctor: foundDoctor,
                status: await this.statusServices.findName(''),
              },
            );
            listPatients.push(patient);
          } catch (error) {
            // console.log('ERR Create patient', error);
          }
        } catch (error) {
          // console.log('ERR Patient integrate ', error);
        }
      }
      return listPatients;
    } catch (error) {
      throw new HttpException(error, HttpStatus.FORBIDDEN);
    }
  }
  @Post('medical')
  @UseInterceptors(FileInterceptor('file'))
  async uploadMedical(@UploadedFile() file: Express.Multer.File) {
    try {
      const fileName = file.originalname;
      const isValid = await this.fileServies.checkFileNameFormat({
        fileName: fileName.toUpperCase(),
        type: 'MEDICAL',
      });
      if (!isValid)
        throw new HttpException(
          `Tên file chưa đúng với định dạng. Vui lòng sửa lại theo format "Medical 20210802 1000001.csv"`,
          HttpStatus.BAD_REQUEST,
        );
      const data = await file.buffer.toString('utf-8');

      const convertData = await this.fileServies.convertCSV(data, false);
      if (!convertData.success)
        throw new HttpException(
          `Vui lòng nhập đúng dữ liệu với format"`,
          HttpStatus.BAD_REQUEST,
        );

      const listMedical = [];

      for (const item of convertData.data) {
        // console.log(`Integration ${fileName}`, item);
        try {
          if (
            !item['HO VA TEN LOT'] ||
            item['HO VA TEN LOT'] === '' ||
            !item['TEN'] ||
            item['TEN'] === '' ||
            !item['DIEN THOAI'] ||
            item['DIEN THOAI'] === '' ||
            !item['NGAY THANG NAM SINH'] ||
            item['NGAY THANG NAM SINH'] === ''
          ) {
            // console.log('Data not valid', item);
            continue;
          }

          let foundPatient = await this.patientServies.findUniqPatient({
            firstname: item['TEN'],
            lastname: item['HO VA TEN LOT'],
            phone: `(+84) ${item['DIEN THOAI']}`,
            dob: item['NGAY THANG NAM SINH'],
          });

          if (!foundPatient) {
            foundPatient = await this.patientServies.integrateCSV(
              {
                firstname: item['TEN'],
                lastname: item['HO VA TEN LOT'],
                phone: `(+84) ${item['DIEN THOAI']}`,
                dob: item['NGAY THANG NAM SINH'],
                source: ETransfer.other,
                quantittyvacin: 0,
                localcode: '',
                gender: EGender.other,
                otherid: '',
                quantityfamily: 0,
                firstdatecovid: DEFAULT_DATE,
                pathogenicdate: DEFAULT_DATE,
                pathogenicname: '',
                beforesick: '',
                notesatussick: '',
                otherreason: '',
                name: '',
                ward: '',
                province: '',
                city: '',
              },
              {
                externalid: fileName.replace('medical', '').trim(),
                isverified: true,
                doctor: await this.doctorServies.findEmail('macdinh@gmail.com'),
                category: await this.categoryServies.findName('Không'),
                pathogenic: [await this.pathogenicServies.findName('Không')],
                sharedoctor: await this.doctorServies.findEmail(
                  'macdinh@gmail.com',
                ),
                status: await this.statusServices.findName(''),
              },
            );
          }
          try {
            const dataMedical = {
              weight: item['CAN NANG'] ? item['CAN NANG'] : 0,
              height: item['CHIEU CAO'] ? item['CHIEU CAO'] : 0,
              statusquo: item['BENH MAN TINH TRUOC DAY'],
              riskassessment: item['DANH GIA PHAN LOAI NGUY CO'],
              medicine: item['THUOC DANG SU DUNG'],
              countdateremains: item['THUOC DANG DUNG CON DU BAO NHIEU NGAY']
                ? item['THUOC DANG DUNG CON DU BAO NHIEU NGAY']
                : 0,
              signal1: item['Mặt hay môi tím tái'] == 1 ? true : false,
              signal2:
                item['Cảm thấy đau hoặc tức ngực nhiều không giảm'] == 1
                  ? true
                  : false,
              signal3: item['Khó thở rất nhiều'] == 1 ? true : false,
              signal4: item['Mất định hướng không gian'] == 1 ? true : false,
              signal5:
                item['Bất tỉnh hoặc rất khó thức giấc'] == 1 ? true : false,
              signal6: item['Nói lắp hoặc khó nói'] == 1 ? true : false,
              signal7: item['Co giật'] == 1 ? true : false,
              signal8: item['Tụt huyết áp'] == 1 ? true : false,
              signal9: item['Mất nước'] == 1 ? true : false,
              signal10:
                item['Không có(Dấu hiệu nguy hiểm)'] == 1 ? true : false,
              signal11:
                item[
                  'Hôm nay ông/bà có cảm thấy điều gì khác thường trong cơ thể so với trước đây không?'
                ] == 1
                  ? true
                  : false,
              signal12: item['Sốt hoặc cảm thấy muốn sốt'] == 1 ? true : false,
              signal13: item['Khó thở'] == 1 ? true : false,
              signal14: item['Đau ngực nhẹ'] == 1 ? true : false,
              signal15: item['Đau họng'] == 1 ? true : false,
              signal16: item['Ho: khan hay đàm'] == 1 ? true : false,
              signal17:
                item['Đau cơ hoặc đau mỏi khắp người'] == 1 ? true : false,
              signal18: item['Nôn ói'] == 1 ? true : false,
              signal19: item['Tiêu chảy'] == 1 ? true : false,
              signal20:
                item['Mất vị giác hoặc khứu giác mới xảy ra'] == 1
                  ? true
                  : false,
              signal21: item['Nghẹt mũi'] == 1 ? true : false,
              signal22: item['Sổ mũi'] == 1 ? true : false,
              signal23: item['Mệt mỏi'] == 1 ? true : false,
              signal24: item['Đau nhức đầu'] == 1 ? true : false,
              signal25:
                item['Không có(triệu chứng hiện có)'] == 1 ? true : false,
              otherSignal: item['Triệu chứng khác'],
              color: item['MAU DA VA MOI'],
              pluse: item['MACH'] ? parseFloat(item['MACH']) : 0,
              notepluse: item['GHI CHU (MACH)'],
              bloodpressure: item['TAM THU'] ? parseFloat(item['TAM THU']) : 0,
              bloodpressure2: item['TAM TRUONG']
                ? parseFloat(item['TAM TRUONG'])
                : 0,
              notebloodpressure: item['GHI CHU (HUYET AP)'],
              spo2home: item['SPO2'] ? parseFloat(item['SPO2']) : 0,
              notespo2home: item['GHI CHU (SPO2)'],
              temperature: item['NHIET DO'] ? parseFloat(item['NHIET DO']) : 0,
              notetemperature: item['GHI CHU (NHIET DO)'],
              other: item['KHAC'],
              attribute: item['DANH GIA MUC DO HIEN TAI'],
              noteattribute: item['GHI CHU ( BENH LY KHAC)'],
              quantityofsick: item['BENH NGAY THU MAY']
                ? parseFloat(item['BENH NGAY THU MAY'])
                : 0,
              guide: item['HUONG DAN CACH CHAM SOC O NHA'],
              summary: item['MIEU TA CACH XU LY'],
              contacdescription:
                item['LIEN HE DOI HIEN TRUONG'] == 1 ? true : false,
              reexamination: item['CÓ CAN TAI KHAM KHONG'] == 1 ? true : false,
              dateofreexamination: item['NGAY TAI KHAM']
                ? moment(item['NGAY TAI KHAM']).toDate()
                : DEFAULT_DATE,
              displaydate: item['NGAY NHAN BENH AN']
                ? moment(item['NGAY NHAN BENH AN'])
                    .subtract(7, 'hours')
                    .toDate()
                : new Date(),
            };

            let mapRisk;
            switch (dataMedical.riskassessment) {
              case 'Nguy cơ thấp':
                mapRisk = ERiskAssesment.low;
                break;
              case 'Nguy cơ trung bình':
                mapRisk = ERiskAssesment.medium;
                break;
              case 'Nguy cơ cao':
                mapRisk = ERiskAssesment.high;
                break;
              case 'Nguy cơ rất cao':
                mapRisk = ERiskAssesment.hightest;
                break;

              default:
                mapRisk = ERiskAssesment.no;
                break;
            }

            let mapColor;
            switch (dataMedical.color) {
              case 'Da niêm nhạt':
                mapColor = EColor.normal;
                break;
              case 'Da niêm hồng':
                mapColor = EColor.haggard;
                break;
              case 'Nổi ban':
                mapColor = EColor.typhus;
                break;

              default:
                mapColor = EColor.no;
                break;
            }

            let mapAttribute;
            switch (dataMedical.attribute) {
              case 'COVID-19 không triệu chứng':
                mapAttribute = ECAttribute.covidhide;
                break;
              case 'COVID-19 mức độ nhẹ (Viêm hô hấp trên cấp)':
                mapAttribute = ECAttribute.codvidsimple;
                break;
              case 'COVID-19 mức độ vừa (Viêm phổi)':
                mapAttribute = ECAttribute.covidmedium;
                break;
              case 'COVID-19 mức độ nặng (Viêm phổi nặng)':
                mapAttribute = ECAttribute.covidhight;
                break;
              case 'Nguy kịch':
                mapAttribute = ECAttribute.covidhightest;
                break;
              case 'F1 có triệu chứng (nghi nhiễm)':
                mapAttribute = ECAttribute.f1showcovid;
                break;
              case 'F1 không triệu chứng':
                mapAttribute = ECAttribute.f1hidecovid;
                break;
              default:
                mapAttribute = ECAttribute.no;
                break;
            }
            const externalid = fileName.replace('patient', '').trim();

            const resMedical = await this.medicalServices.integrateionCSV(
              {
                ...dataMedical,
                riskassessment: mapRisk,
                color: mapColor,
                attribute: mapAttribute,
                externalid,
              },
              { patient: foundPatient },
            );
            const dataOwner = await this.userServices.findAllAdmin();
            const dataLog = Object.assign(new LogMedicalEntity(), {
              createdby: dataOwner[0],
              patient: foundPatient,
              name: `${dataOwner[0].lastname} ${
                dataOwner[0].firstname
              } thêm hồ sơ bệnh án (BA ${moment(resMedical.createddat)
                .utcOffset('+07:00')
                .toDate()
                .valueOf()}) bằng csv`,
              description: '',
            });
            this.logServices.post(dataLog);
            await listMedical.push(dataMedical);
          } catch (error) {
            // console.log(`ERR Create patient ${fileName}`, error);
          }
        } catch (error) {
          // console.log('ERR Patient integrate ', error);
        }
      }
      return listMedical;
    } catch (error) {
      throw new HttpException(error, HttpStatus.FORBIDDEN);
    }
  }
}
