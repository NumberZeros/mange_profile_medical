import { Query, Resolver } from '@nestjs/graphql';

import { UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { EmergencyStatusServices } from 'src/services/core/emergency_status/emergency_stauts.service';
import { EmergencyStatusEntity } from 'src/entities/emegency_status.entity';

@Resolver()
export class EmergencyStatusResolver {
  constructor(private statusServices: EmergencyStatusServices) {}
  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [EmergencyStatusEntity])
  async find_all_emergency_status(): Promise<EmergencyStatusEntity[]> {
    return await this.statusServices.findAll();
  }
}
