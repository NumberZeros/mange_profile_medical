import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { UserServices } from '../../services/core/users/user.service';
import { GroupEntity } from 'src/entities/group.entity';
import { GroupServices } from 'src/services/core/groups/group.service';
import { DoctorServices } from 'src/services/core/users/doctor.service';
import { CreateGroupInput } from 'src/services/core/groups/dto/create.input';
import { ERoles } from 'src/entities/users.entity';
import { UpdateGroupInput } from 'src/services/core/groups/dto/put.input';
import { AddDoctorGroupInput } from 'src/services/core/groups/dto/add_list_doctor.input';
import { uniqBy } from 'lodash';
import { RemoveDoctorOutGroupInput } from 'src/services/core/groups/dto/remove_doctor_out_group.input';
import { SendMessageForGroupInput } from 'src/services/core/groups/dto/send_message_for_group.input';
import * as moment from 'moment';
import { secret } from 'src/configuration';
import { MedicalServices } from 'src/services/core/medicals/medical.service';
import { SMSServices } from 'src/services/core/sms/sms.service';
import { SMSEntity } from 'src/entities/sms.entity';
import { SendMessageForDoctorInput } from 'src/services/core/groups/dto/send_message_for_doctor.input';

@Resolver()
export class GroupResolver {
  constructor(
    private groupServices: GroupServices,
    private doctorServices: DoctorServices,
    private userServices: UserServices,
    private medicalServices: MedicalServices,
    private smsServices: SMSServices,
  ) {}
  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [GroupEntity])
  async find_all_group(
    @Args('adminid') adminid: string,
  ): Promise<GroupEntity[]> {
    // await this.userServices.validateAccount(adminid, ERoles.admin);
    return await this.groupServices.findAll();
  }
  @UseGuards(GraphqlAuthGuard)
  @Query((of) => GroupEntity)
  async find_group(
    @Args('id') id: string,
    @Args('adminid') adminid: string,
  ): Promise<GroupEntity> {
    await this.userServices.validateAccount(adminid, ERoles.admin);
    return await this.groupServices.findOne(id);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => GroupEntity)
  async add_group(
    @Args('input') input: CreateGroupInput,
  ): Promise<GroupEntity> {
    await this.userServices.validateAccount(input.adminid, ERoles.admin);
    return await this.groupServices.post(input);
  }
  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => GroupEntity)
  async put_group(
    @Args('input') input: UpdateGroupInput,
  ): Promise<GroupEntity> {
    await this.userServices.validateAccount(input.adminid, ERoles.admin);
    const foundGroup = await this.groupServices.findOne(input.id);
    if (!foundGroup)
      throw new HttpException('Not found group', HttpStatus.NOT_FOUND);

    return await this.groupServices.put(input);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => GroupEntity)
  async add_doctor(
    @Args('input') input: AddDoctorGroupInput,
  ): Promise<GroupEntity> {
    await this.userServices.validateAccount(input.adminid, ERoles.admin);
    const foundGroup = await this.groupServices.findOne(input.groupid);
    if (!foundGroup || !foundGroup.isverified)
      throw new HttpException('Not found group', HttpStatus.NOT_FOUND);
    for (const doctor of input.listDoctor) {
      const foundDoctor = await this.doctorServices.findOneV2(doctor);
      if (!foundGroup.doctors || foundGroup.doctors.length === 0)
        foundGroup.doctors = [foundDoctor];
      if (foundDoctor && foundGroup.doctors.length > 0)
        foundGroup.doctors.push(foundDoctor);
    }
    return await this.groupServices.putV2({
      ...foundGroup,
      doctors: uniqBy(foundGroup.doctors, 'id'),
    });
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => GroupEntity)
  async remove_doctor_out_group(
    @Args('input') input: RemoveDoctorOutGroupInput,
  ): Promise<GroupEntity> {
    await this.userServices.validateAccount(input.adminid, ERoles.admin);
    const foundGroupNon = await this.groupServices.findName('');
    const foundGroup = await this.groupServices.findOne(input.groupid);
    if (!foundGroup)
      throw new HttpException(
        'Not found group need remove doctor',
        HttpStatus.NOT_FOUND,
      );

    if (!foundGroupNon)
      throw new HttpException('Not found group', HttpStatus.NOT_FOUND);
    for (const doctor of input.listDoctor) {
      const foundDoctor = await this.doctorServices.findOneV2(doctor);
      if (!foundGroupNon.doctors || foundGroupNon.doctors.length === 0)
        foundGroupNon.doctors = [foundDoctor];
      if (foundDoctor && foundGroupNon.doctors.length > 0) {
        const doctor = await this.doctorServices.putV2({
          ...foundDoctor,
          groups:
            !foundDoctor.groups && foundDoctor.groups.length === 0
              ? []
              : foundDoctor.groups.filter((o) => o.id !== input.groupid),
        });
        if (doctor.groups.length === 0) foundGroupNon.doctors.push(doctor);
      }
    }
    return await this.groupServices.findOne(input.groupid);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => [SMSEntity])
  async send_message_for_group(
    @Args('input') input: SendMessageForGroupInput,
  ): Promise<SMSEntity[]> {
    const foundUser = await this.userServices.findOneV2(input.ownerid);

    if (!foundUser)
      throw new HttpException('Not found user', HttpStatus.NOT_FOUND);

    const foundGroup = await this.groupServices.findOne(input.groupid);
    if (!foundGroup)
      throw new HttpException('Not found group', HttpStatus.NOT_FOUND);

    const foundMedical = await this.medicalServices.findOneV2(input.medicalid);
    const foundPatient = foundMedical.patient;

    if (!foundMedical)
      throw new HttpException('Not found medical', HttpStatus.NOT_FOUND);

    const listSMS = [];
    for (const doctor of foundGroup.doctors) {
      const now = new Date().valueOf().toString();
      const message = await `${foundPatient.lastname} ${
        foundPatient.firstname
      } ${moment(foundPatient.dob).utcOffset('+07:00').format('DD/MM/YYYY')} (${
        foundPatient.name
      } ${foundPatient.ward} ${foundPatient.province}) ${
        foundPatient.phone
      } link ${secret.LINK_FE}/${now.slice(9, 13)}?id=${foundMedical.id}`;
      const sms = Object.assign(new SMSEntity(), {
        messageid: '',
        description: message,
        authid: now.slice(9, 13),
        doctor,
        medical: foundMedical,
      });
      const dataSMS = await this.smsServices.post(sms);
      listSMS.push(dataSMS);
    }

    return listSMS;
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => SMSEntity)
  async send_message_for_doctor(
    @Args('input') input: SendMessageForDoctorInput,
  ): Promise<SMSEntity> {
    const foundUser = await this.userServices.findOneV2(input.ownerid);

    if (!foundUser)
      throw new HttpException('Not found user', HttpStatus.NOT_FOUND);

    const foundDoctor = await this.doctorServices.findOneV2(input.doctorid);
    if (!foundDoctor)
      throw new HttpException('Not found doctor', HttpStatus.NOT_FOUND);

    const foundMedical = await this.medicalServices.findOneV2(input.medicalid);
    const foundPatient = foundMedical.patient;

    if (!foundMedical)
      throw new HttpException('Not found medical', HttpStatus.NOT_FOUND);

    const now = new Date().valueOf().toString();
    const message = await `${foundPatient.lastname} ${
      foundPatient.firstname
    } ${moment(foundPatient.dob).utcOffset('+07:00').format('DD/MM/YYYY')} (${
      foundPatient.name
    } ${foundPatient.ward} ${foundPatient.province}) ${
      foundPatient.phone
    } link ${secret.LINK_FE}/${now.slice(9, 13)}?id=${foundMedical.id}`;
    const sms = Object.assign(new SMSEntity(), {
      messageid: '',
      description: message,
      authid: now.slice(9, 13),
      doctor: foundDoctor,
      medical: foundMedical,
    });
    const dataSMS = await this.smsServices.post(sms);

    return dataSMS;
  }
}
