import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { MedicineServices } from 'src/services/core/medicine_details/medicine.service';
import { MedicineEntity } from 'src/entities/medicine_detail.entity';
import { CreateMedicineInput } from 'src/services/core/medicine_details/dto/post.input';
import { UserServices } from 'src/services/core/users/user.service';
import { MedicalServices } from 'src/services/core/medicals/medical.service';
import { PatientServices } from 'src/services/core/patients/patient.service';
import { LogMedicalEntity } from 'src/entities/log_medical.entity';
import { LogMedicalServices } from 'src/services/core/log_medicals/logs.service';
import { UpdateMedicineInput } from 'src/services/core/medicine_details/dto/update.input';
import { CreateMedicineForEmergencyInput } from 'src/services/core/medicine_details/dto/postForEmergency.input';
import { EmergencyServices } from 'src/services/core/emergency/emergency.service';
import { uniqBy } from 'lodash';
import * as moment from 'moment';

@Resolver()
export class MedicineResolver {
  constructor(
    private medicineServices: MedicineServices,
    private userServies: UserServices,
    private medicalServies: MedicalServices,
    private patientServies: PatientServices,
    private logMedicalServices: LogMedicalServices,
    private emergencyServices: EmergencyServices,
  ) {}
  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [MedicineEntity])
  async find_all_medicine(): Promise<MedicineEntity[]> {
    return uniqBy(await this.medicineServices.findAllMedicine(), 'name');
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => [MedicineEntity])
  async add_medicine(
    @Args('input') input: CreateMedicineInput,
  ): Promise<MedicineEntity[]> {
    const foundUser = await this.userServies.findOneV2(input.ownerid);
    if (!foundUser)
      throw new HttpException('Not found owner', HttpStatus.NOT_FOUND);

    const foundMedical = await this.medicalServies.findOneV2(input.medicalid);
    if (!foundMedical)
      throw new HttpException('Not found medical', HttpStatus.NOT_FOUND);

    const foundPatient = await this.patientServies.findOneV2(
      foundMedical.patient.id,
    );
    for (const dataMedicine of input.listMedicine) {
      const medicine = await this.medicineServices.post({
        ...input,
        ...dataMedicine,
      });
      const log = Object.assign(new LogMedicalEntity(), {
        patient: foundPatient,
        createdby: foundUser,
        name: `${foundUser.lastname} ${
          foundUser.firstname
        } thêm Thuốc: ${dataMedicine.name.toUpperCase()}, SL: ${
          dataMedicine.quantity
        }, Ghi chú: ${dataMedicine.description} cho hồ sơ bệnh án (HS ${moment(
          foundMedical.createddat,
        )
          .utcOffset('+07:00')
          .toDate()
          .valueOf()}) từ web`,
        description: `medicalid ${foundMedical.id}- update ${foundMedical.updatedat}`,
      });

      this.logMedicalServices.post(log);
      foundMedical.medicinenow.push(medicine);
    }
    const dataMedical = await this.medicalServies.addMedicine(foundMedical);

    return dataMedical.medicinenow;
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => MedicineEntity)
  async update_medicine(
    @Args('input') input: UpdateMedicineInput,
  ): Promise<MedicineEntity> {
    const foundUser = await this.userServies.findOne(input.ownerid);
    if (!foundUser)
      throw new HttpException('Not found owner', HttpStatus.NOT_FOUND);

    const foundMedicine = await this.medicineServices.findOne(input.id);
    if (!foundMedicine)
      throw new HttpException('Not found medicine', HttpStatus.NOT_FOUND);
    const foundMedical = await this.medicalServies.findOne(input.medicalid);
    if (!foundMedical)
      throw new HttpException('Not found medical', HttpStatus.NOT_FOUND);

    const foundPatient = await this.patientServies.findOne(
      foundMedical.patient.id,
    );
    const medicine = await this.medicineServices.put(input);

    const log = Object.assign(new LogMedicalEntity(), {
      patient: foundPatient,
      createdby: foundUser,
      name: `${foundUser.lastname} ${foundUser.firstname} cập nhật (Thuốc: ${
        foundMedicine.name
      }, SL: ${foundMedicine.quantity}, Ghi chú: ${
        foundMedicine.description
      }) -> (Thuốc: ${medicine.name}, SL: ${medicine.quantity}, Ghi chú: ${
        medicine.description
      }) cho hồ sơ bệnh án (HS ${moment(foundMedical.createddat)
        .utcOffset('+07:00')
        .toDate()
        .valueOf()}) từ web`,
      description: `medicalid ${foundMedical.id}- update ${foundMedical.updatedat}`,
    });
    await this.logMedicalServices.post(log);
    return medicine;
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => [MedicineEntity])
  async add_medicine_emegency(
    @Args('input') input: CreateMedicineForEmergencyInput,
  ): Promise<MedicineEntity[]> {
    const foundUser = await this.userServies.findOneV2(input.ownerid);
    if (!foundUser)
      throw new HttpException('Not found owner', HttpStatus.NOT_FOUND);

    const foundEmergency = await this.emergencyServices.findOneV2(
      input.emergencyid,
    );
    if (!foundEmergency)
      throw new HttpException('Not found emergency', HttpStatus.NOT_FOUND);

    const foundPatient = await this.patientServies.findOneV2(input.patientid);
    if (!foundPatient)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);
    const listMedicine = [];
    for (const itemMedicine of input.listMedicine) {
      const dataMedicine = Object.assign(new MedicineEntity(), {
        ...input,
        ...itemMedicine,
        isverified: true,
      });
      const medicine = await this.medicineServices.putV2ForEmegency(
        dataMedicine,
      );

      const log = Object.assign(new LogMedicalEntity(), {
        patient: foundPatient,
        createdby: foundUser,
        name: `${foundUser.lastname} ${foundUser.firstname} thêm Thuốc: ${
          itemMedicine.name
        }, SL: ${itemMedicine.quantity}, Ghi Chú: ${
          itemMedicine.description
        } cho hồ sơ cấp cứu (CC ${moment(foundEmergency.createddat)
          .utcOffset('+07:00')
          .toDate()
          .valueOf()})  từ web`,
        description: ``,
      });
      this.logMedicalServices.post(log);
      listMedicine.push(medicine);
      foundEmergency.medicines.push(medicine);
    }
    const emergency = await this.emergencyServices.post(foundEmergency);
    return emergency.medicines;
  }
}
