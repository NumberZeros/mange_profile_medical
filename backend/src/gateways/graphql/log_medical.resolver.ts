import { Args, Query, Resolver } from '@nestjs/graphql';

import { UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { PatientServices } from 'src/services/core/patients/patient.service';
import { LogMedicalServices } from 'src/services/core/log_medicals/logs.service';
import { LogMedicalEntity } from 'src/entities/log_medical.entity';

@Resolver()
export class LogMedicalResolver {
  constructor(
    private logMedicalServices: LogMedicalServices,
    private patientServices: PatientServices,
  ) {}
  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [LogMedicalEntity])
  async find_all_log_patient(
    @Args('patientid') patientid: string,
  ): Promise<LogMedicalEntity[]> {
    const foundPatient = await this.patientServices.findOneV2(patientid);
    return await this.logMedicalServices.findAll(foundPatient.id);
  }
}
