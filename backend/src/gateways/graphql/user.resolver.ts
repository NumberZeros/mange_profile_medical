import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UserServices } from '../../services/core/users/user.service';
import { UserEntity } from '../../entities/users.entity';
import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { UpdateUserInput } from '../../services/core/users/dto/update.input';
import { ChangePasswordUserInput } from '../../services/core/users/dto/changePassword.input';

@Resolver()
export class UserResolver {
  constructor(private userServies: UserServices) {}

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => UserEntity)
  async author(
    @Args('id', { type: () => String }) id: string,
  ): Promise<UserEntity> {
    return await this.userServies.findOneV2(id);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => UserEntity)
  async update_info(
    @Args('input') input: UpdateUserInput,
  ): Promise<UserEntity> {
    const dataUser = await this.userServies.findOne(input.id);
    if (!dataUser)
      throw new HttpException('Not found user', HttpStatus.NOT_FOUND);
    if (dataUser.email !== input.email) {
      const foundEmail = await this.userServies.findEmail(input.email);
      if (foundEmail)
        throw new HttpException('Email is exist', HttpStatus.CONFLICT);
    }
    if (dataUser.phone !== input.phone) {
      const foundPhone = await this.userServies.findPhone(input.phone);
      if (foundPhone)
        throw new HttpException('Phone is exist', HttpStatus.CONFLICT);
    }
    return await this.userServies.put({ ...dataUser, ...input });
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => UserEntity)
  async change_password(
    @Args('input') input: ChangePasswordUserInput,
  ): Promise<UserEntity> {
    const foundUser = await this.userServies.findOne(input.id);
    if (!foundUser)
      throw new HttpException('Not found user', HttpStatus.NOT_FOUND);

    const dataUser = await this.userServies.loginUser({
      email: input.email,
      password: input.oldpassword,
    });
    if (!dataUser)
      throw new HttpException('Email or password wrong', HttpStatus.NOT_FOUND);
    const foundEmail = await this.userServies.findEmail(input.email);
    if (!foundEmail)
      throw new HttpException('Email is exist', HttpStatus.NOT_FOUND);
    return await this.userServies.changePassword({ ...foundUser, ...input });
  }
}
