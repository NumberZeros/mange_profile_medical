import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { CategoryEntity } from 'src/entities/categories.entity';
import { CategoryServices } from 'src/services/core/categories/category.service';

@Resolver()
export class CategoryResolver {
  constructor(private categoryServices: CategoryServices) {}

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [CategoryEntity])
  async find_all_category(): Promise<CategoryEntity[]> {
    return await this.categoryServices.findAll();
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => CategoryEntity)
  async find_one_Category(
    @Args('id', { type: () => String }) id: string,
  ): Promise<CategoryEntity> {
    return await this.categoryServices.findOne(id);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => CategoryEntity)
  async create_category(
    @Args('name', { type: () => String }) name: string,
  ): Promise<CategoryEntity> {
    return this.categoryServices.post({ name, isverified: true });
  }
}
