import { Query, Resolver } from '@nestjs/graphql';

import { UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { SMSServices } from 'src/services/core/sms/sms.service';
import { SMSEntity } from 'src/entities/sms.entity';

@Resolver()
export class SMSResolver {
  constructor(private smsServices: SMSServices) {}
  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [SMSEntity])
  async find_all_sms(): Promise<SMSEntity[]> {
    return await this.smsServices.findAll();
  }
}
