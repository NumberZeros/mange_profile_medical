import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { PatientServices } from 'src/services/core/patients/patient.service';
import { PatientEntity } from 'src/entities/patient.entity';
import { CreatePatientInput } from 'src/services/core/patients/dto/post.input';
import { CategoryServices } from 'src/services/core/categories/category.service';
import { PutPatientInput } from 'src/services/core/patients/dto/put.input';
import { UserServices } from 'src/services/core/users/user.service';
import { DoctorServices } from 'src/services/core/users/doctor.service';
import { PathogenicServices } from 'src/services/core/pathogenices/pathogenic.service';
import {
  PaginationPatient,
  PaginationPatientInput,
} from 'src/services/core/patients/dto/pagination.input';
import { VerifiedPatientInput } from 'src/services/core/patients/dto/verified.input';
import { ERoles } from 'src/entities/users.entity';
import { ChangeStatusPatientInput } from 'src/services/core/patients/dto/change_status.input';
import { PatientStatusServices } from 'src/services/core/patient_status/patitent_status.service';
import { ChangeDoctorPatientInput } from 'src/services/core/patients/dto/change_doctor.input';
import { isEmpty } from 'lodash';
import { LogMedicalEntity } from 'src/entities/log_medical.entity';
import { LogMedicalServices } from 'src/services/core/log_medicals/logs.service';
import { SearchGroupPatientInput } from 'src/services/core/patients/dto/search_group_patient.input';
import { TrackingPatientForDepositDoctorInput } from 'src/services/core/patients/dto/tracking_patient_for_deposit.input';
import { MedicalServices } from 'src/services/core/medicals/medical.service';

@Resolver()
export class PatientResolver {
  constructor(
    private userServies: UserServices,
    private patientServies: PatientServices,
    private categoryServies: CategoryServices,
    private doctorServices: DoctorServices,
    private pathogenicServices: PathogenicServices,
    private statusServices: PatientStatusServices,
    private logMedicalServices: LogMedicalServices,
    private medicalServices: MedicalServices,
  ) {}

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [PatientEntity])
  async find_all_patient(
    @Args('id', { type: () => String }) id: string,
  ): Promise<PatientEntity[]> {
    if (!id || id == '')
      throw new HttpException('Please enter id ', HttpStatus.NOT_ACCEPTABLE);
    const foundUser = await this.userServies.findOne(id);
    if (!foundUser) {
      const foundDoctor = await this.doctorServices.findOne(id);
      if (foundDoctor)
        return await this.patientServies.findAllUseDoctor(foundDoctor.id);
      throw new HttpException('Account not accept', HttpStatus.NOT_ACCEPTABLE);
    } else {
      const foundDoctor = await this.doctorServices.findUserID(id);
      if (foundDoctor)
        return await this.patientServies.findAllUseDoctor(foundDoctor.id);
      return await this.patientServies.findAll();
    }
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => PaginationPatient)
  async find_all_patient_use_pagination(
    @Args('pagination') pagination: PaginationPatientInput,
  ): Promise<PaginationPatient> {
    if (!isEmpty(pagination.adminid) && pagination.adminid !== '') {
      await this.userServies.validateAccount(pagination.adminid, ERoles.admin);
      return await this.patientServies.findAllUsePagination(pagination);
    }
    if (!isEmpty(pagination.doctorid) && pagination.doctorid !== '') {
      let foundDoctor = await this.doctorServices.findOneV2(
        pagination.doctorid,
      );
      if (!foundDoctor) {
        foundDoctor = await this.doctorServices.findUserID(pagination.doctorid);
        if (!foundDoctor)
          throw new HttpException("Doctor isn't exist", HttpStatus.NOT_FOUND);
      }
      return await this.patientServies.findAllUsePagination(
        pagination,
        foundDoctor.id,
      );
    }
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => PatientEntity)
  async find_one_patient(
    @Args('id', { type: () => String }) id: string,
  ): Promise<PatientEntity> {
    return await this.patientServies.findOneV2(id);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => PatientEntity)
  async create_patient(
    @Args('input') input: CreatePatientInput,
  ): Promise<PatientEntity> {
    const foundPatientInfo = await this.patientServies.findUniqPatient({
      firstname: input.firstname,
      lastname: input.lastname,
      phone: input.phone,
      dob: input.dob,
    });

    if (foundPatientInfo)
      throw new HttpException('Found patient is exist', HttpStatus.CONFLICT);

    const foundCategory = await this.categoryServies.findOne(input.category);
    if (!foundCategory)
      throw new HttpException("Category isn't exist", HttpStatus.NOT_FOUND);

    let foundDoctor = await this.doctorServices.findOneV2(input.doctorid);

    if (!foundDoctor) {
      foundDoctor = await this.doctorServices.findUserID(input.doctorid);
      if (!foundDoctor)
        throw new HttpException("Doctor isn't exist", HttpStatus.NOT_FOUND);
    }

    if (!foundCategory)
      throw new HttpException("Category isn't exist", HttpStatus.NOT_FOUND);

    if (input.pathogenicid.length === 0)
      throw new HttpException('Not found pathogenic', HttpStatus.NOT_FOUND);

    const listPathogenic = [];
    const listPathogenicNotFound = [];
    for (const id of input.pathogenicid) {
      const foundPathogenic = await this.pathogenicServices.findOne(id);
      if (foundPathogenic) listPathogenic.push(foundPathogenic);
      else listPathogenicNotFound.push(id);
    }

    if (listPathogenicNotFound.length > 0)
      throw new HttpException(
        `Not found pathogenicid ${[...listPathogenic]}`,
        HttpStatus.NOT_FOUND,
      );
    const patient = Object.assign(new PatientEntity(), {
      ...input,
      category: foundCategory,
      doctor: foundDoctor,
      pathogenic: listPathogenic,
      sharedoctor: foundDoctor,
      status: await this.statusServices.findName(''),
    });
    return await this.patientServies.putV2(patient);
  }
  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => PatientEntity)
  async update_patient(
    @Args('input') input: PutPatientInput,
  ): Promise<PatientEntity> {
    const foundPatient = await this.patientServies.findOneV2(input.id);

    const foundPatientInfo = await this.patientServies.findUniqPatient({
      firstname: input.firstname,
      lastname: input.lastname,
      phone: input.phone,
      dob: input.dob,
    });

    if (
      foundPatientInfo &&
      foundPatient.firstname === foundPatientInfo.firstname &&
      foundPatient.lastname === foundPatientInfo.lastname &&
      foundPatient.phone === foundPatientInfo.phone &&
      foundPatient.dob === foundPatientInfo.dob
    )
      throw new HttpException('Found patient is exist', HttpStatus.CONFLICT);

    const foundCategory = await this.categoryServies.findOne(input.category);
    if (!foundCategory)
      throw new HttpException("Category isn't exist", HttpStatus.NOT_FOUND);

    let foundDoctor = await this.doctorServices.findOneV2(input.doctorid);
    if (!foundDoctor) {
      foundDoctor = await this.doctorServices.findUserID(input.doctorid);
      if (!foundDoctor)
        throw new HttpException("Doctor isn't exist", HttpStatus.NOT_FOUND);
    }

    if (input.pathogenicid.length === 0)
      throw new HttpException('Not found pathogenic', HttpStatus.NOT_FOUND);

    const listPathogenic = [];
    const listPathogenicNotFound = [];
    for (const id of input.pathogenicid) {
      const foundPathogenic = await this.pathogenicServices.findOne(id);
      if (foundPathogenic) listPathogenic.push(foundPathogenic);
      else listPathogenicNotFound.push(id);
    }

    if (listPathogenicNotFound.length > 0)
      throw new HttpException(
        `Not found pathogenicid ${[...listPathogenic]}`,
        HttpStatus.NOT_FOUND,
      );
    return await this.patientServies.put(
      { ...foundPatient, ...input, id: input.id },
      {
        category: foundCategory,
        doctor: foundDoctor,
        pathogenic: listPathogenic,
      },
    );
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => PatientEntity)
  async verified_patient(
    @Args('input') input: VerifiedPatientInput,
  ): Promise<PatientEntity> {
    const foundPatient = await this.patientServies.findOneV2(input.id);
    if (!foundPatient)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);

    return await this.patientServies.verified(input, foundPatient);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => PatientEntity)
  async unverified_patient(
    @Args('input') input: VerifiedPatientInput,
  ): Promise<PatientEntity> {
    const foundPatient = await this.patientServies.findOneV2(input.id);
    if (!foundPatient || foundPatient.isverified)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);
    return await this.patientServies.delete({
      ...foundPatient,
      inactive: true,
    });
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => PatientEntity)
  async change_status_patient(
    @Args('input') input: ChangeStatusPatientInput,
  ): Promise<PatientEntity> {
    const foundPatient = await this.patientServies.findOneV2(input.id);
    if (!foundPatient || !foundPatient.isverified)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);

    const validateOwner = await this.doctorServices.findUserID(input.ownerid);
    if (foundPatient.doctor.id !== validateOwner.id)
      throw new HttpException('Account not allow', HttpStatus.NOT_FOUND);

    const foundStatus = await this.statusServices.findOne(input.status);
    if (!foundStatus)
      throw new HttpException('Not found patient status', HttpStatus.NOT_FOUND);

    const dataPatient = await this.patientServies.putV2({
      ...foundPatient,
      status: foundStatus,
    });

    const foundUser = await this.userServies.findOneV2(input.ownerid);
    const log = Object.assign(new LogMedicalEntity(), {
      patient: foundPatient,
      createdby: foundUser,
      name: `${foundUser.lastname} ${foundUser.firstname} đã chuyển trạng thái bệnh nhân "${foundPatient.status.name}" -> "${foundStatus.name}" từ web`,
      description: ``,
    });
    this.logMedicalServices.post(log);
    return dataPatient;
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => PatientEntity)
  async change_doctor_deposit(
    @Args('input') input: ChangeDoctorPatientInput,
  ): Promise<PatientEntity> {
    const foundUser = await this.userServies.findOneV2(input.ownerid);
    if (
      !foundUser ||
      (foundUser.roles !== ERoles.admin && foundUser.roles !== ERoles.doctor)
    )
      throw new HttpException('You not Allow', HttpStatus.METHOD_NOT_ALLOWED);

    // if (foundUser.roles !== ERoles.admin) {
    //   const foundDoctorOwner = await this.patientServies.findOneOwnerDoctor(
    //     input.ownerid,
    //   );
    //   if (!foundDoctorOwner)
    //     throw new HttpException('You not Allow', HttpStatus.METHOD_NOT_ALLOWED);
    // }

    const foundPatient = await this.patientServies.findOneV2(input.id);
    if (!foundPatient || !foundPatient.isverified)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);
    const foundDoctor = await this.doctorServices.findOneV2(input.doctor);
    const dataPatient = await this.patientServies.putV2({
      ...foundPatient,
      sharedoctor: foundDoctor,
    });
    const log = Object.assign(new LogMedicalEntity(), {
      patient: foundPatient,
      createdby: foundUser,
      name: `${foundUser.lastname} ${foundUser.firstname} đã chuyển bệnh nhân cho bác sĩ ${foundDoctor.user.lastname} ${foundDoctor.user.firstname} từ web`,
      description: ``,
    });
    this.logMedicalServices.post(log);
    return dataPatient;
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [PatientEntity])
  async report_all_patient(
    @Args('adminid', { type: () => String }) adminid: string,
    @Args('from', { type: () => Date }) from: Date,
    @Args('to', { type: () => Date }) to: Date,
  ): Promise<PatientEntity[]> {
    await this.userServies.validateAccount(adminid, ERoles.admin);

    const dataPatient = await this.patientServies.reportAllPatient({
      from,
      to,
    });
    return dataPatient.filter((o) => o.medicalprofile.length > 0);
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [PatientEntity])
  async report_emegencies_all_patient(
    @Args('adminid', { type: () => String }) adminid: string,
    @Args('from', { type: () => Date }) from: Date,
    @Args('to', { type: () => Date }) to: Date,
  ): Promise<PatientEntity[]> {
    await this.userServies.validateAccount(adminid, ERoles.admin);

    const dataPatient = await this.patientServies.reportEmegencyAllPatient({
      from,
      to,
    });
    return dataPatient.filter((o) => o.medicalemergency.length > 0);
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => PaginationPatient)
  async find_group_dangerours_patient(
    @Args('pagination', { type: () => SearchGroupPatientInput })
    pagination: SearchGroupPatientInput,
  ): Promise<PaginationPatient> {
    if (pagination.adminid && pagination.adminid !== '') {
      await this.userServies.validateAccount(pagination.adminid, ERoles.admin);
      const dataPatient = await this.patientServies.findGroupDangerousPatient(
        pagination,
        null,
      );
      return dataPatient;
    }
    if (pagination.doctorid && pagination.doctorid !== '') {
      let foundDoctor = await this.doctorServices.findOneV2(
        pagination.doctorid,
      );
      if (!foundDoctor) {
        foundDoctor = await this.doctorServices.findUserID(pagination.doctorid);
        if (!foundDoctor)
          throw new HttpException("Doctor isn't exist", HttpStatus.NOT_FOUND);
      }
      const dataPatient = await this.patientServies.findGroupDangerousPatient(
        pagination,
        foundDoctor.id,
      );
      return dataPatient;
    }
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => PatientEntity)
  async tracking_patient_deposit_doctor(
    @Args('input') input: TrackingPatientForDepositDoctorInput,
  ): Promise<PatientEntity> {
    const foundPatient = await this.patientServies.findOneV2(input.patientid);
    if (!foundPatient || !foundPatient.isverified)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);
    const foundDoctor = await this.doctorServices.findOneV2(input.doctorid);
    if (!foundDoctor || !foundDoctor.isverified)
      throw new HttpException('Not found doctor', HttpStatus.NOT_FOUND);
    const foundMedical = await this.medicalServices.findOneV2(input.medicalid);
    if (!foundMedical || !foundMedical)
      throw new HttpException('Not found medical', HttpStatus.NOT_FOUND);

    await this.medicalServices.putV2({
      ...foundMedical,
      issupport: true,
    });
    await this.patientServies.putV2({
      ...foundPatient,
      sharedoctor: foundDoctor,
    });
    const log = Object.assign(new LogMedicalEntity(), {
      patient: foundPatient,
      createdby: foundDoctor.user,
      name: `${foundDoctor.user.lastname} ${foundDoctor.user.firstname} đã ${input.type} bởi bác sĩ ${foundDoctor.user.lastname} ${foundDoctor.user.firstname} từ web`,
      description: ``,
    });
    this.logMedicalServices.post(log);
    return foundPatient;
  }
}
