import { Args, Mutation, Resolver, Query } from '@nestjs/graphql';
import { UserServices } from '../../services/core/users/user.service';
import { UserEntity } from '../../entities/users.entity';
import { AuthService } from '../../services/packages/auth.service';
import { loginUserInput } from '../../services/core/users/dto/login.input';
import { RegisterUserInput } from '../../services/core/users/dto/resgister.input';
import {
  RedirectInput,
  RedirectOutPut,
} from 'src/services/core/users/dto/redirect.input';
import { SMSServices } from 'src/services/core/sms/sms.service';
import { MedicalEntity } from 'src/entities/medical.entity';
import { HttpException, HttpStatus } from '@nestjs/common';
import { SMSEntity } from 'src/entities/sms.entity';

@Resolver()
export class AuthResolver {
  constructor(
    private userServies: UserServices,
    private authServices: AuthService,
    private smsServices: SMSServices,
  ) {}

  @Query((of) => UserEntity)
  async get_refresh_token(
    @Args('refresh_token') token: string,
  ): Promise<UserEntity> {
    const dataAuth = await this.authServices.validateUser(token);
    const dataUser = await this.userServies.findOne(dataAuth.id);
    const { access_token, refresh_token } =
      await this.authServices.createTokenUser(dataUser);
    dataUser.access_token = access_token;
    dataUser.refresh_token = refresh_token;
    return dataUser;
  }

  @Mutation((of) => UserEntity)
  async login_user(@Args('input') input: loginUserInput): Promise<UserEntity> {
    const dataUser = await this.userServies.loginUser(input);
    const { access_token, refresh_token } =
      await this.authServices.createTokenUser(dataUser);
    dataUser.access_token = access_token;
    dataUser.refresh_token = refresh_token;
    return dataUser;
  }

  @Mutation((of) => UserEntity)
  async register_user(
    @Args('input') input: RegisterUserInput,
  ): Promise<UserEntity> {
    const dataUser = await this.userServies.register(input);
    const { access_token, refresh_token } =
      await this.authServices.createTokenUser(dataUser);
    dataUser.access_token = access_token;
    dataUser.refresh_token = refresh_token;
    return dataUser;
  }

  @Mutation((of) => RedirectOutPut)
  async redirect(@Args('input') input: RedirectInput): Promise<RedirectOutPut> {
    const smsdata = await this.smsServices.loginDoctor(input.authid);
    if (smsdata.medical.id != input.token)
      throw new HttpException(
        'Token or authid is not valid',
        HttpStatus.UNAUTHORIZED,
      );
    if (smsdata.medical.issupport) {
      if (smsdata.medical.patient.sharedoctor.id !== smsdata.doctor.id)
        throw new HttpException(
          'Thank you, The medical profile is supporting another doctor',
          HttpStatus.CONFLICT,
        );
    }
    const { access_token } = await this.authServices.createTokenUser(
      smsdata.doctor.user,
    );
    console.log(access_token);
    return { access_token, sms: smsdata };
  }
}
