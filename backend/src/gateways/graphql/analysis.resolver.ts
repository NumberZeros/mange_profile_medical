import { Args, Query, Resolver } from '@nestjs/graphql';
import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import {
  AnalysisGroupDangerous,
  AnalysisResponse,
} from 'src/entities/analysis.entity';
import * as moment from 'moment';
import { SummaryObject } from 'src/entities/summary_patient';
import { DoctorServices } from 'src/services/core/users/doctor.service';
import { UserServices } from 'src/services/core/users/user.service';
import { ERoles } from 'src/entities/users.entity';
import { AnalaysisServices } from 'src/services/reports/analaysis/analaysis.service';
import { MedicalServices } from 'src/services/core/medicals/medical.service';
import { MedicalTrackServices } from 'src/services/core/medical_tracks/medicalTrack.service';
import { MedicalEntity } from 'src/entities/medical.entity';
import { PatientServices } from 'src/services/core/patients/patient.service';

@Resolver()
export class AnalysisResolver {
  constructor(
    private medicalServices: MedicalServices,
    private userServices: UserServices,
    private doctorServices: DoctorServices,
    private analaysisServices: AnalaysisServices,
    private trackServices: MedicalTrackServices,
    private patientServices: PatientServices,
  ) {}
  DATE_FORMAT = 'DD/MM/YYYY';
  async getDataTableResultTest(query: {
    listDateOfWeek: any;
    startWeek: Date;
    endWeek: Date;
    medicals: MedicalEntity[];
  }) {
    try {
      /* ---------------------- Tình trạng mạn tính hiện tại ---------------------- */
      const dataTableResultTest = query.listDateOfWeek.map((o) => ({
        date: o,
        data: [],
      }));
      const dataTableStatusQuo = query.listDateOfWeek.map((o) => ({
        date: o,
        data: '',
      }));
      const dataTablePluse = query.listDateOfWeek.map((o) => ({
        date: o,
        data: 0,
      }));
      const dataTableTemperature = dataTablePluse;
      const dataTableSpo2home = dataTablePluse;
      const dataTableBloodpressure = dataTablePluse;
      const dataTableBloodpressure2 = dataTablePluse;

      for (const medical of query.medicals) {
        const idx = dataTableStatusQuo.findIndex(
          (o) => o.date === moment(medical.updatedat).format(this.DATE_FORMAT),
        );
        console.log({ idx, status: medical.statusquo });

        if (idx > -1) {
          dataTableStatusQuo[idx].data = medical.statusquo;
          dataTablePluse[idx].data = medical.pluse;
          dataTableTemperature[idx].data = medical.temperature;
          dataTableSpo2home[idx].data = medical.spo2home;
          dataTableBloodpressure[idx].data = medical.bloodpressure;
          dataTableBloodpressure2[idx].data = medical.bloodpressure2;
        }
        /* --------------------------- Kết quả xét nghiệm --------------------------- */

        const trackData = await this.trackServices.findAllMedicalTrackV2(
          medical.id,
        );
        // console.log('trackData', trackData);
        const medicalTrackValid = trackData.filter(
          (track) =>
            track.updatedat.valueOf() >= query.startWeek.valueOf() &&
            track.updatedat.valueOf() <= query.endWeek.valueOf(),
        );
        if (medicalTrackValid.length > 0) {
          for (const track of medicalTrackValid) {
            const idx = dataTableResultTest.findIndex(
              (o) =>
                o.date === moment(track.prcrealdate).format(this.DATE_FORMAT),
            );
            if (idx > -1)
              dataTableResultTest[idx].data.push({
                result: track.medicalresutl,
                time: track.updatedat,
              });
          }
        }
        /* ----------------------------------- End ---------------------------------- */
      }
      return {
        dataTableResultTest,
        dataTableStatusQuo,
        dataTablePluse,
        dataTableTemperature,
        dataTableSpo2home,
        dataTableBloodpressure,
        dataTableBloodpressure2,
      };
    } catch (error) {}
  }
  calculationDateOfWeek = (now: Date) => {
    const startWeek = moment(now).startOf('week').utc().toDate();
    const endWeek = moment(now).endOf('week').utc().toDate();
    const listDateOfWeek = [];
    let countDate = startWeek;
    while (countDate.valueOf() <= endWeek.valueOf()) {
      listDateOfWeek.push(moment(countDate).format(this.DATE_FORMAT));
      countDate = moment(countDate).add(1, 'days').toDate();
    }
    return { listDateOfWeek, startWeek, endWeek };
  };

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => AnalysisResponse)
  async analysis_patients(
    @Args('adminid') adminid: string,
    @Args('now') now: Date,
  ): Promise<AnalysisResponse> {
    const { startWeek } = this.calculationDateOfWeek(now);
    await this.userServices.validateAccount(adminid, ERoles.admin);
    return await this.analaysisServices.findOne({
      user: 'admin',
      startweek: moment(startWeek).format(this.DATE_FORMAT),
    });
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => AnalysisResponse)
  async analysis_patients_for_doctor(
    @Args('now') now: Date,
    @Args('doctorid') doctorid: string,
  ): Promise<AnalysisResponse> {
    let foundDoctor = await this.doctorServices.findOneV2(doctorid);
    if (!foundDoctor) {
      foundDoctor = await this.doctorServices.findUserID(doctorid);
      if (!foundDoctor)
        throw new HttpException("Doctor isn't exist", HttpStatus.NOT_FOUND);
    }

    const { startWeek } = this.calculationDateOfWeek(now);
    return await this.analaysisServices.findOne({
      user: foundDoctor.user.id,
      startweek: moment(startWeek).format(this.DATE_FORMAT),
    });
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => AnalysisGroupDangerous)
  async analaysis_group_dangerous_patient(
    @Args('adminid') adminid?: string,
    @Args('doctorid') doctorid?: string,
  ): Promise<AnalysisGroupDangerous> {
    let foundDoctor;
    if (adminid && adminid !== '') {
      await this.userServices.validateAccount(adminid, ERoles.admin);
      foundDoctor = null;
    }
    if (doctorid && doctorid !== '') {
      foundDoctor = await this.doctorServices.findOneV2(doctorid);
      if (!foundDoctor) {
        foundDoctor = await this.doctorServices.findUserID(doctorid);
        if (!foundDoctor)
          throw new HttpException("Doctor isn't exist", HttpStatus.NOT_FOUND);
      }
    }

    const totalPatient4Date =
      await this.patientServices.reportGroupDangerousPatient(
        {
          from: moment().subtract(4, 'days').toDate(),
          to: moment().toDate(),
        },
        foundDoctor ? foundDoctor.id : null,
      );
    const totalPatient10Date =
      await this.patientServices.reportGroupDangerousPatient(
        {
          from: moment().subtract(10, 'days').toDate(),
          to: moment().subtract(5, 'days').toDate(),
        },
        foundDoctor ? foundDoctor.id : null,
      );
    const totalPatient28Date =
      await this.patientServices.reportGroupDangerousPatient(
        {
          from: moment().subtract(11, 'days').toDate(),
          to: moment().subtract(28, 'days').toDate(),
        },
        foundDoctor ? foundDoctor.id : null,
      );
    return { totalPatient4Date, totalPatient10Date, totalPatient28Date };
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => SummaryObject)
  async summary_patients(
    @Args('now') now: Date,
    @Args('patientid') patientid: string,
  ): Promise<SummaryObject> {
    const { listDateOfWeek, startWeek, endWeek } =
      this.calculationDateOfWeek(now);
    const dataMedical = await (await this.medicalServices.findAll(patientid))
      .filter(
        (medical) =>
          medical.updatedat.valueOf() >= startWeek.valueOf() &&
          medical.updatedat.valueOf() <= endWeek.valueOf(),
      )
      .sort((a, b) => a.updatedat.valueOf() - b.updatedat.valueOf());
    const {
      dataTableResultTest,
      dataTableStatusQuo,
      dataTablePluse,
      dataTableTemperature,
      dataTableSpo2home,
      dataTableBloodpressure,
      dataTableBloodpressure2,
    } = await this.getDataTableResultTest({
      listDateOfWeek,
      startWeek,
      endWeek,
      medicals: dataMedical,
    });
    return {
      dataTableResultTest: JSON.stringify(dataTableResultTest),
      dataTableStatusQuo: JSON.stringify(dataTableStatusQuo),
      dataTablePluse: JSON.stringify(dataTablePluse),
      dataTableTemperature: JSON.stringify(dataTableTemperature),
      dataTableSpo2home: JSON.stringify(dataTableSpo2home),
      dataTableBloodpressure: JSON.stringify(dataTableBloodpressure),
      dataTableBloodpressure2: JSON.stringify(dataTableBloodpressure2),
    };
  }
}
