import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { GroupPatientServices } from 'src/services/core/group_patient/group_patient.service';
import { GroupPatientEntity } from 'src/entities/group_patient.entity';
import { DoctorServices } from 'src/services/core/users/doctor.service';
import { PutGroupPatientInput } from 'src/services/core/group_patient/dto/edit.input';
import { PostGroupPatientInput } from 'src/services/core/group_patient/dto/create.input';
import { HandleMemeberGroupPatientInput } from 'src/services/core/group_patient/dto/handle_patient.input';
import { PatientServices } from 'src/services/core/patients/patient.service';
import { uniqBy } from 'lodash';
import { UpdateIndexGroupPatientInput } from 'src/services/core/group_patient/dto/update_index.input';

@Resolver()
export class GroupPatientResolver {
  constructor(
    private groupServices: GroupPatientServices,
    private doctorServices: DoctorServices,
    private patientServices: PatientServices,
  ) {}

  handleAddPatient = async ({ group, listPatient }) => {
    try {
      const list = group.patient;
      for (const id of listPatient) {
        const foundPatient = await this.patientServices.findOneV2(id);
        if (foundPatient) list.push(foundPatient);
      }
      return uniqBy(list, 'id');
    } catch (error) {
      console.log('ERR', error);
    }
  };

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [GroupPatientEntity])
  async find_all_group_patient(
    @Args('doctorId', { type: () => String }) doctorId: string,
  ): Promise<GroupPatientEntity[]> {
    let foundDoctor = await this.doctorServices.findOneV2(doctorId);
    if (!foundDoctor) {
      foundDoctor = await this.doctorServices.findUserID(doctorId);
      if (!foundDoctor)
        throw new HttpException("Doctor isn't exist", HttpStatus.NOT_FOUND);
    }
    return await this.groupServices.findAll(foundDoctor.id);
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => GroupPatientEntity)
  async find_one_group_patient(
    @Args('id', { type: () => String }) id: string,
  ): Promise<GroupPatientEntity> {
    return await this.groupServices.findOne(id);
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => GroupPatientEntity)
  async find_latested_group_patient(
    @Args('doctorid', { type: () => String }) doctorid: string,
  ): Promise<GroupPatientEntity> {
    return await this.groupServices.findLatestedIndex(doctorid);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => GroupPatientEntity)
  async create_group_patient(
    @Args('input') input: PostGroupPatientInput,
  ): Promise<GroupPatientEntity> {
    let foundDoctor = await this.doctorServices.findOneV2(input.doctorid);
    if (!foundDoctor) {
      foundDoctor = await this.doctorServices.findUserID(input.doctorid);
      if (!foundDoctor)
        throw new HttpException("Doctor isn't exist", HttpStatus.NOT_FOUND);
    }
    const lastedGroup = await this.groupServices.findLatestedIndex(
      foundDoctor.id,
    );
    const groupPatient = Object.assign(new GroupPatientEntity(), {
      ...input,
      doctor: foundDoctor,
      index: !lastedGroup ? 0 : lastedGroup.index + 1,
    });
    return this.groupServices.post(groupPatient);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => GroupPatientEntity)
  async put_group_patient(
    @Args('input') input: PutGroupPatientInput,
  ): Promise<GroupPatientEntity> {
    let foundDoctor = await this.doctorServices.findOneV2(input.doctorid);
    if (!foundDoctor) {
      foundDoctor = await this.doctorServices.findUserID(input.doctorid);
      if (!foundDoctor)
        throw new HttpException("Doctor isn't exist", HttpStatus.NOT_FOUND);
    }
    const foundGroup = await this.groupServices.findOne(input.id);
    if (!foundGroup)
      throw new HttpException("Group isn't exist", HttpStatus.NOT_FOUND);
    const groupPatient = Object.assign(new GroupPatientEntity(), {
      ...foundGroup,
      ...input,
      doctor: foundDoctor,
    });
    return this.groupServices.post(groupPatient);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => GroupPatientEntity)
  async handle_group_patient(
    @Args('input') input: HandleMemeberGroupPatientInput,
  ): Promise<GroupPatientEntity> {
    const foundGroup = await this.groupServices.findOne(input.id);
    if (!foundGroup)
      throw new HttpException('Not found group', HttpStatus.NOT_FOUND);
    let foundDoctor = await this.doctorServices.findOneV2(input.doctorid);
    if (!foundDoctor) {
      foundDoctor = await this.doctorServices.findUserID(input.doctorid);
      if (!foundDoctor)
        throw new HttpException("Doctor isn't exist", HttpStatus.NOT_FOUND);
    }
    if (foundGroup.doctor.id !== foundDoctor.id)
      throw new HttpException(
        "Group isn't base on Doctor",
        HttpStatus.NOT_FOUND,
      );
    let listPatient = [];
    if (input.isRemove)
      listPatient = foundGroup.patient.filter(
        (o) => !input.listPatient.includes(o.id),
      );
    else
      listPatient = await this.handleAddPatient({
        group: foundGroup,
        listPatient: input.listPatient,
      });
    const groupPatient = Object.assign(new GroupPatientEntity(), {
      ...foundGroup,
      ...input,
      patient: listPatient,
    });

    return this.groupServices.post(groupPatient);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => GroupPatientEntity)
  async update_index_group_patient(
    @Args('input') input: UpdateIndexGroupPatientInput,
  ): Promise<GroupPatientEntity> {
    let foundDoctor = await this.doctorServices.findOneV2(input.doctorid);
    if (!foundDoctor) {
      foundDoctor = await this.doctorServices.findUserID(input.doctorid);
      if (!foundDoctor)
        throw new HttpException("Doctor isn't exist", HttpStatus.NOT_FOUND);
    }

    const foundGroup = await this.groupServices.findOne(input.id);
    if (!foundGroup)
      throw new HttpException("Group isn't exist", HttpStatus.NOT_FOUND);

    if (input.index === foundGroup.index)
      throw new HttpException("Group doesn't change", HttpStatus.NOT_FOUND);

    const listGroup = await this.groupServices.findAllBaseOnIndex(
      input.index < foundGroup.index
        ? {
            doctorId: foundDoctor.id,
            startIndex: input.index,
            endIndex: foundGroup.index,
          }
        : {
            doctorId: foundDoctor.id,
            startIndex: foundGroup.index,
            endIndex: input.index,
          },
    );
    let count = 0;
    for (const group of listGroup) {
      if (input.index === 0) {
        await this.groupServices.post({
          ...group,
          index: count + 1,
        });
        count++;
      } else
        await this.groupServices.post({
          ...group,
          index:
            input.index < foundGroup.index ? group.index + 1 : group.index - 1,
        });
    }

    const groupPatient = Object.assign(new GroupPatientEntity(), {
      ...foundGroup,
      ...input,
      doctor: foundDoctor,
    });
    return this.groupServices.post(groupPatient);
  }
}
