import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { UserServices } from '../../services/core/users/user.service';
import { DoctorServices } from 'src/services/core/users/doctor.service';
import { PatientServices } from 'src/services/core/patients/patient.service';
import { LogMedicalServices } from 'src/services/core/log_medicals/logs.service';
import { EmergencyEntity } from 'src/entities/emergency.entity';
import { EmergencyServices } from 'src/services/core/emergency/emergency.service';
import { CreateEmergencyInput } from 'src/services/core/emergency/dto/post.input';
import { ClinicalServices } from 'src/services/core/clinical/clinical.service';
import { BreathServices } from 'src/services/core/breath/breath.service';
import { LogMedicalEntity } from 'src/entities/log_medical.entity';
import { EmergencyStatusServices } from 'src/services/core/emergency_status/emergency_stauts.service';
import { UpdateEmergencyInput } from 'src/services/core/emergency/dto/update.input';
import * as moment from 'moment';

@Resolver()
export class EmergencyResolver {
  constructor(
    private doctorServices: DoctorServices,
    private userServices: UserServices,
    private patientServices: PatientServices,
    private logMedicalServices: LogMedicalServices,
    private clinicalServices: ClinicalServices,
    private emergencyServices: EmergencyServices,
    private breathServices: BreathServices,
    private statusServices: EmergencyStatusServices,
  ) {}
  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [EmergencyEntity])
  async find_all_emergency(
    @Args('patientid') patientid: string,
  ): Promise<EmergencyEntity[]> {
    const foundPatient = await this.patientServices.findOneV2(patientid);
    if (!foundPatient)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);
    return await this.emergencyServices.findAll(foundPatient.id);
  }

  @Query((of) => EmergencyEntity)
  async find_one_emergency(
    @Args('patientid') patientid: string,
    @Args('id') id: string,
  ): Promise<EmergencyEntity> {
    const foundPatient = await this.patientServices.findOneV2(patientid);
    if (!foundPatient)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);
    return await this.emergencyServices.findOneV2(id);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => EmergencyEntity)
  async create_emergency(
    @Args('input') input: CreateEmergencyInput,
  ): Promise<EmergencyEntity> {
    const foundUser = await this.userServices.findOneV2(input.ownerid);
    if (!foundUser)
      throw new HttpException('Not found owner', HttpStatus.NOT_FOUND);

    const foundPatient = await this.patientServices.findOneV2(input.patientid);
    if (!foundPatient)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);

    const listclinical = [];
    for (const clinicalid of input.listclinical) {
      const foundClinical = await this.clinicalServices.findOne(clinicalid);
      if (foundClinical) listclinical.push(foundClinical);
    }

    const foundMethod = await this.breathServices.findOne(input.methodbreathid);
    if (!foundMethod)
      throw new HttpException('Not found methodbreath', HttpStatus.NOT_FOUND);

    const foundStatus = await this.statusServices.findOne(input.statusid);
    if (!foundStatus)
      throw new HttpException('Not found status', HttpStatus.NOT_FOUND);

    const foundDoctor = await this.doctorServices.findOneV2(input.doctorid);
    if (!foundDoctor)
      throw new HttpException('Not found doctor', HttpStatus.NOT_FOUND);
    const emergency = Object.assign(new EmergencyEntity(), {
      ...input,
      patient: foundPatient,
      clinical: listclinical,
      methodbreath: foundMethod,
      status: foundStatus,
    });
    const dataEmergency = await this.emergencyServices.post(emergency);
    const log = Object.assign(new LogMedicalEntity(), {
      patient: foundPatient,
      createdby: foundUser,
      name: `${foundUser.lastname} ${
        foundUser.firstname
      } thêm mới hồ sơ cấp cứu (CC ${moment(dataEmergency.createddat)
        .utcOffset('+07:00')
        .toDate()
        .valueOf()}) từ web`,
      description: '',
    });
    this.logMedicalServices.post(log);
    return dataEmergency;
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => EmergencyEntity)
  async update_emergency(
    @Args('input') input: UpdateEmergencyInput,
  ): Promise<EmergencyEntity> {
    const foundEmergency = await this.emergencyServices.findOne(input.id);
    if (!foundEmergency)
      throw new HttpException('Not found emergency', HttpStatus.NOT_FOUND);

    const foundUser = await this.userServices.findOneV2(input.ownerid);
    if (!foundUser)
      throw new HttpException('Not found owner', HttpStatus.NOT_FOUND);

    const foundPatient = await this.patientServices.findOneV2(input.patientid);
    if (!foundPatient)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);

    const listclinical = [];
    for (const clinicalid of input.listclinical) {
      const foundClinical = await this.clinicalServices.findOne(clinicalid);
      if (foundClinical) listclinical.push(foundClinical);
    }

    const foudMethod = await this.breathServices.findOne(input.methodbreathid);
    if (!foudMethod)
      throw new HttpException('Not found methodbreath', HttpStatus.NOT_FOUND);

    const foundStatus = await this.statusServices.findOne(input.statusid);
    if (!foundStatus)
      throw new HttpException('Not found status', HttpStatus.NOT_FOUND);

    const foundDoctor = await this.doctorServices.findOneV2(input.doctorid);
    if (!foundDoctor)
      throw new HttpException('Not found doctor', HttpStatus.NOT_FOUND);
    const emergency = Object.assign(new EmergencyEntity(), {
      ...input,
      patient: foundPatient,
      clinical: listclinical,
      methodbreath: foudMethod,
      status: foundStatus,
    });
    const dataEmergency = await this.emergencyServices.post(emergency);
    const log = Object.assign(new LogMedicalEntity(), {
      patient: foundPatient,
      createdby: foundUser,
      name: `${foundUser.lastname} ${
        foundUser.firstname
      } sửa hồ sơ cấp cứu (CC ${moment(dataEmergency.createddat)
        .utcOffset('+07:00')
        .toDate()
        .valueOf()}) từ web`,
      description: '',
    });
    this.logMedicalServices.post(log);
    return dataEmergency;
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => EmergencyEntity)
  async find_latest_emergency(
    @Args('patientid') patientid: string,
  ): Promise<EmergencyEntity> {
    const foundPatient = await this.patientServices.findOneV2(patientid);
    if (!foundPatient)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);
    const emergencyData = await this.emergencyServices.findAll(foundPatient.id);
    return await this.emergencyServices.findOneV2(emergencyData[0].id);
  }
}
