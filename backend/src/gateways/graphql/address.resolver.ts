import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { AddressServices } from '../../services/core/addresses/address.service';
import { UserServices } from '../../services/core/users/user.service';
import { AddressEntity } from '../../entities/addresses.entity';
import { CreateAddressInput } from '../../services/core/addresses/dto/create.input';
import { UpdateAddressInput } from 'src/services/core/addresses/dto/put.input';

@Resolver()
export class AddressResolver {
  constructor(
    private addressServices: AddressServices,
    private userServices: UserServices,
  ) {}
  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [AddressEntity])
  async find_all_address(): Promise<AddressEntity[]> {
    return await this.addressServices.findAll();
  }
  @UseGuards(GraphqlAuthGuard)
  @Query((of) => AddressEntity)
  async find_address(@Args('id') id: string): Promise<AddressEntity> {
    return await this.addressServices.findOne(id);
  }
  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => AddressEntity)
  async add_address(
    @Args('input') input: CreateAddressInput,
  ): Promise<AddressEntity> {
    const foudUser = await this.userServices.findOneV2(input.user);
    if (!foudUser)
      throw new HttpException('Not foud user', HttpStatus.NOT_FOUND);
    return await this.addressServices.post(input, foudUser);
  }
  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => AddressEntity)
  async put_address(
    @Args('input') input: UpdateAddressInput,
  ): Promise<AddressEntity> {
    const foundAddress = await this.addressServices.findOne(input.id);
    if (!foundAddress)
      throw new HttpException('Not foud address', HttpStatus.NOT_FOUND);
    const foudUser = await this.userServices.findOneV2(input.user);
    if (!foudUser)
      throw new HttpException('Not foud user', HttpStatus.NOT_FOUND);
    return await this.addressServices.put(
      { ...foundAddress, ...input },
      foudUser,
    );
  }
}
