import { Args, Mutation, Resolver, Query } from '@nestjs/graphql';
import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { PatientServices } from 'src/services/core/patients/patient.service';
import { MedicalEntity } from 'src/entities/medical.entity';
import { CreateMedicalInput } from 'src/services/core/medicals/dto/post.input';
import { MedicalServices } from 'src/services/core/medicals/medical.service';
import { DoctorServices } from 'src/services/core/users/doctor.service';
import { UpdateMedicalInput } from 'src/services/core/medicals/dto/update.input';
import { CreateMedicalTrackInput } from 'src/services/core/medical_tracks/dto/post.input';
import { MedicalTrackServices } from 'src/services/core/medical_tracks/medicalTrack.service';
import { MedicalTrackEntity } from 'src/entities/medical_track.entity';
import { UpdateMedicalTrackInput } from 'src/services/core/medical_tracks/dto/update.input';
import { LogMedicalServices } from 'src/services/core/log_medicals/logs.service';
import { UserServices } from 'src/services/core/users/user.service';
import { LogMedicalEntity } from 'src/entities/log_medical.entity';
import * as moment from 'moment';

@Resolver()
export class MedicalResolver {
  constructor(
    private medicalServies: MedicalServices,
    private patientServies: PatientServices,
    private doctorServies: DoctorServices,
    private medicalTrackServices: MedicalTrackServices,
    private logMedicalServices: LogMedicalServices,
    private userServies: UserServices,
  ) {}

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => MedicalEntity)
  async find_one_medical(
    @Args('id', { type: () => String }) id: string,
  ): Promise<MedicalEntity> {
    return await this.medicalServies.findOneV2(id);
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [MedicalEntity])
  async find_all_medical(
    @Args('patientid', { type: () => String }) patientid: string,
  ): Promise<MedicalEntity[]> {
    const foundPatient = await this.patientServies.findOneV2(patientid);
    if (!foundPatient)
      throw new HttpException('Not found Patient', HttpStatus.NOT_FOUND);
    return await this.medicalServies.findAll(patientid);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => MedicalEntity)
  async create_medical(
    @Args('input') input: CreateMedicalInput,
  ): Promise<MedicalEntity> {
    const foundUser = await this.userServies.findOneV2(input.ownerid);

    if (!foundUser)
      throw new HttpException('Not found owner', HttpStatus.NOT_FOUND);

    const foundPatient = await this.patientServies.findOneV2(input.patientid);
    if (!foundPatient)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);

    const foundDoctor = await this.doctorServies.findOneV2(input.doctorid);
    if (!foundDoctor)
      throw new HttpException('Not found doctor', HttpStatus.NOT_FOUND);

    const dataMedical = await this.medicalServies.post(input, {
      patient: foundPatient,
      doctor: foundDoctor,
    });
    const log = Object.assign(new LogMedicalEntity(), {
      patient: foundPatient,
      createdby: foundUser,
      name: `${foundUser.lastname} ${
        foundUser.firstname
      } thêm mới hồ sơ bệnh án (HS ${moment(dataMedical.createddat)
        .utcOffset('+07:00')
        .toDate()
        .valueOf()}) từ web`,
      description: '',
    });
    this.logMedicalServices.post(log);
    this.patientServies.putV2({
      ...foundPatient,
      height: input.height,
      weight: input.weight,
    });
    return dataMedical;
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => MedicalEntity)
  async update_medical(
    @Args('input') input: UpdateMedicalInput,
  ): Promise<MedicalEntity> {
    const foundUser = await this.userServies.findOneV2(input.ownerid);
    if (!foundUser)
      throw new HttpException('Not found owner', HttpStatus.NOT_FOUND);

    const foundMedical = await this.medicalServies.findOneV2(input.id);
    if (!foundMedical)
      throw new HttpException(
        'Not found medical profile',
        HttpStatus.NOT_FOUND,
      );

    const foundPatient = await this.patientServies.findOneV2(input.patientid);
    if (!foundPatient)
      throw new HttpException('Not found patient', HttpStatus.NOT_FOUND);

    const foundDoctor = await this.doctorServies.findOneV2(input.doctorid);
    if (!foundDoctor)
      throw new HttpException('Not found doctor', HttpStatus.NOT_FOUND);

    const dataMedical = await this.medicalServies.put(
      { ...foundMedical, ...input },
      {
        patient: foundPatient,
        doctor: foundDoctor,
      },
    );
    const log = Object.assign(new LogMedicalEntity(), {
      patient: foundPatient,
      createdby: foundUser,
      name: `${foundUser.lastname} ${
        foundUser.firstname
      } cập nhật hồ sơ bệnh án (HS ${moment(dataMedical.createddat)
        .utcOffset('+07:00')
        .toDate()
        .valueOf()}) từ web`,
      description: '',
    });
    this.logMedicalServices.post(log);
    this.patientServies.putV2({
      ...foundPatient,
      height: input.height,
      weight: input.weight,
    });
    return dataMedical;
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => MedicalTrackEntity)
  async create_medical_track(
    @Args('input') input: CreateMedicalTrackInput,
  ): Promise<MedicalTrackEntity> {
    const foundUser = await this.userServies.findOneV2(input.ownerid);
    if (!foundUser)
      throw new HttpException('Not found owner', HttpStatus.NOT_FOUND);

    const foundMedical = await this.medicalServies.findOneV2(input.medicalid);
    if (!foundMedical)
      throw new HttpException('Not found medical', HttpStatus.NOT_FOUND);
    const medicalTrack = await this.medicalTrackServices.post(input, {
      medical: foundMedical,
    });

    const foundPatient = await this.patientServies.findOneV2(
      foundMedical.patient.id,
    );
    const log = Object.assign(new LogMedicalEntity(), {
      patient: foundPatient,
      createdby: foundUser,
      name: `${foundUser.lastname} ${
        foundUser.firstname
      } thêm kết quả cho hồ sơ bệnh án (HS ${moment(foundMedical.createddat)
        .utcOffset('+07:00')
        .toDate()
        .valueOf()}) từ web`,
      description: `medicalid ${foundMedical.id}- update ${foundMedical.updatedat}`,
    });
    this.logMedicalServices.post(log);
    return medicalTrack;
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => MedicalTrackEntity)
  async update_medical_track(
    @Args('input') input: UpdateMedicalTrackInput,
  ): Promise<MedicalTrackEntity> {
    const foundUser = await this.userServies.findOneV2(input.ownerid);
    if (!foundUser)
      throw new HttpException('Not found owner', HttpStatus.NOT_FOUND);

    const foundTrack = await this.medicalTrackServices.findOne(input.id);
    if (!foundTrack)
      throw new HttpException('Not found medical track', HttpStatus.NOT_FOUND);
    const foundMedical = await this.medicalServies.findOneV2(input.medicalid);
    if (!foundMedical)
      throw new HttpException('Not found medical', HttpStatus.NOT_FOUND);
    const foundPatient = await this.patientServies.findOneV2(
      foundMedical.patient.id,
    );
    const log = Object.assign(new LogMedicalEntity(), {
      patient: foundPatient,
      createdby: foundUser,
      name: `${foundUser.lastname} ${
        foundUser.firstname
      } cập nhật kết quả cho hồ sơ bệnh án (HS ${moment(foundMedical.createddat)
        .utcOffset('+07:00')
        .toDate()
        .valueOf()}) từ web`,
      description: `medicalid ${foundMedical.id}- update ${foundMedical.updatedat}`,
    });
    this.logMedicalServices.post(log);
    return await this.medicalTrackServices.put(input);
  }
}
