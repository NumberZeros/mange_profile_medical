import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UserServices } from '../../services/core/users/user.service';
import { EGender, ERoles, UserEntity } from '../../entities/users.entity';
import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { CategoryEntity } from 'src/entities/categories.entity';
import { CategoryServices } from 'src/services/core/categories/category.service';
import { PathogenicServices } from 'src/services/core/pathogenices/pathogenic.service';
import { DoctorServices } from 'src/services/core/users/doctor.service';
import { DoctorEntity } from 'src/entities/doctor.entity';
import { PatientStatusEntity } from 'src/entities/patient_status.entity';
import { PatientStatusServices } from 'src/services/core/patient_status/patitent_status.service';
import { PatientEntity } from 'src/entities/patient.entity';
import { PatientServices } from 'src/services/core/patients/patient.service';
import { GroupEntity } from 'src/entities/group.entity';
import { GroupServices } from 'src/services/core/groups/group.service';
import { ClinicalEntity } from 'src/entities/clinical.entity';
import { ClinicalServices } from 'src/services/core/clinical/clinical.service';
import { EmergencyStatusEntity } from 'src/entities/emegency_status.entity';
import { EmergencyStatusServices } from 'src/services/core/emergency_status/emergency_stauts.service';
import { BreathEntity } from 'src/entities/breath.entity';
import { BreathServices } from 'src/services/core/breath/breath.service';

@Resolver()
export class InitResolver {
  constructor(
    private userServies: UserServices,
    private doctorServices: DoctorServices,
    private categoryServies: CategoryServices,
    private pathogenicServies: PathogenicServices,
    private statusServices: PatientStatusServices,
    private patientServices: PatientServices,
    private groupServices: GroupServices,
    private clinicalServices: ClinicalServices,
    private emerStatusServices: EmergencyStatusServices,
    private breathServices: BreathServices,
  ) {}

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [CategoryEntity])
  async init_category(): Promise<CategoryEntity[]> {
    const list = ['Khám lần đầu', 'Tái khám', 'Khác', 'Không'];
    const listCategory = [];
    for (const name of list) {
      const foundCategory = await this.categoryServies.findName(name);
      if (!foundCategory)
        listCategory.push(
          await this.categoryServies.post({ name, isverified: true }),
        );
    }
    return listCategory;
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [CategoryEntity])
  async init_pathogenic(): Promise<CategoryEntity[]> {
    const list = [
      'Đái tháo đường',
      'Bệnh phổi tắc nghẽn mãn tính và các bệnh phổi khác',
      'Ung thư (đặc biệt là các khối u ác tính về huyết học, ung thư phổi và bệnh ung thư di căn khác)',
      'Bệnh thận mạn tính',
      'Ghép tạng hoặc cấy ghép tế bào gốc tạo máu',
      'Béo phì, thừa cân',
      'Bệnh tim mạch (suy tim, bệnh động mạch vành hoặc bệnh cơ tim)',
      'Bệnh lý mạch máu não',
      'Hội chứng Down',
      'HIV/AIDS',
      'Bệnh lý thần kinh, bao gồm chứng sa sút trí tuệ',
      'Bệnh hồng cầu hình liềm',
      'Bệnh hen suyễn',
      'Thiếu hụt miễn dịch',
      'Tăng huyết áp',
      'Bệnh gan',
      'Rối loạn sử dụng chất gây nghiện',
      'Sử dụng corticosteroid hoặc các thuốc ức chế miễn dịch khác',
      'Các loại bệnh hệ thống',
      'Khác',
      'Không',
    ];
    const listPathogenic = [];
    for (const name of list) {
      const foundPathogenic = await this.pathogenicServies.findName(name);
      if (!foundPathogenic)
        listPathogenic.push(
          await this.pathogenicServies.post({ name, isverified: true }),
        );
    }
    return listPathogenic;
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [PatientStatusEntity])
  async init_patient_status(): Promise<PatientStatusEntity[]> {
    const list = [
      'Hoàn thành theo dõi (hoàn thành đủ thời gian theo dõi tại nhà)',
      'Cách ly tập trung: bệnh nhân đi cách ly tập trung theo quy định địa phương',
      'Chuyển team 2: BN được team 2 tiếp nhận',
      'Nhập viện: bệnh nhân diễn tiến nặng, team 2 không nhận (do hết giường…)',
      'Tử vong',
      'Bệnh nhân từ chối được theo dõi',
      'Không liên lạc được/mất dấu theo dõi’',
      'Bệnh lý mạch máu não',
      'Nhận hỗ trợ bệnh nhân',
      'Đã đến hiện trường',
      'Hoàn thành hỗ trợ bệnh nhân',
      'Khác',
      '',
    ];
    const listStatus = [];
    for (const name of list) {
      const foundPathogenic = await this.statusServices.findName(name);
      if (!foundPathogenic)
        listStatus.push(
          await this.statusServices.post({ name, description: '' }),
        );
    }
    return listStatus;
  }

  @Mutation((of) => UserEntity)
  async init_admin(
    @Args('email') email: string,
    @Args('phone') phone: number,
  ): Promise<UserEntity> {
    const foundEmail = await this.userServies.findEmail(email);
    if (foundEmail)
      throw new HttpException('Account admin exist', HttpStatus.CONFLICT);
    const dataUser = {
      firstname: 'admin',
      lastname: '',
      email: email,
      password: email,
      phone: `(+84) ${phone}`,
      dob: new Date(),
      isverified: true,
      gender: EGender.male,
      roles: ERoles.admin,
    };
    return await this.userServies.post(dataUser);
  }

  @Mutation((of) => DoctorEntity)
  async init_doctor(
    @Args('email') email: string,
    @Args('phone') phone: number,
  ): Promise<DoctorEntity> {
    const foundEmail = await this.userServies.findEmail(email);
    if (foundEmail)
      throw new HttpException('Account admin exist', HttpStatus.CONFLICT);
    const dataUser = {
      firstname: 'Khác',
      lastname: 'Khác',
      email: email,
      password: email,
      phone: `(+84) ${phone}`,
      dob: new Date(),
      isverified: true,
      gender: EGender.male,
      roles: ERoles.doctor,
    };
    const user = await this.userServies.post(dataUser);
    return await this.doctorServices.post(
      { certificate: 'mac_dinh', isverified: true },
      { user, groups: [await this.groupServices.findName('')] },
    );
  }

  @Query((of) => [PatientEntity])
  async setup_default_patient(): Promise<PatientEntity[]> {
    const dataPatient = await this.patientServices.findAllNullField([
      'status',
      'sharedoctor',
    ]);
    const foundPatientStatus = await this.statusServices.findName('');
    const foundDoctor = await this.doctorServices.findEmail(
      'macdinh@gmail.com',
    );
    for (const patient of dataPatient) {
      if (!patient.status || !patient.sharedoctor) {
        await this.patientServices.putV2({
          ...patient,
          status: foundPatientStatus,
          sharedoctor: foundDoctor,
        });
      }
    }
    return dataPatient;
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => GroupEntity)
  async init_group(): Promise<GroupEntity> {
    const foundGroup = await this.groupServices.findName('');

    if (foundGroup) return foundGroup;
    return await this.groupServices.init('');
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [ClinicalEntity])
  async init_clinical(): Promise<ClinicalEntity[]> {
    const list = ['Khó thở', 'Sốt đột ngột >38oC', 'Ho', 'Hôn mê', 'Khác', ''];
    const listClinical = [];
    for (const name of list) {
      const foundClinical = await this.clinicalServices.findName(name);
      if (!foundClinical) {
        const clinical = Object.assign(new ClinicalEntity(), {
          name,
          isverified: true,
        });

        listClinical.push(await this.clinicalServices.post(clinical));
      }
    }
    return listClinical;
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [EmergencyStatusEntity])
  async init_emergency_status(): Promise<EmergencyStatusEntity[]> {
    const list = [
      'Chuyển team 1',
      'Chuyển team 2',
      'Chuyển viện',
      'Tử vong',
      'Khoẻ/Về nhà',
      'Khác',
      '',
    ];
    const listEmergencyStatus = [];
    for (const name of list) {
      const emerStatus = await this.emerStatusServices.findName(name);
      if (!emerStatus) {
        const emergencyStatus = Object.assign(new EmergencyStatusEntity(), {
          name,
          isverified: true,
        });
        listEmergencyStatus.push(
          await this.emerStatusServices.post(emergencyStatus),
        );
      }
    }
    return listEmergencyStatus;
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [BreathEntity])
  async init_breath(): Promise<BreathEntity[]> {
    const list = ['Khí trời', 'Canula', 'Mask', 'HFNC', 'CPAP', 'Máy thở'];
    const listbreath = [];
    for (const name of list) {
      const breath = await this.breathServices.findName(name);
      if (!breath) {
        const emergencyStatus = Object.assign(new BreathEntity(), {
          name,
          isverified: true,
        });
        listbreath.push(await this.breathServices.post(emergencyStatus));
      }
    }
    return listbreath;
  }
}
