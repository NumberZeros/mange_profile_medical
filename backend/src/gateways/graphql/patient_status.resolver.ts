import { Args, Query, Resolver } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { PatientStatusEntity } from 'src/entities/patient_status.entity';
import { PatientStatusServices } from 'src/services/core/patient_status/patitent_status.service';

@Resolver()
export class PatientStatusResolver {
  constructor(private statusServices: PatientStatusServices) {}

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [PatientStatusEntity])
  async find_all_patient_status(): Promise<PatientStatusEntity[]> {
    return await this.statusServices.findAll();
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => PatientStatusEntity)
  async find_one_pathogenic(
    @Args('id', { type: () => String }) id: string,
  ): Promise<PatientStatusEntity> {
    return await this.statusServices.findOne(id);
  }
}
