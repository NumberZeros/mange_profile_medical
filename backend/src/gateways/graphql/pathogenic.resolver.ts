import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { PathogenicServices } from 'src/services/core/pathogenices/pathogenic.service';
import { PathogenicEntity } from 'src/entities/pathogenic.entity';

@Resolver()
export class PathogenicResolver {
  constructor(private pathogenicServices: PathogenicServices) {}

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [PathogenicEntity])
  async find_all_pathogenic(): Promise<PathogenicEntity[]> {
    return await this.pathogenicServices.findAll();
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => PathogenicEntity)
  async find_one_pathogenic(
    @Args('id', { type: () => String }) id: string,
  ): Promise<PathogenicEntity> {
    return await this.pathogenicServices.findOne(id);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => PathogenicEntity)
  async create_pathogenic(
    @Args('name', { type: () => String }) name: string,
  ): Promise<PathogenicEntity> {
    return this.pathogenicServices.post({ name, isverified: true });
  }
}
