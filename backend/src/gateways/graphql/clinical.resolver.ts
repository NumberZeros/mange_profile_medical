import { Query, Resolver } from '@nestjs/graphql';

import { UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { ClinicalServices } from 'src/services/core/clinical/clinical.service';
import { ClinicalEntity } from 'src/entities/clinical.entity';

@Resolver()
export class ClinicalResolver {
  constructor(private clinicalServices: ClinicalServices) {}
  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [ClinicalEntity])
  async find_all_clinical(): Promise<ClinicalEntity[]> {
    return await this.clinicalServices.findAll();
  }
}
