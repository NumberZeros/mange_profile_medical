import { Query, Resolver } from '@nestjs/graphql';

import { UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { BreathServices } from 'src/services/core/breath/breath.service';
import { BreathEntity } from 'src/entities/breath.entity';

@Resolver()
export class BreathResolver {
  constructor(private clinicalServices: BreathServices) {}
  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [BreathEntity])
  async find_all_breath(): Promise<BreathEntity[]> {
    return await this.clinicalServices.findAll();
  }
}
