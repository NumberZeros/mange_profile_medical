import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UserServices } from '../../services/core/users/user.service';
import { ERoles } from '../../entities/users.entity';
import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from '../../services/packages/grapql.guard';
import { CreateDoctorInput } from 'src/services/core/users/dto/createDoctor.input';
import { DoctorServices } from 'src/services/core/users/doctor.service';
import { DoctorEntity } from 'src/entities/doctor.entity';
import { UpdateDoctorInput } from 'src/services/core/users/dto/updateDoctor.input';
import { VerifiedDoctorInput } from 'src/services/core/users/dto/verifiedDoctor.input';
import { DeleteDoctorInput } from 'src/services/core/users/dto/deleteDoctor.input';
import { BlockDoctorInput } from 'src/services/core/users/dto/blockDoctor.input';
import { GroupServices } from 'src/services/core/groups/group.service';
import {
  PaginationDoctor,
  PaginationDoctorInput,
} from 'src/services/core/users/dto/paginationDoctor.input';
@Resolver()
export class DoctorResolver {
  constructor(
    private userServies: UserServices,
    private doctorServies: DoctorServices,
    private groupServices: GroupServices,
  ) {}

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => [DoctorEntity])
  async find_all_doctor(
    @Args('adminid', { type: () => String }) adminid: string,
  ): Promise<DoctorEntity[]> {
    await this.userServies.validateAccount(adminid, ERoles.admin);
    return await this.doctorServies.findAll();
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => DoctorEntity)
  async find_one_doctor(
    @Args('id', { type: () => String }) id: string,
    @Args('adminid', { type: () => String }) adminid: string,
  ): Promise<DoctorEntity> {
    await this.userServies.validateAccount(adminid, ERoles.admin);
    return await this.doctorServies.findOneV2(id);
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => DoctorEntity)
  async create_doctor(
    @Args('input') input: CreateDoctorInput,
  ): Promise<DoctorEntity> {
    await this.userServies.validateAccount(input.adminid, ERoles.admin);
    const foundEmail = await this.userServies.findEmail(input.email);
    if (foundEmail)
      throw new HttpException('Email is exist', HttpStatus.CONFLICT);

    const foundPhone = await this.userServies.findPhone(input.phone);
    if (foundPhone)
      throw new HttpException('Phone is exist', HttpStatus.CONFLICT);
    const user = await this.userServies.post({
      ...input,
      roles: ERoles.doctor,
      password: input.email.trim().toLocaleLowerCase(),
    });

    const dataDoctor = await this.doctorServies.post(
      { certificate: input.certificate, isverified: true },
      { groups: [await this.groupServices.findName('')], user },
    );
    return dataDoctor;
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => DoctorEntity)
  async update_doctor(
    @Args('input') input: UpdateDoctorInput,
  ): Promise<DoctorEntity> {
    await this.userServies.validateAccount(input.adminid, ERoles.admin);
    const foundDoctor = await this.doctorServies.findOne(input.id);
    if (!foundDoctor || foundDoctor.user.roles !== ERoles.doctor)
      throw new HttpException('Account not found Dotor', HttpStatus.NOT_FOUND);
    if (input.email !== foundDoctor.user.email) {
      const foundEmail = await this.userServies.findEmail(input.email);
      if (foundEmail)
        throw new HttpException('Email is exist', HttpStatus.CONFLICT);
    }
    if (input.phone !== foundDoctor.user.phone) {
      const foundPhone = await this.userServies.findPhone(input.phone);
      if (foundPhone)
        throw new HttpException('Phone is exist', HttpStatus.CONFLICT);
    }
    const foundUser = await this.userServies.put({
      ...foundDoctor.user,
      ...input,
      id: foundDoctor.user.id,
    });
    return await this.doctorServies.put(
      {
        ...foundDoctor,
        ...input,
        id: input.id,
      },
      foundUser,
    );
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => DoctorEntity)
  async verified_doctor(
    @Args('input') input: VerifiedDoctorInput,
  ): Promise<DoctorEntity> {
    await this.userServies.validateAccount(input.adminid, ERoles.admin);
    const foundDoctor = await this.doctorServies.findOne(input.id);
    if (!foundDoctor || foundDoctor.user.roles !== ERoles.doctor)
      throw new HttpException('Account not found Dotor', HttpStatus.NOT_FOUND);
    return await this.doctorServies.delete({
      ...foundDoctor,
      isverified: input.isverified,
    });
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => DoctorEntity)
  async unverified_doctor(
    @Args('input') input: DeleteDoctorInput,
  ): Promise<DoctorEntity> {
    await this.userServies.validateAccount(input.adminid, ERoles.admin);
    const foundDoctor = await this.doctorServies.findOne(input.id);
    if (!foundDoctor || foundDoctor.user.roles !== ERoles.doctor)
      throw new HttpException('Account not found Dotor', HttpStatus.NOT_FOUND);
    else if (foundDoctor.isverified)
      throw new HttpException('Only account unverified', HttpStatus.NOT_FOUND);
    return await this.doctorServies.delete({
      ...foundDoctor,
      inactive: true,
    });
  }

  @UseGuards(GraphqlAuthGuard)
  @Mutation((of) => DoctorEntity)
  async handle_block_doctor(
    @Args('input') input: BlockDoctorInput,
  ): Promise<DoctorEntity> {
    await this.userServies.validateAccount(input.adminid, ERoles.admin);
    const foundDoctor = await this.doctorServies.findOne(input.id);
    if (!foundDoctor || foundDoctor.user.roles !== ERoles.doctor)
      throw new HttpException('Account not found Dotor', HttpStatus.NOT_FOUND);
    return await this.doctorServies.delete({
      ...foundDoctor,
      inactive: input.inactive,
    });
  }

  @UseGuards(GraphqlAuthGuard)
  @Query((of) => PaginationDoctor)
  async find_all_doctor_use_pagination(
    @Args('pagination') pagination: PaginationDoctorInput,
  ): Promise<PaginationDoctor> {
    await this.userServies.validateAccount(pagination.adminid, ERoles.admin);
    return await this.doctorServies.findAllDoctorUserPagination(pagination);
  }
}
