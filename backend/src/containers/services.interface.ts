export interface IBaseServices {
  findAll(pagination?: any): Promise<any>;
  findOne(id: any): Promise<any>;
  post(data: any, another?: any): Promise<any>;
  put(data: any, another?: any): Promise<any>;
  delete(data: any, another?: any): Promise<any>;
}
