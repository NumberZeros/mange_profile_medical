import { DoctorResolver } from '../gateways/graphql/doctor.resolver';
import { PatientResolver } from '../gateways/graphql/patient.resolver';
import { AddressResolver } from '../gateways/graphql/address.resolver';
import { AuthResolver } from '../gateways/graphql/auth.resolver';
import { UserResolver } from '../gateways/graphql/user.resolver';
import { CategoryResolver } from '../gateways/graphql/category.resolver';
import { InitResolver } from '../gateways/graphql/setup.resolver';
import { PathogenicResolver } from 'src/gateways/graphql/pathogenic.resolver';
import { MedicalResolver } from 'src/gateways/graphql/medical.resolver';
import { UploadController } from 'src/gateways/upload/uploadfile.controller';
import { AnalysisResolver } from 'src/gateways/graphql/analysis.resolver';
import { PatientStatusResolver } from 'src/gateways/graphql/patient_status.resolver';
import { GroupResolver } from 'src/gateways/graphql/group.resolver';
import { LogMedicalResolver } from 'src/gateways/graphql/log_medical.resolver';
import { EmergencyResolver } from 'src/gateways/graphql/emergency.resolver';
import { MedicineResolver } from 'src/gateways/graphql/medicine.resolver';
import { ClinicalResolver } from 'src/gateways/graphql/clinical.resolver';
import { EmergencyStatusResolver } from 'src/gateways/graphql/emegency_status.resolver';
import { BreathResolver } from 'src/gateways/graphql/breath.resolver';
import { SMSResolver } from 'src/gateways/graphql/sms.resolver';
import { GroupPatientResolver } from 'src/gateways/graphql/group_patient.resolver';

const combineResolver = [
  InitResolver,
  UserResolver,
  AuthResolver,
  AddressResolver,
  DoctorResolver,
  PatientResolver,
  CategoryResolver,
  PathogenicResolver,
  MedicalResolver,
  UploadController,
  AnalysisResolver,
  GroupResolver,
  PatientStatusResolver,
  LogMedicalResolver,
  EmergencyResolver,
  MedicineResolver,
  ClinicalResolver,
  EmergencyStatusResolver,
  BreathResolver,
  SMSResolver,
  GroupPatientResolver,
];
export default combineResolver;
