import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import RootEntities from './entities.root';
import RootServices from './services.root';
import RootResolver from './resolvers.root';
import RootSchedules from './schedules.root';
import { UploadController } from 'src/gateways/upload/uploadfile.controller';

@Module({
  imports: [TypeOrmModule.forFeature(RootEntities)],
  providers: [
    ...RootResolver,
    //only apply server binh tan
    // ...RootSchedules,
    ...RootServices,
  ],
  controllers: [UploadController],
})
export class RootModule {}
