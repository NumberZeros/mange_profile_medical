import { AddressServices } from '../services/core/addresses/address.service';
import { UserServices } from '../services/core/users/user.service';
import { AuthService } from '../services/packages/auth.service';
import { DoctorServices } from '../services/core/users/doctor.service';
import { PatientServices } from '../services/core/patients/patient.service';
import { CategoryServices } from '../services/core/categories/category.service';
import { PathogenicServices } from 'src/services/core/pathogenices/pathogenic.service';
import { MedicalServices } from 'src/services/core/medicals/medical.service';
import { FilesServices } from 'src/services/core/upload/file.service';
import { MedicalTrackServices } from 'src/services/core/medical_tracks/medicalTrack.service';
import { MedicineServices } from 'src/services/core/medicine_details/medicine.service';
import { GroupServices } from 'src/services/core/groups/group.service';
import { PatientStatusServices } from 'src/services/core/patient_status/patitent_status.service';
import { LogMedicalServices } from 'src/services/core/log_medicals/logs.service';
import { ClinicalServices } from 'src/services/core/clinical/clinical.service';
import { EmergencyStatusServices } from 'src/services/core/emergency_status/emergency_stauts.service';
import { BreathServices } from 'src/services/core/breath/breath.service';
import { EmergencyServices } from 'src/services/core/emergency/emergency.service';
import { AnalaysisServices } from 'src/services/reports/analaysis/analaysis.service';
import { SMSServices } from 'src/services/core/sms/sms.service';
import { GroupPatientServices } from 'src/services/core/group_patient/group_patient.service';

const combineServices = [
  UserServices,
  AuthService,
  AddressServices,
  DoctorServices,
  PatientServices,
  CategoryServices,
  PathogenicServices,
  MedicalServices,
  FilesServices,
  MedicalTrackServices,
  MedicineServices,
  GroupServices,
  PatientStatusServices,
  LogMedicalServices,
  ClinicalServices,
  EmergencyStatusServices,
  BreathServices,
  EmergencyServices,
  AnalaysisServices,
  SMSServices,
  GroupPatientServices,
];
export default combineServices;
