import { Field, ObjectType } from '@nestjs/graphql';
import {
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
@ObjectType()
export abstract class RootObject {
  @Field({ description: `Internal ID` })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field({
    description: `External ID`,
    deprecationReason: 'Using integrate',
  })
  @Column({ default: '' })
  externalid: string;

  @Field({
    description: 'Manual created when isverified is true',
    deprecationReason: 'Using intefrate',
  })
  @Column({ default: false })
  isverified: boolean;

  @Field()
  @Column({ default: false })
  inactive: boolean;

  @Field({ description: `Automatic` })
  @CreateDateColumn()
  createddat: Date;

  @Field({ description: `Automatic` })
  @UpdateDateColumn()
  updatedat: Date;
}
