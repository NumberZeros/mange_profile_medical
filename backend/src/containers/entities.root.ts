import { DoctorEntity } from '../entities/doctor.entity';
import { MedicalEntity } from '../entities/medical.entity';
import { AddressEntity } from '../entities/addresses.entity';
import { UserEntity } from '../entities/users.entity';
import { PatientEntity } from '../entities/patient.entity';
import { CategoryEntity } from '../entities/categories.entity';
import { PathogenicEntity } from '../entities/pathogenic.entity';
import { MedicineEntity } from 'src/entities/medicine_detail.entity';
import { MedicalTrackEntity } from 'src/entities/medical_track.entity';
import { GroupEntity } from 'src/entities/group.entity';
import { PatientStatusEntity } from 'src/entities/patient_status.entity';
import { LogMedicalEntity } from 'src/entities/log_medical.entity';
import { EmergencyEntity } from 'src/entities/emergency.entity';
import { EmergencyStatusEntity } from 'src/entities/emegency_status.entity';
import { ClinicalEntity } from 'src/entities/clinical.entity';
import { BreathEntity } from 'src/entities/breath.entity';
import { AnalysisEntity } from 'src/entities/analysis.entity';
import { SMSEntity } from 'src/entities/sms.entity';
import { GroupPatientEntity } from 'src/entities/group_patient.entity';
const combineEntities = [
  UserEntity,
  AddressEntity,
  DoctorEntity,
  PatientEntity,
  CategoryEntity,
  PathogenicEntity,
  MedicalEntity,
  MedicineEntity,
  MedicalTrackEntity,
  GroupEntity,
  PatientStatusEntity,
  LogMedicalEntity,
  EmergencyEntity,
  EmergencyStatusEntity,
  ClinicalEntity,
  BreathEntity,
  AnalysisEntity,
  SMSEntity,
  GroupPatientEntity,
];
export default combineEntities;
