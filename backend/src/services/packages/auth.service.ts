import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { secret } from '../../configuration';
import { UserEntity } from '../../entities/users.entity';
@Injectable()
export class AuthService {
  constructor() {}
  async validateUser(payload: string): Promise<any> {
    return await jwt.verify(payload, secret.JWT_KEY);
  }

  async createTokenUser(user: UserEntity) {
    const payload = { id: user.id };
    return {
      access_token: await jwt.sign(payload, secret.JWT_KEY, {
        expiresIn: '12h',
      }),
      refresh_token: await jwt.sign(payload, secret.JWT_KEY, {
        expiresIn: '24h',
      }),
    };
  }

  async createTokenNS(user: UserEntity) {
    const payload = { id: user.id };
    return {
      access_token: await jwt.sign(payload, secret.JWT_KEY, {
        expiresIn: '12h',
      }),
    };
  }
}
