import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { UserServices } from '../core/users/user.service';
import { AuthService } from './auth.service';

@Injectable()
export class GraphqlAuthGuard implements CanActivate {
  constructor(
    private userServies: UserServices,
    private authServies: AuthService,
  ) {}
  async canActivate(context: ExecutionContext) {
    try {
      const ctx = GqlExecutionContext.create(context);
      const { req } = ctx.getContext();
      const { authorization } = req.headers;
      if (!authorization || authorization === '')
        throw new HttpException(
          'Access token is not valid',
          HttpStatus.UNAUTHORIZED,
        );

      const accessToken = authorization.split(' ')[1];
      const data = await this.authServies.validateUser(accessToken);
      const dataUser = await this.userServies.findOne(data.id);
      if (dataUser) return true;
    } catch (error) {
      throw error;
    }
  }
}
