import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IBaseServices } from 'src/containers/services.interface';
import { AnalysisEntity } from 'src/entities/analysis.entity';
import { Repository } from 'typeorm/repository/Repository';

@Injectable()
export class AnalaysisServices implements IBaseServices {
  constructor(
    @InjectRepository(AnalysisEntity)
    private readonly analysisEntity: Repository<AnalysisEntity>,
  ) {}
  async findAll(): Promise<AnalysisEntity[]> {
    try {
      return await this.analysisEntity
        .createQueryBuilder()
        .where({ inactive: false, isverified: true })
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(props: {
    user: string;
    startweek: string;
  }): Promise<AnalysisEntity> {
    try {
      return await this.analysisEntity
        .createQueryBuilder()
        .where({ user: props.user, startweek: props.startweek })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async post(data: AnalysisEntity): Promise<AnalysisEntity> {
    try {
      return await this.analysisEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: AnalysisEntity): Promise<AnalysisEntity> {
    try {
      return await this.analysisEntity.save(data);
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }
  async delete(data: AnalysisEntity): Promise<AnalysisEntity> {
    throw new HttpException('error', HttpStatus.NOT_MODIFIED);
  }
}
