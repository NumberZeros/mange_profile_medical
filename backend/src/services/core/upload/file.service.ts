import { Injectable } from '@nestjs/common';

@Injectable()
export class FilesServices {
  constructor() {}

  async isNumeric(value) {
    return /^\d+$/.test(value);
  }
  async checkFileNameFormat({ fileName, type }) {
    try {
      let isValid = true;
      if (fileName.split(' ').length !== 3) return { success: false };
      fileName.split(' ').map((value, index) => {
        if (index === 0 && value !== type) isValid = false;
        if (index == 1 && (!this.isNumeric(value) || value.length !== 8))
          isValid = false;
        if (
          index == 2 &&
          (!this.isNumeric(value.replace('.csv', '')) || value.length !== 10)
        )
          isValid = false;
      });
      return { success: isValid };
    } catch (error) {
      return { success: false };
    }
  }

  async handleLine(value, distinct) {
    try {
      const listValue = [];
      let isConcat = false;
      let listConcat = [];
      for (const data of value.split(distinct)) {
        const idx = data.indexOf(`"`);
        if (isConcat) listConcat.push(data.replace(/['"]+/g, ''));
        else {
          if (idx === -1) {
            listValue.push(data.replace(/['"]+/g, ''));
            listConcat = [];
            continue;
          }
        }
        if (idx === 0 && !isConcat) {
          isConcat = true;
          listConcat.push(data.replace(/['"]+/g, ''));
        } else if (idx === data.length - 1) {
          isConcat = false;
          listValue.push(`${[...listConcat]}`);
        }
      }

      return listValue;
    } catch (error) {
      // console.log({
      //   title: 'ERR handleLine',
      //   details: error,
      // });
    }
  }

  async convertCSV(csv, isJSON): Promise<any> {
    try {
      const lines = csv.split(`\r\n`);
      const result = [];
      const distinct = [...lines[0]].find((o) => o === ';');
      const headers = lines[0].split(distinct ? distinct : ',');
      for (let ih = 0; ih < headers.length; ih++) {
        headers[ih] = headers[ih].trim();
      }
      for (let i = 1; i < lines.length; i++) {
        const obj = {};
        const currentline = await this.handleLine(
          lines[i],
          distinct ? distinct : ',',
        );
        for (let j = 0; j < headers.length; j++) {
          obj[headers[j]] = currentline[j];
        }
        result.push(obj);
      }
      return isJSON
        ? { success: true, data: JSON.stringify(result) } //JSON Object
        : { success: true, data: result }; //JavaScript object
    } catch (error) {
      // console.log({
      //   title: 'ERR convertCSV to object',
      //   details: error,
      // });
      return { success: false };
    }
  }
}
