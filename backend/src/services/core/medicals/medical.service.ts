import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm/repository/Repository';

import { IBaseServices } from '../../../containers/services.interface';
import { CreateMedicalInput } from './dto/post.input';
import { PatientEntity } from 'src/entities/patient.entity';
import { DoctorEntity } from 'src/entities/doctor.entity';
import {
  ERiskAssesment,
  MedicalEntity,
} from '../../../entities/medical.entity';
import { UpdateMedicalInput } from './dto/update.input';
import { MedicalTrackEntity } from 'src/entities/medical_track.entity';
import { IntegrationMedicalInput } from './dto/intergation.input';

@Injectable()
export class MedicalServices implements IBaseServices {
  constructor(
    @InjectRepository(MedicalEntity)
    private medicalEntity: Repository<MedicalEntity>,
  ) {}

  async findAll(patientid): Promise<MedicalEntity[]> {
    try {
      return await this.medicalEntity
        .createQueryBuilder()
        .where({ inactive: false, patient: patientid })
        .orderBy('displaydate', 'DESC')
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<MedicalEntity> {
    try {
      return await this.medicalEntity.findOne({
        id,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async post(
    data: CreateMedicalInput,
    props: {
      patient: PatientEntity;
      doctor: DoctorEntity;
    },
  ): Promise<MedicalEntity> {
    try {
      const medical = Object.assign(new MedicalEntity(), { ...data, ...props });
      return await this.medicalEntity.save({
        ...medical,
        displaydate: data.displaydate ? data.displaydate : medical.updatedat,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(
    data: UpdateMedicalInput,
    props: {
      patient: PatientEntity;
      doctor: DoctorEntity;
    },
  ): Promise<MedicalEntity> {
    try {
      return await this.medicalEntity.save({ ...data, ...props });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async delete(data: MedicalEntity): Promise<MedicalEntity> {
    try {
      return data;
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }

  async integrateionCSV(
    data: IntegrationMedicalInput,
    props: {
      patient: PatientEntity;
    },
  ): Promise<MedicalEntity> {
    try {
      const medical = Object.assign(new MedicalEntity(), { ...data, ...props });
      return await this.medicalEntity.save(medical);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async addMedicalTrack(
    data: MedicalTrackEntity,
    medical: MedicalEntity,
  ): Promise<MedicalEntity> {
    try {
      return await this.medicalEntity.save({
        ...medical,
        medicaltrack: [...medical.medicaltrack, data],
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async addMedicine(medical: MedicalEntity): Promise<MedicalEntity> {
    try {
      return await this.medicalEntity.save(medical);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findOneV2(id: string): Promise<MedicalEntity> {
    try {
      return await this.medicalEntity
        .createQueryBuilder('medical')
        .leftJoinAndSelect('medical.medicinenow', 'medicine_entity')
        .leftJoinAndSelect('medical.medicaltrack', 'medical_track_entity')
        .innerJoinAndMapOne(
          'medical.patient',
          PatientEntity,
          'patient',
          'medical.patientId = patient.id',
        )
        .where({ id, inactive: false })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async putV2(data: MedicalEntity): Promise<MedicalEntity> {
    try {
      return await this.medicalEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
