import { Field, InputType } from '@nestjs/graphql';
import { IsEnum, IsString, IsDate, IsNumber, IsBoolean } from 'class-validator';
import {
  ECAttribute,
  EColor,
  ERiskAssesment,
} from 'src/entities/medical.entity';
@InputType()
export class CreateMedicalInput {
  @Field()
  @IsString()
  ownerid: string;

  @Field()
  @IsString()
  patientid: string;

  @Field({ defaultValue: true })
  @IsBoolean()
  isverified: boolean;

  @Field()
  @IsString()
  doctorid: string;

  @Field()
  @IsNumber()
  weight: number;

  @Field()
  @IsNumber()
  height: number;

  @Field({
    description: `Đánh giá phân loại nguy cơ & xử trí: ${ERiskAssesment.low}, ${ERiskAssesment.medium}, ${ERiskAssesment.hightest}, ${ERiskAssesment.hightest},`,
    defaultValue: ERiskAssesment.low,
  })
  @IsEnum(ERiskAssesment)
  riskassessment: ERiskAssesment;

  @Field()
  @IsString()
  medicine: string;

  @Field({ defaultValue: 0 })
  @IsNumber()
  countdateremains: number;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal1: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal2: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal3: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal4: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal5: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal6: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal7: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal8: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal9: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal10: boolean;

  @Field({
    description:
      'Hôm nay ông/bà có cảm thấy điều gì khác thường trong cơ thể so với trước đây không?',
    defaultValue: false,
  })
  @IsBoolean()
  signal11: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal12: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal13: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal14: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal15: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal16: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal17: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal18: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal19: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal20: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal21: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal22: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal23: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal24: boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  signal25: boolean;

  @Field({ defaultValue: '' })
  @IsString()
  otherSignal: string;

  @Field({ defaultValue: '' })
  @IsString()
  statusquo: string;

  @Field()
  @IsEnum(EColor)
  color: EColor;

  @Field({ defaultValue: 0 })
  @IsNumber()
  pluse: number;

  @Field({ defaultValue: '' })
  @IsString()
  notepluse: string;

  @Field()
  @IsNumber()
  bloodpressure: number;

  @Field()
  @IsNumber()
  bloodpressure2: number;

  @Field({ defaultValue: '' })
  @IsString()
  notebloodpressure: string;

  @Field()
  @IsNumber()
  spo2home: number;

  @Field({ defaultValue: '' })
  @IsString()
  notespo2home: string;

  @Field()
  @IsNumber()
  temperature: number;

  @Field({ defaultValue: '' })
  @IsString()
  notetemperature: string;

  @Field({ defaultValue: '' })
  @IsString()
  other: string;

  @Field()
  @IsEnum(ECAttribute)
  attribute: ECAttribute;

  @Field({ defaultValue: '' })
  @IsString()
  noteattribute: string;

  @Field()
  @IsNumber()
  quantityofsick: number;

  @Field()
  guide: string;

  @Field()
  summary: string;

  @Field()
  @IsBoolean()
  contacdescription: boolean;

  @Field()
  @IsBoolean()
  reexamination: boolean;

  @Field({ defaultValue: new Date() })
  @IsDate()
  dateofreexamination: Date;

  @Field({
    description: 'Thời gian thực tạo bệnh án',
    deprecationReason: 'Kết luận',
    defaultValue: new Date(),
  })
  @IsDate()
  displaydate: Date;
}
