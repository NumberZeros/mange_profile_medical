import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm/repository/Repository';

import { IBaseServices } from '../../../containers/services.interface';
import { PatientEntity } from 'src/entities/patient.entity';
import { CreatePatientInput } from './dto/post.input';
import { CategoryEntity } from 'src/entities/categories.entity';
import { PutPatientInput } from './dto/put.input';
import { DoctorEntity } from 'src/entities/doctor.entity';
import { PathogenicEntity } from 'src/entities/pathogenic.entity';
import { ERiskAssesment, EStatus } from 'src/entities/medical.entity';
import {
  PaginationPatient,
  PaginationPatientInput,
} from './dto/pagination.input';
import * as _ from 'lodash';
import { Equal, ILike, IsNull } from 'typeorm';
import * as moment from 'moment';
import { VerifiedPatientInput } from './dto/verified.input';
import { IntegrationPatientInput } from './dto/integration.input';
import { PatientStatusEntity } from 'src/entities/patient_status.entity';
import { UserEntity } from 'src/entities/users.entity';
import { BreathEntity } from 'src/entities/breath.entity';
import { SearchGroupPatientInput } from './dto/search_group_patient.input';

@Injectable()
export class PatientServices implements IBaseServices {
  constructor(
    @InjectRepository(PatientEntity)
    private readonly patientEntity: Repository<PatientEntity>,
  ) {}
  private DATE_FORMATE = 'DD/MM/YYYY';

  async findAll(): Promise<PatientEntity[]> {
    try {
      return await this.patientEntity
        .createQueryBuilder('patient')
        .leftJoinAndSelect('patient.medicalprofile', 'medical_entity')
        .leftJoinAndSelect('patient.doctor', 'doctor_entity')
        .leftJoinAndMapOne(
          'doctor_entity.user',
          UserEntity,
          'user',
          'doctor_entity.user = user.id',
        )
        .where({ inactive: false, isverified: true })
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findAllUseDoctor(id: string): Promise<PatientEntity[]> {
    try {
      const data = await this.patientEntity.find({
        relations: ['medicalprofile', 'status'],
        where: {
          doctor: {
            id,
          },
          inactive: false,
          isverified: true,
        },
        order: { updatedat: 'DESC' },
      });
      return data;
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findAllNullField(fields: string[]): Promise<PatientEntity[]> {
    try {
      const query = [];
      for (const field of fields) {
        query.push({
          [field]: IsNull(),
        });
      }
      return await this.patientEntity.find({
        where: [...query],
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findAllUsePagination(
    pagination: PaginationPatientInput,
    doctorid?: string,
  ): Promise<PaginationPatient> {
    try {
      const query = { inactive: false, isverified: true };
      let queryForDoctor = '';
      const dataQueryForDoctor = [];
      for (const key in pagination) {
        if (
          key === 'take' ||
          key === 'skip' ||
          key === 'adminid' ||
          key === 'doctorid' ||
          key === 'gender' ||
          key === 'dob' ||
          key === 'updatedat' ||
          _.isEmpty(pagination[key])
        )
          continue;
        if (typeof pagination[key] === 'number')
          query[key] = Equal(pagination[key]);
        if (pagination[key] !== '' && !_.isEmpty(pagination[key])) {
          queryForDoctor += ` and patient.${key} ilike :${key}`;
          query[key] = `%${pagination[key].trim().toLowerCase()}%`;
        }
      }

      const [data, total] = doctorid
        ? await this.patientEntity
            .createQueryBuilder('patient')
            .leftJoinAndMapOne(
              'patient.status',
              PatientStatusEntity,
              'status',
              'patient.status = status.id',
            )
            .where(
              `
              patient.inactive = :inactive and patient.isverified = :isverified
              ${queryForDoctor !== '' ? queryForDoctor : ''}
              and(patient.doctor =:doctor or patient.sharedoctor = :sharedoctor)`,
              {
                inactive: false,
                isverified: true,
                doctor: doctorid,
                sharedoctor: doctorid,
                ...query,
              },
            )
            .limit(pagination.take)
            .offset(pagination.skip * pagination.take)
            .orderBy('patient.updatedat', 'DESC')
            .getManyAndCount()
        : await this.patientEntity
            .createQueryBuilder('patient')
            .leftJoinAndMapOne(
              'patient.status',
              PatientStatusEntity,
              'status',
              'patient.status = status.id',
            )
            .where(
              `
              patient.inactive = :inactive and patient.isverified = :isverified
              ${queryForDoctor !== '' ? queryForDoctor : ''}`,
              {
                inactive: false,
                isverified: true,
                ...query,
              },
            )
            .limit(pagination.take)
            .offset(pagination.skip * pagination.take)
            .orderBy('patient.updatedat', 'DESC')
            .getManyAndCount();
      return {
        patients: data,
        take: pagination.take,
        skip: pagination.skip,
        total,
        nextPage: pagination.take * pagination.skip >= total ? false : true,
      };
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<PatientEntity> {
    try {
      return await this.patientEntity.findOne({
        id,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async post(
    data: CreatePatientInput,
    props: {
      doctor: DoctorEntity;
      pathogenic: PathogenicEntity[];
      category: CategoryEntity;
    },
  ): Promise<PatientEntity> {
    try {
      const dataPatient = await this.patientEntity.save({
        ...data,
        doctor: props.doctor,
        pathogenic: props.pathogenic,
        category: props.category,
      });
      return dataPatient;
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async put(
    data: PutPatientInput,
    props: {
      doctor: DoctorEntity;
      pathogenic: PathogenicEntity[];
      category: CategoryEntity;
    },
  ): Promise<PatientEntity> {
    try {
      const patient = Object.assign(new PatientEntity(), {
        ...data,
        ...props,
      });
      return await this.patientEntity.save(patient);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async delete(data: PatientEntity): Promise<PatientEntity> {
    try {
      return await this.patientEntity.save(data);
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }

  async integrateCSV(
    data: IntegrationPatientInput,
    props: {
      externalid: string;
      isverified?: boolean;
      doctor: DoctorEntity;
      pathogenic: PathogenicEntity[];
      category: CategoryEntity;
      status: PatientStatusEntity;
      sharedoctor: DoctorEntity;
    },
  ): Promise<PatientEntity> {
    try {
      const patient = Object.assign(new PatientEntity(), {
        ...data,
        phoneemerency1: '',
        phoneemerency2: '',
        phoneemerency3: '',
        externalid: props.externalid,
        isverified: props.isverified,
        doctor: props.doctor,
        pathogenic: props.pathogenic,
        category: props.category,
      });
      return await this.patientEntity.save(patient);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async verified(
    data: VerifiedPatientInput,
    props: PatientEntity,
  ): Promise<PatientEntity> {
    try {
      return await this.patientEntity.save({
        ...props,
        isverified: data.isverified,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findPhone(phone: string): Promise<PatientEntity> {
    try {
      return await this.patientEntity
        .createQueryBuilder()
        .where({
          phone,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findUniqPatient(data: {
    firstname: string;
    lastname: string;
    dob: Date;
    phone: string;
  }): Promise<PatientEntity> {
    try {
      return await this.patientEntity.createQueryBuilder().where(data).getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async putV2(data: PatientEntity): Promise<PatientEntity> {
    try {
      return await this.patientEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOneV2(id: string): Promise<PatientEntity> {
    try {
      return await this.patientEntity
        .createQueryBuilder('patient')
        .leftJoinAndSelect('patient.pathogenic', 'pathogenic_entity')
        .leftJoinAndSelect('patient.category', 'category_entity')
        .leftJoinAndSelect('patient.doctor', 'doctor_entity')
        .leftJoinAndSelect('patient.group', 'group_entity')
        .leftJoinAndMapOne(
          'doctor_entity.user',
          UserEntity,
          'user',
          'doctor_entity.user = user.id',
        )
        .leftJoinAndSelect('patient.sharedoctor', 'share_doctor')
        .leftJoinAndMapOne(
          'share_doctor.user',
          UserEntity,
          'share_doctor_user',
          'share_doctor.user = share_doctor_user.id',
        )
        .leftJoinAndMapOne(
          'patient.status',
          PatientStatusEntity,
          'status',
          'patient.status = status.id',
        )
        .where({
          id,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  /**
   *  Report feature
   */
  async countPaitent(doctorid: any): Promise<number> {
    try {
      return await this.patientEntity.count(
        doctorid
          ? {
              isverified: true,
              inactive: false,
              doctor: { id: doctorid },
            }
          : {
              isverified: true,
              inactive: false,
            },
      );
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async calculationPatient(
    startWeek: Date,
    endWeek: Date,
    doctorid: any,
  ): Promise<any> {
    try {
      const totalPatient = await this.patientEntity
        .createQueryBuilder('patient')
        .where(
          doctorid
            ? {
                isverified: true,
                inactive: false,
                doctor: {
                  id: doctorid,
                },
              }
            : {
                isverified: true,
                inactive: false,
              },
        )
        .getCount();
      const totalPatientNow = await this.patientEntity
        .createQueryBuilder('patient')
        .where(
          doctorid
            ? {
                isverified: true,
                inactive: false,
                doctor: {
                  id: doctorid,
                },
              }
            : {
                isverified: true,
                inactive: false,
              },
        )
        .andWhere('patient.updatedat > :from', {
          from: startWeek,
        })
        .andWhere('patient.updatedat < :to', {
          to: endWeek,
        })
        .getCount();

      const totalPatientRiskAssessment = await this.patientEntity
        .createQueryBuilder('patient')
        .leftJoinAndSelect('patient.medicalprofile', 'medical_entity')
        .where(
          doctorid
            ? {
                isverified: true,
                inactive: false,
                doctor: {
                  id: doctorid,
                },
              }
            : {
                isverified: true,
                inactive: false,
              },
        )
        .andWhere({
          'medicalprofile.inactive': false,
          'medicalprofile.isverified': true,
          'medicalprofile.riskassessment': ERiskAssesment.hightest,
        })
        .andWhere('patient.updatedat > :from', {
          from: startWeek,
        })
        .andWhere('patient.updatedat < :to', {
          to: endWeek,
        })
        .getCount();

      return { totalPatientNow, totalPatient, totalPatientRiskAssessment };
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async getDataTableNegativeAndContact(query: {
    startWeek: Date;
    endWeek: Date;
    listDateOfWeek: string[];
    doctorid: string;
  }): Promise<any> {
    try {
      const dataPatient = await this.patientEntity
        .createQueryBuilder('patient')
        .leftJoinAndSelect('patient.medicalprofile', 'medical_entity')
        .leftJoinAndSelect('medical_entity.medicaltrack', 'medical_track')
        .where(
          query.doctorid
            ? {
                isverified: true,
                inactive: false,
                doctor: {
                  id: query.doctorid,
                },
              }
            : {
                isverified: true,
                inactive: false,
              },
        )
        .andWhere('patient.updatedat >= :from', {
          from: query.startWeek,
        })
        .andWhere('patient.updatedat <= :to', {
          to: query.endWeek,
        })
        .getMany();
      /* ------------------------- Số lượng bệnh nhân cần hỗ trợ hiện trường trong tuần ------------------------- */
      const dataMedicalContacDescription = dataPatient.filter(
        (patient) =>
          patient.medicalprofile.length > 0 &&
          patient.medicalprofile.findIndex((o) => o.contacdescription) > -1,
      );
      const dataTablePatientContacDescription = query.listDateOfWeek.map(
        (o) => ({
          date: o,
          data: 0,
        }),
      );
      for (const patient of dataMedicalContacDescription) {
        for (const medical of patient.medicalprofile) {
          const idx = dataTablePatientContacDescription.findIndex(
            (o) =>
              o.date ===
              moment(medical.updatedat)
                .utcOffset('+07:00')
                .format(this.DATE_FORMATE),
          );
          if (idx > -1) dataTablePatientContacDescription[idx].data += 1;
        }
      }
      /* ----------------------------------- end ---------------------------------- */
      /* ------------------------- Số lượng bệnh nhân có kết quả âm tính trong tuần ------------------------- */
      const dataPatientSearchNegative = dataPatient.filter(
        (patient) => patient.medicalprofile.length > 0,
      );
      const dataPatientNegative = query.listDateOfWeek.map((o) => ({
        date: o,
        data: 0,
      }));
      for (const patient of dataPatientSearchNegative) {
        for (const medical of patient.medicalprofile) {
          const dataMedicalTrack = medical.medicaltrack.filter(
            (o) => o.medicalresutl === EStatus.negative,
          );
          for (const track of dataMedicalTrack) {
            const idx = dataPatientNegative.findIndex(
              (o) =>
                o.date ===
                moment(track.prcrealdate)
                  .utcOffset('+07:00')
                  .format(this.DATE_FORMATE),
            );
            if (idx > -1) dataPatientNegative[idx].data += 1;
          }
        }
      }
      /* ----------------------------------- end ---------------------------------- */
      /* ----------------------------------- Số bệnh nhân thêm mới trên toàn hệ thống trong tuần ----------------------------------- */
      const dataTablePatientCreated = query.listDateOfWeek.map((o) => ({
        date: o,
        data: 0,
      }));
      for (const patient of dataPatient) {
        const idx = dataTablePatientCreated.findIndex(
          (o) =>
            o.date ===
            moment(patient.updatedat)
              .utcOffset('+07:00')
              .format(this.DATE_FORMATE),
        );
        if (idx > -1) dataTablePatientCreated[idx].data += 1;
      }
      /* ----------------------------------- end ---------------------------------- */

      return {
        dataPatientNegative: dataPatientNegative,
        dataTablePatientContacDescription,
        dataTablePatientCreated,
      };
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async countPaitentRiskAssessment(doctorid: string): Promise<any> {
    try {
      const data = await this.patientEntity.find({
        inactive: false,
        isverified: true,
        doctor: {
          id: doctorid,
        },
      });
      const listRiskAssessment = [];
      for (const patient of data) {
        const medical = patient.medicalprofile.filter(
          (medical) =>
            medical.riskassessment === ERiskAssesment.hightest ||
            medical.riskassessment === ERiskAssesment.high,
        );
        listRiskAssessment.push(...medical);
      }
      // console.log('risk', listRiskAssessment);
      return listRiskAssessment.length;
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async reportAllPatient(query: {
    from: Date;
    to: Date;
  }): Promise<PatientEntity[]> {
    try {
      console.log(query);
      return await this.patientEntity
        .createQueryBuilder('patient')
        .leftJoinAndSelect('patient.medicalprofile', 'medical_entity')
        .leftJoinAndSelect('patient.doctor', 'doctor_entity')
        .leftJoinAndMapOne(
          'doctor_entity.user',
          UserEntity,
          'user',
          'doctor_entity.user = user.id',
        )
        .leftJoinAndMapOne(
          'patient.status',
          PatientStatusEntity,
          'status',
          'patient.status = status.id',
        )
        .where({ inactive: false, isverified: true })
        .andWhere('patient.updatedat > :from', {
          from: query.from,
        })
        .andWhere('patient.updatedat < :to', {
          to: query.to,
        })
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async reportEmegencyAllPatient(query: {
    from: Date;
    to: Date;
  }): Promise<PatientEntity[]> {
    try {
      return await this.patientEntity
        .createQueryBuilder('patient')
        .leftJoinAndSelect('patient.medicalemergency', 'emergency_entity')
        .leftJoinAndSelect('patient.doctor', 'doctor_entity')
        .leftJoinAndSelect('emergency_entity.clinical', 'clinical_entity')
        .leftJoinAndMapOne(
          'doctor_entity.user',
          UserEntity,
          'user',
          'doctor_entity.user = user.id',
        )
        .leftJoinAndMapOne(
          'patient.status',
          PatientStatusEntity,
          'status',
          'patient.status = status.id',
        )
        .leftJoinAndMapOne(
          'emergency_entity.methodbreath',
          BreathEntity,
          'breath_entity',
          'emergency_entity.methodbreath = breath_entity.id',
        )
        .where({ inactive: false, isverified: true })
        .andWhere('patient.updatedat > :from', {
          from: query.from,
        })
        .andWhere('patient.updatedat < :to', {
          to: query.to,
        })
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findGroupDangerousPatient(
    pagination: SearchGroupPatientInput,
    doctorid?: string,
  ): Promise<PaginationPatient> {
    try {
      console.log(pagination, doctorid);
      const [data, total] = await this.patientEntity
        .createQueryBuilder('patient')
        .leftJoinAndSelect('patient.status', 'patient_status_entity')
        .leftJoinAndSelect('patient.doctor', 'doctor_entity')
        .leftJoinAndSelect('doctor_entity.user', 'user_entity')
        .where(
          `
          patient.inactive = :inactive and patient.isverified = :isverified
          and patient_status_entity.name = :name 
          ${doctorid ? `and doctor_entity.id = :id` : ''}
          and (patient.firstdatequicktest >= :fromfirstdatequicktest and patient.firstdatequicktest <= :tofirstdatequicktest
          or patient.firstdatecovid >= :fromfirstdatecovid and patient.firstdatecovid <= :tofirstdatecovid
          or patient.pathogenicdate >= :frompathogenicdate and patient.pathogenicdate <= :topathogenicdate)`,
          doctorid
            ? {
                inactive: false,
                isverified: true,
                name: '',
                id: doctorid,
                fromfirstdatequicktest: pagination.from,
                tofirstdatequicktest: pagination.to,
                fromfirstdatecovid: pagination.from,
                tofirstdatecovid: pagination.to,
                frompathogenicdate: pagination.from,
                topathogenicdate: pagination.to,
              }
            : {
                inactive: false,
                isverified: true,
                name: '',
                fromfirstdatequicktest: pagination.from,
                tofirstdatequicktest: pagination.to,
                fromfirstdatecovid: pagination.from,
                tofirstdatecovid: pagination.to,
                frompathogenicdate: pagination.from,
                topathogenicdate: pagination.to,
              },
        )
        .limit(pagination.take)
        .offset(pagination.skip * pagination.take)
        .getManyAndCount();
      return {
        patients: data,
        take: pagination.take,
        skip: pagination.skip,
        total,
        nextPage: pagination.take * pagination.skip >= total ? false : true,
      };
    } catch (error) {
      console.error(error);
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async reportGroupDangerousPatient(
    pagination: { from: Date; to: Date },
    doctorid?: string,
  ): Promise<number> {
    try {
      return await this.patientEntity
        .createQueryBuilder('patient')
        .leftJoinAndSelect('patient.status', 'patient_status_entity')
        .leftJoinAndSelect('patient.doctor', 'doctor_entity')
        .leftJoinAndSelect('doctor_entity.user', 'user_entity')
        .where(
          `
          patient.inactive = :inactive and patient.isverified = :isverified
          and patient_status_entity.name = :name 
          ${doctorid ? `and doctor_entity.id = :id` : ''}
          and (patient.firstdatequicktest >= :fromfirstdatequicktest and patient.firstdatequicktest <= :tofirstdatequicktest
          or patient.firstdatecovid >= :fromfirstdatecovid and patient.firstdatecovid <= :tofirstdatecovid
          or patient.pathogenicdate >= :frompathogenicdate and patient.pathogenicdate <= :topathogenicdate)`,
          doctorid
            ? {
                inactive: false,
                isverified: true,
                name: '',
                id: doctorid,
                fromfirstdatequicktest: pagination.from,
                tofirstdatequicktest: pagination.to,
                fromfirstdatecovid: pagination.from,
                tofirstdatecovid: pagination.to,
                frompathogenicdate: pagination.from,
                topathogenicdate: pagination.to,
              }
            : {
                inactive: false,
                isverified: true,
                name: '',
                fromfirstdatequicktest: pagination.from,
                tofirstdatequicktest: pagination.to,
                fromfirstdatecovid: pagination.from,
                tofirstdatecovid: pagination.to,
                frompathogenicdate: pagination.from,
                topathogenicdate: pagination.to,
              },
        )
        .getCount();
    } catch (error) {
      console.error(error);
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
