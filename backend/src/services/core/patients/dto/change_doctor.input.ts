import { Field, InputType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
@InputType()
export class ChangeDoctorPatientInput {
  @Field()
  @IsString()
  ownerid: string;

  @Field()
  @IsString()
  id: string;

  @Field({
    description: 'Doctor team 2',
    deprecationReason: 'Tổng quan',
  })
  doctor: string;
}
