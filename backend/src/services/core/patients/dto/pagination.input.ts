import { Field, InputType, ObjectType } from '@nestjs/graphql';
import { IsEnum, IsNumber, IsString } from 'class-validator';
import { PatientEntity } from 'src/entities/patient.entity';

enum ESort {
  desc = 'DESC',
  asc = 'ASC',
  non = '',
}

@InputType()
export class PaginationPatientInput {
  @Field({ defaultValue: 10 })
  @IsNumber()
  take: number;

  @Field({ defaultValue: 0 })
  @IsNumber()
  skip: number;

  @Field({ description: 'Áp dụng cho admin', defaultValue: '' })
  @IsString()
  adminid?: string;

  @Field({ description: 'Áp dụng cho doctor', defaultValue: '' })
  @IsString()
  doctorid?: string;

  @Field({ defaultValue: '' })
  @IsString()
  firstname: string;

  @Field({ defaultValue: '' })
  @IsString()
  lastname: string;

  @Field({ defaultValue: '' })
  @IsString()
  phone: string;

  @Field({ defaultValue: '' })
  @IsString()
  doctorname: string;

  @Field({ defaultValue: '' })
  @IsString()
  otherid: string;

  @Field({
    defaultValue: ESort.non,
    description: `
  Value is ${ESort.desc}, ${ESort.asc}`,
  })
  @IsEnum(ESort)
  gender: ESort;

  @Field({ defaultValue: null })
  quantityfamily: number;

  @Field({
    defaultValue: ESort.non,
    description: `
  Value is ${ESort.desc}, ${ESort.asc}`,
  })
  @IsEnum(ESort)
  dob: ESort;

  @Field({
    defaultValue: ESort.non,
    description: `
  Value is ${ESort.desc}, ${ESort.asc}`,
  })
  @IsEnum(ESort)
  firstdatecovid: ESort;

  @Field({
    defaultValue: ESort.desc,
    description: `
  Value is ${ESort.desc}, ${ESort.asc}`,
  })
  @IsEnum(ESort)
  updatedat: ESort;
}

@ObjectType()
export class PaginationPatient {
  @Field((of) => [PatientEntity])
  patients: PatientEntity[];

  @Field()
  take: number;

  @Field()
  skip: number;

  @Field()
  total: number;

  @Field()
  nextPage: boolean;
}
