import { Field, InputType } from '@nestjs/graphql';
import { IsEnum, IsString, IsDate, IsNumber } from 'class-validator';
import { ETransfer } from 'src/entities/patient.entity';
import { EGender } from '../../../../entities/users.entity';
@InputType()
export class IntegrationPatientInput {
  @Field()
  @IsString()
  firstname: string;

  @Field({ defaultValue: '' })
  @IsString()
  lastname: string;

  @Field({ defaultValue: '' })
  @IsString()
  localcode: string;

  @Field()
  @IsString()
  phone: string;

  @Field({ defaultValue: new Date() })
  @IsDate()
  dob: Date;

  @Field({
    defaultValue: EGender.male,
    description: `
  Value is ${EGender.male} ${EGender.female}`,
  })
  @IsEnum(EGender)
  gender: EGender;

  @Field()
  @IsString()
  otherid: string;

  @Field()
  @IsNumber()
  quantityfamily: number;

  @Field()
  @IsDate()
  firstdatecovid: Date;

  @Field()
  @IsDate()
  pathogenicdate: Date;

  @Field()
  @IsString()
  pathogenicname: string;

  @Field({ defaultValue: '' })
  @IsString()
  otherreason: string;

  @Field({ defaultValue: '' })
  @IsString()
  beforesick: string;

  @Field({ defaultValue: '' })
  @IsString()
  notesatussick: string;

  @Field({
    description: 'Nơi chuyển viện',
    deprecationReason: 'Tổng quan',
  })
  @IsEnum(ETransfer)
  source: ETransfer;

  @Field({
    description: 'Số lần tiêm vác xin',
    deprecationReason: 'Tổng quan',
  })
  @IsNumber()
  quantittyvacin: number;

  @Field({ defaultValue: '' })
  @IsString()
  name: string;

  @Field({ defaultValue: '' })
  @IsString()
  ward: string;

  @Field({ defaultValue: '' })
  @IsString()
  province: string;

  @Field({ defaultValue: '' })
  @IsString()
  city: string;
}
