import { Field, InputType, ObjectType } from '@nestjs/graphql';
import { IsDate, IsNumber, IsString } from 'class-validator';
import { PatientEntity } from 'src/entities/patient.entity';

@InputType()
export class SearchGroupPatientInput {
  @Field({ defaultValue: 10 })
  @IsNumber()
  take: number;

  @Field({ defaultValue: 0 })
  @IsNumber()
  skip: number;

  @Field({ description: 'Áp dụng cho admin', defaultValue: '' })
  @IsString()
  adminid?: string;

  @Field({ description: 'Áp dụng cho doctor', defaultValue: '' })
  @IsString()
  doctorid?: string;

  @Field({ defaultValue: new Date() })
  @IsDate()
  from: Date;

  @Field({ defaultValue: new Date() })
  @IsDate()
  to: Date;
}

@ObjectType()
export class PaginationPatient {
  @Field((of) => [PatientEntity])
  patients: PatientEntity[];

  @Field()
  take: number;

  @Field()
  skip: number;

  @Field()
  total: number;

  @Field()
  nextPage: boolean;
}
