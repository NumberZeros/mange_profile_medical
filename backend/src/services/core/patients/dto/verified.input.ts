import { Field, InputType } from '@nestjs/graphql';
import { IsString, IsBoolean } from 'class-validator';
@InputType()
export class VerifiedPatientInput {
  @Field({ defaultValue: true })
  @IsBoolean()
  isverified: boolean;

  @Field()
  @IsString()
  id: string;
}
