import { Field, InputType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
@InputType()
export class ChangeStatusPatientInput {
  @Field()
  @IsString()
  ownerid: string;

  @Field()
  @IsString()
  id: string;

  @Field({
    description: 'ID trạng thái hồ sơ',
    deprecationReason: 'Tổng quan',
  })
  status: string;

  @Field({
    description: 'Trạng thái hồ sơ khi chọn khác',
    deprecationReason: 'Tổng quan',
    defaultValue: '',
  })
  @IsString()
  otherstatus: string;
}
