import { Field, InputType } from '@nestjs/graphql';
import {
  IsEnum,
  IsString,
  IsDate,
  IsNumber,
  IsBoolean,
  IsArray,
} from 'class-validator';
import { ETransfer } from 'src/entities/patient.entity';
import { DEFAULT_DATE } from 'src/services/packages';
import { EGender } from '../../../../entities/users.entity';
@InputType()
export class CreatePatientInput {
  @Field({ defaultValue: true })
  @IsBoolean()
  isverified: boolean;

  @Field()
  @IsString()
  doctorid: string;

  @Field()
  @IsString()
  firstname: string;

  @Field({ defaultValue: '' })
  @IsString()
  lastname: string;

  @Field()
  @IsString()
  phone: string;

  @Field({ defaultValue: new Date() })
  @IsDate()
  dob: Date;

  @Field({
    defaultValue: EGender.male,
    description: `
  Value is ${EGender.male} ${EGender.female}`,
  })
  @IsEnum(EGender)
  gender: EGender;

  @Field({ description: 'Khám sàn lọc' })
  @IsString()
  category: string;

  @Field()
  @IsString()
  otherid: string;

  @Field({ description: 'Phone farmily 1', defaultValue: '' })
  phoneemerency1: string;

  @Field({ description: 'Phone farmily 2', defaultValue: '' })
  phoneemerency2: string;

  @Field({ description: 'Phone farmily 3', defaultValue: '' })
  phoneemerency3: string;

  @Field()
  @IsNumber()
  quantityfamily: number;

  @Field()
  @IsDate()
  firstdatecovid: Date;

  @Field((type) => [String])
  @IsArray()
  pathogenicid: string[];

  @Field({ defaultValue: '' })
  @IsString()
  otherreason: string;

  @Field({ defaultValue: '' })
  @IsString()
  beforesick: string;

  @Field({ defaultValue: '' })
  @IsString()
  notesatussick: string;

  @Field()
  @IsDate()
  pathogenicdate: Date;

  @Field()
  @IsString()
  pathogenicname: string;

  @Field({ defaultValue: '' })
  pathogenicnameother: string;

  @Field({ defaultValue: '' })
  @IsString()
  localcode: string;

  @Field({
    description: 'Nơi chuyển viện',
    deprecationReason: 'Tổng quan',
  })
  @IsEnum(ETransfer)
  source: ETransfer;

  @Field({
    description: 'Số lần tiêm vác xin',
    deprecationReason: 'Tổng quan',
  })
  @IsNumber()
  quantittyvacin: number;

  @Field({ defaultValue: '' })
  @IsString()
  name: string;

  @Field({ defaultValue: '' })
  @IsString()
  ward: string;

  @Field({ defaultValue: '' })
  @IsString()
  province: string;

  @Field({ defaultValue: '' })
  @IsString()
  city: string;

  @Field({
    description: 'Ngày có kết quả test nhanh dương tính đầu tiên',
    defaultValue: DEFAULT_DATE,
  })
  @IsDate()
  firstdatequicktest: Date;
}
