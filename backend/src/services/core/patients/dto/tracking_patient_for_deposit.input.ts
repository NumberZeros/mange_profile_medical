import { Field, InputType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
@InputType()
export class TrackingPatientForDepositDoctorInput {
  @Field()
  @IsString()
  patientid: string;

  @Field()
  @IsString()
  type: string;

  @Field({
    description: 'Doctor team 2',
  })
  doctorid: string;

  @Field()
  @IsString()
  medicalid: string;
}
