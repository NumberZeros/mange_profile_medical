import { Field, InputType } from '@nestjs/graphql';
import { IsEnum, IsString, IsDate, IsNumber, IsBoolean } from 'class-validator';
import { EIsContact, EReason } from 'src/entities/emergency.entity';
import { EMedicalPackage } from 'src/entities/medical.entity';
import { DEFAULT_DATE } from 'src/services/packages';
@InputType()
export class CreateEmergencyInput {
  @Field()
  @IsString()
  ownerid: string;

  @Field()
  @IsString()
  patientid: string;

  @Field()
  @IsString()
  doctorid: string;

  @Field({ defaultValue: true })
  @IsBoolean()
  isverified: boolean;

  @Field({
    description: 'Ghi chú về bệnh lý nền',
    deprecationReason: 'Hành chính',
    defaultValue: '',
  })
  @IsString()
  notebackgroup: string;

  @Field({
    description: `Có tiếp xúc với người mắc covid: ${EIsContact.yes}, ${EIsContact.no}, ${EIsContact.abstruse}, ${EIsContact.empty},`,
    deprecationReason: 'Hành chính',
    defaultValue: EIsContact.empty,
  })
  @IsEnum(EIsContact)
  iscontact: EIsContact;

  @Field((type) => [String], {
    description: 'Có biểu hiện lâm sàn',
    deprecationReason: 'Khám - Đánh giá',
  })
  listclinical: string[];

  @Field({
    description: 'Ghi chú (Biểu hiện lâm sàn)',
    deprecationReason: 'Khám - Đánh giá',
    defaultValue: '',
  })
  @IsString()
  listclinicalnote: string;

  @Field({
    description: 'Cảm thấy đâu hoặc tức ngực nhiều không giảm',
    deprecationReason: 'Khám - Đánh giá',
    defaultValue: '',
  })
  @IsString()
  note: string;

  @Field({
    description: 'Tri giác',
    deprecationReason: 'Khám - Đánh giá',
    defaultValue: '',
  })
  @IsString()
  sense: string;

  @Field({
    description: 'Mạch',
    deprecationReason: 'Khám đánh giá',
    defaultValue: 0,
  })
  @IsNumber()
  pluse: number;

  @Field({
    description: 'Huyết áp (Tâm thu)',
    deprecationReason: 'Khám đánh giá',
    defaultValue: 0,
  })
  @IsNumber()
  bloodpressure: number;

  @Field({
    description: 'Huyết áp(Tâm trương)',
    deprecationReason: 'Khám đánh giá',
    defaultValue: 0,
  })
  @IsNumber()
  bloodpressure2: number;

  @Field({
    description: 'SpO2 tại nhà',
    deprecationReason: 'Khám đánh giá',
    defaultValue: 0,
  })
  @IsNumber()
  spo2home: number;

  @Field({
    description: 'Nhiệt độ',
    deprecationReason: 'Khám đánh giá',
    defaultValue: 0,
  })
  @IsNumber()
  temperature: number;

  @Field({
    description: 'Khác',
    deprecationReason: 'Khám đánh giá',
    defaultValue: '',
  })
  @IsString()
  other: string;

  @Field({
    description: `Xác định dương tính với Covid-19: ${EMedicalPackage.quicktest}, ${EMedicalPackage.prc}, ${EMedicalPackage.other}, `,
    deprecationReason: 'Khám và đánh giá',
    defaultValue: EMedicalPackage.no,
  })
  @IsEnum(EMedicalPackage)
  medicalpackage: EMedicalPackage;

  @Field({
    description: 'Ghi chú (Xác định dương tính với Covid-19)',
    deprecationReason: 'Khám đánh giá',
  })
  @IsString()
  medicalpackagenote: string;

  @Field({
    description: 'Ngày xét nghiệp dương tính gần nhất',
    deprecationReason: 'Khám đánh giá',
    defaultValue: DEFAULT_DATE,
  })
  @IsDate()
  resultdate: Date;

  @Field({
    description: 'Phương thức thử tại hiện trường',
    deprecationReason: 'Xữ trí',
  })
  @IsString()
  methodbreathid: string;

  @Field({
    description: 'Lưu lượng O2 (L/phút)',
    deprecationReason: 'Xữ trí',
    defaultValue: 0,
  })
  @IsNumber()
  po2: number;

  @Field({
    description: 'Cập nhật trạng thái bệnh nhân',
    deprecationReason: 'Kết luận bệnh nhân',
  })
  statusid: string;

  @Field({
    description: 'Ghi chú kết luận bệnh nhân',
    deprecationReason: 'Kết luận bệnh nhân',
    defaultValue: '',
  })
  @IsString()
  notestatus: string;

  @Field({
    description: 'Nơi chuyển đến',
    deprecationReason: 'Kết luận bệnh nhân',
    defaultValue: '',
  })
  @IsString()
  transferfrom: string;

  @Field({
    description: `Kết luận bệnh nhân: ${EReason.treated}, ${EReason.gohome}, ${EReason.die}, ${EReason.notinformation},`,
    deprecationReason: 'Khám và đánh giá',
    defaultValue: EReason.no,
  })
  @IsEnum(EReason)
  reason: EReason;

  @Field({
    description: 'Ngày ra kết luận',
    deprecationReason: 'Kết luận bệnh nhân',
    defaultValue: '',
  })
  @IsString()
  reasondate: string;

  @Field({
    description: 'Thời gian hiển thị trên time line',
    defaultValue: new Date(),
  })
  @IsDate()
  displaydate: Date;
}
