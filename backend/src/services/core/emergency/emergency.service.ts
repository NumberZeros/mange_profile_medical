import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BreathEntity } from 'src/entities/breath.entity';
import { ClinicalEntity } from 'src/entities/clinical.entity';
import { EmergencyStatusEntity } from 'src/entities/emegency_status.entity';
import { EmergencyEntity } from 'src/entities/emergency.entity';
import { PatientEntity } from 'src/entities/patient.entity';
import { Repository } from 'typeorm/repository/Repository';
import { IBaseServices } from '../../../containers/services.interface';

@Injectable()
export class EmergencyServices implements IBaseServices {
  constructor(
    @InjectRepository(EmergencyEntity)
    private readonly emerEntity: Repository<EmergencyEntity>,
  ) {}
  async findAll(patientid: string): Promise<EmergencyEntity[]> {
    try {
      return await this.emerEntity
        .createQueryBuilder('emergency')
        .leftJoinAndMapOne(
          'emergency.status',
          EmergencyStatusEntity,
          'status',
          'emergency.status = status.id',
        )
        .leftJoinAndMapOne(
          'emergency.methodbreath',
          BreathEntity,
          'breath',
          'emergency.methodbreath = breath.id',
        )
        .leftJoinAndSelect('emergency.clinical', 'clinical_entity')
        .where({ inactive: false, patient: patientid })
        .orderBy('displaydate', 'DESC')
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<EmergencyEntity> {
    try {
      return await this.emerEntity
        .createQueryBuilder()
        .where({
          id,
          inactive: false,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async post(data: EmergencyEntity): Promise<EmergencyEntity> {
    try {
      return await this.emerEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: EmergencyEntity): Promise<EmergencyEntity> {
    try {
      return await this.emerEntity.save(data);
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }
  async delete(data: EmergencyEntity): Promise<EmergencyEntity> {
    throw new HttpException('error', HttpStatus.NOT_MODIFIED);
  }

  async findOneV2(id: string): Promise<EmergencyEntity> {
    try {
      return await this.emerEntity
        .createQueryBuilder('emergency')
        .leftJoinAndMapOne(
          'emergency.status',
          EmergencyStatusEntity,
          'status',
          'emergency.status = status.id',
        )
        .leftJoinAndMapOne(
          'emergency.methodbreath',
          BreathEntity,
          'breath',
          'emergency.methodbreath = breath.id',
        )
        .leftJoinAndSelect('emergency.clinical', 'clinical_entity')
        .leftJoinAndSelect('emergency.medicines', 'medicine_entity')
        .where({ inactive: false, id })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
