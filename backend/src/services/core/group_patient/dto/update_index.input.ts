import { Field, InputType } from '@nestjs/graphql';
import { IsNumber, IsString } from 'class-validator';
@InputType()
export class UpdateIndexGroupPatientInput {
  @Field()
  @IsString()
  id: string;

  @Field()
  @IsString()
  doctorid: string;

  @Field()
  @IsNumber()
  index: number;
}
