import { Field, InputType } from '@nestjs/graphql';
import { IsBoolean, IsString } from 'class-validator';
@InputType()
export class HandleMemeberGroupPatientInput {
  @Field()
  @IsString()
  id: string;

  @Field()
  @IsString()
  doctorid: string;

  @Field((type) => [String])
  listPatient: string[];

  @Field()
  @IsBoolean()
  isRemove: boolean;
}
