import { Field, InputType } from '@nestjs/graphql';
import { IsNumber, IsString } from 'class-validator';
@InputType()
export class PutGroupPatientInput {
  @Field()
  @IsString()
  id: string;

  @Field()
  @IsString()
  doctorid: string;

  @Field()
  @IsString()
  name: string;

  @Field({ defaultValue: '' })
  @IsString()
  description: string;

  @Field()
  @IsNumber()
  priority: number;
}
