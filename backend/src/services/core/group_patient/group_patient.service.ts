import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GroupPatientEntity } from 'src/entities/group_patient.entity';
import { Repository } from 'typeorm/repository/Repository';
import { IBaseServices } from '../../../containers/services.interface';

@Injectable()
export class GroupPatientServices implements IBaseServices {
  constructor(
    @InjectRepository(GroupPatientEntity)
    private readonly groupPatientEntity: Repository<GroupPatientEntity>,
  ) {}
  async findAll(doctorId: string): Promise<GroupPatientEntity[]> {
    try {
      return await this.groupPatientEntity
        .createQueryBuilder('group')
        .leftJoinAndSelect('group.doctor', 'doctor_entity')
        .leftJoinAndSelect('group.patient', 'patient_entity')
        .leftJoinAndSelect('doctor_entity.user', 'user_entity')
        .where(
          `group.inactive = :inactive and group.doctor = :doctor
        `,
          { inactive: false, doctor: doctorId },
        )
        .orderBy('group.index', 'DESC')
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<GroupPatientEntity> {
    try {
      return await this.groupPatientEntity
        .createQueryBuilder('group')
        .leftJoinAndSelect('group.doctor', 'doctor_entity')
        .leftJoinAndSelect('group.patient', 'patient_entity')
        .leftJoinAndSelect('doctor_entity.user', 'user_entity')
        .where(
          `group.inactive = :inactive and group.id = :id
        `,
          { inactive: false, id },
        )
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async post(data: GroupPatientEntity): Promise<GroupPatientEntity> {
    try {
      return await this.groupPatientEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: GroupPatientEntity): Promise<GroupPatientEntity> {
    try {
      return await this.groupPatientEntity.save(data);
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }
  async delete(data: GroupPatientEntity): Promise<GroupPatientEntity> {
    throw new HttpException('error', HttpStatus.NOT_MODIFIED);
  }

  async findLatestedIndex(doctorid: string): Promise<GroupPatientEntity> {
    try {
      return await this.groupPatientEntity
        .createQueryBuilder('group')
        .leftJoinAndSelect('group.doctor', 'doctor_entity')
        .where(
          `group.inactive = :inactive and doctor_entity.id = :id
        `,
          { inactive: false, id: doctorid },
        )
        .orderBy('index', 'DESC')
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findAllBaseOnIndex(query: {
    doctorId: string;
    startIndex: number;
    endIndex: number;
  }): Promise<GroupPatientEntity[]> {
    try {
      return await this.groupPatientEntity
        .createQueryBuilder('group')
        .leftJoinAndSelect('group.doctor', 'doctor_entity')
        .where(
          `group.inactive = :inactive and group.doctor = :doctor and group.index >=:startIndex and group.index <= :endIndex
        `,
          {
            inactive: false,
            doctor: query.doctorId,
            startIndex: query.startIndex,
            endIndex: query.endIndex,
          },
        )
        // .orderBy('group.index', 'DESC')
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
