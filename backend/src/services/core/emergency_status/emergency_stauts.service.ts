import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EmergencyStatusEntity } from 'src/entities/emegency_status.entity';
import { Repository } from 'typeorm/repository/Repository';
import { IBaseServices } from '../../../containers/services.interface';

@Injectable()
export class EmergencyStatusServices implements IBaseServices {
  constructor(
    @InjectRepository(EmergencyStatusEntity)
    private readonly emerStatusEntity: Repository<EmergencyStatusEntity>,
  ) {}
  async findAll(): Promise<EmergencyStatusEntity[]> {
    try {
      return await this.emerStatusEntity.find({
        where: { inactive: false, isverified: true },
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<EmergencyStatusEntity> {
    try {
      return await this.emerStatusEntity
        .createQueryBuilder()
        .where({
          id,
          inactive: false,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async post(data: EmergencyStatusEntity): Promise<EmergencyStatusEntity> {
    try {
      return await this.emerStatusEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: EmergencyStatusEntity): Promise<EmergencyStatusEntity> {
    try {
      return await this.emerStatusEntity.save(data);
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }
  async delete(data: EmergencyStatusEntity): Promise<EmergencyStatusEntity> {
    throw new HttpException('error', HttpStatus.NOT_MODIFIED);
  }
  async findName(name: string): Promise<EmergencyStatusEntity> {
    try {
      return await this.emerStatusEntity.findOne({ name });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
