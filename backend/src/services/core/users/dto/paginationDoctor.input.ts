import { Field, InputType, ObjectType } from '@nestjs/graphql';
import { IsEnum, IsNumber, IsString } from 'class-validator';
import { DoctorEntity } from 'src/entities/doctor.entity';

enum ESort {
  desc = 'DESC',
  asc = 'ASC',
  non = '',
}

@InputType()
export class PaginationDoctorInput {
  @Field({ defaultValue: 10 })
  @IsNumber()
  take: number;

  @Field({ defaultValue: 0 })
  @IsNumber()
  skip: number;

  @Field({ description: 'Áp dụng cho admin', defaultValue: '' })
  @IsString()
  adminid?: string;

  @Field({ defaultValue: '' })
  @IsString()
  firstname: string;

  @Field({ defaultValue: '' })
  @IsString()
  lastname: string;

  @Field({ defaultValue: '' })
  @IsString()
  phone: string;

  @Field({
    defaultValue: ESort.non,
    description: `
  Value is ${ESort.desc}, ${ESort.asc}`,
  })
  @IsEnum(ESort)
  gender: ESort;

  @Field({
    defaultValue: ESort.non,
    description: `
  Value is ${ESort.desc}, ${ESort.asc}`,
  })
  @IsEnum(ESort)
  dob: ESort;

  @Field({
    defaultValue: ESort.desc,
    description: `
  Value is ${ESort.desc}, ${ESort.asc}`,
  })
  @IsEnum(ESort)
  updatedat: ESort;
}

@ObjectType()
export class PaginationDoctor {
  @Field((of) => [DoctorEntity])
  doctors: DoctorEntity[];

  @Field()
  take: number;

  @Field()
  skip: number;

  @Field()
  total: number;

  @Field()
  nextPage: boolean;
}
