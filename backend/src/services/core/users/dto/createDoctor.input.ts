import { Field, InputType } from '@nestjs/graphql';
import {
  IsEmail,
  IsEnum,
  IsPhoneNumber,
  IsString,
  IsDate,
  IsBoolean,
} from 'class-validator';
import { EGender } from '../../../../entities/users.entity';
@InputType()
export class CreateDoctorInput {
  @Field({ defaultValue: false })
  @IsBoolean()
  isverified: boolean;

  @Field()
  @IsString()
  adminid: string;

  @Field()
  @IsString()
  firstname: string;

  @Field({ defaultValue: '' })
  @IsString()
  lastname: string;

  @Field()
  @IsString()
  @IsEmail()
  email: string;

  @Field({ defaultValue: '' })
  @IsString()
  certificate: string;

  @Field()
  @IsString()
  @IsPhoneNumber()
  phone: string;

  @Field({ defaultValue: new Date() })
  @IsDate()
  dob: Date;

  @Field({
    defaultValue: EGender.male,
    description: `
  Value is ${EGender.male} ${EGender.female}`,
  })
  @IsEnum(EGender)
  gender: EGender;
}
