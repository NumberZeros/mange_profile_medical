import { Field, InputType } from '@nestjs/graphql';
import {
  IsEmail,
  IsEnum,
  IsPhoneNumber,
  IsString,
  IsDate,
  IsBoolean,
} from 'class-validator';
import { EGender } from '../../../../entities/users.entity';
@InputType()
export class UpdateDoctorInput {
  @Field({ defaultValue: false })
  @IsBoolean()
  isverified: boolean;

  @Field()
  @IsString()
  adminid: string;

  @Field()
  @IsString()
  id: string;

  @Field()
  @IsString()
  firstname: string;

  @Field()
  @IsString()
  lastname: string;

  @Field()
  @IsString()
  @IsEmail()
  email: string;

  @Field()
  @IsString()
  @IsPhoneNumber()
  phone: string;

  @Field()
  @IsDate()
  dob: Date;

  @Field({ defaultValue: '' })
  @IsString()
  certificate: string;

  @Field()
  @IsBoolean()
  inactive: boolean;

  @Field({
    defaultValue: EGender.male,
    description: `
  Value is ${EGender.male} ${EGender.female}`,
  })
  @IsEnum(EGender)
  gender: EGender;
}
