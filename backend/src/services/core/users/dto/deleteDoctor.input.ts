import { Field, InputType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
@InputType()
export class DeleteDoctorInput {
  @Field()
  @IsString()
  adminid: string;

  @Field()
  @IsString()
  id: string;
}
