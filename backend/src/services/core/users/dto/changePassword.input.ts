import { Field, InputType } from '@nestjs/graphql';
import {
  IsEmail,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
@InputType()
export class ChangePasswordUserInput {
  @Field()
  @IsString()
  id: string;

  @Field()
  @IsString()
  @IsEmail()
  email: string;

  @Field()
  @IsString()
  oldpassword: string;

  @Field()
  @IsString()
  @MinLength(8)
  @MaxLength(16)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message:
      'Valid password range [8-16]. You can use a combination of numbers, lowercase, uppercase and not special character for your password with at least two kinds of them contained',
  })
  password: string;
}
