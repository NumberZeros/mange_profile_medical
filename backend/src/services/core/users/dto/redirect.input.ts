import { Field, InputType, ObjectType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
import { SMSEntity } from 'src/entities/sms.entity';
@InputType()
export class RedirectInput {
  @Field()
  @IsString()
  authid: string;

  @Field()
  @IsString()
  token: string;
}

@ObjectType()
export class RedirectOutPut {
  @Field()
  access_token: string;

  @Field()
  sms: SMSEntity;
}
