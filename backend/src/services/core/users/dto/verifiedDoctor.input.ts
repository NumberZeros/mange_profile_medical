import { Field, InputType } from '@nestjs/graphql';
import { IsString, IsBoolean } from 'class-validator';
@InputType()
export class VerifiedDoctorInput {
  @Field({ defaultValue: false })
  @IsBoolean()
  isverified: boolean;

  @Field()
  @IsString()
  adminid: string;

  @Field()
  @IsString()
  id: string;
}
