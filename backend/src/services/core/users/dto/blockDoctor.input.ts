import { Field, InputType } from '@nestjs/graphql';
import { IsBoolean, IsString } from 'class-validator';
@InputType()
export class BlockDoctorInput {
  @Field()
  @IsString()
  adminid: string;

  @Field()
  @IsString()
  id: string;

  @Field()
  @IsBoolean()
  inactive: boolean;
}
