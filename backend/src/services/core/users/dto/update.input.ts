import { Field, InputType } from '@nestjs/graphql';
import {
  IsBoolean,
  IsDate,
  IsEmail,
  IsEnum,
  IsPhoneNumber,
  IsString,
} from 'class-validator';
import { RootObject } from '../../../../containers/objects.root';
import { EGender } from '../../../../entities/users.entity';
@InputType()
export class UpdateUserInput extends RootObject {
  @Field({ defaultValue: false })
  @IsBoolean()
  isverified: boolean;

  @Field()
  @IsString()
  id: string;

  @Field()
  @IsString()
  firstname: string;

  @Field()
  @IsString()
  lastname: string;

  @Field()
  @IsString()
  @IsEmail()
  email: string;

  @Field()
  @IsString()
  @IsPhoneNumber()
  phone: string;

  @Field()
  @IsDate()
  dob: Date;

  @Field({
    defaultValue: EGender.male,
    description: `
  Value is ${EGender.male} or ${EGender.female}`,
  })
  @IsEnum(EGender)
  gender: EGender;
}
