import { Field, InputType } from '@nestjs/graphql';
import {
  IsBoolean,
  IsDate,
  IsEmail,
  IsEnum,
  IsPhoneNumber,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { EGender, ERoles } from '../../../../entities/users.entity';
@InputType()
export class CreateUserInput {
  @Field({ defaultValue: true })
  @IsBoolean()
  isverified: boolean;

  @Field()
  @IsString()
  firstname: string;

  @Field({ defaultValue: '' })
  @IsString()
  lastname: string;

  @Field()
  @IsString()
  @IsEmail()
  email: string;

  @Field()
  @IsString()
  @MinLength(8)
  @MaxLength(16)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message:
      'Valid password range [8-16]. You can use a combination of numbers, lowercase, uppercase and not special character for your password with at least two kinds of them contained',
  })
  password: string;

  @Field()
  @IsString()
  @IsPhoneNumber()
  phone: string;

  @Field({ defaultValue: new Date() })
  @IsDate()
  dob: Date;

  @Field({
    defaultValue: EGender.male,
    description: `
  Value is ${EGender.male} ${EGender.female}`,
  })
  @IsEnum(EGender)
  gender: EGender;

  @Field({
    defaultValue: ERoles.patient,
    description: `
  Value is ${ERoles.admin}, ${ERoles.doctor} or ${ERoles.patient},
  `,
  })
  @IsEnum(ERoles)
  roles: ERoles;
}
