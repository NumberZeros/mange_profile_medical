import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm/repository/Repository';
import { ERoles, UserEntity } from '../../../entities/users.entity';
import { RegisterUserInput } from './dto/resgister.input';

import { IBaseServices } from '../../../containers/services.interface';
import { loginUserInput } from './dto/login.input';
import { ChangePasswordUserInput } from './dto/changePassword.input';
import { UpdateUserInput } from './dto/update.input';
import { CreateUserInput } from './dto/createUser.input';
import { secret } from 'src/configuration';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserServices implements IBaseServices {
  constructor(
    @InjectRepository(UserEntity)
    private userEnitty: Repository<UserEntity>,
  ) {}
  private async encryptPassword({ password }) {
    return await bcrypt.hash(password, parseInt(secret.HASH_PASSWORD));
  }

  async findAll(): Promise<UserEntity[]> {
    try {
      return await this.userEnitty.find({ inactive: false });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findAllAdmin(): Promise<UserEntity[]> {
    try {
      return await this.userEnitty.find({
        inactive: false,
        roles: ERoles.admin,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<UserEntity> {
    try {
      return await this.userEnitty.findOne({ id });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async post(data: CreateUserInput): Promise<UserEntity> {
    try {
      const user = {
        ...data,
        email: data.email.trim().toLowerCase(),
        password: await this.encryptPassword({
          password: data.password.trim().toLowerCase(),
        }),
      };
      return await this.userEnitty.save(user);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: UpdateUserInput): Promise<UserEntity> {
    try {
      return await this.userEnitty.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async delete(data: UserEntity): Promise<UserEntity> {
    try {
      return data;
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }

  async loginUser(data: loginUserInput): Promise<UserEntity> {
    try {
      const dataUser = await this.userEnitty
        .createQueryBuilder()
        .where({ email: data.email, inactive: false })
        .getOne();
      if (!dataUser) throw 'Email or password is wrong';
      const isMatch = await bcrypt.compare(data.password, dataUser.password);
      if (!isMatch) throw 'Email or password is wrong';
      return dataUser;
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findEmail(email: string): Promise<UserEntity> {
    try {
      return await this.userEnitty
        .createQueryBuilder()
        .where({
          email,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findPhone(phone: string): Promise<UserEntity> {
    try {
      return await this.userEnitty
        .createQueryBuilder()
        .where({
          phone,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async register(data: RegisterUserInput): Promise<UserEntity> {
    try {
      return await this.userEnitty.save({
        ...data,
        password: await this.encryptPassword({ password: data.password }),
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async changePassword(data: ChangePasswordUserInput): Promise<UserEntity> {
    try {
      return await this.userEnitty.save({
        ...data,
        password: await this.encryptPassword({ password: data.password }),
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async validateAccount(id: string, roles: ERoles) {
    try {
      const foundUser = await this.userEnitty.findOne({ id, roles });
      if (!foundUser)
        throw new HttpException(
          'Account not allow',
          HttpStatus.METHOD_NOT_ALLOWED,
        );
      return foundUser;
    } catch (error) {
      throw error;
    }
  }

  async intergateCSV(
    data: CreateUserInput,
    externalid: string,
  ): Promise<UserEntity> {
    try {
      const user = {
        ...data,
        email: data.email.trim().toLowerCase(),
        password: await this.encryptPassword({
          password: data.password.trim().toLowerCase(),
        }),
        externalid,
      };
      return await this.userEnitty.save(user);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findOneV2(id: string): Promise<UserEntity> {
    try {
      return await this.userEnitty
        .createQueryBuilder()
        .where({
          id,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
