import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm/repository/Repository';

import { IBaseServices } from '../../../containers/services.interface';
import { DoctorEntity } from 'src/entities/doctor.entity';
import { UserEntity } from 'src/entities/users.entity';
import { UpdateDoctorInput } from './dto/updateDoctor.input';
import { GroupEntity } from 'src/entities/group.entity';
import {
  PaginationDoctor,
  PaginationDoctorInput,
} from './dto/paginationDoctor.input';
import { Brackets, Equal, ILike } from 'typeorm';
import { isEmpty, truncate } from 'lodash';
import { AddressEntity } from 'src/entities/addresses.entity';

@Injectable()
export class DoctorServices implements IBaseServices {
  constructor(
    @InjectRepository(DoctorEntity)
    private readonly doctorEntity: Repository<DoctorEntity>,
  ) {}

  async findAll(): Promise<DoctorEntity[]> {
    try {
      return await this.doctorEntity
        .createQueryBuilder('doctor')
        .leftJoinAndMapOne(
          'doctor.user',
          UserEntity,
          'user',
          'doctor.user = user.id',
        )
        .where({ inactive: false })
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<DoctorEntity> {
    try {
      return await this.doctorEntity.findOne({
        id,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async post(
    { certificate, isverified }: { certificate: string; isverified: boolean },
    props: { user: UserEntity; groups: [GroupEntity] },
  ): Promise<DoctorEntity> {
    try {
      return await this.doctorEntity.save({
        ...props,
        certificate,
        isverified,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: UpdateDoctorInput, user: UserEntity): Promise<DoctorEntity> {
    try {
      return await this.doctorEntity.save({ ...data, user });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async putV2(data: DoctorEntity): Promise<DoctorEntity> {
    try {
      return await this.doctorEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async delete(data: DoctorEntity): Promise<DoctorEntity> {
    try {
      return await this.doctorEntity.save(data);
    } catch (error) {
      // console.log(error);
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }

  async findCertificate(certificate: string): Promise<DoctorEntity> {
    try {
      return await this.doctorEntity.findOne({
        certificate,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findEmail(email: string): Promise<DoctorEntity> {
    try {
      return await this.doctorEntity.findOne({
        user: { email },
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findUserID(id: string): Promise<DoctorEntity> {
    try {
      return await this.doctorEntity
        .createQueryBuilder('doctor')
        .leftJoinAndSelect('doctor.user', 'user_entity')
        .where('user_entity.id =:id', {
          id: id,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async integrateCSV(
    { certificate, externalid }: { certificate: string; externalid: string },
    props: { user: UserEntity; groups: [GroupEntity] },
  ): Promise<DoctorEntity> {
    try {
      return await this.doctorEntity.save({
        ...props,
        certificate,
        isverified: false,
        externalid,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findAllDoctorUserPagination(
    pagination: PaginationDoctorInput,
  ): Promise<PaginationDoctor> {
    try {
      const query = {};
      const listParamenter = [];
      for (const key in pagination) {
        if (
          key === 'take' ||
          key === 'skip' ||
          key === 'adminid' ||
          key === 'gender' ||
          key === 'updatedat' ||
          key === 'gender' ||
          key === 'dob' ||
          isEmpty(pagination[key])
        )
          continue;
        if (typeof pagination[key] === 'number')
          query[key] = Equal(pagination[key]);
        if (pagination[key] !== '' && !isEmpty(pagination[key])) {
          query[key] = `%${pagination[key].trim().toLowerCase()}%`;
          listParamenter.push(`user_entity.${key} ilike :${key}`);
        }
      }
      console.log(
        'query',
        `doctor.inactive = :inactive and doctor.isverified = :isverified ${
          listParamenter.length > 0
            ? `and (${listParamenter})`.replace(',', ' and ')
            : listParamenter
        }`,
        {
          inactive: false,
          isverified: true,
          ...query,
        },
      );
      const [data, total] = await this.doctorEntity
        .createQueryBuilder('doctor')
        .leftJoinAndSelect('doctor.user', 'user_entity')
        .where(
          `doctor.inactive = :inactive and doctor.isverified = :isverified ${
            listParamenter.length > 0
              ? `and (${listParamenter})`.replace(',', ' and ')
              : listParamenter
          }`,
          {
            inactive: false,
            isverified: true,
            ...query,
          },
        )
        .limit(pagination.take)
        .offset(pagination.skip * pagination.take)
        .orderBy('user_entity.updatedat', 'DESC')
        .getManyAndCount();
      return {
        doctors: data,
        take: pagination.take,
        skip: pagination.skip,
        total,
        nextPage: pagination.take * pagination.skip >= total ? false : true,
      };
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findAllDoctorForSchedule(): Promise<DoctorEntity[]> {
    try {
      return await this.doctorEntity
        .createQueryBuilder('doctor')
        .leftJoinAndMapOne(
          'doctor.user',
          UserEntity,
          'user',
          'doctor.user = user.id',
        )
        .where({ inactive: false, isverified: true })
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findOneV2(id: string): Promise<DoctorEntity> {
    try {
      return await this.doctorEntity
        .createQueryBuilder('doctor')
        .leftJoinAndSelect('doctor.user', 'user_entity')
        .leftJoinAndSelect('user_entity.addresses', 'address_entity')
        .leftJoinAndSelect('doctor.groups', 'group_entity')
        .where({
          id,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
