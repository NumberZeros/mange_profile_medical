import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BreathEntity } from 'src/entities/breath.entity';
import { Repository } from 'typeorm/repository/Repository';
import { IBaseServices } from '../../../containers/services.interface';

@Injectable()
export class BreathServices implements IBaseServices {
  constructor(
    @InjectRepository(BreathEntity)
    private readonly breathEntity: Repository<BreathEntity>,
  ) {}
  async findAll(): Promise<BreathEntity[]> {
    try {
      return await this.breathEntity.find({
        where: { inactive: false },
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<BreathEntity> {
    try {
      return await this.breathEntity
        .createQueryBuilder()
        .where({
          id,
          inactive: false,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async post(data: BreathEntity): Promise<BreathEntity> {
    try {
      return await this.breathEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: BreathEntity): Promise<BreathEntity> {
    try {
      return await this.breathEntity.save(data);
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }
  async delete(data: BreathEntity): Promise<BreathEntity> {
    throw new HttpException('error', HttpStatus.NOT_MODIFIED);
  }
  async findName(name: string): Promise<BreathEntity> {
    try {
      return await this.breathEntity
        .createQueryBuilder()
        .where({ name })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
