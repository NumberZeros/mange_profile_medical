import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DoctorEntity } from 'src/entities/doctor.entity';
import { GroupEntity } from 'src/entities/group.entity';
import { Repository } from 'typeorm/repository/Repository';
import { IBaseServices } from '../../../containers/services.interface';
import { CreateGroupInput } from './dto/create.input';
import { UpdateGroupInput } from './dto/put.input';

@Injectable()
export class GroupServices implements IBaseServices {
  constructor(
    @InjectRepository(GroupEntity)
    private readonly groupEntity: Repository<GroupEntity>,
  ) {}
  async findAll(): Promise<GroupEntity[]> {
    try {
      return await this.groupEntity
        .createQueryBuilder('group')
        .leftJoinAndSelect('group.doctors', 'doctor_entity')
        .leftJoinAndSelect('doctor_entity.user', 'user_entity')
        .where({ inactive: false, isverified: true })
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<GroupEntity> {
    try {
      return await this.groupEntity
        .createQueryBuilder('group')
        .leftJoinAndSelect('group.doctors', 'doctor_entity')
        .leftJoinAndSelect('doctor_entity.user', 'user_entity')
        .where({ id })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async post(data: CreateGroupInput): Promise<GroupEntity> {
    try {
      return await this.groupEntity.save({
        isverified: true,
        ...data,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: UpdateGroupInput): Promise<GroupEntity> {
    try {
      return await this.groupEntity.save(data);
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }
  async delete(data: GroupEntity): Promise<GroupEntity> {
    throw new HttpException('error', HttpStatus.NOT_MODIFIED);
  }

  async putV2(group: GroupEntity): Promise<GroupEntity> {
    try {
      return await this.groupEntity.save(group);
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }

  async init(name: string): Promise<GroupEntity> {
    try {
      return await this.groupEntity.save({ name });
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }
  async findName(name: string): Promise<GroupEntity> {
    try {
      return await this.groupEntity
        .createQueryBuilder()
        .where({ name })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
