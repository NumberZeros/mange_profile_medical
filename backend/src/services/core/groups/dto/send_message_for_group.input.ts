import { Field, InputType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
@InputType()
export class SendMessageForGroupInput {
  @Field()
  @IsString()
  ownerid: string;

  @Field()
  @IsString()
  groupid: string;

  @Field()
  @IsString()
  medicalid: string;
}
