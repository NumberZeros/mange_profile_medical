import { Field, InputType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
@InputType()
export class RemoveDoctorOutGroupInput {
  @Field()
  @IsString()
  adminid: string;

  @Field()
  @IsString()
  groupid: string;

  @Field((type) => [String])
  listDoctor: string[];
}
