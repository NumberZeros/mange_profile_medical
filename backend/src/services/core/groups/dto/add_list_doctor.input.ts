import { Field, InputType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
@InputType()
export class AddDoctorGroupInput {
  @Field()
  @IsString()
  groupid: string;

  @Field()
  @IsString()
  adminid: string;

  @Field((type) => [String])
  listDoctor: string[];
}
