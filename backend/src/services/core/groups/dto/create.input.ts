import { Field, InputType } from '@nestjs/graphql';
import { IsNumber, IsString } from 'class-validator';
@InputType()
export class CreateGroupInput {
  @Field()
  @IsString()
  adminid: string;

  @Field()
  @IsString()
  name: string;

  @Field({ defaultValue: '' })
  @IsString()
  description: string;

  @Field({
    defaultValue: 0,
    description: 'Thời gian bắt đầu ca làm việc',
  })
  @IsNumber()
  startwork: number;

  @Field({
    defaultValue: 0,
    description: 'Thời gian kết thúc ca làm việc',
  })
  @IsNumber()
  endwork: number;
}
