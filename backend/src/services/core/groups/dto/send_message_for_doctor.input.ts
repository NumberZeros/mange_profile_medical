import { Field, InputType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
@InputType()
export class SendMessageForDoctorInput {
  @Field()
  @IsString()
  ownerid: string;

  @Field()
  @IsString()
  doctorid: string;

  @Field()
  @IsString()
  medicalid: string;
}
