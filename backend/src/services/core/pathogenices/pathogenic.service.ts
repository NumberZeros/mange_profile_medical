import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PathogenicEntity } from 'src/entities/pathogenic.entity';
import { Repository } from 'typeorm/repository/Repository';

import { IBaseServices } from '../../../containers/services.interface';
import { CreatePathogenicInput } from './dto/post.input';

@Injectable()
export class PathogenicServices implements IBaseServices {
  constructor(
    @InjectRepository(PathogenicEntity)
    private pathogenicEntity: Repository<PathogenicEntity>,
  ) {}

  async findAll(): Promise<PathogenicEntity[]> {
    try {
      return await this.pathogenicEntity.find({ inactive: false });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<PathogenicEntity> {
    try {
      return await this.pathogenicEntity
        .createQueryBuilder()
        .where({ id })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async post(data: CreatePathogenicInput): Promise<PathogenicEntity> {
    try {
      const dataCategory = Object.assign(new PathogenicEntity(), data);
      return await this.pathogenicEntity.save(dataCategory);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: any): Promise<PathogenicEntity> {
    try {
      return await this.pathogenicEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async delete(data: PathogenicEntity): Promise<PathogenicEntity> {
    try {
      return data;
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }

  async findName(name: string): Promise<PathogenicEntity> {
    try {
      return await this.pathogenicEntity.findOne({
        name,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
