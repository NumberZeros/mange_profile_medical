import { Field, InputType } from '@nestjs/graphql';
import { IsBoolean, IsString } from 'class-validator';
@InputType()
export class CreatePathogenicInput {
  @Field()
  @IsString()
  name: string;

  @Field({ defaultValue: true })
  @IsBoolean()
  isverified: boolean;
}
