import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PatientEntity } from 'src/entities/patient.entity';
import { Repository } from 'typeorm/repository/Repository';
import { IBaseServices } from '../../../containers/services.interface';
import { AddressEntity } from '../../../entities/addresses.entity';
import { UserEntity } from '../../../entities/users.entity';
import { CreateAddressInput } from './dto/create.input';
import { UpdateAddressInput } from './dto/put.input';

@Injectable()
export class AddressServices implements IBaseServices {
  constructor(
    @InjectRepository(AddressEntity)
    private readonly addressEntity: Repository<AddressEntity>,
  ) {}
  async findAll(): Promise<AddressEntity[]> {
    try {
      return await this.addressEntity.find({
        inactive: false,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<AddressEntity> {
    try {
      return await this.addressEntity.createQueryBuilder().where(id).getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async post(
    data: CreateAddressInput,
    user: UserEntity,
  ): Promise<AddressEntity> {
    try {
      const dataAddress = Object.assign(new AddressEntity(), {
        ...data,
        streetline1: '',
        streetline2: '',
        user,
      });
      return await this.addressEntity.save(dataAddress);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(
    data: UpdateAddressInput,
    user: UserEntity,
  ): Promise<AddressEntity> {
    try {
      return await this.addressEntity.save({
        ...data,
        streetline1: '',
        streetline2: '',
        user,
      });
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }
  async delete(data: AddressEntity): Promise<AddressEntity> {
    throw new HttpException('error', HttpStatus.NOT_MODIFIED);
  }

  async postPatient(
    data: CreateAddressInput,
    user: PatientEntity,
  ): Promise<AddressEntity> {
    try {
      const dataAddress = Object.assign(new AddressEntity(), {
        ...data,
        user,
      });
      return await this.addressEntity.save(dataAddress);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
