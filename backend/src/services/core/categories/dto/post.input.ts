import { Field, InputType } from '@nestjs/graphql';
import { IsBoolean, IsString } from 'class-validator';
@InputType()
export class CreateCategoryInput {
  @Field()
  @IsString()
  name: string;

  @Field({ defaultValue: true })
  @IsBoolean()
  isverified: boolean;
}
