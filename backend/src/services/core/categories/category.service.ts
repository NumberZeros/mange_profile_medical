import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryEntity } from 'src/entities/categories.entity';
import { Repository } from 'typeorm/repository/Repository';

import { IBaseServices } from '../../../containers/services.interface';
import { CreateCategoryInput } from './dto/post.input';

@Injectable()
export class CategoryServices implements IBaseServices {
  constructor(
    @InjectRepository(CategoryEntity)
    private categoryEntity: Repository<CategoryEntity>,
  ) {}

  async findAll(): Promise<CategoryEntity[]> {
    try {
      return await this.categoryEntity.find({ inactive: false });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<CategoryEntity> {
    try {
      return await this.categoryEntity
        .createQueryBuilder()
        .where({ id })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async post(data: CreateCategoryInput): Promise<CategoryEntity> {
    try {
      const dataCategory = Object.assign(new CategoryEntity(), data);
      return await this.categoryEntity.save(dataCategory);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: any): Promise<CategoryEntity> {
    try {
      return await this.categoryEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async delete(data: CategoryEntity): Promise<CategoryEntity> {
    try {
      return data;
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }

  async findName(name: string): Promise<CategoryEntity> {
    try {
      return await this.categoryEntity.findOne({
        name,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
