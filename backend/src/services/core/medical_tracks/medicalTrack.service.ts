import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm/repository/Repository';

import { IBaseServices } from '../../../containers/services.interface';
import { CreateMedicalTrackInput } from './dto/post.input';
import { PatientEntity } from 'src/entities/patient.entity';
import { DoctorEntity } from 'src/entities/doctor.entity';
import { MedicalTrackEntity } from '../../../entities/medical_track.entity';
import { UpdateMedicalTrackInput } from './dto/update.input';
import { MedicalEntity } from 'src/entities/medical.entity';

@Injectable()
export class MedicalTrackServices implements IBaseServices {
  constructor(
    @InjectRepository(MedicalTrackEntity)
    private medicalTrackEntity: Repository<MedicalTrackEntity>,
  ) {}

  async findAll(medical): Promise<MedicalTrackEntity[]> {
    try {
      return await this.medicalTrackEntity.find({ medical, inactive: false });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<MedicalTrackEntity> {
    try {
      return await this.medicalTrackEntity.findOne({
        id,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async post(
    data: CreateMedicalTrackInput,
    props: { medical: MedicalEntity },
  ): Promise<MedicalTrackEntity> {
    try {
      return await this.medicalTrackEntity.save({
        ...data,
        ...props,
        isverified: true,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: UpdateMedicalTrackInput): Promise<MedicalTrackEntity> {
    try {
      return await this.medicalTrackEntity.save({ ...data, isverified: true });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async delete(data: MedicalTrackEntity): Promise<MedicalTrackEntity> {
    try {
      return data;
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }

  async findAllMedicalTrackV2(
    medicalid: string,
  ): Promise<MedicalTrackEntity[]> {
    try {
      return this.medicalTrackEntity
        .createQueryBuilder('track')
        .innerJoinAndMapMany(
          'track.medicalId',
          MedicalEntity,
          'medical',
          'track.medicalId = medical.id',
        )
        .where({ inactive: false, medical: medicalid })
        .getMany();
    } catch (error) {}
  }

  async putV2(data: MedicalTrackEntity): Promise<MedicalTrackEntity> {
    try {
      return await this.medicalTrackEntity.save({ ...data, isverified: true });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
