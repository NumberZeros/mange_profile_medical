import { Field, InputType } from '@nestjs/graphql';
import { IsEnum, IsString, IsDate } from 'class-validator';
import { EMedicalPackage, EStatus } from 'src/entities/medical.entity';
@InputType()
export class UpdateMedicalTrackInput {
  @Field()
  @IsString()
  ownerid: string;

  @Field()
  @IsString()
  id: string;

  @Field()
  @IsString()
  medicalid: string;

  @Field()
  @IsDate()
  prcrealdate: Date;

  @Field({
    defaultValue: EMedicalPackage.other,
    description: `Value are ${EMedicalPackage.prc}, ${EMedicalPackage.quicktest} và ${EMedicalPackage.other}`,
  })
  @IsEnum(EMedicalPackage)
  medicalpackage: EMedicalPackage;

  @Field()
  @IsEnum(EStatus)
  medicalresutl: EStatus;

  @Field({ defaultValue: '' })
  medicalnote: string;
}
