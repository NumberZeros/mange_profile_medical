import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DoctorEntity } from 'src/entities/doctor.entity';
import { MedicalEntity } from 'src/entities/medical.entity';
import { SMSEntity } from 'src/entities/sms.entity';
import { Repository } from 'typeorm/repository/Repository';
import { IBaseServices } from '../../../containers/services.interface';

@Injectable()
export class SMSServices implements IBaseServices {
  constructor(
    @InjectRepository(SMSEntity)
    private readonly smsEntity: Repository<SMSEntity>,
  ) {}
  async findAll(): Promise<SMSEntity[]> {
    try {
      return await this.smsEntity
        .createQueryBuilder('sms')
        .leftJoinAndMapOne(
          'sms.medical',
          MedicalEntity,
          'medical',
          'sms.medical = medical.id',
        )
        .leftJoinAndMapOne(
          'sms.doctor',
          DoctorEntity,
          'doctor',
          'sms.doctor = doctor.id',
        )
        .where({ inactive: false })
        .orderBy('sms.updatedat', 'DESC')
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<SMSEntity> {
    try {
      return await this.smsEntity
        .createQueryBuilder()
        .where({
          id,
          inactive: false,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async post(data: SMSEntity): Promise<SMSEntity> {
    try {
      return await this.smsEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: SMSEntity): Promise<SMSEntity> {
    try {
      return await this.smsEntity.save(data);
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }
  async delete(data: SMSEntity): Promise<SMSEntity> {
    throw new HttpException('error', HttpStatus.NOT_MODIFIED);
  }

  async loginDoctor(authid: string): Promise<SMSEntity> {
    try {
      return await this.smsEntity
        .createQueryBuilder('sms')
        .leftJoinAndSelect('sms.medical', 'medical')
        .leftJoinAndSelect('medical.patient', 'patient')
        .leftJoinAndSelect('patient.sharedoctor', 'sharedoctor')
        .leftJoinAndSelect('patient.pathogenic', 'pathogenic')
        .leftJoinAndSelect('sms.doctor', 'doctor')
        .leftJoinAndSelect('doctor.user', 'user')
        .where({
          authid,
          inactive: false,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
