import { Field, InputType } from '@nestjs/graphql';
import { IsString, IsNumber } from 'class-validator';
@InputType()
export class UpdateMedicineInput {
  @Field()
  @IsString()
  ownerid: string;

  @Field()
  @IsString()
  id: string;

  @Field()
  @IsString()
  medicalid: string;

  @Field({ description: 'Tên thuốc' })
  @IsString()
  name: string;

  @Field({ description: 'Số lượng', defaultValue: 1 })
  @IsNumber()
  quantity: number;

  @Field({ defaultValue: '' })
  @IsString()
  description: string;
}
