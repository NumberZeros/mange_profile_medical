import { Field, InputType } from '@nestjs/graphql';
import { IsString, IsNumber } from 'class-validator';
@InputType()
export class CreateMedicineInput {
  @Field()
  @IsString()
  ownerid: string;

  @Field()
  @IsString()
  medicalid: string;

  @Field((type) => [ListMedicineInput], { description: 'Danh sách thuốc' })
  listMedicine: ListMedicineInput[];
}

@InputType()
export class ListMedicineInput {
  @Field({ description: 'Tên thuốc' })
  @IsString()
  name: string;

  @Field({ description: 'Số lượng', defaultValue: 1 })
  @IsNumber()
  quantity: number;

  @Field({ defaultValue: '' })
  @IsString()
  description: string;
}
