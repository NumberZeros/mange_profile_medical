import { Field, InputType } from '@nestjs/graphql';
import { IsString, IsNumber } from 'class-validator';
@InputType()
export class CreateMedicineForEmergencyInput {
  @Field()
  @IsString()
  ownerid: string;

  @Field()
  @IsString()
  emergencyid: string;

  @Field()
  @IsString()
  patientid: string;

  @Field((type) => [ListEmergencyInput], { description: 'Danh sách thuốc' })
  listMedicine: ListEmergencyInput[];
}

@InputType()
export class ListEmergencyInput {
  @Field({ description: 'Tên thuốc' })
  @IsString()
  name: string;

  @Field({ description: 'Số lượng', defaultValue: 1 })
  @IsNumber()
  quantity: number;

  @Field({ defaultValue: '' })
  @IsString()
  description: string;
}
