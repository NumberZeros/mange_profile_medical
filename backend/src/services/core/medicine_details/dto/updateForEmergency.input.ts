import { Field, InputType } from '@nestjs/graphql';
import { IsString, IsNumber } from 'class-validator';
@InputType()
export class UpdateMedicineForEmergencyInput {
  @Field()
  @IsString()
  id: string;

  @Field()
  @IsString()
  ownerid: string;

  @Field()
  @IsString()
  emergencyid: string;

  @Field({ description: 'Tên thuốc' })
  @IsString()
  name: string;

  @Field({ description: 'Số lượng', defaultValue: 1 })
  @IsNumber()
  quantity: number;

  @Field({ defaultValue: '' })
  @IsString()
  description: string;
}
