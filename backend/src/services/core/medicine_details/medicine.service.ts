import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MedicineEntity } from 'src/entities/medicine_detail.entity';
import { Repository } from 'typeorm/repository/Repository';

import { IBaseServices } from '../../../containers/services.interface';
import { CreateMedicineInput } from './dto/post.input';
import { UpdateMedicineInput } from './dto/update.input';

@Injectable()
export class MedicineServices implements IBaseServices {
  constructor(
    @InjectRepository(MedicineEntity)
    private medicineEntity: Repository<MedicineEntity>,
  ) {}

  async findAllMedicine(): Promise<MedicineEntity[]> {
    try {
      return await this.medicineEntity.find({ inactive: false });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findAll(medical): Promise<MedicineEntity[]> {
    try {
      return await this.medicineEntity.find({ medical, inactive: false });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<MedicineEntity> {
    try {
      return await this.medicineEntity.findOne({
        id,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async post(data: CreateMedicineInput): Promise<MedicineEntity> {
    try {
      return await this.medicineEntity.save({ ...data, isverified: true });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: UpdateMedicineInput): Promise<MedicineEntity> {
    try {
      return await this.medicineEntity.save({ ...data, isverified: true });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async delete(data: MedicineEntity): Promise<MedicineEntity> {
    try {
      return data;
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }

  async putV2ForEmegency(data: MedicineEntity): Promise<MedicineEntity> {
    try {
      return await this.medicineEntity.save({ ...data, isverified: true });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async findName(name: string): Promise<MedicineEntity> {
    try {
      return await this.medicineEntity.findOne({
        name,
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
