import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PatientStatusEntity } from 'src/entities/patient_status.entity';
import { Repository } from 'typeorm/repository/Repository';

import { IBaseServices } from '../../../containers/services.interface';
import { CreatePatientStatusInput } from './dto/post.input';

@Injectable()
export class PatientStatusServices implements IBaseServices {
  constructor(
    @InjectRepository(PatientStatusEntity)
    private statusEntity: Repository<PatientStatusEntity>,
  ) {}

  async findAll(): Promise<PatientStatusEntity[]> {
    try {
      return await this.statusEntity.find({ inactive: false });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<PatientStatusEntity> {
    try {
      return await this.statusEntity
        .createQueryBuilder()
        .where({ id })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async post(data: CreatePatientStatusInput): Promise<PatientStatusEntity> {
    try {
      return await this.statusEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: any): Promise<PatientStatusEntity> {
    try {
      return await this.statusEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }

  async delete(data: PatientStatusEntity): Promise<PatientStatusEntity> {
    try {
      return data;
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }

  async findName(name: string): Promise<PatientStatusEntity> {
    try {
      return await this.statusEntity
        .createQueryBuilder()
        .where({ name })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
