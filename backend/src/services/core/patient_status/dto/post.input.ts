import { Field, InputType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
@InputType()
export class CreatePatientStatusInput {
  @Field()
  @IsString()
  name: string;

  @Field({ defaultValue: '' })
  @IsString()
  description: string;
}
