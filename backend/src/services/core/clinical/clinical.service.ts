import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClinicalEntity } from 'src/entities/clinical.entity';
import { Repository } from 'typeorm/repository/Repository';
import { IBaseServices } from '../../../containers/services.interface';

@Injectable()
export class ClinicalServices implements IBaseServices {
  constructor(
    @InjectRepository(ClinicalEntity)
    private readonly clinicalEntity: Repository<ClinicalEntity>,
  ) {}
  async findAll(): Promise<ClinicalEntity[]> {
    try {
      return await this.clinicalEntity.find({
        where: { inactive: false, isverified: true },
      });
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<ClinicalEntity> {
    try {
      return await this.clinicalEntity
        .createQueryBuilder()
        .where({
          id,
          inactive: false,
        })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async post(data: ClinicalEntity): Promise<ClinicalEntity> {
    try {
      return await this.clinicalEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: ClinicalEntity): Promise<ClinicalEntity> {
    try {
      return await this.clinicalEntity.save(data);
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }
  async delete(data: ClinicalEntity): Promise<ClinicalEntity> {
    throw new HttpException('error', HttpStatus.NOT_MODIFIED);
  }
  async findName(name: string): Promise<ClinicalEntity> {
    try {
      return await this.clinicalEntity
        .createQueryBuilder()
        .where({ name })
        .getOne();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
}
