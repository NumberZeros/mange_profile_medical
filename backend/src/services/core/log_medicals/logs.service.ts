import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LogMedicalEntity } from 'src/entities/log_medical.entity';
import { PatientEntity } from 'src/entities/patient.entity';
import { UserEntity } from 'src/entities/users.entity';
import { Repository } from 'typeorm/repository/Repository';
import { IBaseServices } from '../../../containers/services.interface';

@Injectable()
export class LogMedicalServices implements IBaseServices {
  constructor(
    @InjectRepository(LogMedicalEntity)
    private readonly logEntity: Repository<LogMedicalEntity>,
  ) {}
  async findAll(patientid: string): Promise<LogMedicalEntity[]> {
    try {
      return await this.logEntity
        .createQueryBuilder('log')
        .leftJoinAndMapOne(
          'log.patient',
          PatientEntity,
          'patient',
          'log.patient = patient.id',
        )
        .leftJoinAndMapOne(
          'log.createdby',
          UserEntity,
          'user',
          'log.createdby = user.id',
        )
        .where({ inactive: false, patient: patientid })
        .getMany();
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async findOne(id: string): Promise<LogMedicalEntity> {
    try {
      return await this.logEntity.findOne(id);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async post(data: LogMedicalEntity): Promise<LogMedicalEntity> {
    try {
      return await this.logEntity.save(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.NOT_MODIFIED);
    }
  }
  async put(data: LogMedicalEntity): Promise<LogMedicalEntity> {
    try {
      return await this.logEntity.save(data);
    } catch (error) {
      throw new HttpException('error', HttpStatus.NOT_MODIFIED);
    }
  }
  async delete(data: LogMedicalEntity): Promise<LogMedicalEntity> {
    throw new HttpException('error', HttpStatus.NOT_MODIFIED);
  }
}
