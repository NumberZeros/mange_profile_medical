import { Field, InputType } from '@nestjs/graphql';
import { IsBoolean, IsString } from 'class-validator';
@InputType()
export class UpdateAddressInput {
  @Field()
  @IsString()
  id: string;

  @Field({ defaultValue: true })
  @IsBoolean()
  isverified: boolean;

  @Field()
  @IsString()
  name: string;

  // @Field()
  // @IsString()
  // streetline1: string;

  // @Field({ nullable: true, defaultValue: '' })
  // @IsString()
  // streetline2: string;

  @Field()
  @IsString()
  ward: string;

  @Field()
  @IsString()
  province: string;

  @Field()
  @IsString()
  city: string;

  @Field()
  @IsString()
  user: string;
}
