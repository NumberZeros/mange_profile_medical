import { Field, ObjectType } from '@nestjs/graphql';
import { RootObject } from 'src/containers/objects.root';
import { Column, Entity, OneToMany } from 'typeorm';
import { PatientEntity } from './patient.entity';

@Entity()
@ObjectType()
export class CategoryEntity extends RootObject {
  @Field((type) => [PatientEntity])
  @OneToMany((type) => PatientEntity, (user) => user.category)
  user: PatientEntity[];

  @Field({ description: 'Loại bệnh nhân ' })
  @Column({ unique: true, nullable: true })
  name: string;

  @Field()
  @Column({ unique: true, nullable: true })
  description: string;
}
