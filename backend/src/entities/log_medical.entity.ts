import { Field, ObjectType } from '@nestjs/graphql';
import { RootObject } from 'src/containers/objects.root';
import { Column, Entity, JoinTable, ManyToOne } from 'typeorm';
import { PatientEntity } from './patient.entity';
import { UserEntity } from './users.entity';

@Entity()
@ObjectType()
export class LogMedicalEntity extends RootObject {
  @Field((type) => UserEntity)
  @ManyToOne((type) => UserEntity, (user) => user.log)
  @JoinTable({ name: 'email' })
  createdby: UserEntity;

  @Field((type) => PatientEntity)
  @ManyToOne((type) => PatientEntity, (user) => user.log)
  patient: PatientEntity;

  @Field()
  @Column({ default: '' })
  name: string;

  @Field()
  @Column({ default: '' })
  description: string;
}
