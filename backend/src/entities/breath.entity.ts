import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, JoinTable, OneToMany } from 'typeorm';
import { RootObject } from '../containers/objects.root';
import { EmergencyEntity } from './emergency.entity';

@Entity()
@ObjectType()
export class BreathEntity extends RootObject {
  @Field()
  @Column({ default: '' })
  name: string;

  @Field()
  @Column({ default: '' })
  description: string;

  @Field((type) => [EmergencyEntity], { description: '' })
  @OneToMany(() => EmergencyEntity, (emergency) => emergency.methodbreath)
  @JoinTable()
  emergencies: EmergencyEntity[];
}
