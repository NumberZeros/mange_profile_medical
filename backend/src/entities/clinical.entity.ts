import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, ManyToMany } from 'typeorm';
import { RootObject } from '../containers/objects.root';
import { EmergencyEntity } from './emergency.entity';

@Entity()
@ObjectType()
export class ClinicalEntity extends RootObject {
  @ManyToMany((type) => EmergencyEntity, (emergency) => emergency.clinical)
  emergency: EmergencyEntity[];

  @Field()
  @Column({ default: '' })
  name: string;

  @Field()
  @Column({ default: '' })
  description: string;
}
