import { Field, ObjectType } from '@nestjs/graphql';
import { RootObject } from 'src/containers/objects.root';
import { Column, Entity, ManyToOne } from 'typeorm';
import { MedicalEntity } from './medical.entity';

enum EStatus {
  negative = 'negative',
  positive = 'positive',
  other = 'other',
  no = 'no',
}

enum EMedicalPackage {
  prc = 'prc',
  quicktest = 'quicktest',
  other = 'other',
  no = 'no',
}
@Entity()
@ObjectType()
export class MedicalTrackEntity extends RootObject {
  @Field((type) => MedicalEntity, {
    description: 'Hồ sơ bệnh án',
  })
  @ManyToOne((type) => MedicalEntity, (medical) => medical.medicaltrack)
  medical: MedicalEntity;

  @Field({
    description: 'Ngày thực hiện xét nghiệm',
    deprecationReason: 'Tổng quan',
  })
  @Column()
  prcrealdate: Date;

  @Field({
    description: `Loại xét nghiệm: ${EMedicalPackage.quicktest}, ${EMedicalPackage.prc}, ${EMedicalPackage.other}, `,
    deprecationReason: 'Tổng quan',
  })
  @Column({
    enum: EMedicalPackage,
    default: EMedicalPackage.other,
  })
  medicalpackage: EMedicalPackage;

  @Field({
    description: `Kết quả: ${EStatus.negative}, ${EStatus.positive}, ${EStatus.other}, `,
    deprecationReason: 'Tổng quan',
  })
  @Column({
    enum: EStatus,
    default: EStatus.other,
  })
  medicalresutl: EStatus;

  @Field({ description: 'Ghi chú' })
  @Column({ default: '' })
  medicalnote: string;
}
