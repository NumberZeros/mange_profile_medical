import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, OneToMany } from 'typeorm';
import { RootObject } from '../containers/objects.root';
import { PatientEntity } from './patient.entity';

@Entity()
@ObjectType()
export class PatientStatusEntity extends RootObject {
  @Field((type) => [PatientEntity], { description: 'Bệnh nhân' })
  @OneToMany((type) => PatientEntity, (patitent) => patitent.status)
  patient: PatientEntity[];

  @Field({ description: 'Tên trạng thái' })
  @Column({ default: '' })
  name: string;

  @Field({ description: 'Tên trạng thái' })
  @Column({ default: '' })
  description: string;
}
