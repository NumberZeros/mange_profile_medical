import { Field, ObjectType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
import { RootObject } from 'src/containers/objects.root';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { GroupEntity } from './group.entity';
import { GroupPatientEntity } from './group_patient.entity';
import { PatientEntity } from './patient.entity';
import { SMSEntity } from './sms.entity';
import { UserEntity } from './users.entity';

@Entity()
@ObjectType()
export class DoctorEntity extends RootObject {
  @Field()
  @Column({ default: '' })
  certificate: string;

  @Field()
  @IsString()
  @OneToOne((type) => UserEntity, {
    eager: true,
  })
  @JoinColumn()
  user: UserEntity;

  @Field((type) => [PatientEntity])
  @OneToMany(() => PatientEntity, (medical) => medical.doctor)
  @JoinColumn()
  patient: PatientEntity[];

  @Field((type) => [GroupEntity])
  @ManyToMany(() => GroupEntity, (group) => group.doctors, { eager: true })
  groups: GroupEntity[];

  @Field((type) => PatientEntity)
  @OneToMany((type) => PatientEntity, (patient) => patient.sharedoctor)
  @JoinColumn()
  sharepatient: PatientEntity[];

  @Field((type) => [SMSEntity])
  @ManyToOne(() => SMSEntity, (sms) => sms.doctor)
  sms: SMSEntity[];

  @Field((type) => GroupPatientEntity)
  @OneToMany(
    (type) => GroupPatientEntity,
    (groupPatient) => groupPatient.doctor,
  )
  @JoinColumn()
  grouppatient: GroupPatientEntity[];
}
