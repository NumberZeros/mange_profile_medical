import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, JoinTable, ManyToMany, ManyToOne } from 'typeorm';
import { RootObject } from '../containers/objects.root';
import { DoctorEntity } from './doctor.entity';
import { PatientEntity } from './patient.entity';

@Entity()
@ObjectType()
export class GroupPatientEntity extends RootObject {
  @Field()
  @Column({ default: '' })
  name: string;

  @Field()
  @Column({ default: '' })
  description: string;

  @Field()
  @Column({ default: 0, type: 'float' })
  priority: number;

  @Field((type) => [PatientEntity], { description: 'Danh sách các bệnh nhân' })
  @ManyToMany(() => PatientEntity, (patient) => patient.group)
  @JoinTable()
  patient: PatientEntity[];

  @Field((type) => DoctorEntity)
  @ManyToOne(() => DoctorEntity, (doctor) => doctor.grouppatient, {
    eager: true,
  })
  doctor: DoctorEntity;

  @Field()
  @Column({ default: 0, type: 'float' })
  index: number;
}
