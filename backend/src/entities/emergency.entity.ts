import { Field, ObjectType } from '@nestjs/graphql';
import { DEFAULT_DATE } from 'src/services/packages';
import { Column, Entity, JoinTable, ManyToMany, ManyToOne } from 'typeorm';
import { RootObject } from '../containers/objects.root';
import { BreathEntity } from './breath.entity';
import { ClinicalEntity } from './clinical.entity';
import { EmergencyStatusEntity } from './emegency_status.entity';
import { MedicineEntity } from './medicine_detail.entity';
import { PatientEntity } from './patient.entity';

export enum EIsContact {
  yes = 'yes',
  no = 'no',
  abstruse = 'abstruse',
  empty = '',
}

export enum EReason {
  treated = 'treated',
  gohome = 'gohome',
  die = 'die',
  notinformation = 'notinformation',
  no = '',
}

enum EMedicalPackage {
  prc = 'prc',
  quicktest = 'quicktest',
  other = 'other',
  no = 'no',
}

@Entity()
@ObjectType()
export class EmergencyEntity extends RootObject {
  @Field((type) => PatientEntity)
  @ManyToOne((type) => PatientEntity, (patient) => patient.medicalemergency)
  patient: PatientEntity;

  @Field({
    description: 'Ghi chú về bệnh lý nền',
    deprecationReason: 'Hành chính',
  })
  @Column({ default: '' })
  notebackgroup: string;

  @Field({
    description: `Có tiếp xúc với người mắc covid: ${EIsContact.yes}, ${EIsContact.no}, ${EIsContact.abstruse}, ${EIsContact.empty},`,
    deprecationReason: 'Hành chính',
  })
  @Column({ default: EIsContact.no, enum: EIsContact })
  iscontact: EIsContact;

  @Field((type) => [ClinicalEntity], {
    description: 'Có biểu hiện lâm sàn',
    deprecationReason: 'Khám - Đánh giá',
  })
  @ManyToMany((type) => ClinicalEntity, (clinical) => clinical.emergency, {
    eager: true,
  })
  @JoinTable()
  clinical: ClinicalEntity[];

  @Field({
    description: 'Ghi chú (Triệu chứng lâm sàn)',
    deprecationReason: 'Khám - Đánh giá',
  })
  @Column({ default: '' })
  listclinicalnote: string;

  @Field({
    description: 'Cảm thấy đâu hoặc tức ngực nhiều không giảm',
    deprecationReason: 'Khám - Đánh giá',
  })
  @Column({ default: '' })
  note: string;

  @Field({
    description: 'Triệu chứng lâm sàn',
    deprecationReason: 'Khám - Đánh giá',
  })
  @Column({ default: '' })
  sense: string;

  @Field({
    description: 'Mạch',
    deprecationReason: 'Khám đánh giá',
  })
  @Column({ type: 'float', default: 0 })
  pluse: number;

  @Field({
    description: 'Huyết áp (Tâm thu)',
    deprecationReason: 'Khám đánh giá',
  })
  @Column({ type: 'float', default: 0 })
  bloodpressure: number;

  @Field({
    description: 'Huyết áp(Tâm trương)',
    deprecationReason: 'Khám đánh giá',
  })
  @Column({ type: 'float', default: 0 })
  bloodpressure2: number;

  @Field({
    description: 'SpO2 tại nhà',
    deprecationReason: 'Khám đánh giá',
  })
  @Column({ type: 'float', default: 0 })
  spo2home: number;

  @Field({
    description: 'Nhiệt độ',
    deprecationReason: 'Khám đánh giá',
  })
  @Column({ type: 'float', default: 0 })
  temperature: number;

  @Field({
    description: 'Khác',
    deprecationReason: 'Khám đánh giá',
  })
  @Column({ default: '' })
  other: string;

  @Field({
    description: `Xác định dương tính với Covid-19: ${EMedicalPackage.quicktest}, ${EMedicalPackage.prc}, ${EMedicalPackage.other}, `,
    deprecationReason: 'Khám và đánh giá',
  })
  @Column({
    enum: EMedicalPackage,
    default: EMedicalPackage.other,
  })
  medicalpackage: EMedicalPackage;

  @Field({
    description: 'Ghi chú (Xác định dương tính với Covid-19)',
    deprecationReason: 'Khám đánh giá',
  })
  @Column({ default: '' })
  medicalpackagenote: string;

  @Field({
    description: 'Ngày xét nghiệp dương tính gần nhất',
    deprecationReason: 'Khám đánh giá',
  })
  @Column({ default: DEFAULT_DATE })
  resultdate: Date;

  @Field((type) => BreathEntity, {
    description: 'Phương thức thử tại hiện trường',
    deprecationReason: 'Xữ trí',
  })
  @ManyToOne((type) => BreathEntity, (patient) => patient.emergencies)
  methodbreath: BreathEntity;

  @Field({
    description: 'Lưu lượng O2 (L/phút)',
    deprecationReason: 'Xữ trí',
  })
  @Column({ default: 0, type: 'float' })
  po2: number;

  @Field((type) => [MedicineEntity], {
    description: 'Danh sách thuốc khẩn cấp',
    deprecationReason: 'Thuốc',
  })
  @ManyToMany((type) => MedicineEntity, (medicine) => medicine.emergency, {
    eager: true,
  })
  @JoinTable()
  medicines: MedicineEntity[];

  @Field((type) => EmergencyStatusEntity)
  @ManyToOne(() => EmergencyStatusEntity, (medical) => medical.emergency, {
    eager: true,
  })
  @JoinTable()
  status: EmergencyStatusEntity;

  @Field({
    description: 'Ghi chú kết luận bệnh nhân',
    deprecationReason: 'Kết luận bệnh nhân',
  })
  @Column({ default: '' })
  notestatus: string;

  @Field({
    description: 'Nơi chuyển đến',
    deprecationReason: 'Kết luận bệnh nhân',
  })
  @Column({ default: '' })
  transferfrom: string;

  @Field({
    description: `Kết luận bệnh nhân: ${EReason.treated}, ${EReason.gohome}, ${EReason.die}, ${EReason.notinformation},`,
    deprecationReason: 'Khám và đánh giá',
    defaultValue: EReason.no,
  })
  @Column({
    enum: EReason,
    default: EReason.notinformation,
  })
  reason: EReason;

  @Field({
    description: 'Ngày ra kết luận',
    deprecationReason: 'Kết luận bệnh nhân',
  })
  @Column({ default: '' })
  reasondate: string;

  @Field({
    description: 'Thời gian hiển thị trên time line',
  })
  @Column({ default: DEFAULT_DATE })
  displaydate: Date;
}
