import { Field, ObjectType } from '@nestjs/graphql';
import { RootObject } from 'src/containers/objects.root';
import { Column, Entity, OneToMany } from 'typeorm';
import { EmergencyEntity } from './emergency.entity';

@Entity()
@ObjectType()
export class EmergencyStatusEntity extends RootObject {
  @OneToMany((type) => EmergencyEntity, (medical) => medical.status)
  emergency: EmergencyEntity[];

  @Field({ description: 'Tên trạng thái' })
  @Column()
  name: string;

  @Field()
  @Column({ default: '' })
  description: string;
}
