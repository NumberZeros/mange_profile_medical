import { Field, ObjectType } from '@nestjs/graphql';
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
@ObjectType()
export class AnalysisEntity {
  @Field({ description: `Internal ID` })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field({ description: `Automatic` })
  @CreateDateColumn()
  createddat: Date;

  @Field({ description: `Automatic` })
  @UpdateDateColumn()
  updatedat: Date;

  @Column({
    default: '',
  })
  user: string;

  @Column({
    default: '',
  })
  startweek: string;

  @Field({ description: 'Tổng số bệnh nhân trong hệ thống' })
  @Column({
    default: 0,
    type: 'float',
  })
  totalPatient: number;

  @Field({ description: 'Tổng số bệnh nhân trong hệ thống ngày hôm nay' })
  @Column({
    default: 0,
    type: 'float',
  })
  totalPatientNow: number;

  @Field({ description: 'Số lượng bệnh nhân được phân tầng nguy cơ cao' })
  @Column({
    default: 0,
    type: 'float',
  })
  totalPatientRiskAssessment: number;

  @Field({ description: 'Số bệnh nhân thêm mới trên toàn hệ thống trong tuần' })
  @Column({ default: `[{"date":"","data":0}]` })
  dataTablePatientCreated: string;

  @Field({ description: 'Số lượng bệnh nhân có kết quả âm tính trong tuần' })
  @Column({ default: `[{"date":"","data":0}]` })
  dataTablePatientNegative: string;

  @Field({
    description: 'Số lượng bệnh nhân cần hỗ trợ hiện trường trong tuần',
  })
  @Column({ default: `[{"date":"","data":0}]` })
  dataTablePatientContacDescription: string;
}

@ObjectType()
export class AnalysisResponse {
  @Field({ description: 'Tổng số bệnh nhân trong hệ thống' })
  @Column({
    default: 0,
    type: 'float',
  })
  totalPatient: number;

  @Field({ description: 'Tổng số bệnh nhân trong hệ thống ngày hôm nay' })
  @Column({
    default: 0,
    type: 'float',
  })
  totalPatientNow: number;

  @Field({ description: 'Số lượng bệnh nhân được phân tầng nguy cơ cao' })
  @Column({
    default: 0,
    type: 'float',
  })
  totalPatientRiskAssessment: number;

  @Field({ description: 'Số bệnh nhân thêm mới trên toàn hệ thống trong tuần' })
  @Column({ default: `[{"date":"","data":0}]` })
  dataTablePatientCreated: string;

  @Field({ description: 'Số lượng bệnh nhân có kết quả âm tính trong tuần' })
  @Column({ default: `[{"date":"","data":0}]` })
  dataTablePatientNegative: string;

  @Field({
    description: 'Số lượng bệnh nhân cần hỗ trợ hiện trường trong tuần',
  })
  @Column({ default: `[{"date":"","data":0}]` })
  dataTablePatientContacDescription: string;
}

@ObjectType()
export class AnalysisGroupDangerous {
  @Field({ description: 'Tổng số bệnh nhân trong 4 ngày' })
  @Column({
    default: 0,
    type: 'float',
  })
  totalPatient4Date: number;

  @Field({ description: 'Tổng số bệnh nhân trong 10 ngày' })
  @Column({
    default: 0,
    type: 'float',
  })
  totalPatient10Date: number;

  @Field({ description: 'Tổng số bệnh nhân trong 28 ngày' })
  @Column({
    default: 0,
    type: 'float',
  })
  totalPatient28Date: number;
}
