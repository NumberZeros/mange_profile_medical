import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, OneToMany } from 'typeorm';
import { RootObject } from '../containers/objects.root';
import { AddressEntity } from './addresses.entity';
import { AssetesEntity } from './assetes.entity';
import { LogMedicalEntity } from './log_medical.entity';

export enum ERoles {
  admin = 'admin',
  doctor = 'doctor',
  patient = 'patient',
}

export enum EGender {
  male = 'male',
  female = 'female',
  other = 'other',
}

export enum ELanguage {
  vn = 'vn',
  us = 'us',
  other = 'other',
}

@Entity()
@ObjectType()
export class UserEntity extends RootObject {
  @Field()
  @Column({ length: 200, nullable: false })
  firstname: string;

  @Field()
  @Column({ length: 200, nullable: false })
  lastname: string;

  @Field()
  @Column({ unique: true })
  email: string;

  @Field()
  @Column({ length: 200 })
  password: string;

  @Field()
  @Column({ unique: true })
  phone: string;

  @Field()
  @Column({ default: new Date() })
  dob: Date;

  @Field()
  @Column({
    default: EGender.other,
    type: 'enum',
    enum: EGender,
  })
  gender: EGender;

  @Field()
  @Column({
    default: ERoles.patient,
    type: 'enum',
    enum: ERoles,
  })
  roles: ERoles;

  @Field((type) => [AddressEntity])
  @OneToMany((type) => AddressEntity, (address) => address.user, {
    eager: true,
  })
  addresses: AddressEntity[];

  @Field((type) => [AssetesEntity])
  @OneToMany((type) => AssetesEntity, (asset) => asset.user, {
    eager: true,
  })
  avatar: AssetesEntity[];

  @Field((type) => [LogMedicalEntity])
  @OneToMany((type) => LogMedicalEntity, (log) => log.createdby, {
    eager: true,
  })
  log: LogMedicalEntity[];

  @Field({ description: 'Only using authentication' })
  access_token: string;

  @Field({ description: 'Only using authentication' })
  refresh_token: string;
}
