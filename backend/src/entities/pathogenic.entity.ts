import { Field, ObjectType } from '@nestjs/graphql';
import { RootObject } from 'src/containers/objects.root';
import { Column, Entity, ManyToMany } from 'typeorm';
import { PatientEntity } from './patient.entity';

@Entity()
@ObjectType()
export class PathogenicEntity extends RootObject {
  @Field((type) => [PatientEntity])
  @ManyToMany((type) => PatientEntity, (medical) => medical.pathogenic)
  patient: PatientEntity[];

  @Field({
    description: 'Yếu tố nguy cơ (nếu có YTNC khác thì nhập ở YTNC khác)',
  })
  @Column({ unique: true, nullable: true })
  name: string;

  @Field()
  @Column({ unique: true, nullable: true })
  description: string;
}
