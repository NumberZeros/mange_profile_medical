import { Field, ObjectType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
import { RootObject } from 'src/containers/objects.root';
import { DEFAULT_DATE } from 'src/services/packages';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { AddressEntity } from './addresses.entity';
import { CategoryEntity } from './categories.entity';
import { DoctorEntity } from './doctor.entity';
import { EmergencyEntity } from './emergency.entity';
import { GroupPatientEntity } from './group_patient.entity';
import { LogMedicalEntity } from './log_medical.entity';
import { MedicalEntity } from './medical.entity';
import { PathogenicEntity } from './pathogenic.entity';
import { PatientStatusEntity } from './patient_status.entity';
import { EGender } from './users.entity';

export enum ETransfer {
  hopital = 'hopital',
  healthfacilities = 'healthfacilities',
  single = 'single',
  followed = 'followed',
  other = 'other',
  no = 'no',
}

@Entity()
@ObjectType()
export class PatientEntity extends RootObject {
  @Field()
  @Column({ length: 200, nullable: false })
  firstname: string;

  @Field()
  @Column({ length: 200, nullable: false })
  lastname: string;

  @Field()
  @Column({ nullable: true })
  phone: string;

  @Field()
  @Column({ nullable: true })
  dob: Date;

  @Field()
  @Column({
    nullable: true,
    default: EGender.other,
    type: 'enum',
    enum: EGender,
  })
  gender: EGender;
  @Field((type) => [AddressEntity])
  @OneToMany((type) => AddressEntity, (address) => address.user, {
    eager: true,
  })
  addresses: AddressEntity[];

  @Field((type) => [MedicalEntity])
  @OneToMany((type) => MedicalEntity, (profile) => profile.patient, {
    eager: true,
  })
  medicalprofile: MedicalEntity[];

  @Field((type) => [EmergencyEntity])
  @OneToMany((type) => EmergencyEntity, (emergency) => emergency.patient, {
    eager: true,
  })
  medicalemergency: EmergencyEntity[];

  @Field((type) => [PathogenicEntity], {
    description: 'Yếu tố nguy cơ (nếu có YTNC khác thì nhập ở YTNC khác)',
    deprecationReason: 'Hành chính',
  })
  @ManyToMany((type) => PathogenicEntity, (pathogenic) => pathogenic.patient, {
    eager: true,
  })
  @JoinTable()
  pathogenic: PathogenicEntity[];

  @Field((type) => [PathogenicEntity], {
    description: 'Yếu tố nguy cơ (nếu có YTNC khác thì nhập ở YTNC khác)',
    deprecationReason: 'Hành chính',
  })
  @Field({ description: 'ID Khác', deprecationReason: 'Hành chính' })
  @Column()
  otherid: string;

  @Field({ description: 'Phone farmily 1', deprecationReason: 'Hành chính' })
  @Column({ default: '' })
  phoneemerency1: string;

  @Field({ description: 'Phone farmily 2', deprecationReason: 'Hành chính' })
  @Column({ default: '' })
  phoneemerency2: string;

  @Field({ description: 'Phone farmily 3', deprecationReason: 'Hành chính' })
  @Column({ default: '' })
  phoneemerency3: string;

  @Field({
    description: 'Có bao nhiêu người thân sống chung nhà',
    deprecationReason: 'Hành chính',
  })
  @Column({ default: 1 })
  quantityfamily: number;

  @Field({ description: 'Địa chỉ', deprecationReason: 'Hành chính' })
  @Column({ length: 500, default: '' })
  name: string;

  @Field({
    description: 'Địa chỉ',
    deprecationReason: 'Hành chính',
    defaultValue: '',
  })
  @Column({ default: '' })
  streetline1: string;

  @Field({
    description: 'Địa chỉ',
    deprecationReason: 'Hành chính',
    defaultValue: '',
  })
  @Column({ default: '' })
  streetline2: string;

  @Field({ description: 'Địa chỉ', deprecationReason: 'Hành chính' })
  @Column({ default: '' })
  ward: string;

  @Field({ description: 'Địa chỉ', deprecationReason: 'Hành chính' })
  @Column({ default: '' })
  province: string;

  @Field({ description: 'Địa chỉ', deprecationReason: 'Hành chính' })
  @Column({ default: '' })
  city: string;

  @Field({
    description: 'Mã BN do địa phương cung cấp',
    deprecationReason: 'Hành chính',
  })
  @Column({ default: '' })
  localcode: string;

  @Field((type) => DoctorEntity, {
    description: 'Người đánh giá',
    deprecationReason: 'Tổng quan',
  })
  @ManyToOne((type) => DoctorEntity, (patient) => patient.patient, {
    eager: true,
  })
  doctor: DoctorEntity;

  @Field({
    description: 'Ngày có kết quả PRC dương tính với SARS-COVID 2',
    deprecationReason: 'Hành chính',
  })
  @Column({ nullable: true })
  firstdatecovid: Date;

  @Field((type) => CategoryEntity, {
    description: 'Người đánh giá',
    deprecationReason: 'Tổng quan',
  })
  @IsString()
  @ManyToOne((type) => CategoryEntity, (category) => category.user, {
    eager: true,
  })
  category: CategoryEntity;

  @Field({
    description: 'Yếu tố nguy cơ khác',
    deprecationReason: 'Tổng quan',
  })
  @Column({ nullable: true })
  otherreason: string;

  @Field({
    description: 'Bệnh mạn tính trước đây',
    deprecationReason: 'Tổng quan',
  })
  @Column({ nullable: true })
  beforesick: string;

  @Field({
    description: 'Tình trạng bệnh mạn tính hiện tại',
    deprecationReason: 'Tổng quan',
  })
  @Column({ default: '' })
  notesatussick: string;

  @Field({
    description: 'Ngày có triệu chứng đầu tiên',
    deprecationReason: 'Tổng quan',
  })
  @Column({ default: new Date() })
  pathogenicdate: Date;

  @Field({
    description: 'Triệu chứng đầu tiên',
    deprecationReason: 'Tổng quan',
  })
  @Column({ default: '' })
  pathogenicname: string;

  @Field({
    description: 'Triệu chứng đầu tiên khác',
    deprecationReason: 'Tổng quan',
  })
  @Column({ default: '' })
  pathogenicnameother: string;

  @Field({
    description: `Nơi chuyển viện. Value: ${ETransfer.hopital}, ${ETransfer.followed}, ${ETransfer.healthfacilities}, ${ETransfer.single}, ${ETransfer.other},${ETransfer.no},`,
    deprecationReason: 'Tổng quan',
  })
  @Column({ default: ETransfer.other, enum: ETransfer })
  source: ETransfer;

  @Field({
    description: 'Số lần tiêm vác xin',
    deprecationReason: 'Tổng quan',
  })
  @Column({ default: 0, type: 'float' })
  quantittyvacin: number;

  @Field((type) => PatientStatusEntity, {
    description: 'Trạng thái hồ sơ',
    deprecationReason: 'Tổng quan',
  })
  @ManyToOne((type) => PatientStatusEntity, (status) => status.patient, {
    eager: true,
  })
  status: PatientStatusEntity;

  @Field({
    description: 'Trạng thái hồ sơ khi chọn khác',
    deprecationReason: 'Tổng quan',
  })
  @Column({ default: '' })
  otherstatus: string;

  @Field((type) => DoctorEntity, {
    description: 'Người đánh giá',
    deprecationReason: 'Tổng quan',
  })
  @ManyToOne((type) => DoctorEntity, (patient) => patient.sharepatient, {
    eager: true,
  })
  sharedoctor: DoctorEntity;

  @Field((type) => [LogMedicalEntity])
  @OneToMany((type) => LogMedicalEntity, (log) => log.patient, {
    eager: true,
  })
  log: LogMedicalEntity[];

  @Field({ description: 'Ngày có kết quả test nhanh dương tính đầu tiên' })
  @Column({ default: DEFAULT_DATE })
  firstdatequicktest: Date;

  @Field({
    description: 'Gợi ý chiều cao',
  })
  @Column({ default: 0, type: 'float' })
  height: number;

  @Field({
    description: 'Gợi ý cân nặng',
  })
  @Column({ default: 0, type: 'float' })
  weight: number;

  @Field((type) => [GroupPatientEntity], {
    description: 'Nhóm bệnh nhân',
  })
  @ManyToMany((type) => GroupPatientEntity, (group) => group.patient)
  @JoinTable()
  group: GroupPatientEntity[];
}
