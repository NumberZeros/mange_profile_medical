import { Field, ObjectType } from '@nestjs/graphql';
import { IsString } from 'class-validator';
import { Column, Entity, ManyToOne } from 'typeorm';
import { RootObject } from '../containers/objects.root';
import { UserEntity } from './users.entity';

@Entity()
@ObjectType()
export class AddressEntity extends RootObject {
  @Field()
  @IsString()
  @Column({ length: 500 })
  name: string;

  @Field()
  @IsString()
  @Column({ nullable: true })
  streetline1: string;

  @Field()
  @IsString()
  @Column({ nullable: true })
  streetline2: string;

  @Field()
  @IsString()
  @Column({ nullable: true })
  ward: string;

  @Field()
  @IsString()
  @Column({ nullable: true })
  province: string;

  @Field()
  @IsString()
  @Column({ nullable: true })
  city: string;

  @IsString()
  @ManyToOne((type) => UserEntity, (user) => user.addresses)
  user: UserEntity;
}
