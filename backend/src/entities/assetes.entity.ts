import { Field, ObjectType } from '@nestjs/graphql';
import { IsString, IsUrl } from 'class-validator';
import { Column, Entity, ManyToOne } from 'typeorm';
import { RootObject } from '../containers/objects.root';
import { UserEntity } from './users.entity';

@Entity()
@ObjectType()
export class AssetesEntity extends RootObject {
  @Field()
  @IsString()
  @Column({ length: 500 })
  name: string;

  @Field()
  @IsString()
  @Column({ nullable: true })
  decription: string;

  @Field()
  @IsUrl()
  @Column()
  url: string;

  @IsString()
  @ManyToOne((type) => UserEntity, (user) => user.avatar)
  user: UserEntity;
}
