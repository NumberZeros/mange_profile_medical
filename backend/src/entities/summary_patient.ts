import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class SummaryObject {
  @Field({ description: 'Kết quả xét nghiệm' })
  dataTableResultTest: string;

  @Field({ description: 'Tình trạng mạn tính hiện tại' })
  dataTableStatusQuo: string;

  @Field({ description: 'Thông số mạch' })
  dataTablePluse: string;

  @Field({ description: 'Thông số nhiệt độ' })
  dataTableTemperature: string;

  @Field({ description: 'Thông số spO2' })
  dataTableSpo2home: string;

  @Field({ description: 'Thông số tâm thu' })
  dataTableBloodpressure: string;

  @Field({ description: 'Thông số tâm trương' })
  dataTableBloodpressure2: string;
}
