import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, JoinColumn, ManyToMany, ManyToOne } from 'typeorm';
import { RootObject } from '../containers/objects.root';
import { DoctorEntity } from './doctor.entity';
import { MedicalEntity } from './medical.entity';

@Entity()
@ObjectType()
export class SMSEntity extends RootObject {
  @Field()
  @Column()
  messageid: string;

  @Field()
  @Column({ default: '' })
  description: string;

  @Field()
  @Column()
  authid: string;

  @Field((type) => MedicalEntity)
  @ManyToOne(() => MedicalEntity, (patient) => patient.redirect)
  @JoinColumn()
  medical: MedicalEntity;

  @Field((type) => DoctorEntity)
  @ManyToOne((type) => DoctorEntity, (doctor) => doctor.sms, {
    eager: true,
  })
  doctor: DoctorEntity;
}
