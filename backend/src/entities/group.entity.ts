import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, JoinTable, ManyToMany } from 'typeorm';
import { RootObject } from '../containers/objects.root';
import { DoctorEntity } from './doctor.entity';

@Entity()
@ObjectType()
export class GroupEntity extends RootObject {
  @Field()
  @Column({ default: '' })
  name: string;

  @Field()
  @Column({ default: '' })
  description: string;

  @Field({ description: 'Thời gian bắt đầu ca làm việc' })
  @Column({
    default: 0,
  })
  startwork: number;

  @Field({ description: 'Thời gian kết thúc ca làm việc' })
  @Column({
    default: 0,
  })
  endwork: number;

  @Field((type) => [DoctorEntity], { description: 'Danh sách các bác sĩ' })
  @ManyToMany(() => DoctorEntity, (doctor) => doctor.groups)
  @JoinTable()
  doctors: DoctorEntity[];
}
