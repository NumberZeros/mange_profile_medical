import { Field, ObjectType } from '@nestjs/graphql';
import { RootObject } from 'src/containers/objects.root';
import { Column, Entity, ManyToMany } from 'typeorm';
import { EmergencyEntity } from './emergency.entity';
import { MedicalEntity } from './medical.entity';

@Entity()
@ObjectType()
export class MedicineEntity extends RootObject {
  @ManyToMany((type) => MedicalEntity, (medical) => medical.medicinenow)
  medical: MedicalEntity[];

  @ManyToMany((type) => EmergencyEntity, (medical) => medical.medicines)
  emergency: EmergencyEntity[];

  @Field({ description: 'Tên thuốc' })
  @Column()
  name: string;

  @Field({ description: 'Số lượng' })
  @Column({ default: 0 })
  quantity: number;

  @Field()
  @Column({ default: '' })
  description: string;
}
