import { Field, ObjectType } from '@nestjs/graphql';
import { DEFAULT_DATE } from 'src/services/packages';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { RootObject } from '../containers/objects.root';
import { MedicalTrackEntity } from './medical_track.entity';
import { MedicineEntity } from './medicine_detail.entity';
import { PatientEntity } from './patient.entity';
import { SMSEntity } from './sms.entity';

export enum EStatus {
  negative = 'negative',
  positive = 'positive',
  other = 'other',
  no = 'no',
}

export enum EMedicalPackage {
  prc = 'prc',
  quicktest = 'quicktest',
  other = 'other',
  no = 'no',
}

export enum ERiskAssesment {
  low = 'low',
  medium = 'medium',
  high = 'high',
  hightest = 'hightest',
  no = 'no',
}

export enum EColor {
  normal = 'normal',
  haggard = 'haggard',
  typhus = 'typhus',
  no = 'no',
}

export enum ECAttribute {
  covidhide = 'codvidhide',
  codvidsimple = 'covidsimple',
  covidmedium = 'covidmedium',
  covidhight = 'covidhight',
  covidhightest = 'covidhightest',
  f1hidecovid = 'f1hidecovid',
  f1showcovid = 'f1showcovid',
  no = 'no',
}

@Entity()
@ObjectType()
export class MedicalEntity extends RootObject {
  @Field((type) => PatientEntity)
  @ManyToOne((type) => PatientEntity, (patient) => patient.medicalprofile)
  patient: PatientEntity;

  @Field({
    description: 'Chiều cao',
    deprecationReason: 'Theo dõi xét nghiệm & bệnh lý nền',
  })
  @Column({ type: 'float', default: 0 })
  weight: number;

  @Field({
    description: 'Cân nặng',
    deprecationReason: 'Theo dõi xét nghiệm & bệnh lý nền',
  })
  @Column({ type: 'float', default: 0 })
  height: number;

  @Field({
    description: `Đánh giá phân loại nguy cơ & xử trí: ${ERiskAssesment.low}, ${ERiskAssesment.medium}, ${ERiskAssesment.high}, ${ERiskAssesment.hightest}, ${ERiskAssesment.no},`,
    deprecationReason: 'Theo dõi xét nghiệm & bệnh lý nền',
  })
  @Column({ enum: ERiskAssesment, default: ERiskAssesment.no })
  riskassessment: ERiskAssesment;

  @Field({
    description: 'Thuốc đang sử dụng:',
    deprecationReason: 'Theo dõi xét nghiệm & bệnh lý nền',
  })
  @Column({ default: '' })
  medicine: string;

  @Field({
    description: 'Thuốc đang dùng còn đủ bao nhiêu ngày?',
    deprecationReason: 'Theo dõi xét nghiệm & bệnh lý nền',
  })
  @Column({ type: 'float', default: 0 })
  countdateremains: number;

  @Field({
    description: 'Mặt hay môi tím tái',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal1: boolean;

  @Field({
    description: 'Cảm thấy đâu hoặc tức ngực nhiều không giảm',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal2: boolean;

  @Field({
    description:
      'Khó thở rất nhiều (ví dụ như thở mạnh, hụt hơi khó nói chuyện, khò khè nặng, cánh mũi phập phồng, hoặc cần phải sử dụng các cơ bên ngoài xung quanh ngực để cố gắng thở)',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal3: boolean;

  @Field({
    description: 'Mất định hướng không gian, lú lẫn (mới xuất hiện)',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal4: boolean;

  @Field({
    description: 'Bất tỉnh hoặc rất khó thức giấc',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal5: boolean;

  @Field({
    description:
      'Nói lắp hoặc khó nói (mới xuất hiện hoặc là nặng hơn nếu đã có trước đây)',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal6: boolean;

  @Field({
    description:
      'Co giật (mới xuất hiện hoặc là nặng hơn nếu đã có trước đây)]',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal7: boolean;

  @Field({
    description:
      'Tụt huyết áp (khó đứng dậy, hoa mắt, chóng mặt, choáng váng, cảm thấy da lạnh ẩm, tái nhợt)',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal8: boolean;

  @Field({
    description: 'Mất nước (miệng và môi khô, tiểu ít, mắt lõm)]',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal9: boolean;

  @Field({
    description: 'Không có',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal10: boolean;

  @Field({
    description:
      'Hôm nay ông/bà có cảm thấy điều gì khác thường trong cơ thể so với trước đây không?',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal11: boolean;

  @Field({
    description: 'Sốt (nóng, lạnh run, đổ mồ hôi)',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal12: boolean;

  @Field({
    description: 'Khó thở',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal13: boolean;

  @Field({
    description: 'Đau ngực nhẹ',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal14: boolean;

  @Field({
    description: 'Đau họng',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal15: boolean;

  @Field({
    description: 'Ho Khan hay đàm',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal16: boolean;

  @Field({
    description: 'Đau cơ hoặc đau mỏi người',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal17: boolean;

  @Field({
    description: 'Nôn ói',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal18: boolean;

  @Field({
    description: 'Tiêu chảy',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal19: boolean;

  @Field({
    description: 'Mất vị giác hoặc khứu giác mới xảy ra',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal20: boolean;

  @Field({
    description: 'Nghẹt mũi',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal21: boolean;

  @Field({
    description: 'Sổ mũi',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal22: boolean;

  @Field({
    description: 'Mệt mỏi',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal23: boolean;

  @Field({
    description: 'Đau nhức đầu',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal24: boolean;

  @Field({
    description: 'Không có',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: false })
  signal25: boolean;

  @Field({
    description: 'Triệu chứng khác',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: '' })
  otherSignal: string;

  @Field({
    description: 'Tình trạng bệnh mạn tính hiện tại',
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: '' })
  statusquo: string;

  @Field({
    description: `Màu da và môi: ${EColor.normal}, ${EColor.haggard}, ${EColor.typhus}, ${EColor.no}`,
    deprecationReason: 'Theo dõi hằng ngày',
  })
  @Column({ default: EColor.normal, enum: EColor })
  color: EColor;

  @Field({
    description: 'Mạch',
    deprecationReason: 'Kết luận',
  })
  @Column({ type: 'float', default: 0 })
  pluse: number;

  @Field({
    description: 'Ghi Chú (Mạch)',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: '' })
  notepluse: string;

  @Field({
    description: 'Huyết áp (Tâm thu)',
    deprecationReason: 'Kết luận',
  })
  @Column({ type: 'float', default: 0 })
  bloodpressure: number;

  @Field({
    description: 'Huyết áp(Tâm trương)',
    deprecationReason: 'Kết luận',
  })
  @Column({ type: 'float', default: 0 })
  bloodpressure2: number;

  @Field({
    description: 'Ghi chú (Huyết áp)',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: '' })
  notebloodpressure: string;

  @Field({
    description: 'SpO2 tại nhà',
    deprecationReason: 'Kết luận',
  })
  @Column({ type: 'float', default: 0 })
  spo2home: number;

  @Field({
    description: 'Ghi chú (SpO2 tại nhà)',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: '' })
  notespo2home: string;

  @Field({
    description: 'Nhiệt độ',
    deprecationReason: 'Kết luận',
  })
  @Column({ type: 'float', default: 0 })
  temperature: number;

  @Field({
    description: 'Ghi chú (Nhiệt độ)',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: '' })
  notetemperature: string;

  @Field({
    description: 'Khác',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: '' })
  other: string;

  @Field({
    description: `Đánh giá mức độ hiện tại: 
    ${ECAttribute.covidhide}, 
    ${ECAttribute.codvidsimple}, 
    ${ECAttribute.covidmedium}, 
    ${ECAttribute.covidhight}, 
    ${ECAttribute.covidhightest},
    ${ECAttribute.f1hidecovid},
    ${ECAttribute.f1showcovid},
    ${ECAttribute.no},
    `,
    deprecationReason: 'Kết luận',
  })
  @Column({ default: ECAttribute.no, enum: ECAttribute })
  attribute: ECAttribute;

  @Field({
    description: 'Ghi chú (Bệnh lý khác, bệnh lý nền)',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: '' })
  noteattribute: string;

  @Field({
    description: 'Bệnh ngày thứ mấy',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: 0, type: 'float' })
  quantityofsick: number;

  @Field({
    description: 'Cách xử lý, hướng dẫn bệnh nhân/người nhà tự chăm sóc',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: 0 })
  guide: string;

  @Field({
    description: 'Tình trạng/bệnh lý khác kèm theo (miêu tả ngắn gọn)',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: 0 })
  summary: string;

  @Field({
    description: 'Liên hệ hiện trường',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: false })
  contacdescription: boolean;

  @Field({
    description: 'Có tái khám không',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: false })
  reexamination: boolean;

  @Field({
    description: 'Thời gian tái khám',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: DEFAULT_DATE })
  dateofreexamination: Date;

  @Field((type) => [MedicineEntity], {
    description: 'Danh sách thuốc',
    deprecationReason: 'Hành chính',
  })
  @ManyToMany(
    (type) => MedicineEntity,
    (medicaldetail) => medicaldetail.medical,
    {
      eager: true,
    },
  )
  @JoinTable()
  medicinenow: MedicineEntity[];

  @Field((type) => [MedicalTrackEntity])
  @OneToMany(() => MedicalTrackEntity, (track) => track.medical, {
    eager: true,
  })
  @JoinTable()
  medicaltrack: MedicalTrackEntity[];

  @Field({
    description: 'Thời gian thực tạo bệnh án',
    deprecationReason: 'Kết luận',
  })
  @Column({ default: new Date() })
  displaydate: Date;

  @Field((type) => [SMSEntity])
  @OneToMany((type) => SMSEntity, (redirect) => redirect.medical, {
    eager: true,
  })
  redirect: SMSEntity[];

  @Field({
    description: 'Nhận bệnh án từ team 2',
  })
  @Column({ default: false })
  issupport: boolean;
}
