import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { useQuery } from "@apollo/client";
import jwt_decode from "jwt-decode";
import * as actions from "../../views/Authenticate/store/actions";
import * as apis from "../../views/Authenticate/store/api";
import cookie from "react-cookies";
import Cookies from "js-cookie";
import { Clear } from "constants/logout";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";

const Auth = apis.authen;
const RefreshToken = apis.getRefreshToken;

export default function getAuthorize() {
  const history = useHistory();
  const dispatch = useDispatch();
  const [id, setId] = useState("");

  const { loading, error, data, refetch } = useQuery(Auth, {
    variables: { id: id?.id },
    errorPolicy:"all"
  });
  
  const [refresh, resRefresh] = useMyLazyQuery(RefreshToken, {
    onCompleted: () => {
      if (resRefresh) {
        dispatch(actions.handleGetRefreshSuccess(resRefresh.data.get_refresh_token));
      } else dispatch(actions.handleGetRefreshSuccess());
    },
  });

  useEffect(() => {
    if (Cookies.get("access_token")) {
      setId(jwt_decode(Cookies.get("access_token")));
      if (
        window.sessionStorage.getItem("id") === null ||
        window.sessionStorage.getItem("id") === "undefined"
      )
        window.sessionStorage.setItem("id", id.id);
      dispatch(actions.handleAuth(jwt_decode(Cookies.get("access_token"))));
      if (!loading && data) {
        if (data) dispatch(actions.handleAuthSuccess(data.author));
        else {
          dispatch(actions.handleAuthFail());
          Clear(history);
        }
      } else if (error) {
        if(error.message === "jwt expired"){
          dispatch(actions.handleGetRefresh())
          refresh({refresh_token: Cookies.get("refresh_token")})
        }
        dispatch(actions.handleAuthFail());
        Clear(history);
      }
    } else Clear(history);
  }, [data]);
  return data;
}

