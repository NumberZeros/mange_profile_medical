import { client } from "redux/config";
import Cookies from "js-cookie";

export function Clear(history){
  client.cache.reset();
  // window.sessionStorage.removeItem("access_token")
  // window.sessionStorage.removeItem("refresh_token")
  // Cookies.remove("sv")
  Cookies.remove("access_token");
  Cookies.remove("refresh_token");
  history.push("/detect")
}