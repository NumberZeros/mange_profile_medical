import { createAction } from "redux-actions";

export const handleOpenForm = createAction("DOCTOR/OPEN_FORM");
export const handleCloseForm = createAction("DOCTOR/CLOSE_FORM");

export const handleOpenAddress = createAction("ADDRESS/OPEN_ADDRESS");
export const handleCloseAddress = createAction("ADDRESS/CLOSE_ADDRESS");

export const handleAddAddress = createAction("ADDRESS/ADD_ADDRESS");
export const handleAddAddressSuccess = createAction("ADDRESS/ADD_ADDRESS_SUCCESS");
export const handleAddAddressFail = createAction("ADDRESS/ADD_ADDRESS_FAIL");
