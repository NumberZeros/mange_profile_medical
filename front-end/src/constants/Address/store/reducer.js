/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "Addresses";

const initialState = freeze({
  error: null,
  isWrong: null,
  isSuccess: null,
  isLoading: null,
  isOpenNoti: true,
  isOpenAddress: false,
  isOpenForm: false,
  titleNoti: "",
  idUser: "",
  message: "",
});

export default handleActions(
  {
    [actions.handleOpenAddress]: (state, actions) => {
      return freeze({
        ...state,
        isOpenAddress: true,
        idUser: actions.payload,
      });
    },
    [actions.handleCloseAddress]: (state, actions) => {
      return freeze({
        ...state,
        isOpenAddress: false,
        idUser: "",
      });
    },
  },
  initialState
);
