import { gql } from "@apollo/client";

export const addAddress = gql`
  mutation AddAddress(
    $name: String!, 
    $ward: String!, 
    $province: String!, 
    $city: String!, 
    $user: String! ) {
    add_address(input:{
      name: $name,
      ward: $ward,
      province: $province,
      city: $city,
      user: $user}){
        id,
        name,
        ward,
        province,
        city
      }
    }
`;

export const updateAddress = gql`
  mutation UpdateAddress(
    $id: String!
    $name: String!
    $ward: String!
    $province: String!
    $city: String!
    $user: String!
  ) {
    put_address(
      input: {
        name: $name
        ward: $ward
        province: $province
        city: $city
        id: $id
        user: $user
      }
    ) {
      id
      name
      ward
      province
      city
    }
  }
`;