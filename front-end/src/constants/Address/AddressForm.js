import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useForm, Controller } from "react-hook-form";
import * as actions from "../../views/DoctorsManagement/store/actions";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import * as apis from "./store/api";
import { useHistory } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";
import InputAdornment from "@material-ui/core/InputAdornment";
import ModalConfirm from "components/Modal/ModalConfirm";
// import { name } from "./store/reducer";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import AddressData from "assets/address.json";
import _ from "lodash";
import Autocomplete from "@material-ui/lab/Autocomplete";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    display: "flex",
    flexDirection: "column",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#50AF50",
    color: "white",
  },
  cancel: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#F44336",
    color: "white",
    marginRight: "1em",
  },
  left: {
    background: "#C7E5CE",
    height: "100vh",
    padding: "0 4em",
  },
  right: {
    padding: "3em 3em 0 3em",
  },
  copy: {
    justifyContent: "space-between",
  },
  banner: {
    textAlign: "center",
    marginTop: "20vh",
  },
  title: {
    textAlign: "center",
    marginTop: "1.2em",
    color: "#50AF50",
    fontSize: "2em",
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    "@media (max-width: 1024px)": {
      flexDirection: "column",
    },
  },
  logo: {
    marginBottom: "1.2em",
  },
  p: {
    marginRight: "0.4em",
    "@media (max-width: 900px)": {
      fontSize: "1rem",
    },
  },
  typo: {
    marginBottom: "0.2em",
    alignSelf: "center",
    marginLeft: "1em",
  },
  img: {
    "@media (max-width: 950px)": {
      width: "20em",
    },
  },
  top: {
    display: "flex",
  },
  body: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    flexGrow: "1",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
  label: {
    fontWeight: "bold",
  },
  value: {
    marginLeft: "0.2em",
  },
}));

const UPDATE_ADDRESS = apis.updateAddress;
const ADD_ADDRESS = apis.addAddress;

export default function AddressForm(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const [addAddress, { data, loading }] = useMutation(ADD_ADDRESS, {
    errorPolicy: "all",
  });
  const { idUser, isSuccess, message, doctor } = useSelector(
    (state) => state["Doctors"]
  );
  const [updateAddress, resAddress] = useMutation(UPDATE_ADDRESS, {
    errorPolicy: "all",
  });

  const [openNoti, setOpenNoti] = useState(false);
  const [ip1, setIp1] = useState("");
  const [ip2, setIp2] = useState("");
  const [ip3, setIp3] = useState("");
  const [ip4, setIp4] = useState("");
  const [ip5, setIp5] = useState("");
  const [err6, setErr6] = useState(false);
  const [err1, setErr1] = useState(false);
  const [err3, setErr3] = useState(false);
  const [err4, setErr4] = useState(false);
  const [err5, setErr5] = useState(false);
  const { register, handleSubmit, setValue, control } = useForm();
  const [cancelForm, setCancelForm] = useState(false);
  const [confirmForm, setConfirmForm] = useState(false);
  const [dataForm, setDataForm] = useState(null);

  const [selectedProvince, setSelectedProvince] = useState("");
  const [selectedCity, setSelectedCity] = useState("");
  const [selectedWard, setSelectedWard] = useState("");

  const handleUpdateAddress = () => {
    updateAddress({
      variables: {
        id: doctor.find_one_doctor.user.addresses[0].id,
        user: idUser.id,
        name: dataForm.name,
        // streetline1: dataForm.streetline1,
        // streetline2: dataForm.streetline2 || "",
        ward: dataForm.ward,
        province: dataForm.province,
        city: dataForm.city,
      },
    }).then((res) => {
      if (res.data === null || res.errors) {
        dispatch(actions.handleUpdateAddressFail(res.errors[0].message));
        setConfirmForm(false);
        dispatch(actions.handleOpenDoctorNoti());
        dispatch(actions.handleCloseAddress());
        // showNoti();
      } else {
        dispatch(actions.handleUpdateAddressSuccess(res));
        // history.push("/admin/doctors-management");
        setConfirmForm(false);
        dispatch(actions.handleOpenDoctorNoti());
        dispatch(actions.handleCloseAddress());
        // showNoti();
      }
    });
  };

  const confirmAddAddress = () => {
    dispatch(actions.handleAddAddress(data));
    addAddress({
      variables: {
        user: idUser.id,
        name: dataForm.name,
        // streetline1: dataForm.streetline1,
        // streetline2: dataForm.streetline2 || "",
        ward: dataForm.ward,
        province: dataForm.province,
        city: dataForm.city,
      },
    })
      .then((res) => {
        dispatch(actions.handleCloseAddress());
        dispatch(actions.handleAddAddressSuccess(res));
        setOpenNoti(true);
        setConfirmForm(false);
      })
      .catch((err) => {
        dispatch(actions.handleCloseAddress());
        dispatch(actions.handleAddAddressFail(err));
        setOpenNoti(false);
        setConfirmForm(false);
      });
  };
  const onSubmitAddress = (data) => {
    setDataForm({
      ...data,
      ward: selectedWard.ward,
      province: selectedProvince.province,
      city: selectedCity.city,
    });
  };
  
  const [listFoundProvince, setListFoundProvince] = useState([]);
  const [listFoundWard, setListFoundWard] = useState([]);

  const listCity = _.sortedUniq(AddressData.address.map((item) => item.city));
  const getCity = () => {
    return listCity.map((item) => {
      return { city: item };
    });
  };

  useEffect(() => {
    if (idUser.isEdit) {
      setValue("name", doctor.find_one_doctor.user.addresses[0].name);
      // setValue(
      //   "streetline1",
      //   doctor.find_one_doctor.user.addresses[0].streetline1
      // );
      // setValue(
      //   "streetline2",
      //   doctor.find_one_doctor.user.addresses[0].streetline2
      // );
      //// address checker
      if (_.find(getCity(),{city: doctor.find_one_doctor.user.addresses[0].city})) {
        setSelectedCity({
          city: doctor.find_one_doctor.user.addresses[0].city,
        });
        let provinceCheck = _.uniqBy(
          AddressData.address.map((item) => {
            if (item.city === doctor.find_one_doctor.user.addresses[0].city) {
              return {
                city: item.city,
                province: item.province,
              };
            }
          }),
          function (e) {
            if (e) return e.province;
          }
        )
        if (_.find(provinceCheck.filter(item=>item!=null,{province: doctor.find_one_doctor.user.addresses[0].province}))){
          setSelectedProvince({city: doctor.find_one_doctor.user.addresses[0].city, province: doctor.find_one_doctor.user.addresses[0].province});
          setListFoundProvince(
            _.uniqBy(
              AddressData.address.map((item) => {
                if (item.city === doctor.find_one_doctor.user.addresses[0].city) {
                  return {
                    city: item.city,
                    province: item.province,
                  };
                }
              }),
              function (e) {
                if (e) return e.province;
              }
            )
          );
          let wardCheck = AddressData.address
          .filter((item) => {
            if (
              item.city === doctor.find_one_doctor.user.addresses[0].city &&
              item.province === doctor.find_one_doctor.user.addresses[0].province
            )
              return item;
          })
          .map((item) => {
            return { ward: item.ward };
          }) 
          if(_.find(wardCheck,{ward: doctor.find_one_doctor.user.addresses[0].ward})){
            setSelectedWard({ward: doctor.find_one_doctor.user.addresses[0].ward})
            setListFoundWard(
              AddressData.address
                .filter((item) => {
                  if (
                    item.city ===doctor.find_one_doctor.user.addresses[0].city &&
                    item.province === doctor.find_one_doctor.user.addresses[0].province
                  )
                    return item;
                })
                .map((item) => {
                  return { ward: item.ward };
                })
            );
          } else setSelectedWard("");
        } else setSelectedProvince("");
      } else setSelectedCity("");

      setIp1(doctor.find_one_doctor.user.addresses[0].name);
      setIp2(doctor.find_one_doctor.user.addresses[0].streetline1);
    }
  }, []);

  // console.log(selectedCity);
  // console.log(selectedProvince);
  // console.log(selectedWard);
  ////// Addressss



  return (
    <div>
      <form
        key="address_form"
        className={classes.form}
        style={{
          padding: "3em",
          overflow: "auto",
          background: "#eee",
          borderRadius: "0.4em",
        }}
        onSubmit={handleSubmit(onSubmitAddress)}
        noValidate
      >
        <Typography className={classes.typo} component="h1" variant="h5">
          {doctor?.find_one_doctor?.user?.addresses[0]?.id
            ? "SỬA ĐỊA CHỈ"
            : "THÊM ĐỊA CHỈ"}
        </Typography>

        <Controller
          control={control}
          name="province"
          render={({ field: { onChange, onBlur, value, ref } }) => (
            <Autocomplete
              options={getCity()}
              {...register("province")}
              fullWidth
              value={selectedCity}
              onChange={(option, value) => {
                setSelectedWard("");
                setSelectedProvince("");
                if (value !== null) {
                  setSelectedCity(value);
                  setListFoundProvince(
                    _.uniqBy(
                      AddressData.address.map((item) => {
                        if (item.city === value.city) {
                          return {
                            city: item.city,
                            province: item.province,
                          };
                        }
                      }),
                      function (e) {
                        if (e) return e.province;
                      }
                    )
                  );
                } else setSelectedCity("");
              }}
              getOptionLabel={(option) => option.city}
              renderInput={(params) => (
                <TextField
                  fullWidth
                  {...params}
                  label="Tỉnh/ Thành phố"
                  variant="outlined"
                />
              )}
            />
          )}
        />

        <Controller
          control={control}
          name="city"
          render={({ field: { onChange, onBlur, value, ref } }) => (
            <Autocomplete
              options={listFoundProvince.filter((item) => item != null)}
              {...register("city")}
              style={{ marginTop: "1.2em" }}
              fullWidth
              disabled={selectedCity.length === 0}
              value={selectedProvince}
              onChange={(option, value) => {
                setSelectedWard("");
                if (value !== null) {
                  setSelectedProvince(value);
                  setListFoundWard(
                    AddressData.address
                      .filter((item) => {
                        if (
                          item.city === value.city &&
                          item.province === value.province
                        )
                          return item;
                      })
                      .map((item) => {
                        return { ward: item.ward };
                      })
                  );
                } else setSelectedProvince("");
              }}
              getOptionLabel={(option) => option.province}
              renderInput={(params) => (
                <TextField
                  fullWidth
                  {...params}
                  label="Quận/ Huyện/ Thị xã"
                  variant="outlined"
                />
              )}
            />
          )}
        />

        <Controller
          control={control}
          name="ward"
          margin="normal"
          render={({ field: { onChange, onBlur, value, ref } }) => (
            <Autocomplete
              options={listFoundWard.filter((item) => item != null)}
              {...register("ward")}
              fullWidth
              disabled={
                selectedCity.length === 0 || selectedProvince.length === 0
              }
              style={{ marginTop: "1.2em" }}
              value={selectedWard}
              onChange={(option, value) => {
                if (value !== null) setSelectedWard(value);
                else setSelectedWard("");
              }}
              getOptionLabel={(option) => option.ward}
              renderInput={(params) => (
                <TextField
                  fullWidth
                  {...params}
                  label="Xã/ Phường/ Thị trấn"
                  variant="outlined"
                />
              )}
            />
          )}
        />

        <Controller
          control={control}
          name="name"
          render={({ field: { onChange, onBlur, value, ref } }) => (
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              {...register("name")}
              label="Số nhà"
              InputProps={{
                startAdornment: (
                  <InputAdornment
                    style={{ marginLeft: "-0.1em" }}
                    position="end"
                  ></InputAdornment>
                ),
              }}
              id="name"
              onChange={(e) => setValue("name", e.target.value)}
              onInput={(e) => {
                if (e.target.value.length > 0) setErr6(false);
                else setErr6(true);
                setIp1(e.target.value);
              }}
              error={err6}
              helperText={err6 && "Vui lòng nhập số nhà"}
              value={value}
            />
          )}
        />

        <div style={{ display: "flex" }}>
          <Button
            onClick={() => setCancelForm(true)}
            fullWidth
            variant="contained"
            className={classes.cancel}
          >
            HỦY
          </Button>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            className={classes.submit}
            disabled={
              ip1.length === 0 ||
              err6 ||
              selectedProvince.length === 0 ||
              selectedCity.length === 0 ||
              selectedWard.length === 0
            }
            onClick={() => setConfirmForm(true)}
          >
            {idUser.isEdit ? "LƯU" : "THÊM"}
          </Button>
        </div>
      </form>
      <ModalConfirm
        title="Xác nhận"
        open={confirmForm}
        description={
          idUser.isEdit
            ? "Bạn có muốn cập nhật địa chỉ này?"
            : "Bạn có muốn thêm địa chỉ này?"
        }
        disableBtn={loading || resAddress.loading}
        handleOk={() => {
          idUser.isEdit ? handleUpdateAddress() : confirmAddAddress();
        }}
        handleClose={() => setConfirmForm(false)}
      ></ModalConfirm>
      <ModalConfirm
        title="Hủy"
        open={cancelForm}
        description="Bạn có muốn hủy thao tác?"
        handleOk={() => {
          dispatch(actions.handleCloseAddress());
          setCancelForm(false);
        }}
        handleClose={() => setCancelForm(false)}
      ></ModalConfirm>
    </div>
  );
}
