import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";

function getModalStyle() {
  return {
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: "#eee",
    borderRadius: "0.3em",
    "@media (max-width: 400px)": {
      width:"100%"
    },
  },
  title: {
    padding: "1em 1em 0 1em",
    marginBottom: "0.3em",
    fontWeight: "400",
    marginTop: "0",
    fontSize: "1.4em",
    color: "#0A3A34",
    "@media (max-width: 400px)": {
      fontWeight: "300",
      fontSize: "1.2em",
    },
  },
  description: {
    fontSize: "1em",
    marginTop: "0",
    marginBottm: "0.5em",
    padding: "1em 0 1em 0",
    borderTop: "1px solid lightgray",
    borderBottom: "1px solid lightgray",
    "@media (max-width: 400px)": {
      fontSize: "0.8em",
    },
  },
  content: {
    margin: "0 1em 0 1.3em",
  },
  footer: {
    padding: "0.5em 1em 0.5em 1em",
    display: "flex",
    justifyContent: "flex-end",
  },
  submit: {
    backgroundColor: "#50AF50",
    color: "white",
    boxShadow: "none",
    "&:hover": {
      backgroundColor: "#60BF50",
      boxShadow: "none",
    },
  },
  cancel: {
    backgroundColor: "#eee",
    color: "#F44336",
    marginRight: "0.4em",
    boxShadow: "none",
    border: "1px solid #F44336",
    "&:hover": {
      backgroundColor: "#F55446",
      boxShadow: "none",
      color: "white",
    },
  },
}));

export default function ModalConfirm(props) {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const body = (
    <div style={modalStyle} className={classes.paper}>
      <p className={classes.title}>{props.title || "Xác nhận"}</p>
      <div className={classes.description}>
        <p className={classes.content}>
          {props.description || "Bạn có chắc chắc muốn thực thi"}
        </p>
      </div>
      <div className={classes.footer}>
        <Button
          onClick={props.handleClose}
          variant="contained"
          className={classes.cancel}
          disabled={props.disableBtn}
        >
          HỦY
        </Button>
        <Button
          onClick={props.handleOk}
          variant="contained"
          className={classes.submit}
          disabled={props.disableBtn}
        >
          {props.disableBtn && (
            <CircularProgress
              style={{
                width: "1.6em",
                height: "1.6em",
                marginRight: "1em",
                color:"gray"
              }}
            />
          )}
          XÁC NHẬN
        </Button>
      </div>
    </div>
  );

  return (
    <div>
      <Modal
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
