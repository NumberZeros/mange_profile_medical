import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Pagination from "@material-ui/lab/Pagination";

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: "#eee",
    borderRadius: "0.3em",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 60,
  },
  ul: {
    "& .MuiPaginationItem-root": {
      color: "#444"
    },
    '& .Mui-selected': {
      backgroundColor: '#37774B',
      color:"#fff"
     },
  }
}));

export default function PaginationCustom(props) {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        placeItems: "center",
        justifyContent: "flex-end",
      }}
    >
      <FormControl className={classes.formControl}>
        <InputLabel
          id="demo-simple-select-label"
          style={{ whiteSpace: "nowrap" }}
        >
          Số lượng
        </InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={props.pageSize}
          onChange={(e) => props.handleChangeSize(e.target.value)}
          defaultValue={5}
        >
          <MenuItem value={5}>5</MenuItem>
          <MenuItem value={10}>10</MenuItem>
          <MenuItem value={20}>20</MenuItem>
          <MenuItem value={50}>50</MenuItem>
        </Select>
      </FormControl>

      <Pagination
        onChange={(e,value)=>props.handleChangePage(value-1)}
        classes={{ ul: classes.ul }}
        style={{ marginTop: "0.8em" }}
        count={props.count}
        page={props.page+1}
        shape="rounded"
      />
    </div>
  );
}
