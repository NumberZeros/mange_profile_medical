/*eslint-disable*/
import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
// core components
import styles from "assets/jss/material-dashboard-react/components/footerStyle.js";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(styles);

export default function Footer(props) {
  const classes = useStyles();
  return (
    <footer className={classes.footer}>
      <Grid container>
        <Grid item xs={12} sm={7} md={7}>
          <div className={classes.left}>
            <div className={classes.footerInfo}>
              <p className={classes.footerLeftLabel}>
                <span className={classes.infoSpan}>Cơ quan chủ quản:</span> Công
                ty TNHH Dịch vụ Giáo dục &amp; Y tế MedX
              </p>
            </div>
            <div className={classes.footerInfo}>
              <p className={classes.footerLeftLabel}>
                <span className={classes.infoSpan}>Mã số thuế:</span> 0316785682
                do Sở Kế hoạch và Đầu tư thành phố Hồ Chí Minh cấp ngày 02 tháng
                04 năm 2021
              </p>
            </div>
            <div className={classes.footerInfo}>
              <p className={classes.footerLeftLabel}>
                <span className={classes.infoSpan}>Địa chỉ: </span>
                Tầng 5, Tòa nhà Songdo, 62A Phạm Ngọc Thạch, Phường Võ Thị Sáu,
                Quận 3, Thành phố Hồ Chí Minh, Việt Nam
              </p>
            </div>
            <div className={classes.footerInfo}>
              <p className={classes.footerLeftLabel}>
                <span className={classes.infoSpan}>Email: </span>
                <a
                  href="mailto:someone@example.com"
                  className={classes.contact}
                >
                  medxvn@gmail.com
                </a>
              </p>
            </div>
            <div className={classes.footerInfo}>
              <p className={classes.footerLeftLabel}>
                <span className={classes.infoSpan}>
                  Số điện thoại liên hệ:{" "}
                </span>
                <a className={classes.contact} href="tel:+84949138929">
                  0949 138929
                </a>
              </p>
            </div>
          </div>
        </Grid>
        <Grid item xs={12} sm={5} md={5} className={classes.cpr}>
          <p className={classes.right} style={{ display: "flex" }}>
            <div
              style={{ alignSelf: "center"}}
              variant="body2"
              color="textSecondary"
              align="center"
            >
              {"Copyright © "}
              <Link
                style={{ fontWeight: "bold" }}
                color="inherit"
                href="https://medx.vn"
                target="_blank"
              >
                medx.vn
              </Link>{" "}
              {new Date().getFullYear()}{"."}
            </div>
          </p>
        </Grid>
      </Grid>
      <Grid item span={12} style={{background:"#50AF50", color:"white", fontWeight:"400"}}>
        <p className={classes.bottomTitle} style={{ display: "flex", margin:"0", padding:"1.5em", placeContent:"center", textAlign:"center" }}>
          This website is owned and operated by MedX Education & Health Service
          Limited Liability Company.
        </p>
      </Grid>
    </footer>
  );
}
