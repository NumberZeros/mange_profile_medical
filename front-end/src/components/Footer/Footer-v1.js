/*eslint-disable*/
import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
// core components
import styles from "assets/jss/material-dashboard-react/components/footerStyle.js";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(styles);

export default function FooterOutSide(props) {
  const classes = useStyles();
  return (
    <footer
      className={classes.footer}
      style={{ position: "fixed", width: "100%", backgroundColor:"#eee" }}
    >
      <Grid container>
          <div className={classes.leftOut}>
            <div className={classes.footerInfo} >
              <span className={classes.infoSpan}>Số điện thoại liên hệ: </span>
              <a className={classes.contact} style={{marginLeft:"0.2em"}} href="tel:+84949138929">
                0949 138929
              </a>
            </div>
          </div>
        <Grid item xs={12} sm={5} md={5} className={classes.cpr}>
          <p className={classes.rightOut} style={{ display: "flex" }}>
            <Typography
              style={{ alignSelf: "center", margin:"0 auto" }}
              variant="body2"
              color="textSecondary"
              align="center"
            >
              {"Copyright © "}
              <Link
                style={{ fontWeight: "bold" }}
                color="inherit"
                href="https://medx.vn"
                target="_blank"
              >
                medx.vn
              </Link>{" "}
              {new Date().getFullYear()}
            </Typography>
          </p>
        </Grid>
      </Grid>
      <Grid
        item
        span={12}
        style={{ background: "#50AF50", color: "white", fontWeight: "400" }}
      >
        <p
          style={{
            display: "flex",
            fontSize:"0.8em",
            margin: "0",
            padding: "1.5em",
            placeContent: "center",
            textAlign: "center",
          }}
        >
          This website is owned and operated by MedX Education & Health Service
          Limited Liability Company.
        </p>
      </Grid>
    </footer>
  );
}
