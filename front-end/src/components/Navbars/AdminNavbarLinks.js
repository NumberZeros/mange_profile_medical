import React from "react";
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Hidden from "@material-ui/core/Hidden";
import Poppers from "@material-ui/core/Popper";
import Divider from "@material-ui/core/Divider";
// @material-ui/icons
import Person from "@material-ui/icons/Person";
import Notifications from "@material-ui/icons/Notifications";
import Dashboard from "@material-ui/icons/Dashboard";
import Search from "@material-ui/icons/Search";
import MuiAlert from "@material-ui/lab/Alert";
// core components
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import styles from "assets/jss/material-dashboard-react/components/headerLinksStyle.js";
import cookie from "react-cookies";
import { useHistory } from "react-router-dom";
import getAuthorize from "constants/authen/authen";
import Snackbar from "@material-ui/core/Snackbar";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../../views/Authenticate/store/actions";
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyles = makeStyles(styles);
export default function AdminNavbarLinks() {
  const dispatch = useDispatch();
  getAuthorize();
  const classes = useStyles();
  const history = useHistory();

  const {
    dataUserAuth,
    isSuccessLogin,
    message,
    titleNoti,
    isOpenNoti,
  } = useSelector((state) => state["Login"]);

  const [openNotification, setOpenNotification] = React.useState(null);

  const [openProfile, setOpenProfile] = React.useState(null);

  const handleClickNotification = (event) => {
    if (openNotification && openNotification.contains(event.target)) {
      setOpenNotification(null);
    } else {
      setOpenNotification(event.currentTarget);
    }
  };
  const handleCloseNotification = () => {
    setOpenNotification(null);
  };
  const handleClickProfile = (event) => {
    if (openProfile && openProfile.contains(event.target)) {
      setOpenProfile(null);
    } else {
      setOpenProfile(event.currentTarget);
    }
  };
  const handleCloseProfile = () => {
    setOpenProfile(false);
  };
  const handleLogOut = () => {
    // window.sessionStorage.removeItem("access_token")
    cookie.remove("access_token", { path: "/" });
    cookie.remove("refresh_token", { path: "/" });
    cookie.remove("access_token", { path: "/admin" });
    cookie.remove("refresh_token", { path: "/admin" });
    history.push("/login");
    location.reload();
  };
  return (
    <div>
      <div className={classes.searchWrapper}>
        <CustomInput
          formControlProps={{
            className: classes.margin + " " + classes.search,
          }}
          inputProps={{
            placeholder: "Search",
            inputProps: {
              "aria-label": "Search",
            },
          }}
        />
        <Button color="white" aria-label="edit" justIcon round>
          <Search />
        </Button>
      </div>
      <div className={classes.manager}>
        <Button
          color={window.innerWidth > 959 ? "transparent" : "white"}
          justIcon={window.innerWidth > 959}
          simple={!(window.innerWidth > 959)}
          aria-owns={openProfile ? "profile-menu-list-grow" : null}
          aria-haspopup="true"
          onClick={handleClickProfile}
          className={classes.buttonLink}
        >
          <Person className={classes.icons} />
          <Hidden mdUp implementation="css">
            <p className={classes.linkText}>Profile</p>
          </Hidden>
        </Button>
        <Poppers
          open={Boolean(openProfile)}
          anchorEl={openProfile}
          transition
          disablePortal
          className={
            classNames({ [classes.popperClose]: !openProfile }) +
            " " +
            classes.popperNav
          }
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="profile-menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom",
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleCloseProfile}>
                  <MenuList role="menu">
                    <MenuItem className={classes.dropdownItem}>
                      <b>
                        {dataUserAuth?.lastname?.toUpperCase()}{" "}
                        {dataUserAuth?.firstname?.toUpperCase()}
                      </b>
                    </MenuItem>
                    <MenuItem
                      onClick={() => {
                        history.push("/admin/user");
                        handleCloseProfile();
                      }}
                      className={classes.dropdownItem}
                    >
                      Thông tin
                    </MenuItem>
                    <MenuItem
                      onClick={() => {
                        history.push("/change-password");
                      }}
                      className={classes.dropdownItem}
                    >
                      Đổi mật khẩu
                    </MenuItem>
                    <Divider light />
                    <MenuItem
                      onClick={handleLogOut}
                      className={classes.dropdownItem}
                    >
                      Đăng xuất
                    </MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Poppers>
      </div>
      {isOpenNoti !== null && (
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={isOpenNoti}
          autoHideDuration={3000}
          onClose={() => dispatch(actions.closeNoti())}
        >
          <Alert severity="success">Đăng nhập thành công</Alert>
        </Snackbar>
      )}
    </div>
  );
}
