/*eslint-disable*/
import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles((theme) => ({}));

export default function MultiSelect(props) {
  return (
    <div>
      <TextField
        select
        name="userRoles"
        id="userRoles"
        variant="outlined"
        label="userRoles"
        SelectProps={{
          multiple: true,
        }}
      >
        <MenuItem value="admin">Admin</MenuItem>
        <MenuItem value="user1">User1</MenuItem>
        <MenuItem value="user2">User2</MenuItem>
      </TextField>
    </div>
  );
}
