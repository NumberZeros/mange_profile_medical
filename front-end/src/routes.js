// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import UserProfile from "views/Authenticate/UserProfile";
import Doctors from "views/DoctorsManagement/Doctors";
import Patients from "views/PatientsManagement/Patients";
import PatientDetail from "views/PatientsManagement/PatientDetail.js";
import ImportCsv from "views/ImportCsv/ImportCsv";
import DoctorGroup from "views/DoctorGroup/DoctorGroup";
import GroupIcon from "@material-ui/icons/Group";
import RemoteGroup from "views/RemoteGroup/RemoteGroup";

const dashboardRoutes = [
  {
    role: ["admin", "doctor"],
    isSliderBar: true,
    path: "/dashboard",
    name: "Trang chủ",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin",
  },
  {
    role: ["admin", "doctor"],
    isSliderBar: true,
    path: "/user",
    name: "Thông tin cá nhân",
    icon: Person,
    component: UserProfile,
    layout: "/admin",
  },
  {
    role: ["admin"],
    isSliderBar: true,
    path: "/doctors-management",
    name: "Quản lý bác sĩ",
    icon: "content_paste",
    component: Doctors,
    layout: "/admin",
  },
  {
    role: ["admin"],
    isSliderBar: true,
    path: "/doctor-group",
    name: "Nhóm bác sĩ",
    icon: GroupIcon,
    component: DoctorGroup,
    layout: "/admin",
  },
  {
    role: ["admin","doctor"],
    isSliderBar: true,
    path: "/patients-management",
    name: "Quản lý bệnh nhân",
    icon: LibraryBooks,
    component: Patients,
    layout: "/admin",
  },
  {
    role: ["admin"],
    isSliderBar: true,
    path: "/import-csv",
    name: "Nhập dữ liệu",
    icon: BubbleChart,
    component: ImportCsv,
    layout: "/admin",
  },
  {
    role: ["admin", "doctor"],
    isSliderBar: false,
    path: "/patients-management/:id",
    name: "Patient",
    icon: LibraryBooks,
    component: PatientDetail,
    layout: "/admin",
  },
  // {
  //   role: ["admin","doctor"],
  //   isSliderBar: false,
  //   path: "/hot-contact",
  //   name: "",
  //   icon: LibraryBooks,
  //   component: RemoteGroup,
  //   layout: "/admin",
  // },
];

export default dashboardRoutes;
