import React, { useEffect, useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Drawer from "@material-ui/core/Drawer";
import { Button } from "@material-ui/core";
import * as actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
import * as apis from "./store/api";
import getAuthorize from "constants/authen/authen";
import moment from "moment";
import { useQuery } from "@apollo/client";
import { useMutation } from "@apollo/client";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";
import TextField from "@material-ui/core/TextField";
import DeleteIcon from "@material-ui/icons/Delete";
import { DataGrid } from "@material-ui/data-grid";
import ModalConfirm from "components/Modal/ModalConfirm";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import axios from "axios";
import Cookies from "js-cookie";
import RefreshIcon from "@material-ui/icons/Refresh";
import MenuItem from "@material-ui/core/MenuItem";
import Popper from "@material-ui/core/Popper";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import MenuList from "@material-ui/core/MenuList";
import PropTypes from "prop-types";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import LibraryAddCheckIcon from "@material-ui/icons/LibraryAddCheck";
import DoctorForm from "views/DoctorsManagement/DoctorForm";
import RemoveRedEyeIcon from "@material-ui/icons/RemoveRedEye";
import PatientForm from "views/PatientsManagement/PatientForm";
import { select } from "react-cookies";
import { Q10, Q8, QBT, QO } from "../../config-url";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
function getModalStyle() {
  return {
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`,
  };
}

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      marginTop: "0",
      marginBottom: "0",
      fontSize: "1rem !important",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    fontSize: "1.5rem !important",
    "& small": {
      color: "#777",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  drawer: {
    width: "50vw",
  },
  backdrop: {
    zIndex: 4799,
    color: "#fff",
  },
  topBar: {
    background: "#50AF50 !important",
    display: "flex",
    justifyContent: "space-between",
  },
  searchBar: {
    background: "#ddd",
    borderRadius: "0.4em",
    border: "none !important",
    height: "fit-content",
    width: "-webkit-fill-available",
  },
  headerTable: {
    color: "#0A3A34",
    fontWeight: "bold",
    fontSize: "1rem",
  },
  bodyCell: {
    fontSize: "0.9rem",
    paddingLeft: "1em !important",
    "&:nth-child(1)": {
      paddingLeft: "0 !important",
    },
  },
  buttonAdd: {
    whiteSpace: "nowrap",
    background: "#37774B",
    color: "#eee",
    height: "3.9em",
    padding: "0.2em 1em 0.2em 1em",
    marginRight: "0.5em",
    marginLeft: "1em",
    "&:hover": {
      background: "#59996D",
    },
  },
  buttonGroup: {
    whiteSpace: "nowrap",
    background: "#37774B",
    color: "#eee",
    height: "3.9em",
    // padding: "0.2em 1em 0.2em 1em",
    marginRight: "0.5em",
    marginLeft: "1em",
  },
  buttonRefresh: {
    background: "#37774B",
    color: "#eee",
    "&:hover": {
      background: "#59996D",
    },
  },
  buttonUp: {
    background: "#37774B",
    marginBottom: "0.1em",
    color: "#eee",
    padding: "0.2em 1em 0.2em 1em",
    marginRight: "1em",
    marginTop: "0.4em",
    marginLeft: "1em",
    "&:hover": {
      background: "#59996D",
    },
  },
  btnDel: {
    color: "red",
    minWidth: "auto",
  },
  btnEdit: {
    color: "#37774B",
    minWidth: "auto",
  },
  selecttab: {
    color: "white",
  },
  bigIndicator: {
    backgroundColor: "white",
  },
  tab: {
    minWidth: 100, // a number of your choice
    width: 120, // a number of your choice
  },
  hoverico: {
    whiteSpace: "nowrap",
    background: "#37774B",
    padding: "0",
    color: "#eee",
    // height: "3.9em",
    "&:hover": {
      background: "#59996D",
    },
  },
  hoverselect: {
    whiteSpace: "nowrap",
    background: "#37774B",
    paddingLeft: "1em",
    paddingRight: "0.6em",
    borderRight: "none !important",
    width: "4.3rem",
    color: "#eee",
    // height: "3.9em",
    "&:hover": {
      background: "#59996D",
    },
  },
};
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};
const useStyles = makeStyles(styles);
const FECTH_ALL_DOCTOR = apis.findAllDoctors;
const EDIT_DOCTOR = apis.changeStatus;
const EDIT_PATIENT = apis.updatePatient;
const FECTH_ALL_PATIENTS = apis.findAllPatienss;
const VERIFY_DOCTOR = apis.verifyDoctor;
const UNVERIFY_DOCTOR = apis.unverifyDoctor;
const UNVERIFY_PATIENT = apis.unverifyPatient;

export default function ImportCsv() {
  getAuthorize();
  const [modalStyle] = React.useState(getModalStyle);
  const classes = useStyles();
  const dispatch = useDispatch();
  const [tabs, setTabs] = React.useState(0);
  const [isOpenFormPatient, setIsOpenFormPatient] = useState(false);
  const {
    listDoctors,
    listPatients,
    message,
    isSuccess,
    isOpenForm,
  } = useSelector((state) => state["Intergrate"]);
  const { dataUserAuth } = useSelector((state) => state["Login"]);

  const { loading, error, data, refetch } = useQuery(FECTH_ALL_DOCTOR, {
    variables: { adminid: window.sessionStorage.getItem("id") },
  });

  const [findAllPatients, resPatients] = useMyLazyQuery(FECTH_ALL_PATIENTS, {
    onCompleted: () => {
      if (resPatients) {
        dispatch(
          actions.handleFindAllIntergratePatientSuccess(resPatients.data)
        );
      } else dispatch(actions.handleFindAllIntergratePatientFail());
    },
    fetchPolicy: "network-only",
  });

  const handleFindDoctors = () => {
    dispatch(actions.handleFindAllDoctor());
    if (data) dispatch(actions.handleFindAllDoctorSuccess(data));
    else if (!data) dispatch(actions.handleFindAllDoctorFail());
  };

  const handleFindPatients = () => {
    dispatch(actions.handleFindAllIntergratePatient());
    findAllPatients({
      id: window.sessionStorage.getItem("id"),
    });
  };
  // const [findAllPatients, dataPatients] = useMyLazyQuery(findAllPatienss, {
  //   onCompleted: () => {
  //     if (dataPatients.data) {
  //       dispatch(actions.handleFindAllIntergratePatient(dataPatients.data));
  //     } else dispatch(actions.handleFindAllIntergratePatientFail());
  //     dataPatients.refetch();
  //   },
  //   fetchPolicy: "network-only",
  // });

  const [selectedAction, setSelectedAction] = useState(1);

  const [modalConfirm, setModalConfirm] = useState(false);
  const [modalConfirmMulti, setModalConfirmMulti] = useState(false);

  const [selectedUser, setSelectedUser] = useState(null);
  const [selectedPatient, setSelectedPatient] = useState(null);

  const [selectedDoctor, setSelectedDoctor] = useState(null);
  const [loadingUpload, setLoadingUpload] = useState(false);
  const [changeStatus, resEdit] = useMutation(EDIT_DOCTOR, {
    errorPolicy: "all",
  });

  const [verifyDoctor, resVerifyDoctor] = useMutation(VERIFY_DOCTOR, {
    errorPolicy: "all",
  });
  const [unverifyDoctor, resUnverifyDoctor] = useMutation(UNVERIFY_DOCTOR, {
    errorPolicy: "all",
  });
  const [unverifyPatient, resUnverifyPatient] = useMutation(UNVERIFY_PATIENT, {
    errorPolicy: "all",
  });
  const [updatePatient, resEditPatient] = useMutation(EDIT_PATIENT, {
    errorPolicy: "all",
  });

  const [openNoti, setOpenNoti] = useState(false);
  const [listSelectedDoctor, setListSelectedDoctor] = useState([]);
  const [listSelectedPatient, setListSelectedPatient] = useState([]);
  const [modalDelete, setModalDelete] = useState(false);
  const columnsPat = [
    {
      field: "name",
      headerName: "Họ và tên",
      minWidth: 200,
      flex: 0.5,
      headerClassName: classes.headerTable,
    },
    {
      field: "gender",
      headerName: "Giới tính",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "dob",
      headerName: "Ngày sinh",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "phone",
      headerName: "Số điện thoại",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "actions",
      headerName: `${" "}`,
      width: 150,
      headerClassName: classes.headerTable,
      // eslint-disable-next-line react/display-name
      renderCell: (params) => (
        <div>
          <Button
            className={classes.btnEdit}
            onClick={() => {
              setSelectedPatient(params);
              setIsOpenFormPatient(true);
            }}
          >
            <RemoveRedEyeIcon></RemoveRedEyeIcon>
          </Button>
          <Button
            className={classes.btnEdit}
            onClick={() => {
              setModalConfirm(true);
              setSelectedPatient(params);
            }}
          >
            <LibraryAddCheckIcon></LibraryAddCheckIcon>
          </Button>
          <Button
            className={classes.btnDel}
            onClick={() => {
              setSelectedUser(params);
              setModalDelete(true);
            }}
          >
            <DeleteIcon></DeleteIcon>
          </Button>
        </div>
      ),
    },
  ];

  const columnsDoctor = [
    {
      field: "name",
      headerName: "Họ và tên",
      flex: 0.5,
      minWidth: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "dob",
      headerName: "Ngày sinh",
      minWidth: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "email",
      headerName: "Email",
      minWidth: 250,
      flex: 0.5,
      headerClassName: classes.headerTable,
    },
    {
      field: "phone",
      headerName: "Số điện thoại",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "certificate",
      headerName: "Chứng chỉ",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "actions",
      headerName: `${" "}`,
      width: 150,
      headerClassName: classes.headerTable,
      // eslint-disable-next-line react/display-name
      renderCell: (params) => (
        <div>
          <Button
            className={classes.btnEdit}
            onClick={() => {
              dispatch(actions.handleOpenForm(params.id));
              setSelectedDoctor(params);
            }}
          >
            <RemoveRedEyeIcon></RemoveRedEyeIcon>
          </Button>
          <Button
            className={classes.btnEdit}
            onClick={() => {
              setModalConfirm(true);
              setSelectedUser(params);
            }}
          >
            <LibraryAddCheckIcon></LibraryAddCheckIcon>
          </Button>
          <Button
            className={classes.btnDel}
            onClick={() => {
              setSelectedUser(params);
              setModalDelete(true);
            }}
          >
            <DeleteIcon></DeleteIcon>
          </Button>
        </div>
      ),
    },
  ];

  const options = ["DUYỆT", "XÓA"];
  // const options = ["BÁC SĨ", "BỆNH NHÂN", "BỆNH ÁN"]
  const anchorRef = React.useRef(null);
  const [open, setOpen] = React.useState(false);

  const [selectedIndex, setSelectedIndex] = React.useState(0);

  const handleClick = () => {
    console.info(selectedIndex);
    setModalConfirmMulti(true);
  };

  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index);
    setOpen(false);
  };

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  useEffect(() => {
    handleFindDoctors();
  }, [data]);

  useEffect(() => {
    handleFindPatients();
  }, [resPatients]);

  const handleChange = (event, newValue) => {
    setTabs(newValue);
    console.log(tabs);
    if (newValue === 0) handleFindDoctors();
    else if (newValue === 1) handleFindPatients();
  };

  const onRefresh = () => {
    setLoadRefresh(true);
    if (tabs === 0) refetch();
    else if (tabs === 1) {
      dispatch(actions.handleFindAllIntergratePatient());
      findAllPatients({
        id: window.sessionStorage.getItem("id"),
      });
      if (resPatients) {
        dispatch(
          actions.handleFindAllIntergratePatientSuccess(resPatients.data)
        );
      } else dispatch(actions.handleFindAllIntergratePatientFail());
      // resPatients.refetch();
    }
    setTimeout(() => {
      setLoadRefresh(false);
    }, 600);
  };

  const [loadRefresh, setLoadRefresh] = useState(false);
  const [file, setFile] = useState(null);
  const [pageSizeDoc, setPageSizeDoc] = useState(5);
  const [pageSizePat, setPageSizePat] = useState(5);

  const handleVerifyMulti = () => {
    console.log(listSelectedPatient);
    if (tabs === 0) {
      if (selectedIndex === 0) {
        listSelectedDoctor?.map((item) => {
          dispatch(actions.handleVerifyDoctor());
          verifyDoctor({
            variables: {
              id: item,
              adminid: window.sessionStorage.getItem("id"),
              isverified: true,
            },
          })
            .then((res) => {
              dispatch(actions.handleVerifyDoctorSuccess(res));
              refetch();
              dispatch(actions.handleCloseForm(false));
              setModalConfirmMulti(false);
              setOpenNoti(true);
              // setOnloading(false);
            })
            .catch((err) => {
              // console.log(err);
              dispatch(actions.handleVerifyDoctorFail(err));
              setModalConfirmMulti(false);
              setOpenNoti(true);
              refetch();
              // setOnloading(false);
            });
        });
      } else if (selectedIndex === 1) {
        listSelectedDoctor?.map((item) => {
          dispatch(actions.handleUnverifyDoctor());
          unverifyDoctor({
            variables: {
              id: item,
              adminid: window.sessionStorage.getItem("id"),
            },
          })
            .then((res) => {
              dispatch(actions.handleUnverifyDoctorSuccess(res));
              refetch();
              dispatch(actions.handleCloseForm(false));
              setModalConfirmMulti(false);
              setOpenNoti(true);
              // setOnloading(false);
            })
            .catch((err) => {
              // console.log(err);
              dispatch(actions.handleUnverifyDoctorFail(err));
              setModalConfirmMulti(false);
              setOpenNoti(true);
              refetch();
              // setOnloading(false);
            });
        });
      }
    } else if (tabs === 1) {
      if (selectedIndex === 0) {
        listSelectedPatient?.map((item) => {
          dispatch(actions.handleVerifyPatient());
          updatePatient({
            variables: {
              id: item,
              isverified: true,
            },
          })
            .then((res) => {
              dispatch(actions.handleVerifyPatientSuccess(res));
              refetch();
              dispatch(actions.handleCloseForm(false));
              setModalConfirmMulti(false);
              setOpenNoti(true);
              // setOnloading(false);
            })
            .catch((err) => {
              // console.log(err);
              dispatch(actions.handleVerifyPatientFail(err));
              setModalConfirmMulti(false);
              setOpenNoti(true);
              refetch();
              // setOnloading(false);
            });
        });
      } else if (selectedIndex === 1) {
        listSelectedPatient?.map((item) => {
          dispatch(actions.handleUnverifyPatient());
          unverifyPatient({
            variables: {
              id: item,
            },
          })
            .then((res) => {
              dispatch(actions.handleUnverifyPatientSuccess(res));
              refetch();
              dispatch(actions.handleCloseForm(false));
              setModalConfirmMulti(false);
              setOpenNoti(true);
              // setOnloading(false);
            })
            .catch((err) => {
              // console.log(err);
              dispatch(actions.handleUnverifyPatientFail(err));
              setModalConfirmMulti(false);
              setOpenNoti(true);
              refetch();
              // setOnloading(false);
            });
        });
      }
    }
  };
  const svid = Cookies.get("sv").toString();

  const UPLOAD_AXIOS =
    svid === "1"
      ? QBT.UPLOAD_AXIOS
      : svid === "2"
      ? Q8.UPLOAD_AXIOS
      : svid === "3"
      ? Q10.UPLOAD_AXIOS
      : svid === "4"
      ? QO.UPLOAD_AXIOS
      : "";

  const UploadFile = async (file) => {
    try {
      const url =
        tabs === 0
          ? `${UPLOAD_AXIOS}/doctors`
          : tabs === 1
          ? `${UPLOAD_AXIOS}/patients`
          : `${UPLOAD_AXIOS}/medical`;
      const data = file;
      const formData = new FormData();
      formData.append("file", data);
      const res = await axios.post(
        url,
        formData
        // {
        //   headers:{
        //     // Authorization: 'Bearer ' + Cookies.get("access_token"),
        //     "Content-type": 'application/json;charset=UTF-8'
        //   },
        // }
      );
      return res;
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <GridContainer>
      <Card>
        <CardHeader color="primary" className={classes.topBar}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <Tabs
              value={tabs}
              classes={{ indicator: classes.bigIndicator }}
              onChange={handleChange}
              aria-label="disabled tabs example"
            >
              <Tab classes={{ root: classes.tab }} label="Bác sĩ" />
              <Tab classes={{ root: classes.tab }} label="Bệnh nhân" />
              <Tab classes={{ root: classes.tab }} label="Bệnh án" />
            </Tabs>

            <div style={{ display: "flex" }}>
              <div style={{ display: "flex" }}>
                <TextField
                  accept="csv/*"
                  className={classes.input}
                  style={{ display: "none" }}
                  id="contained-button-file"
                  multiple
                  type="file"
                  // onFocus={()=>setFile(null)}
                  onChange={(e) => {
                    setLoadingUpload(true);
                    UploadFile(e.target.files[0]);
                    setLoadingUpload(false);
                  }}
                />
                <label htmlFor="contained-button-file">
                  <Button
                    htmlFor="contained-button-file"
                    variant="contained"
                    className={classes.buttonAdd}
                    component="span"
                  >
                    <CloudUploadIcon
                      style={{ marginRight: "0.2em" }}
                    ></CloudUploadIcon>
                    TẢI LÊN
                  </Button>
                </label>
                <Button
                  onClick={() => onRefresh()}
                  className={classes.buttonRefresh}
                >
                  <RefreshIcon style={{ marginRight: "0.2em" }}></RefreshIcon>
                </Button>
                <div>
                  <ButtonGroup
                    variant="contained"
                    className={classes.buttonGroup}
                    ref={anchorRef}
                    aria-label="split button"
                  >
                    <Button
                      className={classes.hoverselect}
                      onClick={handleClick}
                    >
                      {options[selectedIndex]}
                    </Button>
                    <Button
                      className={classes.hoverico}
                      size="small"
                      aria-controls={open ? "split-button-menu" : undefined}
                      aria-expanded={open ? "true" : undefined}
                      aria-label="select merge strategy"
                      aria-haspopup="menu"
                      onClick={handleToggle}
                    >
                      <ArrowDropDownIcon />
                    </Button>
                  </ButtonGroup>
                  <Popper
                    open={open}
                    anchorEl={anchorRef.current}
                    role={undefined}
                    transition
                    disablePortal
                  >
                    {({ TransitionProps, placement }) => (
                      <Grow
                        {...TransitionProps}
                        style={{
                          transformOrigin:
                            placement === "bottom"
                              ? "center top"
                              : "center bottom",
                        }}
                      >
                        <Paper>
                          <ClickAwayListener onClickAway={handleClose}>
                            <MenuList id="split-button-menu">
                              {options.map((option, index) => (
                                <MenuItem
                                  key={option}
                                  disabled={index === 2}
                                  selected={index === selectedIndex}
                                  onClick={(event) =>
                                    handleMenuItemClick(event, index)
                                  }
                                >
                                  {option}
                                </MenuItem>
                              ))}
                            </MenuList>
                          </ClickAwayListener>
                        </Paper>
                      </Grow>
                    )}
                  </Popper>
                </div>
              </div>
            </div>
          </div>
        </CardHeader>
        <CardBody>
          <TabPanel value={tabs} index={0}>
            <div style={{ overflowY: "auto", width: "100%" }}>
              <div style={{ display: "flex", height: "100%" }}>
                <DataGrid
                  onSelectionModelChange={(params) => {
                    // console.log(params);
                    // console.log(
                    setListSelectedDoctor(params);
                  }}
                  loading={loading || loadRefresh}
                  rows={
                    !loadRefresh
                      ? listDoctors?.map((item) => {
                          return {
                            ...item,
                            id: item.id,
                            name: `${item.user.lastname} ${item.user.firstname} `,
                            dob: moment(item.user.dob).format("DD-MM-YYYY"),
                            email: item.user.email,
                            phone: item.user.phone,
                          };
                        })
                      : []
                  }
                  // key={}
                  getCellClassName={() => {
                    return classes.bodyCell;
                  }}
                  autoHeight={true}
                  columns={columnsDoctor}
                  checkboxSelection
                  pageSize={pageSizeDoc}
                  onPageSizeChange={(newPageSize) =>
                    setPageSizeDoc(newPageSize)
                  }
                  rowsPerPageOptions={[5, 10, 20]}
                  disableSelectionOnClick
                />
              </div>
            </div>
          </TabPanel>
          <TabPanel value={tabs} index={1}>
            <div style={{ overflowY: "auto", width: "100%" }}>
              <div style={{ display: "flex", height: "100%" }}>
                <DataGrid
                  loading={resPatients.loading || loadRefresh}
                  rows={
                    !loadRefresh
                      ? listPatients?.map((item) => {
                          return {
                            ...item,
                            name: `${item.lastname} ${item.firstname} `,
                            dob: moment(item.dob).format("DD-MM-YYYY"),
                            phone: item.phone,
                            createddat: moment(item.createddat).format(
                              "DD-MM-YYYY"
                            ),
                            gender:
                              item.gender === "male"
                                ? "Nam"
                                : item.gender === "female"
                                ? "Nữ"
                                : "Không xác định",
                          };
                        })
                      : []
                  }
                  getCellClassName={() => {
                    return classes.bodyCell;
                  }}
                  autoHeight={true}
                  columns={columnsPat}
                  checkboxSelection
                  pageSize={pageSizePat}
                  onPageSizeChange={(newPageSize) =>
                    setPageSizePat(newPageSize)
                  }
                  onSelectionModelChange={(params) => {
                    // console.log(params);
                    // console.log(
                    setListSelectedPatient(params);
                  }}
                  rowsPerPageOptions={[5, 10, 20]}
                  disableSelectionOnClick
                />
              </div>
            </div>
          </TabPanel>
          <TabPanel value={tabs} index={2}>
            <div style={{ overflowY: "auto", width: "100%" }}>
              Nhập dữ liệu bệnh án
            </div>
          </TabPanel>
        </CardBody>
      </Card>
      <Drawer
        anchor="right"
        open={isOpenForm}
        onClose={() => {
          dispatch(actions.handleCloseForm(true));
        }}
      >
        <DoctorForm doctor={selectedDoctor}></DoctorForm>
      </Drawer>
      <Drawer
        anchor="right"
        open={isOpenFormPatient}
        onClose={() => {
          setIsOpenFormPatient(false);
        }}
      >
        <PatientForm patient={selectedPatient}></PatientForm>
      </Drawer>
      <ModalConfirm
        title="Duyệt thông tin"
        open={modalConfirm}
        disableBtn={
          resEdit.loading || resEditPatient.loading || resVerifyDoctor.loading
        }
        description={
          tabs === 0
            ? "Bạn có muốn duyệt thông tin bác sĩ này?"
            : "Bạn có muốn duyệt thông tin bệnh nhân này?"
        }
        handleOk={() => {
          if (tabs === 0) {
            dispatch(actions.handleVerifyDoctor());
            verifyDoctor({
              variables: {
                id: selectedUser.id,
                adminid: window.sessionStorage.getItem("id"),
                isverified: true,
              },
            })
              .then((res) => {
                dispatch(actions.handleVerifyDoctorSuccess(res));
                refetch();
                dispatch(actions.handleCloseForm(false));
                setModalConfirm(false);
                setOpenNoti(true);
                // setOnloading(false);
              })
              .catch((err) => {
                console.log(err);
                dispatch(actions.handleVerifyDoctorFail(err));
                setModalConfirm(false);
                setOpenNoti(true);
                refetch();
                // setOnloading(false);
              });
          } else if (tabs === 1) {
            console.log(selectedPatient);
            dispatch(actions.handleVerifyPatient());
            updatePatient({
              variables: {
                id: selectedPatient.id,
                isverified: true,
              },
            })
              .then((res) => {
                dispatch(actions.handleVerifyPatientSuccess(res));
                dispatch(actions.handleFindAllIntergratePatient());
                findAllPatients({
                  id: window.sessionStorage.getItem("id"),
                });
                setModalConfirm(false);
                setOpenNoti(true);
                // setOnloading(false);
              })
              .catch((err) => {
                dispatch(actions.handleVerifyPatientFail(err));
                setModalConfirm(false);
                setOpenNoti(true);
                handleFindPatients();
                // setOnloading(false);
              });
          }
        }}
        handleClose={() => {
          setModalConfirm(false);
        }}
      ></ModalConfirm>
      <ModalConfirm
        title="Xóa thông tin"
        open={modalDelete}
        disableBtn={resUnverifyDoctor.loading || resUnverifyPatient.loading}
        description={
          tabs === 0
            ? "Bạn có muốn xóa thông tin bác sĩ này?"
            : "Bạn có muốn xóa thông tin bệnh nhân này?"
        }
        handleOk={() => {
          if (tabs === 0) {
            dispatch(actions.handleUnverifyDoctor());
            unverifyDoctor({
              variables: {
                id: selectedUser.id,
                adminid: window.sessionStorage.getItem("id"),
              },
            })
              .then((res) => {
                dispatch(actions.handleUnverifyDoctorSuccess(res));
                refetch();
                dispatch(actions.handleCloseForm(false));
                setModalDelete(false);
                setOpenNoti(true);
                // setOnloading(false);
              })
              .catch((err) => {
                console.log(err);
                dispatch(actions.handleUnverifyDoctorFail(err));
                setModalDelete(false);
                setOpenNoti(true);
                refetch();
                // setOnloading(false);
              });
          } else if (tabs === 1) {
            dispatch(actions.handleUnverifyPatient());
            unverifyPatient({
              variables: {
                id: selectedUser.id,
              },
            })
              .then((res) => {
                dispatch(actions.handleUnverifyPatientSuccess(res));
                dispatch(actions.handleFindAllIntergratePatient());
                findAllPatients({
                  id: window.sessionStorage.getItem("id"),
                });
                setModalDelete(false);
                setOpenNoti(true);
                // setOnloading(false);
              })
              .catch((err) => {
                dispatch(actions.handleUnverifyPatientFail(err));
                setModalDelete(false);
                setOpenNoti(true);
                handleFindPatients();
                // setOnloading(false);
              });
          }
        }}
        handleClose={() => {
          setModalDelete(false);
        }}
      ></ModalConfirm>
      <ModalConfirm
        title={selectedIndex === 0 ? "Duyệt thông tin" : "Xóa thông tin"}
        open={modalConfirmMulti}
        disableBtn={
          resVerifyDoctor.loading ||
          resEditPatient.loading ||
          resEditPatient.loading
        }
        description={
          selectedIndex === 0
            ? "Bạn có muốn duyệt thông tin này?"
            : "Bạn có muốn xóa thông tin này?"
        }
        handleOk={() => handleVerifyMulti()}
        handleClose={() => {
          setModalConfirmMulti(false);
        }}
      ></ModalConfirm>
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        autoHideDuration={3000}
        open={openNoti}
        onClose={() => setOpenNoti(false)}
      >
        <Alert severity={isSuccess ? "success" : "error"}>{message}</Alert>
      </Snackbar>
    </GridContainer>
  );
}
