import { createAction } from "redux-actions";

export const handleOpenForm = createAction("INTERGRATE/OPEN_FORM");
export const handleCloseForm = createAction("INTERGRATE/CLOSE_FORM");

// export const handleAddDoctor = createAction("INTERGRATE/ADD_DOCTOR");
// export const handleAddDoctorSuccess = createAction("INTERGRATE/ADD_DOCTOR_SUCCESS");
// export const handleAddDoctorFail = createAction("INTERGRATE/ADD_DOCTOR_FAIL");

export const handleFindAllDoctor = createAction("INTERGRATE/FIND_ALL_DOCTOR");
export const handleFindAllDoctorSuccess = createAction("INTERGRATE/FIND_ALL_DOCTOR_SUCCESS");
export const handleFindAllDoctorFail = createAction("INTERGRATE/FIND_ALL_DOCTOR_FAIL");

export const handleFindOneDoctor = createAction("INTERGRATE/FIND_ONE_DOCTOR");
export const handleFindOneDoctorSuccess = createAction("INTERGRATE/FIND_ONE_DOCTOR_SUCCESS");
export const handleFindOneDoctorFail = createAction("INTERGRATE/FIND_ONE_DOCTOR_FAIL");

export const handleVerifyDoctor = createAction("INTERGRATE/VERIFY_DOCTOR");
export const handleVerifyDoctorSuccess = createAction("INTERGRATE/VERIFY_DOCTOR_SUCCESS");
export const handleVerifyDoctorFail = createAction("INTERGRATE/VERIFY_DOCTOR_FAIL");

export const handleOpenAddress = createAction("DOCTOR/OPEN_ADDRESS");
export const handleCloseAddress = createAction("DOCTOR/CLOSE_ADDRESS");

export const handleFindAllIntergratePatient = createAction("INTERGRATE/FIND_ALL_PATIENT");
export const handleFindAllIntergratePatientSuccess = createAction("INTERGRATE/FIND_ALL_PATIENT_SUCCESS");
export const handleFindAllIntergratePatientFail = createAction("INTERGRATE/FIND_ALL_PATIENT_FAIL");

export const handleVerifyPatient = createAction("INTERGRATE/EDIT_PATIENT");
export const handleVerifyPatientSuccess = createAction("INTERGRATE/EDIT_PATIENT_SUCCESS");
export const handleVerifyPatientFail = createAction("INTERGRATE/EDIT_PATIENT_FAIL");

export const handleUnverifyPatient = createAction("INTERGRATE/UNVERIFY_PATIENT");
export const handleUnverifyPatientSuccess = createAction("INTERGRATE/UNVERIFY_PATIENT_SUCCESS");
export const handleUnverifyPatientFail = createAction("INTERGRATE/UNVERIFY_PATIENT_FAIL");

export const handleUnverifyDoctor = createAction("INTERGRATE/UNVERIFY_DOCTOR");
export const handleUnverifyDoctorSuccess = createAction("INTERGRATE/UNVERIFY_DOCTOR_SUCCESS");
export const handleUnverifyDoctorFail = createAction("INTERGRATE/UNVERIFY_DOCTOR_FAIL");