import { gql } from "@apollo/client";

export const addDoctor = gql`
  mutation CreateDoctor(
    $adminid: String!
    $firstname: String!
    $lastname: String!
    $email: String!
    $phone: String!
    $dob: DateTime!
    $certificate: String!
    $gender: String!
  ) {
    create_doctor(
      input: {
        adminid: $adminid
        firstname: $firstname
        lastname: $lastname
        email: $email
        certificate: $certificate
        phone: $phone
        dob: $dob
        gender: $gender
      }
    ) {
      id
      inactive
      certificate
      user {
        firstname
        lastname
        email
        phone
        dob
        gender
      }
    }
  }
`;

export const editDoctor = gql`
  mutation EditDoctor(
    $adminid: String!
    $id: String!
    $firstname: String!
    $lastname: String!
    $email: String!
    $phone: String!
    $dob: DateTime!
    $certificate: String!
    $gender: String!
    $isverified: String!
  ) {
    update_doctor(
      input: {
        id: $id
        adminid: $adminid
        firstname: $firstname
        lastname: $lastname
        email: $email
        phone: $phone
        dob: $dob
        certificate: $certificate
        gender: $gender
        isverified: $isverified
      }
    ) {
      id
      user {
        id
        firstname
        lastname
        email
        phone
        dob
        gender
        addresses {
          id
          name
          ward
          province
          city
        }
      }
      inactive
      isverified
      certificate
    }
  }
`;

export const changeStatus = gql`
  mutation EditDoctor(
    $adminid: String!
    $id: String!
    $isverified: Boolean!
    $inactive: Boolean!
    $firstname: String!
    $lastname: String!
    $email: String!
    $phone: String!
    $dob: DateTime!
    $certificate: String!
    $gender: String!
  ) {
    update_doctor(
      input: {
        id: $id
        adminid: $adminid
        isverified: $isverified
        inactive: $inactive
        firstname: $firstname
        lastname: $lastname
        email: $email
        certificate: $certificate
        phone: $phone
        dob: $dob
        gender: $gender
      }
    ) {
      id
      user {
        id
        firstname
        lastname
        email
        phone
        dob
        gender
        addresses {
          id
          name
          ward
          province
          city
        }
      }
      inactive
      isverified
      certificate
    }
  }
`;

export const findAllDoctors = gql`
  query FindAllDoctors($adminid: String!) {
    find_all_doctor(adminid: $adminid) {
      id
      user {
        id
        firstname
        lastname
        email
        phone
        dob
        gender
        addresses {
          id
          name
          ward
          province
          city
        }
      }
      inactive
      isverified
      certificate
    }
  }
`;

export const findAllPatienss = gql`
  query FindAllPatient($id: String!) {
    find_all_patient(id: $id) {
      id
      inactive
      createddat
      updatedat
      category {
        name
        id
      }
      pathogenicname
      pathogenicdate
      otherid
      otherreason
      beforesick
      firstdatecovid
      firstname
      lastname
      phone
      notesatussick
      dob
      gender
      quantityfamily
      phone
      category {
        id
        name
      }
      phoneemerency1
      phoneemerency2
      phoneemerency3
      pathogenic {
        id
        name
      }
      doctor {
        id
        user {
          firstname
          lastname
        }
        inactive
      }
      name
      isverified
      ward
      province
      city
    }
  }
`;

export const updatePatient = gql`
  mutation updatePatient($id: String!, $isverified: Boolean!) {
    verified_patient(input: { id: $id, isverified: $isverified }) {
      id
      inactive
      createddat
      updatedat
      category {
        name
        id
      }
      pathogenicname
      pathogenicdate
      otherid
      otherreason
      beforesick
      firstdatecovid
      firstname
      lastname
      phone
      notesatussick
      dob
      gender
      quantityfamily
      phone
      category {
        id
        name
      }
      phoneemerency1
      phoneemerency2
      phoneemerency3
      pathogenic {
        id
        name
      }
      doctor {
        id
        user {
          firstname
          lastname
        }
        inactive
      }
      name
      isverified
      ward
      province
      city
    }
  }
`;

export const verifyDoctor = gql`
  mutation updatePatient(
    $id: String!
    $isverified: Boolean!
    $adminid: String!
  ) {
    verified_doctor(
      input: { id: $id, isverified: $isverified, adminid: $adminid }
    ) {
      id
      user {
        id
        firstname
        lastname
        email
        phone
        dob
        gender
        addresses {
          id
          name
          ward
          province
          city
        }
      }
      inactive
      isverified
      certificate
    }
  }
`;

export const unverifyDoctor = gql`
  mutation updatePatient($id: String!, $adminid: String!) {
    unverified_doctor(input: { id: $id, adminid: $adminid }) {
      id
      user {
        id
        firstname
        lastname
        email
        phone
        dob
        gender
        addresses {
          id
          name
          ward
          province
          city
        }
      }
      inactive
      isverified
      certificate
    }
  }
`;

export const unverifyPatient = gql`
  mutation unverifyPatient($id: String!) {
    unverified_patient(input: { id: $id }) {
      id
      inactive
      createddat
      updatedat
      category {
        name
        id
      }
      pathogenicname
      pathogenicdate
      otherid
      otherreason
      beforesick
      firstdatecovid
      firstname
      lastname
      phone
      notesatussick
      dob
      gender
      quantityfamily
      phone
      category {
        id
        name
      }
      phoneemerency1
      phoneemerency2
      phoneemerency3
      pathogenic {
        id
        name
      }
      doctor {
        id
        user {
          firstname
          lastname
        }
        inactive
      }
      name
      isverified
      ward
      province
      city
    }
  }
`;
