/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "Intergrate";

const initialState = freeze({
  listDoctors: [],
  listPatients: [],
  isOpenModal: null,
  isOpenNoti: null,
  isSuccess: null,
  message: "",
});

export default handleActions(
  {
    [actions.handleOpenAddress]: (state, actions) => {
      return freeze({
        ...state,
        isOpenAddress: true,
        idUser: actions.payload,
      });
    },
    [actions.handleCloseAddress]: (state, actions) => {
      return freeze({
        ...state,
        isOpenAddress: false,
        idUser: "",
      });
    },
    [actions.handleOpenForm]: (state, actions) => {
      return freeze({
        ...state,
        isOpenForm: true,
        idLocation: actions.payload,
      });
    },
    [actions.handleCloseForm]: (state, actions) => {
      return freeze({
        ...state,
        isOpenForm: false,
        idLocation: "",
      });
    },

    [actions.handleFindAllDoctor]: (state, actions) => {
      return freeze({
        ...state,
        listDoctors: [],
        isLoading: true,
      });
    },
    [actions.handleFindAllDoctorSuccess]: (state, actions) => {
      // let listInverified = actions.payload.find_all_doctor.map(item=>{
      //   if(item.isverified === false) return item;
      //   else return
      // })
      return freeze({
        ...state,
        listDoctors: [...actions.payload.find_all_doctor.filter(item=>!item.isverified), ...state.listDoctors],
        isLoading: false,
      });
    },
    [actions.handleFindAllDoctorFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleVerifyDoctor]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleVerifyDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        doctor: {
          ...state.doctor,
          find_one_doctor: actions.payload.data.update_doctor,
        },

        listDoctors: state.listDoctors.map((item) => {
          if (item.id === actions.payload.data.verified_doctor.id) {
            return {
              ...actions.payload.data.verified_doctor,
            };
          } else return item;
        }),

        isLoading: false,
        isSuccess: true,
        message: "Duyệt thông tin thành công",
      });
    },
    [actions.handleVerifyDoctorFail]: (state, actions) => {
      console.log(actions)
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
        message: "Duyệt thông tin thất bại! Vui lòng kiểm tra lại thông tin.",
        // message: actions.payload.message.includes("Email")
        //   ? "Duyệt thất bại, email đã tồn tại!"
        //   : actions.payload.includes("Certificate")
        //   ? "Duyệt thất bại, chứng chỉ đã tồn tại"
        //   : "Duyệt thất bại, số điện thoại không hợp lệ hoặc đã tồn tại",
      });
    },
    [actions.handleFindOneDoctor]: (state, actions) => {
      return freeze({
        ...state,
        isLoading:
          actions.payload === state.doctor?.find_one_doctor.id ? false : true,
      });
    },
    [actions.handleFindOneDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        doctor: actions.payload,
        isLoading: false,
      });
    },
    [actions.handleFindOneDoctorFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    /////patient
    [actions.handleFindAllIntergratePatient]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindAllIntergratePatientSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listPatients: actions.payload.find_all_patient.filter(item=>!item.isverified && !item.inactive),
        isLoading: false,
      });
    },
    [actions.handleFindAllIntergratePatientFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleVerifyPatient]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleVerifyPatientSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listPatients: state.listPatients.filter(item=>item.id !== actions.payload.data.verified_patient.id),
        isLoading: false,
        isSuccess: true,
        message: "Duyệt thông tin thành công",
      });
    },
    [actions.handleVerifyPatientFail]: (state, actions) => {
      console.log(actions)
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
        message: "Duyệt thông tin thất bại! Vui lòng kiểm tra lại thông tin.",
        // message: actions.payload.message.includes("Email")
        //   ? "Duyệt thất bại, email đã tồn tại!"
        //   : actions.payload.includes("Certificate")
        //   ? "Duyệt thất bại, chứng chỉ đã tồn tại"
        //   : "Duyệt thất bại, số điện thoại không hợp lệ hoặc đã tồn tại",
      });
    },
    [actions.handleUnverifyPatient]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleUnverifyPatientSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listPatients: state.listPatients.filter(item=>item.id!==actions.payload.data.unverified_patient.id),
        isLoading: false,
        isSuccess: true,
        message: "Xóa thông tin thành công",
      });
    },
    [actions.handleUnverifyPatientFail]: (state, actions) => {
      console.log(actions)
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
        message: "Xóa thông tin thất bại! Vui lòng kiểm tra lại thông tin.",
        // message: actions.payload.message.includes("Email")
        //   ? "Duyệt thất bại, email đã tồn tại!"
        //   : actions.payload.includes("Certificate")
        //   ? "Duyệt thất bại, chứng chỉ đã tồn tại"
        //   : "Duyệt thất bại, số điện thoại không hợp lệ hoặc đã tồn tại",
      });
    },
    [actions.handleUnverifyDoctor]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleUnverifyDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: true,
        message: "Xóa thông tin thành công",
      });
    },
    [actions.handleUnverifyDoctorFail]: (state, actions) => {
      console.log(actions)
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
        message: "Xóa thông tin thất bại! Vui lòng kiểm tra lại thông tin.",
        // message: actions.payload.message.includes("Email")
        //   ? "Duyệt thất bại, email đã tồn tại!"
        //   : actions.payload.includes("Certificate")
        //   ? "Duyệt thất bại, chứng chỉ đã tồn tại"
        //   : "Duyệt thất bại, số điện thoại không hợp lệ hoặc đã tồn tại",
      });
    },
  },
  initialState
);
