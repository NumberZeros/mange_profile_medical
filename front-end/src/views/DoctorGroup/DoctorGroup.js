import React, { useEffect, useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Drawer from "@material-ui/core/Drawer";
import { Button } from "@material-ui/core";
import * as actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
import * as apis from "./store/api";
import getAuthorize from "constants/authen/authen";
import moment from "moment";
import { useMutation } from "@apollo/client";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";
import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { DataGrid } from "@material-ui/data-grid";
import Modal from "@material-ui/core/Modal";
import ModalConfirm from "components/Modal/ModalConfirm";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import PropTypes from "prop-types";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Autocomplete from "@material-ui/lab/Autocomplete";
import PlaylistAddIcon from "@material-ui/icons/PlaylistAdd";
import { useForm, Controller } from "react-hook-form";
import ClearIcon from "@material-ui/icons/Clear";
import GroupForm from "./GroupForm";
import Grid from "@material-ui/core/Grid";
import PlaylistAddCheckIcon from "@material-ui/icons/PlaylistAddCheck";
import PaginationCustom from "components/Pagination/Pagination";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
function getModalStyle() {
  return {
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`,
  };
}

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      marginTop: "0",
      marginBottom: "0",
      fontSize: "1rem !important",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    fontSize: "1.5rem !important",
    "& small": {
      color: "#777",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  drawer: {
    width: "50vw",
  },
  backdrop: {
    zIndex: 4799,
    color: "#fff",
  },
  topBar: {
    background: "#50AF50 !important",
    justifyContent: "space-between",
  },
  searchBar: {
    background: "#ddd",
    borderRadius: "0.4em",
    border: "none !important",
    height: "fit-content",
    width: "-webkit-fill-available",
  },
  headerTable: {
    color: "#0A3A34",
    fontWeight: "bold",
    fontSize: "1rem",
  },
  bodyCell: {
    fontSize: "0.9rem",
    paddingLeft: "1em !important",
    "&:nth-child(1)": {
      paddingLeft: "0 !important",
    },
  },
  buttonAdd: {
    whiteSpace: "nowrap",
    background: "#37774B",
    color: "#eee",
    height: "3.9em",
    padding: "0.2em 1em 0.2em 1em",
    marginRight: "0.5em",
    marginLeft: "1em",
    "&:hover": {
      background: "#59996D",
    },
  },
  buttonGroup: {
    whiteSpace: "nowrap",
    background: "#37774B",
    color: "#eee",
    height: "3.9em",
    // padding: "0.2em 1em 0.2em 1em",
    marginRight: "0.5em",
    marginLeft: "1em",
  },
  buttonRefresh: {
    background: "#37774B",
    color: "#eee",
    "&:hover": {
      background: "#59996D",
    },
  },
  buttonUp: {
    background: "#37774B",
    marginBottom: "0.1em",
    color: "#eee",
    "&:hover": {
      background: "#59996D",
    },
  },
  btnDel: {
    color: "red",
    minWidth: "auto",
  },
  btnEdit: {
    color: "#37774B",
    minWidth: "auto",
  },
  selecttab: {
    color: "white",
  },
  bigIndicator: {
    backgroundColor: "white",
  },
  tab: {
    minWidth: 100, // a number of your choice
    width: 120, // a number of your choice
  },
  hoverico: {
    whiteSpace: "nowrap",
    background: "#37774B",
    padding: "0",
    color: "#eee",
    // height: "3.9em",
    "&:hover": {
      background: "#59996D",
    },
  },
  hoverselect: {
    whiteSpace: "nowrap",
    background: "#37774B",
    paddingLeft: "1em",
    paddingRight: "0.6em",
    borderRight: "none !important",
    width: "4.3rem",
    color: "#eee",
    // height: "3.9em",
    "&:hover": {
      background: "#59996D",
    },
  },
  typo: {
    marginBottom: "0.2em",
    alignSelf: "center",
  },
  textAdvance: {
    marginRight: "0.3em",
  },
  buttonClear: {
    background: "#37774B",
    marginBottom: "0.1em",
    color: "#eee",
    padding: "1em",
    marginLeft: "0.2em",
    height: "max-content",
    "&:hover": {
      background: "#50AF50",
    },
  },
};

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const useStyles = makeStyles(styles);
const list = [
  {
    name: "Bác sĩ quận 8 - Ca 1",
    id: "1",
  },
];

const FIND_ALL = apis.findAllGroup;
const FIND_ALL_DOC = apis.findAdvanceDoctor;
const FIND_ONE_GROUP = apis.findOneGroup;
const ADD_DOCTOR = apis.addDoctor;
const REMOVE_DOCTOR = apis.removeDoctor;

export default function DoctorGroup() {
  getAuthorize();

  const {
    isOpenGroup,
    isSuccess,
    isOpenNoti,
    message,
    listGroup,
    listDoctors,
    pagination,
    group,
    listDoctorInGroup,
  } = useSelector((state) => state["Group"]);
  const { register, handleSubmit, setValue, control } = useForm();

  const dispatch = useDispatch();
  const [modalStyle] = React.useState(getModalStyle);
  const classes = useStyles();
  const [listGroups, setListGroups] = useState([]);
  const [selectedId, setSelectedId] = useState("");
  const [dataProps, setDataProps] = useState(null);
  const [addDoc, setAddDoc] = useState(false);
  const [nameGroup, setNameGroup] = useState("");
  const [listSelectedDoc, setListSelectedDoc] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [query, setQuery] = useState({
    firstname: "",
    lastname: "",
    dob: "",
    certificate: "",
    phone: "",
  });
  ////
  const [pageSize, setPageSize] = useState(5);
  const [page, setPage] = useState(0);

  const [pageSizeOne, setPageSizeOne] = useState(5);
  const [pageSizeDoc, setPageSizeDoc] = useState(5);
  const [modalRemove, setModalRemove] = useState(false);
  const [selectedDoctor, setSelectedDoctor] = useState("");

  const [findAll, resFindAll] = useMyLazyQuery(FIND_ALL, {
    onCompleted: () => {
      if (resFindAll.data) {
        dispatch(actions.handleFindAllGroupDoctorSuccess(resFindAll.data));
      } else dispatch(actions.handleFindAllGroupDoctorFail());
    },
    fetchPolicy: "network-only",
  });

  const [findAllDoctor, resFindAllDoc] = useMyLazyQuery(FIND_ALL_DOC, {
    onCompleted: () => {
      if (resFindAllDoc.data) {
        dispatch(actions.handleFindNoGroupDoctorsSuccess(resFindAllDoc.data));
      } else dispatch(actions.handleFindNoGroupDoctorsFail());
    },
    fetchPolicy: "network-only",
  });

  const [findOne, resFindOne] = useMyLazyQuery(FIND_ONE_GROUP, {
    onCompleted: () => {
      if (resFindOne.data) {
        dispatch(actions.handleFindOneGroupSuccess(resFindOne.data));
      } else dispatch(actions.handleFindOneGroupFail());
    },
    fetchPolicy: "network-only",
  });

  const [addDoctor, resAddDoctor] = useMutation(ADD_DOCTOR, {
    errorPolicy: "all",
  });

  const [removeDoctor, resRemoveDoctor] = useMutation(REMOVE_DOCTOR, {
    errorPolicy: "all",
  });

  useEffect(() => {
    dispatch(actions.handleFindAllGroupDoctor());
    findAll({ adminid: window.sessionStorage.getItem("id") });
  }, []);

  useEffect(() => {
    if (listGroup) setListGroups(listGroup);
  }, [listGroup]);

  const handleChangePage = (e) => {
    setPage(e);
  };

  const handleChangeSize = (e) => {
    setPageSizeDoc(e);
  };

  const onAdvanceSearch = (dataForm) => {
    setQuery({
      ...query,
      lastname: dataForm.lastname || "",
      firstname: dataForm.firstname || "",
      phone: dataForm.phone || "",
      dob: dataForm.dob || "",
      certificate: dataForm.certificate || "",
    });
  };

  useEffect(() => {
    findAllDoctor({
      adminid: window.sessionStorage.getItem("id"),
      doctorid: "",
      take: pageSizeDoc,
      skip: 0,
      firstname: query.firstname,
      lastname: query.lastname,
      phone: query.phone,
      dob: query.dob,
      certificate: query.certificate,
    });
    setPage(0);
  }, [pageSizeDoc]);

  useEffect(() => {
    findAllDoctor({
      adminid: window.sessionStorage.getItem("id"),
      take: pageSizeDoc,
      skip: page,
      firstname: query.firstname,
      lastname: query.lastname,
      phone: query.phone,
      dob: query.dob,
      certificate: query.certificate,
    });
  }, [page]);

  useEffect(() => {
    findAllDoctor({
      adminid: window.sessionStorage.getItem("id"),
      take: pageSizeDoc,
      skip: 0,
      firstname: query.firstname,
      lastname: query.lastname,
      phone: query.phone,
      dob: query.dob,
      certificate: query.certificate,
    });
    setPage(0);
  }, [query]);

  useEffect(() => {
    dispatch(actions.handleFindOneGroup());
    findOne({
      adminid: window.sessionStorage.getItem("id"),
      id: selectedId,
    });
  }, [selectedId, listDoctorInGroup]);

  useEffect(() => {
    setValue("firstname","")
    setValue("lastname","")
    setValue("phone","")
    setPageSizeDoc(5)
    setQuery({
      take: 5,
      skip: 0,
      doctorid: "",
      firstname: "",
      lastname: "",
      phone: "",
      certificate: "",
      dob: "",
    })
    findAllDoctor({
      adminid: window.sessionStorage.getItem("id"),
      take: 5,
      skip: 0,
      doctorid: "",
      firstname: "",
      lastname: "",
      phone: "",
      certificate: "",
      dob: "",
    });
  }, [addDoc]);

  const columns = [
    {
      field: "name",
      headerName: "Họ và tên",
      flex: 0.5,
      minWidth: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "dob",
      headerName: "Ngày sinh",
      minWidth: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "email",
      headerName: "Email",
      minWidth: 250,
      flex: 0.5,
      headerClassName: classes.headerTable,
    },
    {
      field: "phone",
      headerName: "Số điện thoại",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "certificate",
      headerName: "Chứng chỉ",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "actions",
      headerName: `${" "}`,
      width: 120,
      headerClassName: classes.headerTable,
      hide: addDoc,
      // eslint-disable-next-line react/display-name
      renderCell: (params) => (
        <div>
          <Button
            className={classes.btnDel}
            onClick={() => {
              setSelectedDoctor(params.id);
              setModalRemove(true);
            }}
          >
            <DeleteIcon></DeleteIcon>
          </Button>
        </div>
      ),
    },
  ];

  return (
    <GridContainer>
      <Card>
        <CardHeader color="primary" className={classes.topBar}>
          <GridContainer>
            <GridItem xs={8} sm={8} md={8} style={{ display: "flex" }}>
              <Autocomplete
                fullWidth
                disableClearable={true}
                onChange={(option, value) => {
                  setSelectedId(value.id);
                  setNameGroup(value.name);
                }}
                options={listGroups?.map((item) => {
                  return { name: item.name, id: item.id };
                })}
                getOptionLabel={(option) => option.name}
                renderInput={(params) => (
                  <TextField
                    fullWidth
                    style={{ background: "#eee", borderRadius: "0.4em" }}
                    {...params}
                    placeholder="Nhóm bác sĩ"
                    variant="outlined"
                  />
                )}
              />

              <Button
                className={classes.buttonUp}
                style={{ marginLeft: "0.5em" }}
                disabled={selectedId === ""}
                onClick={() => {
                  setDataProps(
                    listGroup.find((item) => item.id === selectedId)
                  );
                  dispatch(actions.handleOpenGroupForm());
                }}
              >
                <EditIcon></EditIcon>
              </Button>
              <Button
                className={classes.buttonUp}
                style={{ marginLeft: "0.5em" }}
                disabled={selectedId === ""}
                onClick={() => {
                  setAddDoc(true);
                  findAllDoctor({
                    adminid: window.sessionStorage.getItem("id"),
                    take: 5,
                    skip: 0,
                    doctorid: "",
                    firstname: "",
                    lastname: "",
                    phone: "",
                    certificate: "",
                    dob: "",
                  });
                }}
              >
                <PersonAddIcon></PersonAddIcon>
              </Button>
            </GridItem>
            <GridItem
              xs={4}
              sm={4}
              md={4}
              style={{
                display: "flex",
                alignSelf: "center",
                justifyContent: "flex-end",
              }}
            >
              <Button
                className={classes.buttonUp}
                style={{ marginLeft: "0.5em", height: "4em" }}
                onClick={() => {
                  setDataProps(null);
                  dispatch(actions.handleOpenGroupForm());
                }}
              >
                <PlaylistAddIcon></PlaylistAddIcon>
              </Button>
            </GridItem>
          </GridContainer>
        </CardHeader>
        <CardBody>
          <div>
            <div
              style={{ color: "#0A3A34", fontSize: "1.2em", margin: "0.5em 0" }}
            >
              <b>Mô tả: </b>
              {group?.description || "Không có mô tả"}
            </div>
            <div
              style={{ color: "#0A3A34", fontSize: "1.2em", margin: "0.5em 0" }}
            >
              <b>Giờ bắt đầu ca làm việc:</b> {group?.startwork} giờ
            </div>
            <div
              style={{ color: "#0A3A34", fontSize: "1.2em", margin: "0.5em 0" }}
            >
              <b>Giờ tan ca:</b> {group?.endwork} giờ
            </div>
          </div>
          <div style={{ overflowY: "auto", width: "100%" }}>
            <div style={{ display: "flex", height: "100%" }}>
              <DataGrid
                loading={resFindOne.loading || resFindAll.loading}
                rows={listDoctorInGroup?.map((item) => {
                  return {
                    ...item,
                    id: item.id,
                    name: `${item.user.lastname} ${item.user.firstname} `,
                    dob: moment(item.user.dob).format("DD-MM-YYYY"),
                    email: item.user.email,
                    phone: item.user.phone,
                  };
                })}
                getCellClassName={() => {
                  return classes.bodyCell;
                }}
                autoHeight={true}
                columns={columns}
                checkboxSelection
                pageSize={pageSizeOne}
                onPageSizeChange={(newPageSize) => setPageSizeOne(newPageSize)}
                rowsPerPageOptions={[5, 10, 20]}
                disableSelectionOnClick
              />
            </div>
          </div>
        </CardBody>
      </Card>
      <Modal
        open={addDoc}
        style={{ maxHeight: "20vh", maxWidth: "90vw" }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          style={{
            overflowY: "scroll",
            padding: "0 0 1em 0",
            transform: "translate(25%, 10%)",
            maxHeight: "80vh",
            background: "#eee",
            width: "76%",
          }}
          item
          xs={12}
          sm={12}
          md={12}
        >
          <div style={{ overflowY: "auto", width: "100%" }}>
            <div style={{ textAlign: "end" }}>
              <Button onClick={() => setAddDoc(false)}>
                <ClearIcon></ClearIcon>
              </Button>
            </div>

            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Typography
                className={classes.typo}
                style={{ margin: "1em 0 1em 1em" }}
                component="h1"
                variant="h5"
              >
                CHỌN BÁC SĨ - {nameGroup}
              </Typography>
              <Button
                style={{ margin: "1em 0 1em 0" }}
                disabled={listSelectedDoc.length === 0}
                onClick={() => setShowModal(true)}
              >
                <PlaylistAddCheckIcon></PlaylistAddCheckIcon>
              </Button>
            </div>
            <GridItem xs={12} sm={12} md={12}>
              <form
                key="address_form"
                className={classes.form}
                style={{
                  padding: "0",
                  background: "#eee",
                  borderRadius: "0.4em",
                }}
                onSubmit={handleSubmit(onAdvanceSearch)}
                noValidate
              >
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0.5em",
                    whiteSpace: "nowrap",
                  }}
                >
                  <div
                    style={{
                      maxWidth: "fit-content",
                      overflow: "auto",
                      marginRight: "1em",
                    }}
                  >
                    <Controller
                      control={control}
                      name="lastname"
                      margin="normal"
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          className={classes.textAdvance}
                          margin="normal"
                          {...register("lastname")}
                          label="Họ và tên lót"
                          value={value}
                          onChange={onChange}
                        />
                      )}
                    />
                    <Controller
                      control={control}
                      name="firstname"
                      margin="normal"
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          className={classes.textAdvance}
                          margin="normal"
                          {...register("firstname")}
                          label="Tên"
                          value={value}
                          onChange={onChange}
                        />
                      )}
                    />
                    <Controller
                      control={control}
                      name="phone"
                      margin="normal"
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          className={classes.textAdvance}
                          margin="normal"
                          {...register("phone")}
                          label="Số điện thoại"
                          value={value}
                          onChange={onChange}
                        />
                      )}
                    />
                    <Controller
                      control={control}
                      name="dob"
                      margin="normal"
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          className={classes.textAdvance}
                          margin="normal"
                          {...register("dob")}
                          label="Ngày sinh"
                          value={value}
                          onChange={onChange}
                        />
                      )}
                    />
                  </div>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <Button
                      type="submit"
                      variant="contained"
                      className={classes.buttonClear}
                    >
                      <SearchIcon></SearchIcon>
                    </Button>
                  </div>
                </div>
              </form>
            </GridItem>
            <div style={{ display: "flex", height: "100%" }}>
              <DataGrid
                loading={resFindAllDoc.loading}
                rows={listDoctors?.map((item) => {
                  return {
                    ...item,
                    id: item.id,
                    name: `${item.user.lastname} ${item.user.firstname} `,
                    dob: moment(item.user.dob).format("DD-MM-YYYY"),
                    email: item.user.email,
                    phone: item.user.phone,
                  };
                })}
                getCellClassName={() => {
                  return classes.bodyCell;
                }}
                autoHeight={true}
                columns={columns}
                checkboxSelection
                hideFooterPagination={true}
                pageSize={pageSizeDoc}
                disableSelectionOnClick
                onSelectionModelChange={(e) => {
                  setListSelectedDoc(e);
                }}
              />
            </div>
            <PaginationCustom
              handleChangeSize={(e) => handleChangeSize(e)}
              handleChangePage={(e) => handleChangePage(e)}
              count={pagination?.totalPage}
              page={page}
            ></PaginationCustom>
          </div>
        </Grid>
      </Modal>
      <Drawer anchor="right" open={isOpenGroup} style={{ width: "520px" }}>
        <GroupForm group={dataProps}></GroupForm>
      </Drawer>

      <ModalConfirm
        title="Thông báo"
        open={showModal}
        description="Xác nhận thêm thông tin"
        disableBtn={resAddDoctor.loading}
        handleOk={() => {
          dispatch(actions.handleAddDocIntoGroup());
          addDoctor({
            variables: {
              groupid: selectedId,
              adminid: window.sessionStorage.getItem("id"),
              listDoctor: listSelectedDoc.map((item) => {
                return item;
              }),
            },
          }).then((res) => {
            if (res.data !== null) {
              dispatch(actions.handleAddDocIntoGroupSuccess(res.data));
              location.reload()
              setAddDoc(false);
              setShowModal(false);
            } else dispatch(actions.handleAddDocIntoGroupFail());
          });
        }}
        handleClose={() => setShowModal(false)}
      ></ModalConfirm>

      <ModalConfirm
        title="Thông báo"
        open={modalRemove}
        description="Xác nhận xóa bác sĩ khỏi nhóm."
        disableBtn={resRemoveDoctor.loading}
        handleOk={() => {
          dispatch(actions.handleRemoveDocGroup());
          removeDoctor({
            variables: {
              adminid: window.sessionStorage.getItem("id"),
              listDoctor: [selectedDoctor],
              groupid: selectedId,
            },
          }).then((res) => {
            if (res.data !== null) {
              dispatch(actions.handleRemoveDocGroupSuccess(selectedDoctor));
              setModalRemove(false);
            } else dispatch(actions.handleRemoveDocGroupFail());
          });
        }}
        handleClose={() => setModalRemove(false)}
      ></ModalConfirm>

      {isSuccess !== null && (
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={isOpenNoti}
          autoHideDuration={3000}
          onClose={() => dispatch(actions.handleCloseGroupNoti())}
        >
          <Alert severity={isSuccess ? "success" : "error"}>{message}</Alert>
        </Snackbar>
      )}
    </GridContainer>
  );
}
