import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useForm, Controller } from "react-hook-form";
import * as actions from "./store/actions";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import * as apis from "./store/api";
import { useHistory } from "react-router-dom";
import InputAdornment from "@material-ui/core/InputAdornment";
import moment from "moment";
// import { name } from "./store/reducer";
import ClearIcon from "@material-ui/icons/Clear";
import ModalConfirm from "components/Modal/ModalConfirm";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    display: "flex",
    flexDirection: "column",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#50AF50",
    color: "white",
  },
  cancel: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#F44336",
    color: "white",
    marginRight: "1em",
  },
  root: {
    height: "100vh",
  },
  left: {
    background: "#C7E5CE",
    height: "100vh",
    padding: "0 4em",
  },
  right: {
    padding: "3em 3em 0 3em",
  },
  copy: {
    justifyContent: "space-between",
  },
  banner: {
    textAlign: "center",
    marginTop: "20vh",
  },
  title: {
    textAlign: "center",
    marginTop: "1.2em",
    color: "#50AF50",
    fontSize: "2em",
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    "@media (max-width: 1024px)": {
      flexDirection: "column",
    },
  },
  logo: {
    marginBottom: "1.2em",
  },
  p: {
    marginRight: "0.4em",
    "@media (max-width: 900px)": {
      fontSize: "1rem",
    },
  },
  typo: {
    marginBottom: "0.2em",
  },
  img: {
    "@media (max-width: 950px)": {
      width: "20em",
    },
  },
  top: {
    display: "flex",
  },
  body: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    flexGrow: "1",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 100,
    color: "#fff",
  },
  label: {
    fontWeight: "bold",
  },
  value: {
    marginLeft: "0.2em",
  },
  disabled: {
    color: "black",
  },
  tfdisable: {
    "& .MuiInputBase-root.Mui-disabled": {
      color: "rgba(0, 0, 0, 1)", // (default alpha is 0.38)
    },
  },
}));

const ADD_GROUP = apis.addGroup;
const EDIT_GROUP = apis.editGroup;

export default function GroupForm(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const { isOpenGroup } = useSelector((state) => state["Group"]);

  const [addGroup, resAdd] = useMutation(ADD_GROUP, {
    errorPolicy: "all",
  });

  const [editGroup, resEdit] = useMutation(EDIT_GROUP, {
    errorPolicy: "all",
  });

  const [isModalOpen, setIsModalOpen] = useState(false);
  const { register, handleSubmit, setValue, control } = useForm();

  const [dataOk, setDataOk] = useState(null);
  const [errName, setErrName] = useState(false);
  const [name, setName] = useState("");
  const [errStart, setErrStart] = useState(false);
  const [start, setStart] = useState("");
  const [errEnd, setErrEnd] = useState(false);
  const [end, setEnd] = useState("");

  const handleOk = () => {
    if (props.group) {
      dispatch(actions.handleEditGroup());
      editGroup({
        variables: {
          adminid: window.sessionStorage.getItem("id"),
          id: props.group.id,
          name: dataOk.name,
          description: dataOk.description || "",
          startwork: parseFloat(dataOk.startwork),
          endwork: parseFloat(dataOk.endwork),
        },
      }).then((res) => {
        if (res.data === null || res.errors) {
          dispatch(actions.handleEditGroupFail(res.errors[0].message));
          setIsModalOpen(false);
          // dispatch(actions.handleCloseGroupNoti);
        } else {
          dispatch(actions.handleEditGroupSuccess(res));
          dispatch(actions.handleCloseGroupForm());
          setIsModalOpen(false);
          // showNoti();
        }
      });
    } else {
      dispatch(actions.handleAddGroup());
      addGroup({
        variables: {
          adminid: window.sessionStorage.getItem("id"),
          name: dataOk.name,
          description: dataOk.description || "",
          startwork: parseFloat(dataOk.startwork),
          endwork: parseFloat(dataOk.endwork),
        },
      }).then((res) => {
        if (res.data === null || res.errors) {
          dispatch(actions.handleAddGroupFail(res.errors[0].message));
          setIsModalOpen(false);
          // dispatch(actions.handleCloseGroupNoti);
        } else {
          dispatch(actions.handleAddGroupSuccess(res));
          dispatch(actions.handleCloseGroupForm());
          setIsModalOpen(false);
          // showNoti();
        }
      });
    }

    // .catch((err) => {
    //   console.log(err.message)
    //   dispatch(actions.handleAddDoctorFail(err));
    //   setIsModalOpen(false);
    //   showNoti();
    // });
  };

  const onSubmit = (dataForm) => {
    setIsModalOpen(true);
    setDataOk(dataForm);
    console.log(dataForm);
  };

  useEffect(() => {
    if (props.group) {
      setValue("name", props.group.name);
      setValue("description", props.group.description);
      setValue("endwork", props.group.endwork);
      setValue("startwork", props.group.startwork);

      setName(props.group.name);
      setStart(props.group.startwork);
      setEnd(props.group.endwork);
    } else {
      setValue("name", "");
      setValue("description", "");
      setValue("endwork", "");
      setValue("startwork", "");
    }
  }, [isOpenGroup]);

  return (
    <GridContainer component="main" style={{ padding: "2em" }}>
      <GridItem className={classes.right} item xs={12} sm={12} md={12}>
        <div className={classes.paper}>
          <div style={{ textAlign: "end" }}>
            <Button onClick={() => dispatch(actions.handleCloseGroupForm())}>
              <ClearIcon></ClearIcon>
            </Button>
          </div>
          <Typography className={classes.typo} component="h1" variant="h5">
            {props.group ? "SỬA THÔNG TIN" : "THÊM NHÓM BÁC SĨ"}
          </Typography>
          <form
            key="doctor_form"
            onSubmit={handleSubmit(onSubmit)}
            className={classes.form}
            noValidate
          >
            <div className={classes.body}>
              <Controller
                control={control}
                name="name"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <TextField
                    style={{ marginTop: "1.4em" }}
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    {...register("name")}
                    label="Tên nhóm"
                    error={errName}
                    helperText={errName && "Vui lòng nhập tên nhóm"}
                    onChange={(e) => {
                      if (e.target.value) setErrName(false);
                      else setErrName(true);
                      setValue("name", e.target.value);
                      setName(e.target.value);
                    }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    value={value}
                  />
                )}
              />
              <Controller
                control={control}
                name="description"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <TextField
                    style={{ marginTop: "1.4em" }}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    {...register("description")}
                    label="Mô tả"
                    onChange={(e) =>
                      setValue("description", e.target.value || "")
                    }
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    value={value}
                  />
                )}
              />
              <GridContainer>
                <GridItem item xs={12} sm={12} md={6}>
                  <Controller
                    control={control}
                    name="startwork"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        style={{ marginTop: "1.4em" }}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        {...register("startwork")}
                        label="Giờ bắt đầu"
                        onChange={(e) => {
                          if (e.target.value >= 0 && e.target.value <= 24)
                            setErrStart(false);
                          else setErrStart(true);
                          setValue("startwork", e.target.value);
                          setStart(e.target.value);
                        }}
                        type="number"
                        error={errStart}
                        helperText={errStart && "Giờ bắt đầu không hợp lệ"}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        value={value}
                      />
                    )}
                  />
                </GridItem>
                <GridItem item xs={12} sm={12} md={6}>
                  <Controller
                    control={control}
                    name="endwork"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        style={{ marginTop: "1.4em" }}
                        variant="outlined"
                        margin="normal"
                        {...register("endwork")}
                        required
                        fullWidth
                        onChange={(e) => {
                          if (e.target.value >= 0 && e.target.value <= 24)
                            setErrEnd(false);
                          else setErrEnd(true);
                          setValue("endwork", e.target.value);
                          setEnd(e.target.value);
                        }}
                        label="Giờ kết thúc"
                        type="number"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        error={errEnd}
                        helperText={errEnd && "Giờ kết thúc không hợp lệ"}
                        value={value}
                      />
                    )}
                  />
                </GridItem>
              </GridContainer>
              <div style={{ textAlign: "end" }}>
                <Button
                  variant="contained"
                  onClick={() => dispatch(actions.handleCloseGroupForm())}
                  className={classes.cancel}
                >
                  HỦY
                </Button>
                <Button
                  type="submit"
                  variant="contained"
                  className={classes.submit}
                  disabled={
                    errName ||
                    errStart ||
                    errEnd ||
                    start === "" ||
                    end === "" ||
                    name === ""
                  }
                >
                  {props.group ? "LƯU" : "THÊM"}
                </Button>
              </div>
            </div>
          </form>
        </div>
      </GridItem>
      <ModalConfirm
        title="Thông báo"
        open={isModalOpen}
        disableBtn={resAdd.loading || resEdit.loading}
        description="Xác nhận thêm thông tin"
        handleOk={() => handleOk()}
        handleClose={() => setIsModalOpen(false)}
      ></ModalConfirm>
    </GridContainer>
  );
}
