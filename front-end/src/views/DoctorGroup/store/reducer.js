/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "Group";

const initialState = freeze({
  listDoctors: [],
  group: null,
  listDoctorInGroup: [],
  listGroup: [],
  isOpenModal: null,
  isOpenNoti: null,
  message: "",
  isOpenGroup: null,
  isSuccess: null,
  selectedId: "",
  pagination: null,
});

export default handleActions(
  {
    [actions.handleOpenGroupForm]: (state, actions) => {
      return freeze({
        ...state,
        isOpenGroup: true,
        // selectedId: actions.payload
      });
    },
    [actions.handleCloseGroupForm]: (state, actions) => {
      return freeze({
        ...state,
        isOpenGroup: false,
      });
    },
    [actions.handleOpenGroupNoti]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: true,
      });
    },
    [actions.handleCloseGroupNoti]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: false,
      });
    },
    [actions.handleAddGroup]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleAddGroupSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: true,
        isOpenNoti: true,
        listGroup: [
          ...state.listGroup,
          { ...actions.payload.data.add_group, doctors: [] },
        ],
        message: "Thêm thông tin thành công",
      });
    },
    [actions.handleAddGroupFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        isSuccess: false,
        message: "Thêm nhóm thất bại",
      });
    },
    [actions.handleFindAllGroupDoctor]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindAllGroupDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        listGroup: actions.payload.find_all_group,
      });
    },
    [actions.handleFindAllGroupDoctorFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleEditGroup]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleEditGroupSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        isSuccess: true,
        listGroup: state.listGroup.map((item) => {
          if (item.id === actions.payload.data.put_group.id) {
            return {
              ...item,
              description: actions.payload.data.put_group.description,
              endwork: actions.payload.data.put_group.endwork,
              id: actions.payload.data.put_group.id,
              name: actions.payload.data.put_group.name,
              startwork: actions.payload.data.put_group.startwork,
            };
          }
          return item;
        }),
        group: {
          ...state.group,
          description: actions.payload.data.put_group.description,
          endwork: actions.payload.data.put_group.endwork,
          id: actions.payload.data.put_group.id,
          name: actions.payload.data.put_group.name,
          startwork: actions.payload.data.put_group.startwork,
        },
        message: "Sửa thông tin thành công",
      });
    },
    [actions.handleEditGroupFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        isSuccess: false,
        message: "Sửa thông tin nhóm thất bại",
      });
    },
    [actions.handleFindNoGroupDoctors]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindNoGroupDoctorsSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        listDoctors: actions.payload.find_all_doctor_use_pagination.doctors,
        pagination: {
          totalPage: Math.ceil(
            actions.payload.find_all_doctor_use_pagination.total /
              actions.payload.find_all_doctor_use_pagination.take
          ),
        },
      });
    },
    [actions.handleFindNoGroupDoctorsFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleFindOneGroup]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindOneGroupSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        group: actions.payload.find_group,
        listDoctorInGroup: actions.payload.find_group.doctors,
      });
    },
    [actions.handleFindOneGroupFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleAddDocIntoGroup]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleAddDocIntoGroupSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        isSuccess: true,
        // listDoctorInGroup: actions.payload.add_doctor.doctors,
        // listDoctor: state.listDoctors.filter(item=>),
        message: "Thêm bác sĩ vào nhóm thành công.",
      });
    },
    [actions.handleAddDocIntoGroupFail]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: true,
        isSuccess: false,
        isLoading: false,
        message: "Thêm bác sĩ vào nhóm thất bại, vui lòng kiểm tra lại!",
      });
    },
    [actions.handleRemoveDocGroup]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleRemoveDocGroupSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: true,
        isOpenNoti: true,
        listDoctorInGroup: state.listDoctorInGroup.filter(
          (item) => item.id !== actions.payload
        ),
        message: "Xóa bác sĩ khỏi nhóm thành công.",
      });
    },
    [actions.handleRemoveDocGroupFail]: (state, actions) => {
      return freeze({
        ...state,
        isSuccess: false,
        isOpenNoti: true,
        isLoading: false,
        message: "Xóa bác sĩ khỏi nhóm thất bại, vui lòng kiểm tra lại!",
      });
    },
  },
  initialState
);
