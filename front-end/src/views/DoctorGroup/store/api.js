import { gql } from "@apollo/client";

export const addDoctor = gql`
  mutation AddDoctor(
    $groupid: String!
    $adminid: String!
    $listDoctor: [String!]!
  ) {
    add_doctor(
      input: { groupid: $groupid, adminid: $adminid, listDoctor: $listDoctor }
    ) {
      id
      name
      description
      startwork
      endwork
    }
  }
`;

export const addGroup = gql`
  mutation AddGroup(
    $adminid: String!
    $name: String!
    $description: String!
    $startwork: Float!
    $endwork: Float!
  ) {
    add_group(
      input: {
        adminid: $adminid
        name: $name
        description: $description
        startwork: $startwork
        endwork: $endwork
      }
    ) {
      id
      name
      description
      startwork
      endwork
    }
  }
`;

export const editGroup = gql`
  mutation EditGroup(
    $adminid: String!
    $id: String!
    $name: String!
    $description: String!
    $startwork: Float!
    $endwork: Float!
  ) {
    put_group(
      input: {
        id: $id
        adminid: $adminid
        name: $name
        description: $description
        startwork: $startwork
        endwork: $endwork
      }
    ) {
      id
      name
      description
      startwork
      endwork
    }
  }
`;

export const findAllGroup = gql`
  query FindAllGroup($adminid: String!) {
    find_all_group(adminid: $adminid) {
      id
      name
      description
      startwork
      endwork
    }
  }
`;

export const findAdvanceDoctor = gql`
  query FindAdvanceDoctor(
    $adminid: String!
    $take: Float!
    $skip: Float!
    $firstname: String!
    $lastname: String!
    $phone: String!
  ) {
    find_all_doctor_use_pagination(
      pagination: {
        adminid: $adminid
        take: $take
        skip: $skip
        firstname: $firstname
        lastname: $lastname
        phone: $phone
      }
    ) {
      take
      skip
      total
      doctors {
        id
        user {
          id
          firstname
          lastname
          email
          phone
          dob
          gender
        }
        inactive
        isverified
        certificate
      }
    }
  }
`;

export const findOneGroup = gql`
  query FindOneGroup($adminid: String!, $id: String!) {
    find_group(adminid: $adminid, id: $id) {
      id
      name
      description
      startwork
      endwork
      doctors {
        id
        user {
          firstname
          lastname
          dob
          email
          phone
        }
        certificate
      }
    }
  }
`;

export const removeDoctor = gql`
  mutation RemoveDoctor(
    $adminid: String!
    $listDoctor: [String!]!
    $groupid: String!
  ) {
    remove_doctor_out_group(
      input: { adminid: $adminid, listDoctor: $listDoctor, groupid: $groupid }
    ) {
      id
      name
      description
      startwork
      endwork
      doctors {
        id
        user {
          firstname
          lastname
          dob
          email
          phone
        }
        certificate
      }
    }
  }
`;
