import { createAction } from "redux-actions";

export const handleOpenGroupForm = createAction("GROUP/OPEN_FORM");
export const handleCloseGroupForm = createAction("GROUP/CLOSE_FORM");

export const handleOpenGroupNoti = createAction("GROUP/OPEN_NOTI");
export const handleCloseGroupNoti = createAction("GROUP/CLOSE_NOTI");

export const handleFindAllGroupDoctor = createAction("GROUP/FIND_ALL_GROUP");
export const handleFindAllGroupDoctorSuccess = createAction("GROUP/FIND_ALL_GROUP_SUCCESS");
export const handleFindAllGroupDoctorFail = createAction("GROUP/FIND_ALL_GROUP_FAIL");

export const handleFindOneGroup = createAction("GROUP/FIND_ONE_GROUP");
export const handleFindOneGroupSuccess = createAction("GROUP/FIND_ONE_GROUP_SUCCESS");
export const handleFindOneGroupFail = createAction("GROUP/FIND_ONE_GROUP_FAIL");

export const handleAddGroup = createAction("GROUP/ADD_GROUP");
export const handleAddGroupSuccess = createAction("GROUP/ADD_GROUP_SUCCESS");
export const handleAddGroupFail = createAction("GROUP/ADD_GROUP_FAIL");

export const handleEditGroup = createAction("GROUP/EDIT_GROUP");
export const handleEditGroupSuccess = createAction("GROUP/EDIT_GROUP_SUCCESS");
export const handleEditGroupFail = createAction("GROUP/EDIT_GROUP_FAIL");

export const handleFindNoGroupDoctors = createAction("GROUP/FIND_ALL_DOCTOR_NO_GROUP");
export const handleFindNoGroupDoctorsSuccess = createAction("GROUP/FIND_ALL_DOCTOR_NO_GROUP_SUCCESS");
export const handleFindNoGroupDoctorsFail = createAction("GROUP/FIND_ALL_DOCTOR_NO_GROUP_FAIL");

export const handleAddDocIntoGroup = createAction("GROUP/ADD_DOCTOC_INTO_GROUP");
export const handleAddDocIntoGroupSuccess = createAction("GROUP/ADD_DOCTOC_INTO_GROUP_SUCCESS");
export const handleAddDocIntoGroupFail = createAction("GROUP/ADD_DOCTOC_INTO_GROUP_FAIL");

export const handleRemoveDocGroup = createAction("GROUP/HANDLE_REMOVE_DOCTOR_GROUP");
export const handleRemoveDocGroupSuccess = createAction("GROUP/HANDLE_REMOVE_DOCTOR_GROUP_SUCCESS");
export const handleRemoveDocGroupFail = createAction("GROUP/HANDLE_REMOVE_DOCTOR_GROUP_FAIL");