import { gql } from "@apollo/client";

export const addPatient = gql`
  mutation AddPatient(
    $doctorid: String!
    $firstname: String!
    $lastname: String!
    $phone: String!
    $dob: DateTime!
    $otherid: String!
    $gender: String!
    $category: String!
    $notesatussick: String!
    $firstdatequicktest: DateTime!
    $firstdatecovid: DateTime!
    $phoneemerency1: String!
    $phoneemerency2: String!
    $phoneemerency3: String!
    $quantityfamily: Float!
    $beforesick: String!
    $pathogenicname: String!
    $otherreason: String!
    $pathogenicid: [String!]!
    $quantittyvacin: Float!
    $source: String!
    $pathogenicnameother: String!
    $ward: String!
    $province: String!
    $city: String!
    $name: String!
    $pathogenicdate: DateTime!
  ) {
    create_patient(
      input: {
        doctorid: $doctorid
        firstname: $firstname
        lastname: $lastname
        phone: $phone
        dob: $dob
        otherid: $otherid
        gender: $gender
        category: $category
        firstdatecovid: $firstdatecovid
        firstdatequicktest: $firstdatequicktest
        phoneemerency1: $phoneemerency1
        phoneemerency2: $phoneemerency2
        phoneemerency3: $phoneemerency3
        quantityfamily: $quantityfamily
        notesatussick: $notesatussick
        quantittyvacin: $quantittyvacin
        beforesick: $beforesick
        pathogenicname: $pathogenicname
        otherreason: $otherreason
        pathogenicid: $pathogenicid
        pathogenicnameother: $pathogenicnameother
        source: $source
        pathogenicdate: $pathogenicdate
        ward: $ward
        province: $province
        city: $city
        name: $name
      }
    ) {
      id
      inactive
      createddat
      updatedat
      category {
        name
      }
      firstname
      lastname
      phone
      dob
      gender
      quantityfamily
      notesatussick
      phone
      category {
        id
        name
      }
      phoneemerency1
      otherid
      source
      phoneemerency2
      phoneemerency3
      quantittyvacin
    }
  }
`;

export const updatePatient = gql`
  mutation updatePatient(
    $id: String!
    $doctorid: String!
    $firstname: String!
    $lastname: String!
    $phone: String!
    $dob: DateTime!
    $otherid: String!
    $gender: String!
    $category: String!
    $firstdatecovid: DateTime!
    $firstdatequicktest: DateTime!
    $phoneemerency1: String!
    $phoneemerency2: String!
    $phoneemerency3: String!
    $quantityfamily: Float!
    $beforesick: String!
    $notesatussick: String!
    $pathogenicname: String!
    $otherreason: String!
    $pathogenicid: [String!]!
    $pathogenicdate: DateTime!
    $name: String!
    $ward: String!
    $quantittyvacin: Float!
    $province: String!
    $source: String!
    $city: String!
    $pathogenicnameother: String!
  ) {
    update_patient(
      input: {
        id: $id
        doctorid: $doctorid
        firstname: $firstname
        lastname: $lastname
        phone: $phone
        dob: $dob
        otherid: $otherid
        gender: $gender
        category: $category
        firstdatecovid: $firstdatecovid
        firstdatequicktest: $firstdatequicktest
        phoneemerency1: $phoneemerency1
        phoneemerency2: $phoneemerency2
        phoneemerency3: $phoneemerency3
        quantityfamily: $quantityfamily
        beforesick: $beforesick
        pathogenicname: $pathogenicname
        otherreason: $otherreason
        pathogenicid: $pathogenicid
        pathogenicdate: $pathogenicdate
        name: $name
        ward: $ward
        province: $province
        city: $city
        notesatussick: $notesatussick
        source: $source
        quantittyvacin: $quantittyvacin
        pathogenicnameother: $pathogenicnameother
      }
    ) {
      id
    }
  }
`;

export const findAllPatienss = gql`
  query FindAllPatient($id: String!) {
    find_all_patient(id: $id) {
      id
      inactive
      createddat
      isverified
      category {
        name
      }
      firstname
      lastname
      phone
      updatedat
      dob
      gender
      quantityfamily
      category {
        id
        name
      }
      phoneemerency1
      phoneemerency2
      phoneemerency3
      source
      otherid
      pathogenicname
      pathogenicdate
      otherid
      otherreason
      beforesick
      notesatussick
      pathogenic {
        id
        name
      }
      doctor {
        id
        user {
          firstname
          lastname
        }
        inactive
      }
      name
      ward
      province
      city
      source
      pathogenicnameother
      firstdatecovid
      firstdatequicktest
      localcode
      quantityfamily
    }
  }
`;

export const findOnePatient = gql`
  query FindOnePatient($id: String!) {
    find_one_patient(id: $id) {
      id
      height
      weight
      inactive
      createddat
      updatedat
      pathogenicname
      pathogenicdate
      otherid
      otherreason
      beforesick
      firstname
      lastname
      phone
      notesatussick
      dob
      gender
      quantityfamily
      category {
        id
        name
      }
      phoneemerency1
      phoneemerency2
      phoneemerency3
      pathogenic {
        id
        name
      }
      doctor {
        id
        user {
          firstname
          lastname
          id
        }
        inactive
      }
      sharedoctor {
        id
        user {
          firstname
          lastname
          id
        }
      }
      name
      quantittyvacin
      ward
      province
      city
      source
      pathogenicnameother
      firstdatecovid
      firstdatequicktest
      status {
        id
        name
      }
    }
  }
`;

export const findAllCate = gql`
  query FindAllCate {
    find_all_category {
      id
      name
    }
  }
`;

export const findAllPatho = gql`
  query FindAllPatho {
    find_all_pathogenic {
      id
      name
    }
  }
`;

export const addMedical = gql`
  mutation AddMedical(
    $doctorid: String!
    $ownerid: String!
    $patientid: String!
    $weight: Float!
    $height: Float!
    $riskassessment: String!
    $medicine: String!
    $countdateremains: Float!
    $signal1: Boolean!
    $signal2: Boolean!
    $signal3: Boolean!
    $signal4: Boolean!
    $signal5: Boolean!
    $signal6: Boolean!
    $signal7: Boolean!
    $signal8: Boolean!
    $signal9: Boolean!
    $signal10: Boolean!
    $signal11: Boolean!
    $signal12: Boolean!
    $signal13: Boolean!
    $signal14: Boolean!
    $signal15: Boolean!
    $signal16: Boolean!
    $signal17: Boolean!
    $signal18: Boolean!
    $signal19: Boolean!
    $signal20: Boolean!
    $signal21: Boolean!
    $signal22: Boolean!
    $signal23: Boolean!
    $signal24: Boolean!
    $signal25: Boolean!
    $otherSignal: String!
    $statusquo: String!
    $color: String!
    $pluse: Float!
    $notepluse: String!
    $bloodpressure: Float!
    $bloodpressure2: Float!
    $notebloodpressure: String!
    $spo2home: Float!
    $notespo2home: String!
    $temperature: Float!
    $notetemperature: String!
    $other: String!
    $attribute: String!
    $noteattribute: String!
    $quantityofsick: Float!
    $guide: String!
    $summary: String!
    $contacdescription: Boolean!
    $reexamination: Boolean!
    $dateofreexamination: DateTime!
    $displaydate: DateTime!
  ) {
    create_medical(
      input: {
        ownerid: $ownerid
        doctorid: $doctorid
        patientid: $patientid
        weight: $weight
        height: $height
        riskassessment: $riskassessment
        medicine: $medicine
        countdateremains: $countdateremains
        signal1: $signal1
        signal2: $signal2
        signal3: $signal3
        signal4: $signal4
        signal5: $signal5
        signal6: $signal6
        signal7: $signal7
        signal8: $signal8
        signal9: $signal9
        signal10: $signal10
        signal11: $signal11
        signal12: $signal12
        signal13: $signal13
        signal14: $signal14
        signal15: $signal15
        signal16: $signal16
        signal17: $signal17
        signal18: $signal18
        signal19: $signal19
        signal20: $signal20
        signal21: $signal21
        signal22: $signal22
        signal23: $signal23
        signal24: $signal24
        signal25: $signal25
        otherSignal: $otherSignal
        statusquo: $statusquo
        color: $color
        pluse: $pluse
        notepluse: $notepluse
        bloodpressure: $bloodpressure
        bloodpressure2: $bloodpressure2
        notebloodpressure: $notebloodpressure
        spo2home: $spo2home
        notespo2home: $notespo2home
        temperature: $temperature
        notetemperature: $notetemperature
        other: $other
        attribute: $attribute
        noteattribute: $noteattribute
        quantityofsick: $quantityofsick
        guide: $guide
        summary: $summary
        contacdescription: $contacdescription
        reexamination: $reexamination
        dateofreexamination: $dateofreexamination
        displaydate: $displaydate
      }
    ) {
      id
      summary
      attribute
      reexamination
      dateofreexamination
      summary
      guide
      createddat
      displaydate
      weight
      height
    }
  }
`;

export const findAllMedical = gql`
  query FindAllMedical($patientid: String!) {
    find_all_medical(patientid: $patientid) {
      id
      summary
      attribute
      reexamination
      dateofreexamination
      summary
      guide
      createddat
      updatedat
      isverified
      displaydate
      contacdescription
    }
  }
`;

export const findAllLogPatients = gql`
  query FindAllLogPatients($patientid: String!) {
    find_all_log_patient(patientid: $patientid) {
      id
      name
      description
      createddat
    }
  }
`;

export const findOneMedical = gql`
  query findOneMedical($id: String!) {
    find_one_medical(id: $id) {
      id
      summary
      weight
      height
      riskassessment
      medicine
      countdateremains
      signal1
      signal2
      signal3
      signal4
      signal5
      signal6
      signal7
      signal8
      signal9
      signal10
      signal11
      signal12
      signal13
      signal14
      signal15
      signal16
      signal17
      signal18
      signal19
      signal20
      signal21
      signal22
      signal23
      signal24
      signal25
      otherSignal
      statusquo
      color
      pluse
      notepluse
      bloodpressure
      bloodpressure2
      notebloodpressure
      spo2home
      notespo2home
      temperature
      notetemperature
      other
      attribute
      noteattribute
      quantityofsick
      guide
      summary
      contacdescription
      reexamination
      dateofreexamination
      displaydate
      medicaltrack {
        id
        medicalnote
        prcrealdate
        medicalpackage
        medicalresutl
      }
      medicinenow {
        id
        name
        quantity
        description
        createddat
      }
    }
  }
`;

export const updateMedical = gql`
  mutation UpdateMedical(
    $ownerid: String!
    $id: String!
    $doctorid: String!
    $patientid: String!
    $weight: Float!
    $height: Float!
    $riskassessment: String!
    $medicine: String!
    $countdateremains: Float!
    $signal1: Boolean!
    $signal2: Boolean!
    $signal3: Boolean!
    $signal4: Boolean!
    $signal5: Boolean!
    $signal6: Boolean!
    $signal7: Boolean!
    $signal8: Boolean!
    $signal9: Boolean!
    $signal10: Boolean!
    $signal11: Boolean!
    $signal12: Boolean!
    $signal13: Boolean!
    $signal14: Boolean!
    $signal15: Boolean!
    $signal16: Boolean!
    $signal17: Boolean!
    $signal18: Boolean!
    $signal19: Boolean!
    $signal20: Boolean!
    $signal21: Boolean!
    $signal22: Boolean!
    $signal23: Boolean!
    $signal24: Boolean!
    $signal25: Boolean!
    $otherSignal: String!
    $statusquo: String!
    $color: String!
    $pluse: Float!
    $notepluse: String!
    $bloodpressure: Float!
    $bloodpressure2: Float!
    $notebloodpressure: String!
    $spo2home: Float!
    $notespo2home: String!
    $temperature: Float!
    $notetemperature: String!
    $other: String!
    $attribute: String!
    $noteattribute: String!
    $quantityofsick: Float!
    $guide: String!
    $summary: String!
    $contacdescription: Boolean!
    $reexamination: Boolean!
    $dateofreexamination: DateTime!
    $displaydate: DateTime!
  ) {
    update_medical(
      input: {
        ownerid: $ownerid
        id: $id
        doctorid: $doctorid
        patientid: $patientid
        weight: $weight
        height: $height
        riskassessment: $riskassessment
        countdateremains: $countdateremains
        signal1: $signal1
        signal2: $signal2
        signal3: $signal3
        signal4: $signal4
        signal5: $signal5
        signal6: $signal6
        signal7: $signal7
        signal8: $signal8
        signal9: $signal9
        signal10: $signal10
        signal11: $signal11
        signal12: $signal12
        signal13: $signal13
        signal14: $signal14
        signal15: $signal15
        signal16: $signal16
        signal17: $signal17
        signal18: $signal18
        signal19: $signal19
        signal20: $signal20
        signal21: $signal21
        signal22: $signal22
        signal23: $signal23
        signal24: $signal24
        signal25: $signal25
        medicine: $medicine
        otherSignal: $otherSignal
        statusquo: $statusquo
        color: $color
        bloodpressure: $bloodpressure
        pluse: $pluse
        notepluse: $notepluse
        bloodpressure2: $bloodpressure2
        notebloodpressure: $notebloodpressure
        spo2home: $spo2home
        notespo2home: $notespo2home
        temperature: $temperature
        notetemperature: $notetemperature
        other: $other
        attribute: $attribute
        noteattribute: $noteattribute
        quantityofsick: $quantityofsick
        guide: $guide
        summary: $summary
        contacdescription: $contacdescription
        reexamination: $reexamination
        dateofreexamination: $dateofreexamination
        displaydate: $displaydate
      }
    ) {
      id
      summary
      attribute
      reexamination
      dateofreexamination
      summary
      guide
      createddat
      displaydate
    }
  }
`;

export const addResult = gql`
  mutation AddResult(
    $medicalid: String!
    $prcrealdate: DateTime!
    $medicalpackage: String!
    $medicalresutl: String!
    $medicalnote: String!
    $ownerid: String!
  ) {
    create_medical_track(
      input: {
        ownerid: $ownerid
        medicalid: $medicalid
        prcrealdate: $prcrealdate
        medicalpackage: $medicalpackage
        medicalresutl: $medicalresutl
        medicalnote: $medicalnote
      }
    ) {
      id
      updatedat
      medicalnote
      prcrealdate
      medicalpackage
      medicalresutl
    }
  }
`;

export const addMedicine = gql`
  mutation AddMedicine(
    $medicalid: String!
    $listMedicine: [ListMedicineInput!]!
    $ownerid: String!
  ) {
    add_medicine(
      input: {
        ownerid: $ownerid
        medicalid: $medicalid
        listMedicine: $listMedicine
      }
    ) {
      id
      name
      quantity
      description
    }
  }
`;

export const addMedicineEmer = gql`
  mutation AddMedicineEmer(
    $emergencyid: String!
    $ownerid: String!
    $patientid: String!
    $listMedicine: [ListEmergencyInput!]!
  ) {
    add_medicine_emegency(
      input: {
        ownerid: $ownerid
        emergencyid: $emergencyid
        patientid: $patientid
        listMedicine: $listMedicine
      }
    ) {
      id
      name
      quantity
      description
    }
  }
`;

export const findAdvancePatients = gql`
  query findAdvance(
    $adminid: String!
    $doctorid: String!
    $take: Float!
    $skip: Float!
    $firstname: String!
    $lastname: String!
    $phone: String!
    $otherid: String!
    $dob: String!
    $updatedat: String!
  ) {
    find_all_patient_use_pagination(
      pagination: {
        adminid: $adminid
        doctorid: $doctorid
        take: $take
        skip: $skip
        firstname: $firstname
        lastname: $lastname
        phone: $phone
        otherid: $otherid
        dob: $dob
        updatedat: $updatedat
      }
    ) {
      patients {
        id
        firstname
        lastname
        phone
        updatedat
        dob
        gender
        name
        ward
        province
        city
        source
        status {
          id
          name
        }
      }
      take
      skip
      total
      nextPage
    }
  }
`;

export const findAllEmerStatus = gql`
  query FindAllEmerStatus {
    find_all_emergency_status {
      id
      name
      description
    }
  }
`;

export const findAllClinical = gql`
  query FindAllClinical {
    find_all_clinical {
      id
      name
      description
    }
  }
`;

export const findAllBreath = gql`
  query FindAllBreath {
    find_all_breath {
      id
      name
      description
    }
  }
`;

export const findAllEmer = gql`
  query FindAllEmer($patientid: String!) {
    find_all_emergency(patientid: $patientid) {
      id
      createddat
      status {
        id
        name
      }
      displaydate
      notestatus
      notebackgroup
      note
      notestatus
      transferfrom
      reason
      reasondate
    }
  }
`;

export const findOneEmer = gql`
  query FindOneEmer($patientid: String!, $id: String!) {
    find_one_emergency(patientid: $patientid, id: $id) {
      id
      createddat
      status {
        id
        name
      }
      notestatus
      notebackgroup
      iscontact
      clinical {
        id
        name
      }
      note
      sense
      pluse
      bloodpressure
      bloodpressure2
      spo2home
      temperature
      medicalpackage
      resultdate
      methodbreath {
        name
        id
      }
      po2
      medicines {
        name
        quantity
        description
      }
      notestatus
      transferfrom
      reason
      reasondate
      medicalpackagenote
      displaydate
    }
  }
`;

export const addEmerProfile = gql`
  mutation AddEmerProfile(
    $ownerid: String!
    $patientid: String!
    $doctorid: String!
    $isverified: Boolean!
    $notebackgroup: String!
    $iscontact: String!
    $listclinical: [String!]!
    $note: String!
    $sense: String!
    $pluse: Float!
    $bloodpressure: Float!
    $bloodpressure2: Float!
    $spo2home: Float!
    $temperature: Float!
    $other: String!
    $medicalpackage: String!
    $resultdate: DateTime!
    $methodbreathid: String!
    $po2: Float!
    $statusid: String!
    $notestatus: String!
    $displaydate: DateTime!
    $transferfrom: String!
    $reason: String!
    $reasondate: String!
    $medicalpackagenote: String!
  ) {
    create_emergency(
      input: {
        ownerid: $ownerid
        patientid: $patientid
        doctorid: $doctorid
        isverified: $isverified
        notebackgroup: $notebackgroup
        iscontact: $iscontact
        listclinical: $listclinical
        note: $note
        sense: $sense
        pluse: $pluse
        bloodpressure: $bloodpressure
        bloodpressure2: $bloodpressure2
        spo2home: $spo2home
        displaydate: $displaydate
        temperature: $temperature
        other: $other
        medicalpackage: $medicalpackage
        resultdate: $resultdate
        methodbreathid: $methodbreathid
        po2: $po2
        statusid: $statusid
        notestatus: $notestatus
        transferfrom: $transferfrom
        reason: $reason
        reasondate: $reasondate
        medicalpackagenote: $medicalpackagenote
      }
    ) {
      id
      createddat
      status {
        id
        name
      }
      notestatus
      notebackgroup
      note
      notestatus
      transferfrom
      reason
      reasondate
      displaydate
      medicalpackagenote
    }
  }
`;

export const updateEmerProfile = gql`
  mutation UpdateEmerProfile(
    $ownerid: String!
    $id: String!
    $patientid: String!
    $doctorid: String!
    $isverified: Boolean!
    $notebackgroup: String!
    $iscontact: String!
    $listclinical: [String!]!
    $note: String!
    $sense: String!
    $pluse: Float!
    $bloodpressure: Float!
    $bloodpressure2: Float!
    $spo2home: Float!
    $temperature: Float!
    $other: String!
    $medicalpackage: String!
    $resultdate: DateTime!
    $displaydate: DateTime!
    $methodbreathid: String!
    $po2: Float!
    $statusid: String!
    $notestatus: String!
    $transferfrom: String!
    $reason: String!
    $reasondate: String!
    $medicalpackagenote: String!
  ) {
    update_emergency(
      input: {
        ownerid: $ownerid
        id: $id
        patientid: $patientid
        doctorid: $doctorid
        isverified: $isverified
        notebackgroup: $notebackgroup
        iscontact: $iscontact
        listclinical: $listclinical
        note: $note
        displaydate: $displaydate
        sense: $sense
        pluse: $pluse
        bloodpressure: $bloodpressure
        bloodpressure2: $bloodpressure2
        spo2home: $spo2home
        temperature: $temperature
        other: $other
        medicalpackage: $medicalpackage
        resultdate: $resultdate
        methodbreathid: $methodbreathid
        po2: $po2
        statusid: $statusid
        notestatus: $notestatus
        transferfrom: $transferfrom
        reason: $reason
        reasondate: $reasondate
        medicalpackagenote: $medicalpackagenote
      }
    ) {
      id
      status {
        id
        name
      }
      notestatus
      notebackgroup
      iscontact
      clinical {
        id
        name
      }
      note
      sense
      pluse
      bloodpressure
      bloodpressure2
      spo2home
      temperature
      medicalpackage
      resultdate
      methodbreath {
        name
        id
      }
      po2
      notestatus
      transferfrom
      reason
      reasondate
      displaydate
      medicalpackagenote
    }
  }
`;

export const findAllMedicine = gql`
  query FindAllMedicine {
    find_all_medicine {
      name
      id
    }
  }
`;

export const sendSMSGroup = gql`
  mutation SendSMSGroup(
    $groupid: String!
    $ownerid: String!
    $medicalid: String!
  ) {
    send_message_for_group(
      input: { ownerid: $ownerid, groupid: $groupid, medicalid: $medicalid }
    ) {
      id
      createddat
      description
    }
  }
`;

export const sendSMSDoctor = gql`
  mutation SendSMSDoctor(
    $doctorid: String!
    $ownerid: String!
    $medicalid: String!
  ) {
    send_message_for_doctor(
      input: { ownerid: $ownerid, doctorid: $doctorid, medicalid: $medicalid }
    ) {
      id
      createddat
    }
  }
`;

export const exportPatient = gql`
  query ExportPatient($adminid: String!, $from: DateTime!, $to: DateTime!) {
    report_all_patient(adminid: $adminid, from: $from, to: $to) {
      id
      updatedat
      firstname
      firstdatecovid
      firstdatequicktest
      pathogenicdate
      lastname
      phone
      dob
      gender
      localcode
      name
      ward
      province
      city
      status {
        name
      }
      doctor {
        id
        user {
          email
        }
      }
      medicalprofile {
        id
        weight
        height
        riskassessment
        medicine
        countdateremains
        signal1
        signal2
        signal3
        signal4
        signal5
        signal6
        signal7
        signal8
        signal9
        signal10
        signal11
        signal12
        signal13
        signal14
        signal15
        signal16
        signal17
        signal18
        signal19
        signal20
        signal21
        signal22
        signal23
        signal24
        signal25
        otherSignal
        statusquo
        color
        pluse
        notepluse
        bloodpressure
        bloodpressure2
        notebloodpressure
        spo2home
        notespo2home
        temperature
        notetemperature
        other
        attribute
        quantityofsick
        guide
        summary
        contacdescription
        reexamination
        dateofreexamination
        displaydate
        createddat
      }
    }
  }
`;

export const exportEmer = gql`
  query ExportEmer($adminid: String!, $from: DateTime!, $to: DateTime!) {
    report_emegencies_all_patient(adminid: $adminid, from: $from, to: $to) {
      id
      status {
        name
      }
      createddat
      firstname
      lastname
      phone
      dob
      gender
      firstdatequicktest
      height
      weight
      otherid
      name
      ward
      province
      city
      localcode
      doctor {
        id
        user {
          email
        }
      }
      firstdatecovid
      otherreason
      beforesick
      notesatussick
      pathogenicdate
      pathogenicname
      pathogenicnameother
      source
      quantittyvacin
      otherstatus
      medicalemergency {
        id
        createddat
        updatedat
        displaydate
        iscontact
        clinical {
          name
        }
        sense
        pluse
        bloodpressure
        bloodpressure2
        spo2home
        temperature
        other
        medicalpackage
        resultdate
        methodbreath {
          name
        }
        po2
        transferfrom
        reason
        reasondate
      }
    }
  }
`;

export const assignDoctor = gql`
  mutation AssignDoctor(
    $id: String!
    $ownerid: String!
    $doctor: String!
  ) {
    change_doctor_deposit(
      input: { ownerid: $ownerid, id: $id, doctor: $doctor }
    ) {
      id
    }
  }
`;