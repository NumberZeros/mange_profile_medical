/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import _ from "lodash";

export const name = "Patients";

const initialState = freeze({
  listPatients: [],
  patient: null,
  mediProfile: null,
  isAuthenticated: false,
  error: null,
  isWrong: null,
  isSuccess: null,
  isLoading: null,
  isOpenNoti: true,
  isOpenForm: false,
  isOpenMed: false,
  isOpenEmer: false,
  message: "",
  categories: [],
  pathos: [],
  doctors: [],
  doctor: null,
  medicals: null,
  medical: null,
  idEdit: null,
  isLoadingMed: null,
  isOpenResult: null,
  idMed: null,
  isOpenMedicine: null,
  successAdd: null,
  pagination: null,
  listBreath: [],
  listEmerStatus: [],
  listClinical: [],
  emers: [],
  oneEmer: null,
  idEmer: null,
  logs: [],
  medType: null,
  isUpdate: null,
  medicines: [],
  statusTypes: {},
  isOpenComplete: null,
  mode: "",
  groups:[],
  exports: [],
  exportEmers: [],
});

export default handleActions(
  {
    [actions.handleOpenResult]: (state, actions) => {
      return freeze({
        ...state,
        isOpenResult: true,
        idMed: actions.payload,
      });
    },
    [actions.handleCloseResult]: (state, actions) => {
      return freeze({
        ...state,
        isOpenResult: false,
        idMed: null,
      });
    },
    [actions.handleOpenPatientStatus]: (state, actions) => {
      return freeze({
        ...state,
        isOpenComplete: true,
      });
    },
    [actions.handleClosePatientStatus]: (state, actions) => {
      return freeze({
        ...state,
        isOpenComplete: false,
      });
    },
    [actions.handleOpenMedicine]: (state, actions) => {
      return freeze({
        ...state,
        isOpenMedicine: true,
        idMed: actions.payload.idMed,
        medType: actions.payload.medType,
      });
    },
    [actions.handleCloseMedicine]: (state, actions) => {
      return freeze({
        ...state,
        isOpenMedicine: false,
        idMed: null,
        medType: null,
      });
    },
    [actions.handleOpenForm]: (state, actions) => {
      return freeze({
        ...state,
        isOpenForm: true,
        mode: actions.payload,
      });
    },
    [actions.handleCloseForm]: (state, actions) => {
      return freeze({
        ...state,
        mode: "",
        isOpenForm: false,
      });
    },
    [actions.handleOpenNoti]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: true,
      });
    },
    [actions.handleCloseNoti]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: false,
      });
    },
    [actions.handleOpenMed]: (state, actions) => {
      return freeze({
        ...state,
        isOpenMed: true,
        mode: actions.payload.mode,
        idEdit: actions.payload.id,
      });
    },
    [actions.handleCloseMed]: (state, actions) => {
      return freeze({
        ...state,
        isOpenMed: false,
        mode: null,
        idEdit: null,
      });
    },
    [actions.handleOpenEmer]: (state, actions) => {
      return freeze({
        ...state,
        isOpenEmer: true,
        idEmer: actions.payload.id,
        mode: actions.payload.mode,
      });
    },
    [actions.handleCloseEmer]: (state, actions) => {
      return freeze({
        ...state,
        isOpenEmer: false,
        idEmer: null,
        mode: "",
        idEdit: null,
      });
    },
    [actions.handleAddPatient]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleAddPatientSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listPatients: [
          {
            ...actions.payload.data.create_patient,
            status: {
              name: "",
              id: "",
            },
          },
          ...state.listPatients,
        ],
        successAdd: true,
        isOpenNoti: true,
        message: "Thêm thông tin bệnh nhân thành công.",
        isLoading: false,
        isOpenForm: false,
      });
    },
    [actions.handleAddPatientFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        message: "Thêm thông tin thất bại! Vui lòng kiểm tra lại.",
        isSuccess: false,
        successAdd: false,
      });
    },
    [actions.handleFindAllPatient]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindAllPatientSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listPatients: actions.payload.find_all_patient_use_pagination.patients,
        isLoading: false,
        pagination: {
          totalPage: Math.ceil(
            actions.payload.find_all_patient_use_pagination.total /
              actions.payload.find_all_patient_use_pagination.take
          ),
        },
      });
    },
    [actions.handleFindAllPatientFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleFindOnePatient]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindOnePatientSuccess]: (state, actions) => {
      return freeze({
        ...state,
        patient: actions.payload.find_one_patient,
        isLoading: false,
      });
    },
    [actions.handleFindOnePatientFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleUpdatePatient]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
        isUpdate: true,
      });
    },
    [actions.handleUpdatePatientSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: true,
        message: "Cập nhật thông tin thành công",
        isUpdate: false,
      });
    },
    [actions.handleUpdatePatientFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
        isUpdate: false,
        message: "Cập nhật thông tin thất bại!",
      });
    },
    [actions.handleFindAllCate]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindAllCateSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        categories: actions.payload.data.find_all_category,
      });
    },
    [actions.handleFindAllCateFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleFindAllPatho]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindAllPathoSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        pathos: actions.payload.data.find_all_pathogenic,
      });
    },
    [actions.handleFindAllPathoFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleFindAllDoctor]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindAllDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        doctors: actions.payload.data.find_all_doctor.filter(
          (item) => item.isverified
        ),
      });
    },
    [actions.handleFindAllDoctorFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleFindOneDoctor]: (state, actions) => {
      return freeze({
        ...state,
        isLoading:
          actions.payload === state.doctor?.find_one_doctor.id ? false : true,
      });
    },
    [actions.handleFindOneDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        doctor: actions.payload,
        isLoading: false,
      });
    },
    [actions.handleFindOneDoctorFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleFindAllMed]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindAllMedSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        medicals: actions.payload.find_all_medical,
      });
    },
    [actions.handleFindAllMedFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleAddMedical]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
        isLoadingMed: true,
      });
    },
    [actions.handleAddMedicalSuccess]: (state, actions) => {
      // console.log([...state.medicals, actions.payload.data.create_medical]);
      return freeze({
        ...state,
        medicals: [actions.payload.data.create_medical, ...state.medicals],
        patient: {
          ...state.patient,
          weight: actions.payload.data.create_medical.weight,
          height: actions.payload.data.create_medical.height,
        },
        isLoading: false,
        isOpenForm: false,
        isSuccess: true,
        isLoadingMed: false,
        message: "Thêm bệnh án thành công.",
      });
    },
    [actions.handleAddMedicalFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenForm: false,
        isSuccess: false,
        isOpenNoti: true,
        message: "Thêm bệnh án thất bại! Vui lòng kiểm tra lại thông tin",
        isLoadingMed: false,
      });
    },
    [actions.handleFindOneMed]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindOneMedSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        medical: actions.payload.data,
      });
    },
    [actions.handleFindOneMedFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleEditMed]: (state, actions) => {
      return freeze({
        ...state,
        isLoadingMed: true,
      });
    },
    [actions.handleEditMedSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoadingMed: false,
        isOpenNoti: true,
        isSuccess: true,
        message: "Sửa thông tin bệnh án thành công",

        medicals: state.medicals.map((item) => {
          if (item.id === actions.payload.data.update_medical.id) {
            return {
              ...item,
              ...actions.payload.data.update_medical,
            };
          } else return item;
        }),

        idEdit: null,
      });
    },
    [actions.handleEditMedFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoadingMed: false,
        isSuccess: false,
        isOpenNoti: true,
        message: "Sửa thông tin bệnh án thất bại",
      });
    },
    [actions.handleAddResult]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleAddResultSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: true,
        message: "Thêm kết quả thành công",
        isLoading: false,
        isSuccess: true,
        isOpenResult: false,
      });
    },
    [actions.handleAddResultFail]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: true,
        message: "Thêm kết quả thất bại, vui lòng kiểm tra lại!",
        isOpenResult: false,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleAddMedicine]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleAddMedicineSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: true,
        message: "Thêm thuốc thành công",
        isLoading: false,
        isSuccess: true,
        isOpenMedicine: false,
      });
    },
    [actions.handleAddMedicineFail]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: true,
        message: "Thêm thuốc thất bại, vui lòng kiểm tra lại!",
        isOpenMedicine: false,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleAddMedicineEmer]: (state, actions) => {
      return freeze({
        ...state,
        isLoadingMed: true,
        isLoading: true,
      });
    },
    [actions.handleAddMedicineEmerSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: true,
        message: "Thêm thuốc thành công",
        isLoading: false,
        // oneEmer: {...state.oneEmer, medicines:[...state.oneEmer.medicines,actions.payload.data.add_medicine_emegency]},
        isSuccess: true,
        isLoadingMed: false,
        isOpenMedicine: false,
      });
    },
    [actions.handleAddMedicineEmerFail]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: true,
        message: "Thêm thuốc thất bại, vui lòng kiểm tra lại!",
        isOpenMedicine: false,
        isLoading: false,
        isLoadingMed: false,
        isSuccess: false,
      });
    },
    [actions.handleFindEmerStatus]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindEmerStatusSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        listEmerStatus: actions.payload.find_all_emergency_status,
      });
    },
    [actions.handleFindEmerStatusFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleFindBreathStatus]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindBreathStatusSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        listBreath: actions.payload.find_all_breath,
      });
    },
    [actions.handleFindBreathStatusFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleFindClinical]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindClinicalSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        listClinical: actions.payload.find_all_clinical,
      });
    },
    [actions.handleFindClinicalFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleAddEmer]: (state, actions) => {
      return freeze({
        ...state,
        isLoadingMed: true,
      });
    },
    [actions.handleAddEmerSuccess]: (state, actions) => {
      return freeze({
        ...state,
        emers: [...state.emers, actions.payload.create_emergency],
        isOpenNoti: true,
        isSuccess: true,
        idEmer: null,
        isLoadingMed: false,
        message: "Thêm hồ sơ cấp cứu thành công",
      });
    },
    [actions.handleAddEmerFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        isSuccess: false,
        isLoadingMed: false,
        message: "Thêm hồ sơ cấp cứu thất bại",
      });
    },
    [actions.handleFindAllEmer]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindAllEmerSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        emers: actions.payload.find_all_emergency,
      });
    },
    [actions.handleFindAllEmerFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleUpdateEmer]: (state, actions) => {
      return freeze({
        ...state,
        isLoadingMed: true,
      });
    },
    [actions.handleUpdateEmerSuccess]: (state, actions) => {
      return freeze({
        ...state,
        oneEmer: actions.payload.update_emergency,
        isOpenNoti: true,
        isSuccess: true,
        isLoadingMed: false,
        message: "Cập nhật hồ sơ cấp cứu thành công",
      });
    },
    [actions.handleUpdateEmerFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        isSuccess: false,
        isLoadingMed: false,
        message: "Cập nhật hồ sơ cấp cứu thất bại",
      });
    },
    [actions.handleFindLog]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindLogSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        logs: actions.payload.find_all_log_patient,
      });
    },
    [actions.handleFindLogFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleFindOneEmer]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindOneEmerSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        oneEmer: actions.payload.find_one_emergency,
      });
    },
    [actions.handleFindOneEmerFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleFindMedicine]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindMedicineSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        medicines: _.uniqBy(
          actions.payload.find_all_medicine.map((item) => {
            return {
              ...item,
              name: item.name.trim(),
            };
          }),
          "name"
        ),
      });
    },
    [actions.handleFindMedicineFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleFindAllStatus]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindAllStatusSuccess]: (state, actions) => {
      return freeze({
        ...state,
        statusTypes: actions.payload.find_all_patient_status.filter(
          (item) => item.name === ""
        )[0],
        isLoading: false,
      });
    },
    [actions.handleFindAllStatusFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleChangeStatus]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleChangeStatusSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        isSuccess: true,
        message: "Mở lại hồ sơ bệnh nhân thành công",
      });
    },
    [actions.handleChangeStatusFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        isSuccess: false,
        message: "Mở lại hồ sơ bệnh nhân thất bại. Vui lòng kiểm tra lại",
      });
    },
    [actions.handleFindGroups]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindGroupsSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        groups: actions.payload.find_all_group,
      });
    },
    [actions.handleFindGroupsFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleGetExport]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleGetExportSuccess]: (state, actions) => {
      const table = [];
      for (const patient of actions.payload.report_all_patient) {
        if (patient.medicalprofile.length === 0) continue;
        for (const med of patient.medicalprofile) {
          table.push({
            ...patient,
            medicalprofile: med
          });
        }
      }

      return freeze({
        ...state,
        isLoading: false,
        exports: table,
      });
    },
    [actions.handleGetExportFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleGetExportEmer]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleGetExportEmerSuccess]: (state, actions) => {
      const tableEmer = [];
      for (const patient of actions.payload.report_emegencies_all_patient) {
        if (patient.medicalemergency.length === 0) continue;
        for (const med of patient.medicalemergency) {
          tableEmer.push({
            ...patient,
            medicalemergency: med
          });
        }
      }

      return freeze({
        ...state,
        isLoading: false,
        exportEmers: tableEmer,
      });
    },
    [actions.handleGetExportEmerFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleAssignDoctor]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleAssignDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        isSuccess: true,
        message: "Phân công cho bác sĩ thành công.",
      });
    },
    [actions.handleAssignDoctorFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        isSuccess: false,
        message: "Phân công cho bác sĩ thất bại! Vui lòng kiểm tra lại.",
      });
    },
  },
  initialState
);
