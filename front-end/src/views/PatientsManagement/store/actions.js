import { createAction } from "redux-actions";

export const handleOpenForm = createAction("PATIENT/OPEN_FORM");
export const handleCloseForm = createAction("PATIENT/CLOSE_FORM");

export const handleOpenResult = createAction("PATIENT/OPEN_RESULT");
export const handleCloseResult = createAction("PATIENT/CLOSE_RESULT");

export const handleOpenMed = createAction("PATIENT/OPEN_MED");
export const handleCloseMed = createAction("PATIENT/CLOSE_MED");

export const handleOpenEmer = createAction("PATIENT/OPEN_EMERGENCY");
export const handleCloseEmer = createAction("PATIENT/CLOSE_EMERGENCY");

export const handleOpenMedicine = createAction("PATIENT/OPEN_MEDICINE");
export const handleCloseMedicine = createAction("PATIENT/CLOSE_MEDICINE");

export const handleOpenNoti = createAction("PATIENT/OPEN_NOTI");
export const handleCloseNoti = createAction("PATIENT/CLOSE_NOTI");

export const handleAddPatient = createAction("PATIENT/ADD_PATIENT");
export const handleAddPatientSuccess = createAction("PATIENT/ADD_PATIENT_SUCCESS");
export const handleAddPatientFail = createAction("PATIENT/ADD_PATIENT_FAIL");

export const handleUpdatePatient = createAction("PATIENT/UPDATE_PATIENT");
export const handleUpdatePatientSuccess = createAction("PATIENT/UPDATE_PATIENT_SUCCESS");
export const handleUpdatePatientFail = createAction("PATIENT/UPDATE_PATIENT_FAIL");

export const handleFindAllPatient = createAction("PATIENT/FIND_ALL_PATIENT");
export const handleFindAllPatientSuccess = createAction("PATIENT/FIND_ALL_PATIENT_SUCCESS");
export const handleFindAllPatientFail = createAction("PATIENT/FIND_ALL_PATIENT_FAIL");

export const handleFindAllCate = createAction("PATIENT/FIND_ALL_CATE");
export const handleFindAllCateSuccess = createAction("PATIENT/FIND_ALL_CATE_SUCCESS");
export const handleFindAllCateFail = createAction("PATIENT/FIND_ALL_CATE_FAIL");

export const handleFindAllPatho = createAction("PATIENT/FIND_ALL_PATHO");
export const handleFindAllPathoSuccess = createAction("PATIENT/FIND_ALL_PATHO_SUCCESS");
export const handleFindAllPathoFail = createAction("PATIENT/FIND_ALL_PATHO_FAIL");

export const handleFindOnePatient = createAction("PATIENT/FIND_ONE_PATIENT");
export const handleFindOnePatientSuccess = createAction("PATIENT/FIND_ONE_PATIENT_SUCCESS");
export const handleFindOnePatientFail = createAction("PATIENT/FIND_ONE_PATIENT_FAIL");

export const handleEditPatient = createAction("PATIENT/EDIT_PATIENT");
export const handleEditPatientSuccess = createAction("PATIENT/EDIT_PATIENT_SUCCESS");
export const handleEditPatientFail = createAction("PATIENT/EDIT_PATIENT_FAIL");

export const handleFindAllDoctor = createAction("PATIENT/FIND_ALL_DOCTOR");
export const handleFindAllDoctorSuccess = createAction("PATIENT/FIND_ALL_DOCTOR_SUCCESS");
export const handleFindAllDoctorFail = createAction("PATIENT/FIND_ALL_DOCTOR_FAIL");

export const handleFindOneDoctor = createAction("PATIENT/FIND_ONE_DOCTOR");
export const handleFindOneDoctorSuccess = createAction("PATIENT/FIND_ONE_DOCTOR_SUCCESS");
export const handleFindOneDoctorFail = createAction("PATIENT/FIND_ONE_DOCTOR_FAIL");

export const handleAddMedical = createAction("PATIENT/ADD_MEDICAL");
export const handleAddMedicalSuccess = createAction("PATIENT/ADD_MEDICAL_SUCCESS");
export const handleAddMedicalFail = createAction("PATIENT/ADD_MEDICAL_FAIL");

export const handleFindAllMed = createAction("PATIENT/FIND_ALL_MED");
export const handleFindAllMedSuccess = createAction("PATIENT/FIND_ALL_MED_SUCCESS");
export const handleFindAllMedFail = createAction("PATIENT/FIND_ALL_MED_FAIL");

export const handleFindOneMed = createAction("PATIENT/FIND_ONE_MED");
export const handleFindOneMedSuccess = createAction("PATIENT/FIND_ONE_MED_SUCCESS");
export const handleFindOneMedFail = createAction("PATIENT/FIND_ONE_MED_FAIL");

export const handleEditMed = createAction("PATIENT/HANDLE_EDIT_MED");
export const handleEditMedSuccess = createAction("PATIENT/HANDLE_EDIT_MED_SUCCESS");
export const handleEditMedFail = createAction("PATIENT/HANDLE_EDIT_MED_FAIL");

export const handleAddResult = createAction("PATIENT/HANDLE_ADD_RESULT");
export const handleAddResultSuccess = createAction("PATIENT/HANDLE_ADD_RESULT_SUCCESS");
export const handleAddResultFail = createAction("PATIENT/HANDLE_ADD_RESULT_FAIL");

export const handleAddMedicine = createAction("PATIENT/HANDLE_ADD_MEDICINE");
export const handleAddMedicineSuccess = createAction("PATIENT/HANDLE_ADD_MEDICINE_SUCCESS");
export const handleAddMedicineFail = createAction("PATIENT/HANDLE_ADD_MEDICINE_FAIL");

export const handleAddMedicineEmer = createAction("PATIENT/HANDLE_ADD_MEDICINE_EMER");
export const handleAddMedicineEmerSuccess = createAction("PATIENT/HANDLE_ADD_MEDICINE_EMER_SUCCESS");
export const handleAddMedicineEmerFail = createAction("PATIENT/HANDLE_ADD_MEDICINE_EMER_FAIL");

export const handleFindEmerStatus = createAction("PATIENT/FIND_ALL_EMER_STATUS");
export const handleFindEmerStatusSuccess = createAction("PATIENT/FIND_ALL_EMER_STATUS_SUCCESS");
export const handleFindEmerStatusFail = createAction("PATIENT/FIND_ALL_EMER_STATUS_FAIL");

export const handleFindBreathStatus = createAction("PATIENT/FIND_ALL_BREATH_STATUS");
export const handleFindBreathStatusSuccess = createAction("PATIENT/FIND_ALL_BREATH_STATUS_SUCCESS");
export const handleFindBreathStatusFail = createAction("PATIENT/FIND_ALL_BREATH_STATUS_FAIL");

export const handleFindClinical = createAction("PATIENT/FIND_ALL_CLINICAL_STATUS");
export const handleFindClinicalSuccess = createAction("PATIENT/FIND_ALL_CLINICAL_STATUS_SUCCESS");
export const handleFindClinicalFail = createAction("PATIENT/FIND_ALL_CLINICAL_STATUS_FAIL");

export const handleAddEmer = createAction("PATIENT/HANDLE_ADD_EMER");
export const handleAddEmerSuccess = createAction("PATIENT/HANDLE_ADD_EMER_SUCCESS");
export const handleAddEmerFail = createAction("PATIENT/HANDLE_ADD_EMER_FAIL");

export const handleFindAllEmer = createAction("PATIENT/HANDLE_FIND_ALL_EMER");
export const handleFindAllEmerSuccess = createAction("PATIENT/HANDLE_FIND_ALL_EMER_SUCCESS");
export const handleFindAllEmerFail = createAction("PATIENT/HANDLE_FIND_ALL_EMER_FAIL");

export const handleUpdateEmer = createAction("PATIENT/HANDLE_UPDATE_EMER");
export const handleUpdateEmerSuccess = createAction("PATIENT/HANDLE_UPDATE_EMER_SUCCESS");
export const handleUpdateEmerFail = createAction("PATIENT/HANDLE_UPDATE_EMER_FAIL");

export const handleFindLog = createAction("PATIENT/HANDLE_FIND_LOG");
export const handleFindLogSuccess = createAction("PATIENT/HANDLE_FIND_LOG_SUCCESS");
export const handleFindLogFail = createAction("PATIENT/HANDLE_FIND_LOG_FAIL");

export const handleFindOneEmer = createAction("PATIENT/HANDLE_FIND_ONE_EMER");
export const handleFindOneEmerSuccess = createAction("PATIENT/HANDLE_FIND_ONE_EMER_SUCCESS");
export const handleFindOneEmerFail = createAction("PATIENT/HANDLE_FIND_ONE_EMER_FAIL");

export const handleFindMedicine = createAction("PATIENT/FIND_ALL_MEDICINE");
export const handleFindMedicineSuccess = createAction("PATIENT/FIND_ALL_MEDICINE_SUCCESS");
export const handleFindMedicineFail = createAction("PATIENT/FIND_ALL_MEDICINE_FAIL");

export const handleFindAllStatus = createAction("PATIENT/FIND_ALL_STATUS");
export const handleFindAllStatusSuccess = createAction("PATIENT/FIND_ALL_STATUS_SUCCESS");
export const handleFindAllStatusFail = createAction("PATIENT/FIND_ALL_STATUS_FAIL");

export const handleChangeStatus = createAction("PATIENT/CHANGE_PATIENT_STATUS");
export const handleChangeStatusSuccess = createAction("PATIENT/CHANGE_PATIENT_STATUS_SUCCESS");
export const handleChangeStatusFail = createAction("PATIENT/CHANGE_PATIENT_STATUS_FAIL");

export const handleClosePatientStatus = createAction("PATIENT/CLOSE_PATIENT_STATUS");
export const handleOpenPatientStatus = createAction("PATIENT/OPEN_PATIENT_STATUS");

export const handleFindGroups = createAction("PATIENT/FIND_ALL_GROUPS");
export const handleFindGroupsSuccess = createAction("PATIENT/FIND_ALL_GROUPS_SUCCESS");
export const handleFindGroupsFail = createAction("PATIENT/FIND_ALL_GROUPS_FAIL");

export const handleSendSMSGroup = createAction("PATIENT/SEND_SMS_GROUPS");
export const handleSendSMSGroupSuccess = createAction("PATIENT/SEND_SMS_GROUPS_SUCCESS");
export const handleSendSMSGroupFail = createAction("PATIENT/SEND_SMS_GROUPS_FAIL");

export const handleSendSMSDoctor = createAction("PATIENT/SEND_SMS_DOCTOR");
export const handleSendSMSDoctorSuccess = createAction("PATIENT/SEND_SMS_DOCTOR_SUCCESS");
export const handleSendSMSDoctorFail = createAction("PATIENT/SEND_SMS_DOCTOR_FAIL");

export const handleGetExport = createAction("PATIENT/HANDLE_GET_EXPORT");
export const handleGetExportSuccess = createAction("PATIENT/HANDLE_GET_EXPORT_SUCCESS");
export const handleGetExportFail = createAction("PATIENT/HANDLE_GET_EXPORT_FAIL");

export const handleGetExportEmer = createAction("PATIENT/HANDLE_GET_EXPORT_EMER");
export const handleGetExportEmerSuccess = createAction("PATIENT/HANDLE_GET_EXPORT_EMER_SUCCESS");
export const handleGetExportEmerFail = createAction("PATIENT/HANDLE_GET_EXPORT_EMER_FAIL");

export const handleAssignDoctor = createAction("PATIENT/ASSIGN_DOCTOR");
export const handleAssignDoctorSuccess = createAction("PATIENT/ASSIGN_DOCTOR_SUCCESS");
export const handleAssignDoctorFail = createAction("PATIENT/ASSIGN_DOCTOR_FAIL");