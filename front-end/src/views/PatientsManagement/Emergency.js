import React, { useEffect, useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import * as actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import * as apis from "./store/api";
import MuiAlert from "@material-ui/lab/Alert";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useForm, Controller } from "react-hook-form";
import blank_avt from "assets/img/blank_avt.png";
import InputAdornment from "@material-ui/core/InputAdornment";
import moment from "moment";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
// import { name } from "./store/reducer";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  DateTimePicker,
  KeyboardDateTimePicker,
} from "@material-ui/pickers";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import PropTypes from "prop-types";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import ClearIcon from "@material-ui/icons/Clear";
import Backdrop from "@material-ui/core/Backdrop";
import ModalConfirm from "components/Modal/ModalConfirm";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { DataGrid } from "@material-ui/data-grid";
import DeleteIcon from "@material-ui/icons/Delete";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import _ from "lodash";
import { useHistory } from "react-router-dom";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  checkboxLable: {
    color: "#444",
    width: "100%",
    justifyContent: "space-between",
    "&:hover": {
      background: "#a2e0ad",
    },
  },
  nextBtn: {
    color: "#fff",
    background: "#37774B",
    border: "none",
    borderRadius: "0.3em",
    "&:hover": {
      background: "#6A995B",
    },
  },
  titleTab: {
    marginTop: "1em",
    fontSize: "2em",
    "@media (max-width: 769px)": {
      fontSize: "1.5em",
    },
    "@media (max-width: 425px)": {
      fontSize: "1.2em",
    },
  },
  reportBtn: {
    marginTop: "0.3em",
    color: "#fff",
    background: "#37774B",
    border: "none",
    borderRadius: "0.3em",
    "&:hover": {
      background: "#6A995B",
    },
  },
  backdrop: {
    zIndex: 4799,
    color: "#fff",
  },
  checkboxes: {
    alignSelf: "start",
    marginLeft: "0.2em",
    minWidth: "80vw",
    justifyContent: "space-between",
    marginTop: "0.2em",
    "&:hover": {
      background: "#b6e5b0",
    },
    "@media (min-width: 1024px)": {
      minWidth: "61vw",
      maxWidth: "66vw",
    },
  },
  formatStyle: {
    fontSize: "0.9rem",
    "& .MuiInputBase-root.Mui-disabled": {
      color: "rgba(0, 0, 0, 1)", // (default alpha is 0.38)
    },
  },
  disabled: {
    color: "black",
  },
  cancel: {
    backgroundColor: "#F44336",
    color: "white",
    marginRight: "1em",
  },
}));
const ADD_RESULT = apis.addResult;
const ADD_EMER = apis.addEmerProfile;
const UPDATE_EMER = apis.updateEmerProfile;

const FIND_CLINICALS = apis.findAllClinical;
const FIND_BREATHS = apis.findAllBreath;
const FIND_EMERSTATUS = apis.findAllEmerStatus;
const FIND_ONE_EMER = apis.findOneEmer;

export default function Emergency(props) {
  const classes = useStyles();
  const history = useHistory();
  const {
    patient,
    isOpenForm,
    medical,
    idEdit,
    message,
    isOpenNoti,
    listBreath,
    listEmerStatus,
    listClinical,
    idEmer,
    oneEmer,
    mode,
  } = useSelector((state) => state["Patients"]);
  const { dataUserAuth } = useSelector((state) => state["Login"]);
  const [addEmer, addEmerRes] = useMutation(ADD_EMER, {
    errorPolicy: "all",
  });
  const [updateEmer, updateEmerRes] = useMutation(UPDATE_EMER, {
    errorPolicy: "all",
  });
  const [findAllClinical, resFindAllClinical] = useMyLazyQuery(FIND_CLINICALS, {
    onCompleted: () => {
      if (resFindAllClinical.data) {
        dispatch(actions.handleFindClinicalSuccess(resFindAllClinical.data));
      } else dispatch(actions.handleFindClinicalFail());
    },
    fetchPolicy: "network-only",
  });
  const [findOneEmer, dataEmer] = useMyLazyQuery(FIND_ONE_EMER, {
    onCompleted: () => {
      if (dataEmer.data) {
        dispatch(actions.handleFindOneEmerSuccess(dataEmer.data));
      } else dispatch(actions.handleFindOneEmerFail());
    },
    fetchPolicy: "network-only",
  });
  const [findAllBreath, resFindAllBreath] = useMyLazyQuery(FIND_BREATHS, {
    onCompleted: () => {
      if (resFindAllBreath.data) {
        dispatch(actions.handleFindBreathStatusSuccess(resFindAllBreath.data));
      } else dispatch(actions.handleFindBreathStatusFail());
    },
    fetchPolicy: "network-only",
  });

  const [findAllEmerStatus, resFindAllEmerStatus] = useMyLazyQuery(
    FIND_EMERSTATUS,
    {
      onCompleted: () => {
        if (resFindAllEmerStatus.data) {
          dispatch(
            actions.handleFindEmerStatusSuccess(resFindAllEmerStatus.data)
          );
        } else dispatch(actions.handleFindEmerStatusFail());
      },
      fetchPolicy: "network-only",
    }
  );
  const dispatch = useDispatch();

  // <MenuItem value={"treated"}>Đang điều trị</MenuItem>
  // <MenuItem value={"die"}>Tử vong</MenuItem>
  // <MenuItem value={"gohome"}>Khỏe/ Về nhà</MenuItem>
  // <MenuItem value={"notinformation"}>
  //   Chưa rõ thông tin
  // </MenuItem>

  const [listBhls, setListBhls] = useState([]);
  const [errBhls, setErrBhls] = useState(false);

  const [clinicals, setClinicals] = useState([]);
  const [breaths, setBreaths] = useState([]);
  const [emerStatuses, setEmerStatuses] = useState([]);
  const [dataSubmit, setDataSubmit] = useState(null);
  const [tab, setTab] = useState(0);
  const { register, handleSubmit, setValue, control } = useForm();

  const [breathMethod, setBreathMethod] = useState("");
  const [errBreath, setErrBreath] = useState(false);
  const [currentStatus, setCurrentStatus] = useState(null);
  const [errCurrentStatus, setErrCurrentStatus] = useState(false);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const handleChange = (event, newValue) => {
    setTab(newValue);
  };

  const onSubmit = (dataForm) => {
    setDataSubmit({
      ownerid: window.sessionStorage.getItem("id"),
      patientid: history.location.pathname.replace(
        "/admin/patients-management/",
        ""
      ),
      displaydate: moment(dataForm.displaydate).utc() || moment().utc(),
      isverified: true,
      doctorid: patient.doctor.id,
      notebackgroup: dataForm.notebackgroup || "",
      iscontact: dataForm.iscontact || "abstruse",
      note: dataForm.note || "",
      sense: dataForm.sense || "",
      pluse: parseFloat(dataForm.pluse) || parseFloat(0),
      bloodpressure: parseFloat(dataForm.bloodpressure) || parseFloat(0),
      bloodpressure2: parseFloat(dataForm.bloodpressure2) || parseFloat(0),
      spo2home: parseFloat(dataForm.spo2home) || parseFloat(0),
      temperature: parseFloat(dataForm.temperature) || parseFloat(0),
      medicalpackage: dataForm.medicalpackage || "other",
      medicalpackagenote: dataForm.medicalpackagenote || "",
      resultdate: moment(dataForm.resultdate).utc() || moment().utc(),
      methodbreathid: breathMethod,
      po2: parseFloat(dataForm.po2) || parseFloat(0),
      other: "",
      listclinical: listBhls.map((item) => {
        return item.id;
      }),
      statusid: currentStatus,
      notestatus: dataForm.notestatus || "",
      transferfrom: dataForm.transferfrom || "",
      reason: dataForm.reason || "treated",
      reasondate: moment(dataForm.reasondate).utc() || moment().utc(),
    });
    setIsModalOpen(true);
  };

  useEffect(() => {
    if (listClinical?.length === 0) {
      dispatch(actions.handleFindClinical());
      findAllClinical({});
    }
    if (listEmerStatus?.length === 0) {
      dispatch(actions.handleFindEmerStatus());
      findAllEmerStatus({});
    }
    if (listBreath?.length === 0) {
      dispatch(actions.handleFindBreathStatus());
      findAllBreath({});
    }
  }, []);

  useEffect(() => {
    setClinicals(listClinical);
    setBreaths(listBreath);
    setEmerStatuses(listEmerStatus);
  }, [listBreath, listClinical, listEmerStatus]);

  useEffect(() => {
    if (!idEmer) {
      setValue("reasondate", moment());
      setValue("resultdate", moment());
      setValue("displaydate", moment());
    }
  }, []);

  useEffect(() => {
    dispatch(actions.handleFindOneEmer());
    findOneEmer({ patientid: patient.id, id: idEmer });
  }, []);
  const handleOk = () => {
    if (idEmer) {
      dispatch(actions.handleUpdateEmer());
      updateEmer({
        variables: {
          ...dataSubmit,
          id: idEmer,
        },
      }).then((res) => {
        if (res.data !== null) {
          dispatch(actions.handleUpdateEmerSuccess(res.data));
          setIsModalOpen(false);
          dispatch(actions.handleCloseEmer());
        } else dispatch(actions.handleUpdateEmerFail());
      });
    } else {
      dispatch(actions.handleAddEmer());
      addEmer({
        variables: {
          ...dataSubmit,
        },
      }).then((res) => {
        if (res.data !== null) {
          dispatch(actions.handleAddEmerSuccess(res.data));
          setIsModalOpen(false);
          dispatch(actions.handleCloseEmer());
          location.reload();
        } else dispatch(actions.handleAddEmerFail());
      });
    }
  };

  useEffect(() => {
    if (idEmer && oneEmer) {
      setValue("notebackgroup", oneEmer?.notebackgroup);
      setValue("iscontact", oneEmer?.iscontact);
      // setValue("clinical", oneEmer?.clinical);
      setListBhls(oneEmer?.clinical);
      setValue("note", oneEmer?.note);
      setValue("sense", oneEmer?.sense);
      setValue("pluse", oneEmer?.pluse);
      setValue("bloodpressure", oneEmer?.bloodpressure);
      setValue("bloodpressure2", oneEmer?.bloodpressure2);
      setValue("spo2home", oneEmer?.spo2home);
      setValue("temperature", oneEmer?.temperature);
      setValue("medicalpackage", oneEmer?.medicalpackage);
      setValue("medicalpackagenote", oneEmer?.medicalpackagenote);
      setValue("resultdate", oneEmer?.resultdate);
      setValue("displaydate", oneEmer?.displaydate);
      // setValue("methodbreathid", oneEmer?.methodbreathid);
      setBreathMethod(oneEmer?.methodbreath.id);
      // setValue("statusid", oneEmer?.statusid);
      setCurrentStatus(oneEmer?.status.id);
      setValue("notestatus", oneEmer?.notestatus);
      setValue("po2", oneEmer?.po2);
      setValue("transferfrom", oneEmer?.transferfrom);
      setValue("reason", oneEmer?.reason);
      setValue("reasondate", oneEmer?.reasondate);
    }
  }, [oneEmer]);
  return (
    <Container component="main" maxWidth="l" style={{ paddingTop: "1em" }}>
      <Backdrop
        className={classes.backdrop}
        open={
          resFindAllClinical?.loading ||
          resFindAllBreath?.loading ||
          resFindAllEmerStatus?.loading ||
          dataEmer?.loading
        }
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div className={classes.titleTab}>HỒ SƠ CẤP CỨU</div>
        <Button onClick={() => dispatch(actions.handleCloseEmer())}>
          <ClearIcon></ClearIcon>
        </Button>
      </div>
      <form
        onSubmit={handleSubmit(onSubmit)}
        noValidate
        className={classes.form}
      >
        {/* <div style={{ display: "flex", justifyContent: "flex-end" }}>
          <Button onClick={() => setTab(1)}>
            <ArrowForwardIosIcon></ArrowForwardIosIcon>
          </Button>
        </div> */}
        <div className={classes.body}>
          <Accordion defaultExpanded>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>HÀNH CHÍNH</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        style={{ fontSize: "4rem" }}
                        className={classes.formatStyle}
                        label="Bác sĩ"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        disabled
                        value={`${patient?.doctor?.user?.lastname} ${patient?.doctor?.user?.firstname}`}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="displaydate"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDateTimePicker
                          variant="inline"
                          format="dd-MM-yyyy HH:mm"
                          margin="normal"
                          {...register("displaydate")}
                          id="date-picker-inline"
                          className={classes.formatStyle}
                          fullWidth
                          onChange={(e) => setValue("displaydate", e)}
                          error={
                            value?.toString() === "Invalid Date" ||
                            value == null
                          }
                          helperText={
                            (value?.toString() === "Invalid Date" ||
                              value == null) &&
                            "Ngày không hợp lệ"
                          }
                          label="Ngày nhận bệnh"
                          value={value}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        disabled
                        className={classes.formatStyle}
                        fullWidth
                        label="Mã bệnh nhân khác (Nếu có)"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        value={patient?.otherid}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        disabled
                        className={classes.formatStyle}
                        fullWidth
                        label="Họ và tên bệnh nhân"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        value={`${patient.lastname} ${patient.firstname}`}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        label="Giới tính"
                        margin="normal"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        className={classes.formatStyle}
                        fullWidth
                        disabled
                        value={
                          patient.gender === "male"
                            ? "Nam"
                            : patient.gender === "female"
                            ? "Nữ"
                            : "Không xác định"
                        }
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          variant="inline"
                          format="dd-MM-yyyy"
                          margin="normal"
                          id="date-picker-inline"
                          className={classes.formatStyle}
                          fullWidth
                          label="Ngày sinh"
                          value={patient.dob}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                          disabled
                        />
                      </MuiPickersUtilsProvider>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        label="Số điện thoại"
                        margin="normal"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              ( +84 )
                            </InputAdornment>
                          ),
                        }}
                        className={classes.formatStyle}
                        fullWidth
                        disabled
                        type="number"
                        value={patient.phone.replace("(+84)", "").trim()}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        label="Số điện thoại người thân 1"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              ( +84 )
                            </InputAdornment>
                          ),
                        }}
                        className={classes.formatStyle}
                        fullWidth
                        disabled
                        value={patient.phoneemerency1
                          .replace("(+84)", "")
                          .trim()}
                        type="number"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        label="Số điện thoại người thân 2"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              ( +84 )
                            </InputAdornment>
                          ),
                        }}
                        className={classes.formatStyle}
                        fullWidth
                        disabled
                        value={patient.phoneemerency2
                          .replace("(+84)", "")
                          .trim()}
                        type="number"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.formatStyle}
                        margin="normal"
                        label="Số điện thoại người thân 3"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              ( +84 )
                            </InputAdornment>
                          ),
                        }}
                        value={patient.phoneemerency3
                          .replace("(+84)", "")
                          .trim()}
                        fullWidth
                        disabled
                        type="number"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.formatStyle}
                        label="Địa chỉ"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        fullWidth
                        disabled
                        value={
                          patient?.name || patient?.city
                            ? `${patient?.name ? patient?.name + ", " : ""} ${
                                patient?.ward ? patient?.ward + ", " : ""
                              }${
                                patient?.province
                                  ? patient?.province + ", "
                                  : ""
                              }${patient?.city ? patient?.city : ""}`
                            : "Không có"
                        }
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.formatStyle}
                        label="Số người ở chung nhà"
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              Người
                            </InputAdornment>
                          ),
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        fullWidth
                        disabled
                        value={patient.quantityfamily}
                        type="number"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.formatStyle}
                        label="Yếu tố nguy cơ"
                        multiline
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        fullWidth
                        disabled
                        value={patient.pathogenic.map((item) => {
                          return item.name;
                        })}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.formatStyle}
                        label="Bệnh mạn tính"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        fullWidth
                        disabled
                        value={patient.beforesick}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <hr />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="notebackgroup"
                    render={({ field: { value, onChange } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.formatStyle}
                        label="Ghi chú về bệnh lý nền"
                        {...register("notebackgroup")}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        fullWidth
                        value={value}
                        onChange={onChange}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="iscontact"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControl
                        style={{ marginTop: "1em" }}
                        variant="outlined"
                        className={classes.formControl}
                        fullWidth
                      >
                        <InputLabel>
                          Có tiếp xúc với người mắc Covid-19 hay không
                        </InputLabel>
                        <Select
                          id="demo-simple-select-outlined"
                          label="Có tiếp xúc với người mắc Covid-19 hay không"
                          {...register("iscontact")}
                          onChange={onChange}
                          required
                          value={value}
                          defaultValue={"abstruse"}
                        >
                          <MenuItem value={"yes"}>Có</MenuItem>
                          <MenuItem value={"no"}>Không</MenuItem>
                          <MenuItem value={"abstruse"}>Không biết</MenuItem>
                        </Select>
                      </FormControl>
                    )}
                  />
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>
                KHÁM - ĐÁNH GIÁ
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    {...register("clinical")}
                    render={({ field: { value } }) => (
                      <Autocomplete
                        multiple
                        filterSelectedOptions
                        style={{ marginTop: "0.5em" }}
                        id="multiple-limit-tags"
                        {...register("clinical")}
                        options={clinicals.map((item) => {
                          return { name: item.name, id: item.id };
                        })}
                        value={listBhls}
                        onChange={(e, value) => {
                          if (value) setErrBhls(false);
                          else setErrBhls(true);
                          // setListBhls(value);
                          // setValue("clinical",value);
                          setListBhls(_.uniqBy(value, "name"));
                        }}
                        getOptionLabel={(option) => option.name}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            variant="outlined"
                            label="Các biểu hiện lâm sàng"
                          />
                        )}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="note"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        style={{ marginTop: "0.5em" }}
                        {...register("note")}
                        label="Ghi chú"
                        value={value}
                        onChange={(e) => setValue("note", e.target.value)}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="sense"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        style={{ marginTop: "0.5em" }}
                        {...register("sense")}
                        label="Tri giác"
                        value={value}
                        onChange={(e) => setValue("sense", e.target.value)}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="pluse"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        {...register("pluse")}
                        style={{ marginTop: "0.5em" }}
                        label="Mạch"
                        value={value}
                        error={value < 0}
                        helperText={value < 0 && "Thông số mạch không hợp lệ"}
                        onChange={(e) => setValue("pluse", e.target.value)}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                          endAdornment: (
                            <InputAdornment position="end">
                              Lần/phút
                            </InputAdornment>
                          ),
                        }}
                        type="number"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="bloodpressure"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        {...register("bloodpressure")}
                        label="Huyết áp tâm thu"
                        value={value}
                        onChange={(e) =>
                          setValue("bloodpressure", e.target.value)
                        }
                        error={value < 0}
                        helperText={
                          value < 0 && "Thông số huyết áp không hợp lệ"
                        }
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                          endAdornment: (
                            <InputAdornment position="end">mmHg</InputAdornment>
                          ),
                        }}
                        type="number"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="bloodpressure2"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        {...register("bloodpressure2")}
                        label="Huyết áp tâm trương"
                        value={value}
                        onChange={(e) =>
                          setValue("bloodpressure2", e.target.value)
                        }
                        error={value < 0}
                        helperText={
                          value < 0 && "Thông số huyết áp không hợp lệ"
                        }
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                          endAdornment: (
                            <InputAdornment position="end">mmHg</InputAdornment>
                          ),
                        }}
                        type="number"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="spo2home"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        {...register("spo2home")}
                        label="Chỉ số SpO2"
                        value={value}
                        error={value < 0}
                        helperText={value < 0 && "Thông số SpO2 không hợp lệ"}
                        onChange={(e) => setValue("spo2home", e.target.value)}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                          endAdornment: (
                            <InputAdornment position="end">%</InputAdornment>
                          ),
                        }}
                        type="number"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="temperature"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        {...register("temperature")}
                        label="Nhiệt độ"
                        value={value}
                        onChange={(e) =>
                          setValue("temperature", e.target.value)
                        }
                        error={value < 0}
                        helperText={
                          value < 0 && "Thông số nhiệt độ không hợp lệ"
                        }
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                          endAdornment: (
                            <InputAdornment position="end">
                              <sup>o</sup>C
                            </InputAdornment>
                          ),
                        }}
                        type="number"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="medicalpackage"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControl
                        style={{ marginTop: "1em" }}
                        variant="outlined"
                        className={classes.formControl}
                        fullWidth
                      >
                        <InputLabel>
                          Xác định dương tính với với Covid-19 qua:
                        </InputLabel>
                        <Select
                          id="demo-simple-select-outlined"
                          label="Xác định dương tính với với Covid-19 qua:"
                          {...register("medicalpackage")}
                          onChange={onChange}
                          required
                          value={value}
                          defaultValue={"other"}
                        >
                          <MenuItem value={"quicktest"}>Test nhanh</MenuItem>
                          <MenuItem value={"prc"}>PCR</MenuItem>
                          <MenuItem value={"other"}>Khác</MenuItem>
                        </Select>
                      </FormControl>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="medicalpackagenote"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        multiline
                        rows={2}
                        maxRows={4}
                        {...register("medicalpackagenote")}
                        label="Ghi chú"
                        value={value}
                        onChange={(e) =>
                          setValue("medicalpackagenote", e.target.value)
                        }
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="resultdate"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          variant="inline"
                          format="dd-MM-yyyy"
                          margin="normal"
                          id="date-picker-inline"
                          {...register("resultdate")}
                          className={classes.formatStyle}
                          onChange={(e) => {
                            setValue("resultdate", e);
                          }}
                          error={
                            value?.toString() === "Invalid Date" ||
                            value == null
                          }
                          helperText={
                            (value?.toString() === "Invalid Date" ||
                              value == null) &&
                            "Ngày không hợp lệ"
                          }
                          defaultValue={moment()}
                          fullWidth
                          label="Ngày xét nghiệm gần nhất dương tính Covid-19"
                          value={value}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    )}
                  />
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>XỬ TRÍ</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="methodbreathid"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControl
                        style={{ marginTop: "1em" }}
                        variant="outlined"
                        className={classes.formControl}
                        fullWidth
                        {...register("methodbreathid")}
                      >
                        <InputLabel>Phương thức thở tại hiện trường</InputLabel>
                        <Select
                          id="demo-simple-select-outlined"
                          label="Phương thức thở tại hiện trường"
                          {...register("methodbreathid")}
                          onChange={(e) => setBreathMethod(e.target.value)}
                          required
                          value={breathMethod ? breathMethod : ""}
                        >
                          {breaths?.map((item) => {
                            return (
                              <MenuItem key={item.id} value={item.id}>
                                {item.name}
                              </MenuItem>
                            );
                          })}
                        </Select>
                      </FormControl>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="po2"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        style={{ marginTop: "0.5em" }}
                        {...register("po2")}
                        label="Lưu lượng O2"
                        value={value}
                        onChange={(e) => setValue("po2", e.target.value)}
                        error={value < 0}
                        helperText={
                          value < 0 && "Thông số Lưu lượng O2 không hợp lệ"
                        }
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                          endAdornment: (
                            <InputAdornment position="end">
                              l/phút
                            </InputAdornment>
                          ),
                        }}
                        type="number"
                      />
                    )}
                  />
                </Grid>
                {/* <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="displaydate"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        style={{ marginTop: "0.5em" }}
                        {...register("pathogenicnameother")}
                        label="Ghi chú"
                        value={value}
                        onChange={(e) =>
                          setValue("pathogenicnameother", e.target.value)
                        }
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                      />
                    )}
                  />
                </Grid> */}
                {idEmer && (
                  <Grid item xs={12} sm={12} md={12}>
                    <div>THUỐC</div>
                    {oneEmer?.medicines?.length > 0 &&
                      oneEmer?.medicines.map((item) => {
                        return (
                          <Grid container spacing={1} key={item.id}>
                            <Grid item xs={8} sm={8} md={4}>
                              <TextField
                                variant="outlined"
                                fullWidth
                                style={{ marginTop: "0.5em" }}
                                label="Tên thuốc"
                                value={item.name}
                                disabled
                                className={classes.formatStyle}
                                InputProps={{
                                  startAdornment: (
                                    <InputAdornment
                                      style={{ marginLeft: "-0.1em" }}
                                      position="end"
                                    ></InputAdornment>
                                  ),
                                }}
                              />
                            </Grid>
                            <Grid item xs={4} sm={4} md={2}>
                              <TextField
                                variant="outlined"
                                fullWidth
                                style={{ marginTop: "0.5em" }}
                                label="Số lượng"
                                disabled
                                className={classes.formatStyle}
                                value={item.quantity}
                                InputProps={{
                                  startAdornment: (
                                    <InputAdornment
                                      style={{ marginLeft: "-0.1em" }}
                                      position="end"
                                    ></InputAdornment>
                                  ),
                                }}
                              />
                            </Grid>
                            <Grid item xs={12} sm={12} md={6}>
                              <TextField
                                variant="outlined"
                                fullWidth
                                style={{ marginTop: "0.5em" }}
                                label="Ghi chú"
                                disabled
                                className={classes.formatStyle}
                                value={item.description}
                                InputProps={{
                                  startAdornment: (
                                    <InputAdornment
                                      style={{ marginLeft: "-0.1em" }}
                                      position="end"
                                    ></InputAdornment>
                                  ),
                                }}
                              />
                            </Grid>
                          </Grid>
                        );
                      })}
                    {oneEmer?.medicines?.length === 0 && "Chưa có thuốc"}
                  </Grid>
                )}
              </Grid>
            </AccordionDetails>
          </Accordion>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>
                KẾT LUẬN BỆNH NHÂN
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="statusid"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControl
                        style={{ marginTop: "1em" }}
                        variant="outlined"
                        className={classes.formControl}
                        fullWidth
                      >
                        <InputLabel>Trạng thái bệnh nhân</InputLabel>
                        <Select
                          id="demo-simple-select-outlined"
                          label="Trạng thái bệnh nhân"
                          {...register("statusid")}
                          onChange={(e) => setCurrentStatus(e.target.value)}
                          required
                          value={currentStatus ? currentStatus : ""}
                          // defaultValue={"unknown"}
                        >
                          {emerStatuses?.map((item) => {
                            return (
                              <MenuItem key={item.id} value={item.id}>
                                {item.name}
                              </MenuItem>
                            );
                          })}
                          {/* <MenuItem value={"yes"}>Có</MenuItem>
                          <MenuItem value={"no"}>Không</MenuItem>
                          <MenuItem value={"unknown"}>Không biết</MenuItem> */}
                        </Select>
                      </FormControl>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="notestatus"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        style={{ marginTop: "0.5em" }}
                        {...register("notestatus")}
                        label="Ghi chú"
                        value={value}
                        onChange={(e) => setValue("notestatus", e.target.value)}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="transferfrom"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        {...register("transferfrom")}
                        label="Nơi chuyển đến"
                        style={{ marginTop: "0.5em" }}
                        value={value}
                        onChange={(e) =>
                          setValue("transferfrom", e.target.value)
                        }
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    {...register("reason")}
                    render={({ field: { value, onChange } }) => (
                      <FormControl
                        variant="outlined"
                        className={classes.formControl}
                        fullWidth
                      >
                        <InputLabel>Kết cục bệnh nhân</InputLabel>
                        <Select
                          id="demo-simple-select-outlined"
                          label="Kết cục bệnh nhân"
                          {...register("reason")}
                          onChange={onChange}
                          required
                          value={value}
                          defaultValue={"treated"}
                        >
                          <MenuItem value={"treated"}>Đang điều trị</MenuItem>
                          <MenuItem value={"die"}>Tử vong</MenuItem>
                          <MenuItem value={"gohome"}>Khỏe/ Về nhà</MenuItem>
                          <MenuItem value={"notinformation"}>
                            Chưa rõ thông tin
                          </MenuItem>
                        </Select>
                      </FormControl>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="reasondate"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          variant="inline"
                          format="dd-MM-yyyy"
                          // margin="normal"
                          {...register("reasondate")}
                          id="date-picker-inline"
                          className={classes.formatStyle}
                          fullWidth
                          onChange={(e) => setValue("reasondate", e)}
                          error={
                            value?.toString() === "Invalid Date" ||
                            value == null
                          }
                          helperText={
                            (value?.toString() === "Invalid Date" ||
                              value == null) &&
                            "Ngày không hợp lệ"
                          }
                          label="Ngày ghi nhận kết cục"
                          value={value}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    )}
                  />
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>
        </div>
        {mode !== "view" && (
          <div
            style={{ textAlign: "end", marginBottom: "1em", marginTop: "1em" }}
          >
            <Button
              className={classes.cancel}
              onClick={() => dispatch(actions.handleCloseEmer())}
            >
              HỦY
            </Button>
            <Button
              type="submit"
              className={classes.nextBtn}
              disabled={breathMethod === "" || currentStatus === null}
            >
              {idEmer ? "LƯU" : "THÊM"}
            </Button>
          </div>
        )}

        <ModalConfirm
          title="Xác nhận"
          open={isModalOpen}
          handleOk={handleOk}
          disableBtn={addEmerRes.loading || updateEmerRes.loading}
          description={
            idEmer
              ? "Xác nhận sửa bệnh án cấp cứu"
              : "Xác nhận thêm bệnh án cấp cứu"
          }
          handleClose={() => setIsModalOpen(false)}
        ></ModalConfirm>
      </form>
    </Container>
  );
}
