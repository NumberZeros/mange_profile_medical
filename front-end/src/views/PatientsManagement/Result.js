import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import * as actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import * as apis from "./store/api";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useForm, Controller } from "react-hook-form";
import moment from "moment";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
// import { name } from "./store/reducer";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  DateTimePicker,
} from "@material-ui/pickers";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";

import PropTypes from "prop-types";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import ClearIcon from "@material-ui/icons/Clear";
import Backdrop from "@material-ui/core/Backdrop";
import ModalConfirm from "components/Modal/ModalConfirm";
import { DataGrid } from "@material-ui/data-grid";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  checkboxLable: {
    color: "#444",
    width: "100%",
    justifyContent: "space-between",
    "&:hover": {
      background: "#a2e0ad",
    },
  },
  nextBtn: {
    color: "#fff",
    background: "#37774B",
    border: "none",
    borderRadius: "0.3em",
    "&:hover": {
      background: "#6A995B",
    },
  },
  titleTab: {
    fontSize: "2em",
    "@media (max-width: 490px)": {
      fontSize: "1.4em",
    },
  },
  reportBtn: {
    marginTop: "0.3em",
    color: "#fff",
    background: "#37774B",
    border: "none",
    borderRadius: "0.3em",
    "&:hover": {
      background: "#6A995B",
    },
  },
  backdrop: {
    zIndex: 4799,
    color: "#fff",
  },
  checkboxes: {
    alignSelf: "start",
    marginLeft: "0.2em",
    minWidth: "80vw",
    justifyContent: "space-between",
    marginTop: "0.2em",
    "&:hover": {
      background: "#b6e5b0",
    },
    "@media (min-width: 1024px)": {
      minWidth: "61vw",
      maxWidth: "66vw",
    },
  },
  formatStyle: {
    fontSize: "0.9rem",
  },
  cancel: {
    backgroundColor: "#F44336",
    color: "white",
    marginRight: "1em",
  },
}));
const ADD_RESULT = apis.addResult;
const UPDATE_MEDICAL = apis.updateMedical;
const FIND_ONE_MEDICAL = apis.findOneMedical;

export default function ResultTest(props) {
  const classes = useStyles();

  const { isOpenMed, medical, idMed } = useSelector(
    (state) => state["Patients"]
  );

  const [findOneMedical, dataMed] = useMyLazyQuery(FIND_ONE_MEDICAL, {
    onCompleted: () => {
      if (dataMed) {
        dispatch(actions.handleFindOneMedSuccess(dataMed));
      } else dispatch(actions.handleFindOneMedFail());
    },
    fetchPolicy: "network-only",
  });

  const [addResult, addResultRes] = useMutation(ADD_RESULT, {
    errorPolicy: "all",
  });

  const [dataSubmit, setDataSubmit] = useState(null);

  const { register, handleSubmit, setValue, control } = useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const dispatch = useDispatch();
  const [pcrDate, setPcrdate] = useState(moment().utc().format());

  useEffect(() => {
    if (idMed !== "");
    dispatch(actions.handleFindOneMed());
    findOneMedical({ id: idMed });
  }, [isOpenMed]);

  const onSubmit = (dataForm) => {
    setDataSubmit({ ...dataForm, prcrealdate: pcrDate });
    setIsModalOpen(true);
  };
  return (
    <Container component="main" maxWidth="l" style={{ paddingTop: "1em" }}>
      <Backdrop className={classes.backdrop} open={dataMed?.loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <div style={{ textAlign: "end" }}>
        <Button onClick={() => dispatch(actions.handleCloseResult())}>
          <ClearIcon></ClearIcon>
        </Button>
      </div>

      <form
        onSubmit={handleSubmit(onSubmit)}
        noValidate
        className={classes.form}
      >
        <div className={classes.titleTab}>
          KẾT QUẢ XÉT NGHIỆM <hr />
        </div>
        <div className={classes.typo}></div>
        <div className={classes.body}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="prcrealdate"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <DateTimePicker
                      variant="inline"
                      className={classes.formatStyle}
                      required
                      fullWidth
                      format="dd-MM-yyyy HH:mm"
                      margin="normal"
                      id="date-picker-inline"
                      label="Ngày thực hiện xét nghiệm"
                      value={pcrDate}
                      onChange={(e) => setPcrdate(e)}
                      defaultValue={moment().utc().format()}
                      KeyboardButtonProps={{
                        "aria-label": "change date",
                      }}
                    />
                  </MuiPickersUtilsProvider>
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="medicalpackage"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <FormControl
                    style={{ marginTop: "1em" }}
                    variant="outlined"
                    className={classes.formControl}
                    fullWidth
                    required
                  >
                    <InputLabel>Loại xét nghiệm</InputLabel>
                    <Select
                      id="demo-simple-select-outlined"
                      label=" Loại  xét  nghiệm "
                      {...register("medicalpackage")}
                      className={classes.formatStyle}
                      value={value}
                      onChange={onChange}
                    >
                      <MenuItem value={"prc"}>PCR</MenuItem>
                      <MenuItem value={"quicktest"}>Test nhanh</MenuItem>
                      <MenuItem value={"other"}>Khác</MenuItem>
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="medicalresutl"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <FormControl
                    style={{ marginTop: "1em" }}
                    variant="outlined"
                    className={classes.formControl}
                    fullWidth
                  >
                    <InputLabel>Kết quả</InputLabel>
                    <Select
                      id="demo-simple-select-outlined"
                      className={classes.formatStyle}
                      label="Kết quả"
                      {...register("medicalresutl")}
                      required
                      onChange={onChange}
                      value={value}
                    >
                      <MenuItem value={"negative"}>Âm tính</MenuItem>
                      <MenuItem value={"positive"}>Dương tính</MenuItem>
                      <MenuItem value={"other"}>Khác</MenuItem>
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="medicalnote"
                render={({ field: { onChange, value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    className={classes.formatStyle}
                    fullWidth
                    {...register("medicalnote")}
                    onChange={(e) => setValue("medicalnote", e.target.value)}
                    label="Ghi chú"
                    id="medicalnote"
                    value={value}
                  />
                )}
              />
            </Grid>
          </Grid>

          <div
            style={{ textAlign: "end", marginBottom: "1em", marginTop: "1em" }}
          >
            <Button
              className={classes.cancel}
              onClick={() => dispatch(actions.handleCloseResult())}
            >
              HỦY
            </Button>
            <Button type="submit" className={classes.nextBtn}>
              LƯU
            </Button>
          </div>
          <ModalConfirm
            title="Xác nhận thay đổi"
            open={isModalOpen}
            disableBtn={addResultRes.loading}
            description="Bạn có muốn xác nhận thay đổi?"
            handleOk={() => {
              console.log(dataSubmit);
              dispatch(actions.handleAddResult());
              addResult({
                variables: {
                  ownerid: window.sessionStorage.getItem("id"),
                  medicalid: props.idMed,
                  prcrealdate: dataSubmit.prcrealdate,
                  medicalpackage: dataSubmit.medicalpackage,
                  medicalresutl: dataSubmit.medicalresutl,
                  medicalnote: dataSubmit.medicalnote
                    ? dataSubmit.medicalnote
                    : "",
                },
              })
                .then((res) => {
                  dispatch(actions.handleAddResultSuccess(res));
                  setIsModalOpen(false);
                  // setOnloading(false);
                })
                .catch((err) => {
                  dispatch(actions.handleAddResultFail(err));
                  // setOnloading(false);
                });
            }}
            handleClose={() => setIsModalOpen(false)}
          ></ModalConfirm>
        </div>
      </form>
    </Container>
  );
}
