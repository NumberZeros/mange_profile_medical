import React, { useEffect, useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import { Button } from "@material-ui/core";
import * as actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import * as apis from "./store/api";
import getAuthorize from "constants/authen/authen";
import moment from "moment";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import { DataGrid, GridToolbar } from "@material-ui/data-grid";
import Switch from "@material-ui/core/Switch";
// import { BrowserRouter, Route, Redirect,  } from "react-router-dom";
import Drawer from "@material-ui/core/Drawer";
import PatientForm from "./PatientForm";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import RemoveRedEyeIcon from "@material-ui/icons/RemoveRedEye";
import PaginationCustom from "components/Pagination/Pagination";
import { useForm, Controller } from "react-hook-form";
import CloudDownloadIcon from "@material-ui/icons/CloudDownload";
import { Modal } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
  DatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import ClearIcon from "@material-ui/icons/Clear";
import Box from "@material-ui/core/Box";
import PropTypes from "prop-types";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import * as actionDashboard from "../Dashboard/store/actions";
import {
  findAllPatientGroup,
  summaryPateints,
  findOnePatient,
} from "views/Dashboard/store/api";
import Summary from "views/Dashboard/Summary";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}
const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      marginTop: "0",
      marginBottom: "0",
      fontSize: "1rem !important",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    fontSize: "1.5rem !important",
    "& small": {
      color: "#777",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  drawer: {
    width: "50vw",
  },
  backdrop: {
    zIndex: 1000,
    color: "#fff",
  },
  topBar: {
    background: "#50AF50 !important",
    display: "flex",
    justifyContent: "space-between",
  },
  searchBar: {
    background: "#ddd",
    borderRadius: "0.4em",
    border: "none !important",
    height: "fit-content",
    width: "-webkit-fill-available",
    marginTop: "0.3em",
  },
  headerTable: {
    color: "#0A3A34",
    fontWeight: "bold",
    fontSize: "1rem",
  },
  bodyCell: {
    fontSize: "0.9rem",
    paddingLeft: "1em !important",
    "&:nth-child(1)": {
      paddingLeft: "0 !important",
    },
  },
  buttonAdd: {
    background: "#37774B",
    marginBottom: "0.1em",
    color: "#eee",
    padding: "1em",
    marginRight: "1em",
    marginTop: "0.4em",
    marginLeft: "1em",
  },
  buttonClear: {
    background: "#37774B",
    marginBottom: "0.1em",
    color: "#eee",
    padding: "1em",
    marginLeft: "0.2em",
    height: "max-content",
    "&:hover": {
      background: "#50AF50",
    },
  },
  btnDel: {
    color: "red",
    minWidth: "auto",
  },
  btnEdit: {
    color: "#37774B",
    minWidth: "auto",
  },
  hidenCell: {
    display: "none",
  },
  textAdvance: {
    background: "#fff",
    marginRight: "0.3em",
  },
  rssModal: {
    width: "80%",
    overflowY: "scroll",
    padding: "2em",
    transform: "translate(15%, 10%)",
    maxHeight: "80vh",
    background: "#eee",
    "@media (max-width: 769px)": {
      transform: "translate(5%, 10%)",
      width: "95%",
      padding: "1em",
    },
    "@media (max-width: 425px)": {
      transform: "translate(6%, 5%)",
      width: "98%",
      padding: "0.5em",
    },
  },
  rssModalSum: {
    width: "76%",
    overflowY: "scroll",
    padding: "0 0 1em 0",
    transform: "translate(25%, 15%)",
    maxHeight: "80vh",
    background: "#eee",
    "@media (max-width: 769px)": {
      transform: "translate(15%, 15%)",
      width: "85%",
    },
    "@media (max-width: 425px)": {
      transform: "translate(6%, 15%)",
      width: "98%",
    },
  },
};

const useStyles = makeStyles(styles);
const FECTH_ALL_PATIENTS = apis.findAdvancePatients;
const EXPORT_DATA = apis.exportPatient;
const EXPORT_EMER = apis.exportEmer;

export default function Patients() {
  getAuthorize();
  const {
    listPatients,
    isOpenForm,
    openNoti,
    message,
    isSuccess,
    successAdd,
    isOpenNoti,
    pagination,
    exports,
    exportEmers,
  } = useSelector((state) => state["Patients"]);

  const { register, handleSubmit, setValue, control } = useForm();

  const { dataUserAuth } = useSelector((state) => state["Login"]);
  ////pagination variables
  const [tab, setTab] = useState(0);
  const [pageSize, setPageSize] = useState(5);
  const [page, setPage] = useState(0);
  const [isOpenExport, setIsOpenExport] = useState(false);
  const [query, setQuery] = useState({
    firstname: "",
    lastname: "",
    dob: "",
    updatedat: "",
    phone: "",
    otherid: "",
  });
  const {
    chartList,
    listDoctors,
    chartDocList,
    isOpenSummary,
    patient,
    summary,
  } = useSelector((state) => state["Dashboard"]);
  ////
  const [doctorId, setDoctorId] = useState("");
  // const [isOpenForm, setIsOpenForm] = useState(false);
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [onLoading, setOnloading] = useState(false);

  const [selectedFrom, setSelectedFrom] = useState(moment());
  const [selectedTo, setSelectedTo] = useState(moment());

  const [selectedFromEmer, setSelectedFromEmer] = useState(moment());
  const [selectedToEmer, setSelectedToEmer] = useState(moment());
  // const { loading, error, data, refetch } = useMyLazyQuery(FECTH_ALL_PATIENTS, {
  //   variables: {
  //     id: window.sessionStorage.getItem("id"),
  //   },
  // });

  const handleChangePage = (e) => {
    setPage(e);
  };

  const handleChangeSize = (e) => {
    setPageSize(e);
  };

  const [fetchSummary, dataSummary] = useMyLazyQuery(summaryPateints, {
    onCompleted: () => {
      if (dataSummary.data) {
        dispatch(actionDashboard.handleFindSummarySuccess(dataSummary.data));
      } else dispatch(actionDashboard.handleFindSummaryFail());
    },
  });

  const [findOne, dataOne] = useMyLazyQuery(findOnePatient, {
    onCompleted: () => {
      if (dataOne.data) {
        dispatch(actionDashboard.handleFindSummaryPatientSuccess(dataOne.data));
      } else dispatch(actionDashboard.handleFindSummaryPatientFail());
    },
  });

  const [advancePatients, dataAdvance] = useMyLazyQuery(FECTH_ALL_PATIENTS, {
    onCompleted: () => {
      if (dataAdvance) {
        dispatch(actions.handleFindAllPatientSuccess(dataAdvance.data));
      } else dispatch(actions.handleFindAllPatientFail());
      // dataDoctor.refetch();
    },
    fetchPolicy: "network-only",
  });

  const [exportData, exportRes] = useMyLazyQuery(EXPORT_DATA, {
    onCompleted: () => {
      if (exportRes) {
        dispatch(actions.handleGetExportSuccess(exportRes.data));
      } else dispatch(actions.handleGetExportFail());
      // dataDoctor.refetch();
    },
    fetchPolicy: "network-only",
  });

  const [exportDataEmer, exportEmerRes] = useMyLazyQuery(EXPORT_EMER, {
    onCompleted: () => {
      if (exportEmerRes) {
        dispatch(actions.handleGetExportEmerSuccess(exportEmerRes.data));
      } else dispatch(actions.handleGetExportEmerFail());
      // dataDoctor.refetch();
    },
    fetchPolicy: "network-only",
  });

  const onOpenExport = () => {
    setIsOpenExport(true);
  };

  const findData = () => {
    if (isOpenExport) {
      dispatch(actions.handleGetExport());
      exportData({
        adminid: window.sessionStorage.getItem("id"),
        from: moment(selectedFrom).utc(),
        to: moment(selectedTo).utc(),
      });
    }
  };

  const findDataEmer = () => {
    if (isOpenExport) {
      dispatch(actions.handleGetExportEmer());
      exportDataEmer({
        adminid: window.sessionStorage.getItem("id"),
        from: moment(selectedFromEmer).utc(),
        to: moment(selectedToEmer).utc(),
      });
    }
  };

  useEffect(() => {
    if (isOpenExport) {
      dispatch(actions.handleGetExport());
      exportData({
        adminid: window.sessionStorage.getItem("id"),
        from: moment().utc(),
        to: moment().utc(),
      });
    }
  }, [isOpenExport]);

  const onAdvanceSearch = (dataForm) => {
    setQuery({
      ...query,
      lastname: dataForm.lastname || "",
      firstname: dataForm.firstname || "",
      phone: dataForm.phone || "",
      otherid: dataForm.otherid || "",
      dob: dataForm.dob || "",
      quantityfamily: parseFloat(dataForm.quantityfamily) || "",
    });
  };
  // useEffect(() => {
  //   dispatch(actions.handleFindAllPatient());
  //   if (data) dispatch(actions.handleFindAllPatientSuccess(data));
  //   else if (!data) dispatch(actions.handleFindAllPatientFail());
  // }, [data]);
  useEffect(() => {
    advancePatients({
      adminid:
        dataUserAuth?.roles === "admin"
          ? window.sessionStorage.getItem("id")
          : "",
      doctorid:
        dataUserAuth?.roles === "admin"
          ? ""
          : window.sessionStorage.getItem("id"),
      take: pageSize,
      skip: 0,
      firstname: query.firstname,
      lastname: query.lastname,
      phone: query.phone,
      otherid: query.otherid,
      dob: query.dob,
      quantityfamily: parseFloat(query.quantityfamily),
      updatedat: query.updatedat,
    });
    setPage(0);
  }, [pageSize]);

  useEffect(() => {
    advancePatients({
      adminid:
        dataUserAuth?.roles === "admin"
          ? window.sessionStorage.getItem("id")
          : "",
      doctorid:
        dataUserAuth?.roles === "admin"
          ? ""
          : window.sessionStorage.getItem("id"),
      take: pageSize,
      skip: page,
      firstname: query.firstname,
      lastname: query.lastname,
      phone: query.phone,
      otherid: query.otherid,
      quantityfamily: parseFloat(query.quantityfamily),
      dob: query.dob,
      updatedat: query.updatedat,
    });
  }, [page]);

  useEffect(() => {
    advancePatients({
      adminid:
        dataUserAuth?.roles === "admin"
          ? window.sessionStorage.getItem("id")
          : "",
      doctorid:
        dataUserAuth?.roles === "admin"
          ? ""
          : window.sessionStorage.getItem("id"),
      take: pageSize,
      skip: 0,
      firstname: query.firstname,
      lastname: query.lastname,
      phone: query.phone,
      otherid: query.otherid,
      quantityfamily: parseFloat(query.quantityfamily),
      dob: query.dob,
      updatedat: query.updatedat,
    });
    setPage(0);
  }, [query]);

  useEffect(() => {
    advancePatients({
      adminid:
        dataUserAuth?.roles === "admin"
          ? window.sessionStorage.getItem("id")
          : "",
      doctorid:
        dataUserAuth?.roles === "admin"
          ? ""
          : window.sessionStorage.getItem("id"),
      take: 5,
      skip: 0,
      firstname: "",
      lastname: "",
      phone: "",
      otherid: "",
      dob: "",
      updatedat: "",
      quantityfamily: parseFloat(0),
    });
  }, []);

  // useEffect(() => {}, [listPatients]);

  const dgmdht = {
    covidhide: "COVID-19 không triệu chứng",
    codvidsimple: "COVID-19 mức độ nhẹ (Viêm hô hấp trên cấp)",
    covidmedium: "COVID-19 mức độ vừa (Viêm phổi)",
    covidhight: "COVID-19 mức độ nặng (Viêm phổi nặng) và nguy kịch",
    f1hidecovid: "F1 có triệu chứng (nghi nhiễm)",
    f1showcovid: "F1 không triệu chứng",
  };

  const dgpl = {
    low: "Nguy cơ thấp",
    medium: "Nguy cơ trung bình",
    high: "Nguy cơ cao",
    hightest: "Nguy cơ rất cao",
  };

  const mdvm = {
    haggard: "Xanh xao",
    typhus: "Da niêm nhạt",
    normal: "Da niêm hồng",
  };
  const txf0 = {
    yes: "Có",
    no: "Không",
    abstruse: "Không biết",
  };
  const xddt = {
    quicktest: "Test nhanh",
    prc: "PCR",
    other: "Khác",
  };
  const kcbn = {
    treated: "Đang điều trị",
    die: "Tử vong",
    gohome: "Khỏe/ Về nhà",
    notinformation: "Chưa rõ thông tin",
  };
  const sourcePat = [
    {
      value: "hopital",
      name: "Bệnh viện",
    },
    {
      value: "followed",
      name: "F1 đang theo dõi",
    },
    {
      value: "healthfacilities",
      name: "Phòng khám",
    },
    {
      value: "single",
      name: "Tự đăng ký",
    },
    {
      value: "other",
      name: "Khác",
    },
  ];
  const columns = [
    {
      field: "name",
      headerName: "Họ và tên",
      minWidth: 200,
      flex: 0.4,
      headerClassName: classes.headerTable,
      // eslint-disable-next-line react/display-name
      renderCell: (params) => (
        <div>
          <span
            style={{ cursor: "pointer" }}
            onClick={() => {
              dispatch(actionDashboard.handleOpenSummary());
              dispatch(actionDashboard.handleFindSummary());
              dispatch(actionDashboard.handleFindSummaryPatient());
              fetchSummary({
                patientid: params.id,
                now: moment().utc(),
              });
              findOne({ id: params.id });
            }}
          >
            {params.row.name}
          </span>
          {/* <Button className={classes.btnDel}>
                  <DeleteIcon></DeleteIcon>
                </Button> */}
        </div>
      ),
    },
    {
      field: "status",
      headerName: "Trạng thái",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "gender",
      headerName: "Giới tính",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "dob",
      headerName: "Ngày sinh",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "quantityfamily",
      headerName: "Số người nhà",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "phone",
      headerName: "Số điện thoại",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "otherid",
      headerName: "Mã bệnh nhân (Khác)",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "actions",
      headerName: `${" "}`,
      width: 120,
      headerClassName: classes.headerTable,
      // eslint-disable-next-line react/display-name
      renderCell: (params) => (
        <div>
          <Button
            className={classes.btnEdit}
            onClick={() => {
              dispatch(actionDashboard.handleOpenSummary());
              dispatch(actionDashboard.handleFindSummary());
              dispatch(actionDashboard.handleFindSummaryPatient());
              fetchSummary({
                patientid: params.id,
                now: moment().utc(),
              });
              findOne({ id: params.id });
            }}
          >
            <RemoveRedEyeIcon></RemoveRedEyeIcon>
          </Button>
          {/* <Button className={classes.btnDel}>
            <DeleteIcon></DeleteIcon>
          </Button> */}
        </div>
      ),
    },
  ];

  const columnsExport = [
    {
      field: "idp",
      headerName: "MA BENH NHAN",
      minWidth: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "lastname",
      headerName: "HO VA TEN LOT",
      minWidth: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "firstname",
      headerName: "TEN",
      minWidth: 120,
      headerClassName: classes.headerTable,
    },
    {
      field: "status",
      headerName: "Trạng thái",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "doctor",
      headerName: "BAC SI",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "phone",
      headerName: "DIEN THOAI",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "dob",
      headerName: "NGAY THANG NAM SINH",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "pathogenicdate",
      headerName: "NGAY CO TRIEU CHUNG DAU TIEN",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "firstdatecovid",
      headerName: "NGAY CO KET QUA DUONG TINH",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "firstdatequicktest",
      headerName: "NGAY CO KET QUA TEST NHANH DUONG TINH",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medriskassessment",
      headerName: "DANH GIA PHAN LOAI NGUY CO",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medweight",
      headerName: "CAN NANG",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "medheight",
      headerName: "CHIEU CAO",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "name",
      headerName: "SO NHA",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "ward",
      headerName: "PHUONG",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "province",
      headerName: "QUAN HUYEN",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "city",
      headerName: "TINH THANH PHO",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "beforesick",
      headerName: "BENH MAN TINH TRUOC DAY",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medid",
      headerName: "MA HO SO",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medriskassessment",
      headerName: "DANH GIA PHAN LOAI NGUY CO",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medcountdateremains",
      headerName: "THUOC DANG DUNG CON DU BAO NHIEU NGAY",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal1",
      headerName: "Mặt hay môi tím tái",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal2",
      headerName: "Cảm thấy đau hoặc tức ngực nhiều không giảm",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal4",
      headerName: "Mất định hướng không gian",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal5",
      headerName: "Bất tỉnh hoặc rất khó thức giấc",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal6",
      headerName: "Nói lắp hoặc khó nói",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal7",
      headerName: "Co giật",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal8",
      headerName: "Tụt huyết áp",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal9",
      headerName: "Mất nước",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal10",
      headerName: "Không có(Dấu hiệu nguy hiểm)",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal11",
      headerName:
        "Hôm nay ông/bà có cảm thấy điều gì khác thường trong cơ thể so với trước đây không?",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal12",
      headerName: "Sốt hoặc cảm thấy muốn sốt",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal13",
      headerName: "Khó thở",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal14",
      headerName: "Đau ngực nhẹ",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal15",
      headerName: "Đau họng",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal16",
      headerName: "Ho: khan hay đàm",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal17",
      headerName: "Đau cơ hoặc đau mỏi khắp người",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal18",
      headerName: "Nôn ói",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal19",
      headerName: "Tiêu chảy",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal20",
      headerName: "Mất vị giác hoặc khứu giác mới xảy ra",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal21",
      headerName: "Nghẹt mũi",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal22",
      headerName: "Sổ mũi",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal23",
      headerName: "Mệt mỏi",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal24",
      headerName: "Đau nhức đầu",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medotherreason",
      headerName: "Yếu tố nguy cơ khác",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medsignal25",
      headerName: "Không có(triệu chứng hiện có)",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medpathogenicnameother",
      headerName: "Triệu chứng khác",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medcolor",
      headerName: "MAU DA VA MOI",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medpluse",
      headerName: "MACH",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medbloodpressure",
      headerName: "TAM THU",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medbloodpressure2",
      headerName: "TAM TRUONG",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medspo2home",
      headerName: "SPO2",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medtemperature",
      headerName: "NHIET DO",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medother",
      headerName: "KHAC",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medattribute",
      headerName: "DANH GIA MUC DO HIEN TAI",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medquantityofsick",
      headerName: "BENH NGAY THU MAY",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medcontacdescription",
      headerName: "LIEN HE DOI HIEN TRUONG",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "medreexamination",
      headerName: "CÓ CAN TAI KHAM KHONG",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "meddateofreexamination",
      headerName: "NGAY TAI KHAM",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "meddisplaydate",
      headerName: "NGAY NHAN BENH AN",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
  ];

  const columnsExportEmer = [
    {
      field: "idp",
      headerName: "MA BENH NHAN",
      minWidth: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "lastname",
      headerName: "HO VA TEN LOT",
      minWidth: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "firstname",
      headerName: "TEN",
      minWidth: 120,
      headerClassName: classes.headerTable,
    },
    {
      field: "status",
      headerName: "TRANG THAI",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "doctor",
      headerName: "BAC SI",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "phone",
      headerName: "DIEN THOAI",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "dob",
      headerName: "NGAY THANG NAM SINH",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "pathogenicdate",
      headerName: "NGAY CO TRIEU CHUNG DAU TIEN",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "firstdatecovid",
      headerName: "NGAY CO KET QUA DUONG TINH",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "firstdatequicktest",
      headerName: "NGAY CO KET QUA TEST NHANH DUONG TINH",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "name",
      headerName: "SO NHA",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "ward",
      headerName: "PHUONG",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "province",
      headerName: "QUAN HUYEN",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "city",
      headerName: "TINH THANH PHO",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "beforesick",
      headerName: "BENH MAN TINH TRUOC DAY",
      minWidth: 250,
      headerClassName: classes.headerTable,
    },
    {
      field: "emerid",
      headerName: "MA HO SO CC",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emerdisplaydate",
      headerName: "NGAY TAO HO SO",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emeriscontact",
      headerName: "TIEP XUC VOI F0",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emerclinical",
      headerName: "BIEU HIEN LAM SANG",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emersense",
      headerName: "TRI GIAC",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emerpluse",
      headerName: "MACH",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emerbloodpressure",
      headerName: "HUYET AP(TAM THU)",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emerbloodpressure2",
      headerName: "HUYET AP(TAM TRUONG)",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emerspo2home",
      headerName: "SPO2",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emertemperature",
      headerName: "NHIET DO",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emermedicalpackage",
      headerName: "XAC DINH DUONG TINH QUA",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emerresultdate",
      headerName: "NGAY XN GAN NHAT CO KQ DUONG TINH",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emermethodbreath",
      headerName: "PHUONG PHAP THO TAI HIEN TRUONG",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emerpo2",
      headerName: "PO2",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emertransferfrom",
      headerName: "NOI CHUYEN DEN",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emerreason",
      headerName: "KET CUC",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "emerreasondate ",
      headerName: "NGAY GHI NHAN KET CUC",
      width: 200,
      headerClassName: classes.headerTable,
    },
  ];
  return (
    <GridContainer>
      <Backdrop
        className={classes.backdrop}
        open={dataSummary.loading || dataOne.loading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader
            style={{
              display: "flex",
              justifyContent: "space-between",
            }}
            color="primary"
            className={classes.topBar}
          >
            <div style={{ flex: "0.5" }}>
              <h4 className={classes.cardTitleWhite}>Danh sách bệnh nhân</h4>
              <p className={classes.cardCategoryWhite}>Medx</p>
            </div>
            <div
              style={{
                alignSelf: "center",
                flex: "1",
                textAlign: "end",
              }}
            >
              {dataUserAuth?.roles === "admin" && (
                <Button
                  className={classes.buttonAdd}
                  disabled={dataAdvance.loading}
                  onClick={() => {
                    onOpenExport();
                  }}
                >
                  <CloudDownloadIcon></CloudDownloadIcon>
                </Button>
              )}

              <Button
                className={classes.buttonAdd}
                onClick={() => {
                  dispatch(actions.handleOpenForm("add"));

                  setQuery({
                    ...query,
                    firstname: "",
                    lastname: "",
                    dob: "",
                    updatedat: "",
                    phone: "",
                    otherid: "",
                    quantityfamily: "",
                  });
                  setValue("firstname", "");
                  setValue("lastname", "");
                  setValue("phone", "");
                  setValue("dob", "");
                  setValue("otherid", "");
                  setValue("quantityfamily", "");
                }}
                disabled={dataAdvance.loading}
              >
                <PersonAddIcon></PersonAddIcon>
              </Button>
            </div>
          </CardHeader>
          <GridItem xs={12} sm={12} md={12}>
            <form
              key="address_form"
              className={classes.form}
              style={{
                padding: "0",
                background: "#eee",
                borderRadius: "0.4em",
              }}
              onSubmit={handleSubmit(onAdvanceSearch)}
              noValidate
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  padding: "0.5em",
                  whiteSpace: "nowrap",
                }}
              >
                <div
                  style={{
                    maxWidth: "fit-content",
                    overflow: "auto",
                    marginRight: "1em",
                  }}
                >
                  <Controller
                    control={control}
                    name="lastname"
                    margin="normal"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.textAdvance}
                        margin="normal"
                        {...register("lastname")}
                        label="Họ và tên lót"
                        value={value}
                        onChange={onChange}
                      />
                    )}
                  />
                  <Controller
                    control={control}
                    name="firstname"
                    margin="normal"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.textAdvance}
                        margin="normal"
                        {...register("firstname")}
                        label="Tên"
                        value={value}
                        onChange={onChange}
                      />
                    )}
                  />
                  {/* <Controller
                    control={control}
                    name="quantityfamily"
                    margin="normal"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.textAdvance}
                        margin="normal"
                        {...register("quantityfamily")}
                        label="Số người nhà"
                        value={value}
                        onChange={onChange}
                        type="number"
                      />
                    )}
                  /> */}
                  <Controller
                    control={control}
                    name="phone"
                    margin="normal"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.textAdvance}
                        margin="normal"
                        {...register("phone")}
                        label="Số điện thoại"
                        value={value}
                        onChange={onChange}
                      />
                    )}
                  />
                  {/* <Controller
                    control={control}
                    name="dob"
                    margin="normal"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.textAdvance}
                        margin="normal"
                        {...register("dob")}
                        label="Ngày sinh"
                        value={value}
                        onChange={onChange}
                      />
                    )}
                  /> */}
                  <Controller
                    control={control}
                    name="otherid"
                    margin="normal"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.textAdvance}
                        margin="normal"
                        {...register("otherid")}
                        label="Mã bệnh nhân (Khác)"
                        value={value}
                        onChange={onChange}
                      />
                    )}
                  />
                </div>
                <div style={{ display: "flex", alignItems: "center" }}>
                  <Button
                    type="submit"
                    variant="contained"
                    className={classes.buttonClear}
                  >
                    <SearchIcon></SearchIcon>
                  </Button>
                  {/* <Button
                    variant="contained"
                    className={classes.buttonClear}
                    onClick={() => {
                      setQuery({
                        ...query,
                        firstname: "",
                        lastname: "",
                        dob: "",
                        updatedat: "",
                        phone: "",
                        otherid: "",
                      });
                      setValue("firstname", "");
                      setValue("lastname", "");
                      setValue("phone", "");
                      setValue("dob", "");
                      setValue("otherid", "");
                    }}
                  >
                    <RefreshIcon></RefreshIcon>
                  </Button> */}
                </div>
              </div>
            </form>
          </GridItem>
          <CardBody>
            <div style={{ overflowY: "auto", width: "100%" }}>
              <div style={{ display: "flex", height: "100%" }}>
                <DataGrid
                  loading={dataAdvance.loading || onLoading}
                  rows={
                    !(dataAdvance.loading || onLoading)
                      ? listPatients?.map((item) => {
                          return {
                            ...item,
                            status: item?.status?.name || "",
                            name: `${item.lastname} ${item.firstname} `,
                            dob: moment(item.dob).format("DD-MM-YYYY"),
                            phone: item.phone,
                            createddat: moment(item.createddat).format(
                              "DD-MM-YYYY"
                            ),
                            quantityfamily: item.quantityfamily,
                            updatedat: moment(item.updatedat).format(
                              "DD/MM/YYYY"
                            ),
                            otherid: item.otherid,
                            gender:
                              item.gender === "male"
                                ? "Nam"
                                : item.gender === "female"
                                ? "Nữ"
                                : "Không xác định",
                          };
                        })
                      : []
                  }
                  style={{ overflow: "auto" }}
                  getCellClassName={() => {
                    return classes.bodyCell;
                  }}
                  hideFooterPagination={true}
                  autoHeight={true}
                  columns={columns}
                  checkboxSelection
                  disableSelectionOnClick
                />
              </div>

              <PaginationCustom
                handleChangeSize={(e) => handleChangeSize(e)}
                handleChangePage={(e) => handleChangePage(e)}
                count={pagination?.totalPage}
                page={page}
              ></PaginationCustom>
            </div>
          </CardBody>
          <Drawer anchor="right" open={isOpenForm} style={{ width: "500px" }}>
            <PatientForm></PatientForm>
          </Drawer>
        </Card>
      </GridItem>
      <Modal
        open={isOpenExport}
        style={{ maxHeight: "20vh", maxWidth: "90vw" }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid className={classes.rssModal}>
          <div style={{ textAlign: "end" }}>
            <Button onClick={() => setIsOpenExport(false)}>
              <ClearIcon></ClearIcon>
            </Button>
          </div>
          <Tabs
            value={tab}
            onChange={(e, value) => {
              setTab(value);
            }}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            aria-label="full width tabs example"
            style={{ marginBottom: "1em" }}
          >
            <Tab label="BỆNH ÁN" {...a11yProps(0)} />
            <Tab label="CẤP CỨU" {...a11yProps(1)} />
          </Tabs>

          <TabPanel hidden={tab !== 0} value={tab} index={0}>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <div style={{ fontWeight: "bold" }}>
                Chọn khoảng thời gian - HS Bệnh án
              </div>
            </div>
            <Grid item xs={12} md={12} lg={12}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DatePicker
                  disableToolbar
                  variant="inline"
                  fullWidth
                  format="dd/MM/yyyy"
                  margin="normal"
                  id="date-picker-inline"
                  label="Từ ngày"
                  value={selectedFrom}
                  onChange={(e) => setSelectedFrom(e)}
                />
              </MuiPickersUtilsProvider>
            </Grid>

            <Grid item xs={12} md={12} lg={12}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DatePicker
                  disableToolbar
                  fullWidth
                  variant="inline"
                  format="dd/MM/yyyy"
                  margin="normal"
                  id="date-picker-inline"
                  label="Đến ngày"
                  value={selectedTo}
                  onChange={(e) => setSelectedTo(e)}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <div style={{ textAlign: "end" }}>
              <Button
                disabled={exportRes.loading}
                className={classes.btnEdit}
                onClick={() => findData()}
              >
                Tìm kiếm
              </Button>
            </div>

            <Grid item xs={12} md={12} lg={12} style={{ maxHeight: "40vh" }}>
              <DataGrid
                loading={exportRes.loading}
                rows={
                  !exportRes.loading
                    ? exports?.map((item, index) => {
                        return {
                          idp: item.id,
                          id: index,
                          firstname: item?.firstname,
                          lastname: item?.lastname,
                          dob: moment(item?.dob).format("DD/MM/YYYY"),
                          phone: item?.phone,
                          createddat: moment(item?.createddat).format(
                            "DD/MM/YYYY"
                          ),
                          status: item?.status?.name || "",
                          name: item?.name,
                          ward: item?.ward,
                          province: item?.province,
                          city: item?.city,
                          doctor: item?.doctor.user.email,
                          quantityfamily: item?.quantityfamily,
                          updatedat: moment(item?.updatedat).format(
                            "DD/MM/YYYY"
                          ),
                          otherid: item?.otherid,
                          firstdatequicktest:
                            moment(item.firstdatequicktest)
                              .format("DD/MM/YYYY")
                              .toString() === "01/01/2200"
                              ? ""
                              : moment(item.firstdatequicktest).format(
                                  "DD/MM/YYYY"
                                ),
                          pathogenicdate:
                            moment(item.pathogenicdate)
                              .format("DD/MM/YYYY")
                              .toString() === "01/01/2200"
                              ? ""
                              : moment(item.pathogenicdate).format(
                                  "DD/MM/YYYY"
                                ),
                          firstdatecovid:
                            moment(item.firstdatecovid)
                              .format("DD/MM/YYYY")
                              .toString() === "01/01/2200"
                              ? ""
                              : moment(item.firstdatecovid).format(
                                  "DD/MM/YYYY"
                                ),
                          displaydate: moment(item.displaydate).format(
                            "DD/MM/YYYY"
                          ),
                          gender:
                            item?.gender === "male"
                              ? "Nam"
                              : item?.gender === "female"
                              ? "Nữ"
                              : "Không xác định",
                          medid: `HS ${moment(
                            item.medicalprofile.createddat
                          ).valueOf()}`,
                          medweight: item?.medicalprofile?.weight,
                          medheight: item?.medicalprofile?.height,
                          medriskassessment:
                            dgpl[item?.medicalprofile?.riskassessment],
                          medmedicine: item?.medicalprofile?.medicine,
                          medcountdateremains:
                            item?.medicalprofile?.countdateremains,
                          medsignal1: item?.medicalprofile?.signal1 ? 1 : 0,
                          medsignal2: item?.medicalprofile?.signal2 ? 1 : 0,
                          medsignal3: item?.medicalprofile?.signal3 ? 1 : 0,
                          medsignal4: item?.medicalprofile?.signal4 ? 1 : 0,
                          medsignal5: item?.medicalprofile?.signal5 ? 1 : 0,
                          medsignal6: item?.medicalprofile?.signal6 ? 1 : 0,
                          medsignal7: item?.medicalprofile?.signal7 ? 1 : 0,
                          medsignal8: item?.medicalprofile?.signal8 ? 1 : 0,
                          medsignal9: item?.medicalprofile?.signal9 ? 1 : 0,
                          medsignal10: item?.medicalprofile?.signal10 ? 1 : 0,
                          medsignal11: item?.medicalprofile?.signal11 ? 1 : 0,
                          medsignal12: item?.medicalprofile?.signal12 ? 1 : 0,
                          medsignal13: item?.medicalprofile?.signal13 ? 1 : 0,
                          medsignal14: item?.medicalprofile?.signal14 ? 1 : 0,
                          medsignal15: item?.medicalprofile?.signal15 ? 1 : 0,
                          medsignal16: item?.medicalprofile?.signal16 ? 1 : 0,
                          medsignal17: item?.medicalprofile?.signal17 ? 1 : 0,
                          medsignal18: item?.medicalprofile?.signal18 ? 1 : 0,
                          medsignal19: item?.medicalprofile?.signal19 ? 1 : 0,
                          medsignal20: item?.medicalprofile?.signal20 ? 1 : 0,
                          medsignal21: item?.medicalprofile?.signal21 ? 1 : 0,
                          medsignal22: item?.medicalprofile?.signal22 ? 1 : 0,
                          medsignal23: item?.medicalprofile?.signal23 ? 1 : 0,
                          medsignal24: item?.medicalprofile?.signal24 ? 1 : 0,
                          medsignal25: item?.medicalprofile?.signal25 ? 1 : 0,
                          medotherSignal: item?.medicalprofile?.otherSignal,
                          medstatusquo: item?.medicalprofile?.statusquo,
                          medcolor: mdvm[item?.medicalprofile?.color],
                          medpluse: item?.medicalprofile?.pluse,
                          mednotepluse: item?.medicalprofile?.notepluse,
                          medbloodpressure: item?.medicalprofile?.bloodpressure,
                          medbloodpressure2:
                            item?.medicalprofile?.bloodpressure2,
                          mednotebloodpressure:
                            item?.medicalprofile?.notebloodpressure,
                          medspo2home: item?.medicalprofile?.spo2home,
                          mednotespo2home: item?.medicalprofile?.notespo2home,
                          medtemperature: item?.medicalprofile?.temperature,
                          mednotetemperature:
                            item?.medicalprofile?.notetemperature,
                          medother: item?.medicalprofile?.other,
                          medattribute: dgmdht[item?.medicalprofile?.attribute],
                          medquantityofsick:
                            item?.medicalprofile?.quantityofsick,
                          medguide: item?.medicalprofile?.guide,
                          medsummary: item?.medicalprofile?.summary,
                          medcontacdescription: item?.medicalprofile
                            ?.contacdescription
                            ? 1
                            : 0,
                          medreexamination: item?.medicalprofile?.reexamination
                            ? 1
                            : 0,
                          meddateofreexamination: moment(
                            item?.medicalprofile?.dateofreexamination
                          ).format("DD/MM/YYYY"),
                          meddisplaydate: moment(
                            item?.medicalprofile?.displaydate
                          ).format("DD/MM/YYYY"),
                        };
                      })
                    : []
                }
                style={{ overflow: "auto" }}
                getCellClassName={() => {
                  return classes.bodyCell;
                }}
                autoHeight={true}
                columns={columnsExport}
                checkboxSelection
                disableSelectionOnClick
                pageSize={5}
                components={{
                  Toolbar: GridToolbar,
                }}
              />
            </Grid>
          </TabPanel>
          <TabPanel hidden={tab !== 1} value={tab} index={1}>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <div style={{ fontWeight: "bold" }}>
                Chọn khoảng thời gian - HS Cấp cứu
              </div>
            </div>
            <Grid item xs={12} md={12} lg={12}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DatePicker
                  disableToolbar
                  variant="inline"
                  fullWidth
                  format="dd/MM/yyyy"
                  margin="normal"
                  id="date-picker-inline"
                  label="Từ ngày"
                  value={selectedFromEmer}
                  onChange={(e) => setSelectedFromEmer(e)}
                />
              </MuiPickersUtilsProvider>
            </Grid>

            <Grid item xs={12} md={12} lg={12}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DatePicker
                  disableToolbar
                  fullWidth
                  variant="inline"
                  format="dd/MM/yyyy"
                  margin="normal"
                  id="date-picker-inline"
                  label="Đến ngày"
                  value={selectedToEmer}
                  onChange={(e) => setSelectedToEmer(e)}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <div style={{ textAlign: "end" }}>
              <Button
                disabled={exportEmerRes.loading}
                className={classes.btnEdit}
                onClick={() => findDataEmer()}
              >
                Tìm kiếm
              </Button>
            </div>

            <Grid item xs={12} md={12} lg={12} style={{ maxHeight: "40vh" }}>
              <DataGrid
                loading={exportEmerRes.loading}
                rows={
                  !exportEmerRes.loading
                    ? exportEmers?.map((item, index) => {
                        return {
                          idp: item.id,
                          id: index,
                          firstname: item?.firstname,
                          lastname: item?.lastname,
                          dob: moment(item?.dob).format("DD/MM/YYYY"),
                          phone: item?.phone,
                          createddat: moment(item?.createddat).format(
                            "DD/MM/YYYY"
                          ),
                          status: item?.status?.name || "",
                          name: item?.name,
                          ward: item?.ward,
                          province: item?.province,
                          city: item?.city,
                          doctor: item?.doctor.user.email,
                          otherid: item?.otherid,
                          firstdatequicktest:
                            moment(item.firstdatequicktest)
                              .format("DD/MM/YYYY")
                              .toString() === "01/01/2200"
                              ? ""
                              : moment(item.firstdatequicktest).format(
                                  "DD/MM/YYYY"
                                ),
                          pathogenicdate:
                            moment(item.pathogenicdate)
                              .format("DD/MM/YYYY")
                              .toString() === "01/01/2200"
                              ? ""
                              : moment(item.pathogenicdate).format(
                                  "DD/MM/YYYY"
                                ),
                          firstdatecovid:
                            moment(item.firstdatecovid)
                              .format("DD/MM/YYYY")
                              .toString() === "01/01/2200"
                              ? ""
                              : moment(item.firstdatecovid).format(
                                  "DD/MM/YYYY"
                                ),
                          gender:
                            item?.gender === "male"
                              ? "Nam"
                              : item?.gender === "female"
                              ? "Nữ"
                              : "Không xác định",
                          emerid: `CC ${moment(
                            item.medicalemergency.createddat
                          ).valueOf()}`,
                          emerdisplaydate: moment(
                            item.medicalemergency.displaydate
                          ).format("DD/MM/YYYY"),
                          emeriscontact: txf0[item.medicalemergency.iscontact],
                          emerclinical: `${item.medicalemergency.clinical.map(
                            (item) => item.name
                          )}`.replace(",", ";"),
                          emersense: item.medicalemergency.sense.replace(
                            ",",
                            ";"
                          ),
                          emerpluse: item.medicalemergency.pluse,
                          emerbloodpressure:
                            item.medicalemergency.bloodpressure,
                          emerbloodpressure2:
                            item.medicalemergency.bloodpressure2,
                          emerspo2home: item.medicalemergency.spo2home,
                          emertemperature: item.medicalemergency.temperature,
                          // emerother: item.medicalemergency.other,
                          emermedicalpackage:
                            xddt[item.medicalemergency.medicalpackage],
                          emerresultdate: moment(
                            item.medicalemergency.resultdate
                          ).format("DD/MM/YYYY"),
                          emermethodbreath:
                            item.medicalemergency.methodbreath.name,
                          emerpo2: item.medicalemergency.po2,
                          emertransferfrom: item.medicalemergency.transferfrom,
                          emerreason: kcbn[item.medicalemergency.reason],
                          emerreasondate: item.medicalemergency.reasondate,
                        };
                      })
                    : []
                }
                style={{ overflow: "auto" }}
                getCellClassName={() => {
                  return classes.bodyCell;
                }}
                autoHeight={true}
                columns={columnsExportEmer}
                checkboxSelection
                disableSelectionOnClick
                pageSize={5}
                components={{
                  Toolbar: GridToolbar,
                }}
              />
            </Grid>
          </TabPanel>
        </Grid>
      </Modal>
      <Modal
        open={isOpenSummary && !dataOne.loading && !dataSummary.loading}
        style={{ maxHeight: "20vh", maxWidth: "90vw" }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid className={classes.rssModalSum} item xs={12} sm={12} md={12}>
          <Summary data={{ ...patient, ...summary }}></Summary>
        </Grid>
      </Modal>
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={openNoti}
        autoHideDuration={3000}
        onClose={() => dispatch(actions.handleCloseNoti())}
      >
        <Alert severity={isSuccess ? "success" : "error"}>{message}</Alert>
      </Snackbar>
      {successAdd !== null && (
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={isOpenNoti}
          autoHideDuration={3000}
          onClose={() => dispatch(actions.handleCloseNoti())}
        >
          <Alert
            severity={
              successAdd === true
                ? "success"
                : successAdd === false
                ? "error"
                : ""
            }
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </GridContainer>
  );
}
