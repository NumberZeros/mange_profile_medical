import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useForm, Controller } from "react-hook-form";
import Grid from "@material-ui/core/Grid";
import * as actions from "./store/actions";
import * as actionsAddresses from "../../constants/Address/store/actions";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import * as apis from "./store/api";
import logo from "assets/img/medx-logo.svg";
import blank_avt from "assets/img/blank_avt.png";
import { useHistory, useParams } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";
import InputAdornment from "@material-ui/core/InputAdornment";
import moment from "moment";
// import { name } from "./store/reducer";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import ClearIcon from "@material-ui/icons/Clear";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import ModalConfirm from "components/Modal/ModalConfirm";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";
import { findAllLessDoctors } from "views/DoctorsManagement/store/api";
import AddressData from "assets/address.json";
import _ from "lodash";
import Chip from "@material-ui/core/Chip";
import * as actionsDashboard from "../Dashboard/store/actions";

const useStyles = makeStyles((theme) => ({
  paper: {
    display: "flex",
    flexDirection: "column",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#50AF50",
    color: "white",
  },
  cancel: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#F44336",
    color: "white",
    marginRight: "1em",
  },
  root: {
    height: "100vh",
  },
  left: {
    background: "#C7E5CE",
    height: "100vh",
    padding: "0 4em",
  },
  right: {
    padding: "3em 3em 0 3em",
    width: "42rem",
    "@media (max-width: 690px)": {
      width: "100%",
    },
    "@media (max-width: 430px)": {
      padding: "1em 1em 0 1em",
      fontSize: "0.8em",
    },
  },
  copy: {
    justifyContent: "space-between",
  },
  banner: {
    textAlign: "center",
    marginTop: "20vh",
  },
  title: {
    textAlign: "center",
    marginTop: "1.2em",
    color: "#50AF50",
    fontSize: "2em",
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    "@media (max-width: 1024px)": {
      flexDirection: "column",
    },
  },
  logo: {
    marginBottom: "1.2em",
  },
  p: {
    marginRight: "0.4em",
    "@media (max-width: 900px)": {
      fontSize: "1rem",
    },
  },
  typo: {
    marginBottom: "0.2em",
    alignSelf: "center",
    marginLeft: "1em",
    "@media (max-width: 350px)": {
      fontSize: "1.4em",
    },
  },
  img: {
    "@media (max-width: 950px)": {
      width: "20em",
    },
  },
  top: {
    display: "flex",
  },
  body: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    flexGrow: "1",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
  label: {
    fontWeight: "bold",
  },
  value: {
    marginLeft: "0.2em",
  },
  multiSelect: {
    whiteSpace: "normal !imporant",
    display: "-webkit-inline-box",
    "MuiOutlinedInput-inputMultiline": {
      whiteSpace: "normal !imporant",
    },
    ["MuiSelect-selectMenu"]: {
      whiteSpace: "normal !imporant",
    },
  },
  patho: {
    "&:hover": {
      background: "#a5d6a7",
    },
  },
  selected: {
    backgroundColor: "#a5d6a7 !important",
  },
  doctorSelect: {
    color: "black",
    "&.placeholder": {
      color: "black !important",
    },
  },
}));

const ADD_PATIENT = apis.addPatient;
const UPDATE_PATIENT = apis.updatePatient;
const FIND_ONE_PATIENT = apis.findOnePatient;
const FIND_ALL_CATE = apis.findAllCate;
const FIND_ALL_PATHO = apis.findAllPatho;

export default function PatientForm(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const {
    isOpenForm,
    isLoading,
    categories,
    pathos,
    doctors,
    patient,
    mode,
  } = useSelector((state) => state["Patients"]);
  const { dataUserAuth } = useSelector((state) => state["Login"]);
  const { isOpenAddress } = useSelector((state) => state["Addresses"]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  //// addresssssssssssssss
  const [selectedProvince, setSelectedProvince] = useState("");
  const [selectedCity, setSelectedCity] = useState("");
  const [selectedWard, setSelectedWard] = useState("");

  const [listFoundProvince, setListFoundProvince] = useState([]);
  const [listFoundWard, setListFoundWard] = useState([]);

  const listCity = _.sortedUniq(AddressData.address.map((item) => item.city));
  const getCity = () => {
    return listCity.map((item) => {
      return { city: item };
    });
  };
  /////
  const [findAllCate, dataCate] = useMyLazyQuery(FIND_ALL_CATE, {
    onCompleted: () => {
      if (dataCate) {
        dispatch(actions.handleFindAllCateSuccess(dataCate));
      } else dispatch(actions.handleFindAllCateFail());
    },
    fetchPolicy: "network-only",
  });
  const [findAllPatho, dataPatho] = useMyLazyQuery(FIND_ALL_PATHO, {
    onCompleted: () => {
      if (dataPatho) {
        dispatch(actions.handleFindAllPathoSuccess(dataPatho));
      } else dispatch(actions.handleFindAllPathoFail());
    },
    fetchPolicy: "network-only",
  });
  const [findAllDoc, dataDoc] = useMyLazyQuery(findAllLessDoctors, {
    onCompleted: () => {
      if (dataDoc) {
        dispatch(actions.handleFindAllDoctorSuccess(dataDoc));
      } else dispatch(actions.handleFindAllDoctorFail());
    },
    fetchPolicy: "network-only",
  });

  const [findOne, dataPatient] = useMyLazyQuery(FIND_ONE_PATIENT, {
    onCompleted: () => {
      if (dataPatient.data) {
        dispatch(actions.handleFindOnePatientSuccess(dataPatient.data));
      } else dispatch(actions.handleFindOnePatientFail());
    },
    fetchPolicy: "network-only",
  });

  const [addPatient, resAddPatient] = useMutation(ADD_PATIENT, {
    errorPolicy: "all",
  });
  const [updatePatient, resUpdatePatient] = useMutation(UPDATE_PATIENT, {
    errorPolicy: "all",
  });

  const [selectedDate, setSelectedDate] = React.useState(
    moment().format("DD-MM-YYYY")
  );
  const params = useParams();

  const [vaxcinTime, setVaxcinTime] = useState(0);

  const [addressDoctor, setAddressDoctor] = useState([]);
  const [selectedAddress, setSelectedAddress] = useState();
  const [gender, setGender] = useState("male");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [emailAddress, setEmailAddress] = useState("");
  const [inactive, setInactive] = useState(null);
  const { register, handleSubmit, setValue, control } = useForm();
  const [isEdit, setIsEdit] = useState(false);
  const [err1, setErr1] = useState(false);
  const [firstname, setFirstname] = useState("");
  const [err2, setErr2] = useState(false);
  const [lastname, setLastname] = useState("");
  const [err3, setErr3] = useState(false);
  const [cancelForm, setCancelForm] = useState(false);
  const [dataOk, setDataOk] = useState(null);
  const [selectedCate, setSelectedCate] = useState("");
  const [errPhone, setErrPhone] = useState(false);
  const [phone, setPhone] = useState("");
  const [errPhone1, setErrPhone1] = useState(false);
  const [phone1, setPhone1] = useState("");
  const [errPhone2, setErrPhone2] = useState(false);
  const [phone2, setPhone2] = useState("");
  const [errPhone3, setErrPhone3] = useState(false);
  const [phone3, setPhone3] = useState("");
  const [errQty, setErrQty] = useState(false);
  const [qty, setQty] = useState(0);
  const [source, setSource] = useState("");
  const [errVx, setErrVx] = useState(false);
  const [errFirstCheck, setErrFirstCheck] = useState(false);
  const [selectedFirstCheck, setSelectedFirstCheck] = useState("");

  const [selectedYtnc, setSelectedYtnc] = useState([]);

  const [patho, setPatho] = useState(null);
  const [selectedDoctor, setSelectedDoctor] = useState(
    dataUserAuth.roles === "admin" ? "" : window.sessionStorage.getItem("id")
  );
  const [listDoc, setListDoc] = useState(null);
  const [doctorName, setDoctorName] = useState("");

  const [errFirstSick, setErrFirstSick] = useState(false);
  const [firstSick, setFirstSick] = useState("");

  // const [doctorId, setDoctorId] = useState()
  useEffect(() => {
    if (doctors != null)
      setListDoc(
        doctors
          ?.map((item) => {
            return {
              title: `${item.user.lastname} ${item.user.firstname}`,
              id: item.user.id,
            };
          })
          .filter((item) => item != null)
      );
    // setDoctorName()
  }, [doctors]);

  useEffect(() => {
    if (!props.isEdit) {
      setValue("dob", moment());
    }
    dispatch(actions.handleFindOnePatient());
    findOne({
      id: params.id,
    });
  }, []);

  const handleOk = () => {
    if (!props.isEdit) {
      if (history.location.pathname === "/admin/dashboard")
        dispatch(actionsDashboard.handleAddPatientDashboard());
      else dispatch(actions.handleAddPatient());
      addPatient({
        variables: {
          ...dataOk,
          doctorid:
            dataUserAuth.roles === "admin"
              ? dataOk.doctorid
              : window.sessionStorage.getItem("id"),
          otherreason: dataOk.otherreason || `${" "}`,
          firstname: dataOk.firstname,
          lastname: dataOk.lastname,
          email: dataOk.email,
          otherid: dataOk.otherid || `${" "}`,
          phone: `(+84) ${dataOk.phone}`,
          gender: gender || `${"other"}`,
          category: selectedCate,
          phoneemerency1: `(+84) ${dataOk.phoneemerency1}`,
          phoneemerency2: `(+84) ${dataOk.phoneemerency2}`,
          phoneemerency3: `(+84) ${dataOk.phoneemerency3}`,
          quantityfamily: parseFloat(dataOk.quantityfamily) || 0,
          notesatussick: dataOk.notesatussick || " ",
          pathogenicid:
            selectedYtnc.map((item) => item.id).length === 0
              ? pathos
                  ?.map((item) => {
                    if (item.name === "Không") return item.id;
                  })
                  .filter((item) => item != null)
              : selectedYtnc.map((item) => item.id),
          pathogenicnameother: dataOk.pathogenicnameother || "",
          pathogenicname: listTc.map((item) => item.name).join(";") || "",
          beforesick: dataOk.beforesick || " ",
          quantittyvacin: parseFloat(vaxcinTime) || 0,
          source: dataOk.source || "",
          name: dataOk.name || "",
          ward: dataOk.ward || "",
          city: dataOk.city || "",
          province: dataOk.province || "",
        },
      }).then((res) => {
        if (res.data === null || res.errors) {
          if (history.location.pathname === "/admin/dashboard")
            dispatch(
              actionsDashboard.handleAddPatientDashboardFail(
                res.errors[0].message || "Phone wrong"
              )
            );
          else
            dispatch(
              actions.handleAddPatientFail(
                res.errors[0].message || "Phone wrong"
              )
            );
          setIsModalOpen(false);
          if (history.location.pathname === "/admin/dashboard")
            dispatch(actionsDashboard.handleOpenDashboardNoti());
          else dispatch(actions.handleOpenNoti());
          // showNoti();
        } else {
          if (history.location.pathname === "/admin/dashboard")
            dispatch(actionsDashboard.handleAddPatientDashboardSuccess(res));
          else dispatch(actions.handleAddPatientSuccess(res));
          // history.push("/admin/doctors-management");
          if (history.location.pathname === "/admin/dashboard")
            dispatch(actionsDashboard.handleCloseDashboardForm());
          dispatch(actions.handleCloseForm());
          setIsModalOpen(false);
          if (history.location.pathname === "/admin/dashboard")
            dispatch(actionsDashboard.handleOpenDashboardNoti());
          else dispatch(actions.handleOpenNoti());
          // showNoti();
        }
      });
    } else if (props.isEdit) {
      dispatch(actions.handleUpdatePatient());
      updatePatient({
        variables: {
          ...dataOk,
          doctorid:
            dataUserAuth.roles === "admin"
              ? dataOk.doctorid
              : window.sessionStorage.getItem("id"),
          otherreason: dataOk.otherreason || `${" "}`,
          notesatussick: dataOk.notesatussick,
          name: dataOk.name || "",
          ward: dataOk.ward || "",
          city: dataOk.city || "",
          province: dataOk.province || "",
          id: patient.id,
          firstname: dataOk.firstname,
          lastname: dataOk.lastname,
          email: dataOk.email,
          otherid: dataOk.otherid || `${" "}`,
          phone: `(+84) ${dataOk.phone}`,
          category: selectedCate,
          phoneemerency1: `(+84) ${dataOk.phoneemerency1}`,
          phoneemerency2: `(+84) ${dataOk.phoneemerency2}`,
          phoneemerency3: `(+84) ${dataOk.phoneemerency3}`,
          quantityfamily: parseFloat(dataOk.quantityfamily) || 0,
          pathogenicid:
            selectedYtnc.map((item) => item.id).length === 0
              ? pathos
                  ?.map((item) => {
                    if (item.name === "Không") return item;
                  })
                  .filter((item) => item != null)
              : selectedYtnc.map((item) => item.id),
          pathogenicnameother: dataOk.pathogenicnameother || "",
          pathogenicname: listTc.map((item) => item.name).join(";") || "",
          berforesick: dataOk.berforsick || `${" "}`,
          quantittyvacin: parseFloat(vaxcinTime) || 0,
          source: dataOk.source || "",
          gender: gender,
        },
      }).then((res) => {
        if (res.data === null || res.errors) {
          dispatch(
            actions.handleUpdatePatientFail(
              res.errors[0].message || "Phone wrong"
            )
          );
          setIsModalOpen(false);
          dispatch(actions.handleOpenNoti());
          // showNoti();
        } else {
          dispatch(actions.handleUpdatePatientSuccess(res.data));
          dispatch(actions.handleCloseForm());
          setIsModalOpen(false);
          dispatch(actions.handleOpenNoti());
          location.reload();
          // showNoti();
        }
      });
    }
  };
  const [listTc, setListTc] = useState([]);

  useEffect(() => {
    if (categories.length === 0) {
      dispatch(actions.handleFindAllCate());
      findAllCate({});
    }
    if (doctors.length === 0) {
      dispatch(actions.handleFindAllDoctor());
      findAllDoc({ adminid: window.sessionStorage.getItem("id") });
    }
    if (pathos.length === 0) {
      dispatch(actions.handleFindAllPatho());
      findAllPatho({});
    }
    // setListCate(categories.find_all_category);
  }, [isOpenForm]);
  useEffect(() => {
    // console.log(patient)
    // console.log(props.isEdit)
    if (props.isEdit) {
      if (patient) {
        // console.log(patient);
        setValue("doctorid", patient.doctor.id);
        setSelectedDoctor(patient.doctor.id);
        setDoctorName({
          title: `${patient.doctor.user.lastname} ${patient.doctor.user.firstname}`,
          id: patient.doctor.id,
        });
        setValue("localcode", patient.localcode);
        setValue("firstname", patient.firstname);
        setValue("lastname", patient.lastname);
        setValue("phone", patient.phone?.replace("(+84)", "").trim());
        setValue("dob", patient.dob);
        // console.log(patient.gender);
        setValue("gender", patient.gender);
        setGender(patient.gender);
        setValue("category", patient.category);
        setSelectedCate(patient.category.id);
        setValue("otherid", patient.otherid);
        setValue(
          "phoneemerency1",
          patient.phoneemerency1?.replace("(+84)", "").trim()
        );
        setValue(
          "phoneemerency2",
          patient.phoneemerency2?.replace("(+84)", "").trim()
        );
        setValue(
          "phoneemerency3",
          patient.phoneemerency3?.replace("(+84)", "").trim()
        );
        setValue("quantityfamily", patient.quantityfamily);
        // setValue("firstdatecovid", patient.firstdatecovid);
        setValue(
          "firstdatecovid",
          moment(patient.firstdatecovid).format("DD/MM/YYYY").toString() ===
            "01/01/2200"
            ? null
            : moment(patient.firstdatecovid)
        );

        setValue(
          "firstdatequicktest",
          moment(patient.firstdatequicktest).format("DD/MM/YYYY").toString() ===
            "01/01/2200"
            ? null
            : moment(patient.firstdatequicktest)
        );
        const selectedTempYtnc = patient.pathogenic.map((item) => {
          return { title: item.name, id: item.id };
        });
        setSelectedYtnc(selectedTempYtnc);
        setValue("otherreason", patient.otherreason);
        setValue("beforesick", patient.beforesick);
        setValue("notesatussick", patient.notesatussick);
        setValue(
          "pathogenicdate",
          moment(patient.pathogenicdate).format("DD/MM/YYYY").toString() ===
            "01/01/2200"
            ? null
            : moment(patient.pathogenicdate)
        );
        setValue("pathogenicnameother", patient.pathogenicnameother);

        setListTc(
          patient.pathogenicname.split(";").map((item) => {
            return { name: item };
          })
        );
        // setValue("pathogenicname",patient.pathogenicname.split(";").map((item) => {
        //   return { name: item };
        // }));
        // setValue("quantittyvacin", patient.quantittyvacin);
        setVaxcinTime(patient.quantittyvacin);

        // setValue("source", patient.source);
        setSource(patient.source);
        setValue("name", patient.name);
        if (_.find(getCity(), { city: patient.city })) {
          setSelectedCity({
            city: patient.city,
          });
          let provinceCheck = _.uniqBy(
            AddressData.address.map((item) => {
              if (item.city === patient.city) {
                return {
                  city: item.city,
                  province: item.province,
                };
              }
            }),
            function (e) {
              if (e) return e.province;
            }
          );
          if (
            _.find(
              provinceCheck.filter((item) => item != null, {
                province: patient.province,
              })
            )
          ) {
            setSelectedProvince({
              city: patient.city,
              province: patient.province,
            });
            setListFoundProvince(
              _.uniqBy(
                AddressData.address.map((item) => {
                  if (item.city === patient.city) {
                    return {
                      city: item.city,
                      province: item.province,
                    };
                  }
                }),
                function (e) {
                  if (e) return e.province;
                }
              )
            );
            let wardCheck = AddressData.address
              .filter((item) => {
                if (
                  item.city === patient.city &&
                  item.province === patient.province
                )
                  return item;
              })
              .map((item) => {
                return { ward: item.ward };
              });
            if (_.find(wardCheck, { ward: patient.ward })) {
              setSelectedWard({ ward: patient.ward });
              setListFoundWard(
                AddressData.address
                  .filter((item) => {
                    if (
                      item.city === patient.city &&
                      item.province === patient.province
                    )
                      return item;
                  })
                  .map((item) => {
                    return { ward: item.ward };
                  })
              );
            } else setSelectedWard("");
          } else setSelectedProvince("");
        } else setSelectedCity("");
      }
    } else if (props.patient && !props.isEdit) {
      setValue("doctorid", props.patient.row.doctor.id);
      setSelectedDoctor(props.patient.row.doctor.id);
      setValue("firstname", props.patient.row.firstname);
      setValue("lastname", props.patient.row.lastname);
      setValue("phone", props.patient.row.phone?.replace("(+84)", "").trim());
      setValue("dob", props.patient.row.dob);
      setValue("localcode", props.patient.row.localcode);
      // console.log(props.patient.row.gender);
      setValue("gender", props.patient.row.gender);
      setSource(patient.source);
      setGender(props.patient.row.gender);
      setValue("category", props.patient.row.category);
      setSelectedCate(props.patient.row.category.id);
      setValue("otherid", props.patient.row.otherid);

      setValue(
        "phoneemerency1",
        props.patient.row.phoneemerency1?.replace("(+84)", "").trim()
      );
      setValue(
        "phoneemerency2",
        props.patient.row.phoneemerency2?.replace("(+84)", "").trim()
      );
      setValue(
        "phoneemerency3",
        props.patient.row.phoneemerency3?.replace("(+84)", "").trim()
      );
      setValue("quantityfamily", props.patient.row.quantityfamily);
      // setValue("firstdatecovid", props.patient.row.firstdatecovid);
      setValue(
        "firstdatecovid",
        moment(props.patient.row.firstdatecovid)
          .format("DD/MM/YYYY")
          .toString() === "01/01/2200"
          ? null
          : moment(props.patient.row.firstdatecovid)
      );
      setValue(
        "firstdatequicktest",
        moment(props.patient.row.firstdatequicktest)
          .format("DD/MM/YYYY")
          .toString() === "01/01/2200"
          ? null
          : moment(props.patient.row.firstdatequicktest)
      );
      const selectedTempYtnc = props.patient.row.pathogenic.map((item) => {
        return { title: item.name, id: item.id };
      });

      setSelectedYtnc(selectedTempYtnc);
      setValue("otherreason", props.patient.row.otherreason);
      setValue("beforesick", props.patient.row.beforesick);
      setValue("notesatussick", props.patient.row.notesatussick);
      setValue(
        "pathogenicdate",
        moment(props.patient.row.pathogenicdate)
          .format("DD/MM/YYYY")
          .toString() === "01/01/2200"
          ? null
          : moment(props.patient.row.pathogenicdate)
      );
      setListTc(
        patient.pathogenicname.split(";").map((item) => {
          return { name: item };
        })
      );
      // setValue("pathogenicname", props.patient.row.pathogenicname);
      // setValue("pathogenicname", patient.pathogenicname.split(";").map((item) => {
      //   return { name: item };
      // }));
      // setValue("quantittyvacin", props.patient.row.quantittyvacin);
      setVaxcinTime(props.patient.row.quantittyvacin);

      setValue("name", props.patient.row.name);
      setValue("ward", props.patient.row.ward);
      setValue("province", props.patient.row.province);
      setValue("city", props.patient.row.city);
    }
  }, [patient]);

  const onSubmit = (dataForm) => {
    // console.log(selectedYtnc) firstdatequicktest
    setDataOk({
      ...dataForm,
      firstdatecovid:
        dataForm.firstdatecovid === null
          ? moment("1/1/2200").utc()
          : moment(dataForm.firstdatecovid).utc(),
      firstdatequicktest:
        dataForm.firstdatequicktest === null
          ? moment("1/1/2200").utc()
          : moment(dataForm.firstdatequicktest).utc(),
      firstcheckhealth: moment(dataForm.firstcheckhealth).utc(),
      pathogenicdate:
        dataForm.pathogenicdate === null
          ? moment("1/1/2200").utc()
          : moment(dataForm.pathogenicdate).utc(),
      dob: moment(dataForm.dob).utc(),
      ward: selectedWard.ward || "",
      city: selectedCity.city || "",
      province: selectedProvince.province || "",
      source: source,
      // pathogenicid: selectedYtnc,
    });
    // console.log(dataForm.pathogenicid)

    // console.log(dataForm);
    setIsModalOpen(true);
  };
  const listReason = [
    {
      name: "Sốt hoặc cảm thấy muốn sốt (nóng, lạnh run, đổ mồ hôi",
    },
    {
      name: "Khó thở",
    },
    {
      name: "Đau ngực nhẹ",
    },
    {
      name: "Đau họng",
    },
    {
      name: "Ho: khan hay đàm",
    },
    {
      name: "Đau cơ hoặc đau mỏi khắp người",
    },
    {
      name: "Nôn ói",
    },
    {
      name: "Tiêu chảy",
    },
    {
      name: "Mất vị giác hoặc khứu giác mới xảy ra",
    },
    {
      name: "Nghẹt mũi",
    },
    {
      name: "Sổ mũi",
    },
    {
      name: "Mệt mỏi",
    },
    {
      name: "Đau nhức đầu",
    },
    {
      name: "Không có",
    },
    {
      name: "Khác",
    },
  ];

  return (
    <Grid container component="main" className={classes.root}>
      <Backdrop
        className={classes.backdrop}
        open={dataDoc.loading || dataPatho.loading || dataCate.loading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>

      <Grid className={classes.right} item xs={12} sm={12} md={12}>
        <div className={classes.paper}>
          <div className={classes.top}>
            {!props.isEdit && !props.patient ? (
              <LazyLoadImage
                width="70em"
                className={classes.logo}
                alt="medx.vn"
                src={logo}
              />
            ) : (
              <LazyLoadImage
                width="70em"
                className={classes.logo}
                alt="medx.vn"
                src={blank_avt}
              />
            )}
            <Typography className={classes.typo} component="h1" variant="h5">
              {props.isEdit || props.patient
                ? `${
                    patient?.lastname
                      ? patient?.lastname?.toUpperCase()
                      : props.patient
                      ? props.patient.row.lastname
                      : ""
                  }${" "}
                ${
                  patient?.firstname
                    ? patient?.firstname?.toUpperCase()
                    : props.patient
                    ? props.patient.row.firstname
                    : ""
                }${" "}`
                : "THÊM BỆNH NHÂN"}
            </Typography>
            {!props.patient && (
              <Button
                onClick={() => {
                  setCancelForm(true);
                }}
                style={{ position: "absolute", top: "0.1em", right: 0 }}
              >
                <ClearIcon></ClearIcon>
              </Button>
            )}
          </div>
          {(props.isEdit || props.patient) && (
            <>
              <div style={{ display: "flex" }}>
                <p className={classes.label}>Điện thoại:</p>
                <p className={classes.value}>
                  {patient?.phone
                    ? patient.phone
                    : props?.patient?.row?.phone
                    ? props.patient.row.phone
                    : ""}
                </p>
              </div>
            </>
          )}

          <form
            key="doctor_form"
            onSubmit={handleSubmit(onSubmit)}
            className={classes.form}
            noValidate
          >
            <div className={classes.body}>
              <Grid container spacing={2}>
                {dataUserAuth.roles === "admin" && (
                  <>
                    <div style={{ fontSize: "0.9em", paddingLeft: "0.5em" }}>
                      Chọn bác sĩ
                    </div>

                    <Grid item xs={12} sm={12} md={12}>
                      <Controller
                        control={control}
                        name="doctorid"
                        render={({
                          field: { onChange, onBlur, value, ref },
                        }) => (
                          <Autocomplete
                            fullWidth
                            margin="normal"
                            label="Chọn bác sĩ"
                            disabled={listDoc == []}
                            {...register("doctorid")}
                            id="name"
                            onChange={(event, value) => {
                              if (value) {
                                setValue("doctorid", value.id);
                                setSelectedDoctor(value.id);
                                setDoctorName(value);
                              }
                            }}
                            options={listDoc}
                            getOptionLabel={(option) => option?.title}
                            renderInput={(params) => {
                              return (
                                <TextField
                                  placeholder="Chọn bác sĩ"
                                  InputProps={{
                                    startAdornment: (
                                      <InputAdornment
                                        style={{ marginLeft: "-0.1em" }}
                                        position="end"
                                      ></InputAdornment>
                                    ),
                                    classes: { input: classes.doctorSelect },
                                  }}
                                  {...{
                                    ...params,
                                    inputProps: {
                                      ...params.inputProps,
                                      value: doctorName.title,
                                    },
                                  }}
                                  onChange={(e) => {
                                    setDoctorName(e);
                                  }}
                                  variant="outlined"
                                />
                              );
                            }}
                          />
                        )}
                      />
                    </Grid>
                  </>
                )}

                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="lastname"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        disabled={props?.patient}
                        fullWidth
                        {...register("lastname")}
                        label="Họ và tên lót"
                        error={err2}
                        onInput={(e) => {
                          if (e.target.value.length > 0) setErr2(false);
                          else setErr2(true);
                          setLastname(e.target.value);
                        }}
                        helperText={err2 && "Vui lòng nhập Họ và tên lót"}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        id="lastname"
                        onChange={(e) => setValue("lastname", e.target.value)}
                        value={value}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="firstname"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        disabled={props?.patient}
                        {...register("firstname")}
                        label="Tên"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        error={err1}
                        onInput={(e) => {
                          if (e.target.value.length > 0) setErr1(false);
                          else setErr1(true);
                          setFirstname(e.target.value);
                        }}
                        helperText={err1 && "Vui lòng nhập tên"}
                        id="firstname"
                        onChange={(e) => setValue("firstname", e.target.value)}
                        value={value}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="otherid"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        style={{ marginTop: "0.1em" }}
                        fullWidth
                        disabled={props?.patient}
                        {...register("otherid")}
                        label="Mã bệnh nhân khác (Nếu có)"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        onChange={(e) => setValue("otherid", e.target.value)}
                        value={value}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="localcode"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        style={{ marginTop: "0.1em" }}
                        fullWidth
                        disabled={props?.patient}
                        {...register("localcode")}
                        label="Mã bệnh nhân do địa phương cung cấp"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        onChange={(e) => setValue("localcode", e.target.value)}
                        value={value}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    {...register("source")}
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControl
                        style={{ marginTop: "0.1em" }}
                        variant="outlined"
                        className={classes.formControl}
                        required
                        fullWidth
                      >
                        <InputLabel>Nơi chuyển viện</InputLabel>
                        <Select
                          id="demo-simple-select-outlined"
                          label="Nơi chuyển viện"
                          {...register("source")}
                          value={source}
                          disabled={props?.patient}
                          onChange={(e) => setSource(e.target.value)}
                        >
                          <MenuItem className={classes.patho} value={"hopital"}>
                            Bệnh viện
                          </MenuItem>
                          <MenuItem
                            className={classes.patho}
                            value={"healthfacilities"}
                          >
                            Phòng khám
                          </MenuItem>
                          <MenuItem className={classes.patho} value={"single"}>
                            Tự đăng ký
                          </MenuItem>
                          <MenuItem
                            className={classes.patho}
                            value={"followed"}
                          >
                            F1 đang theo dõi
                          </MenuItem>
                          <MenuItem className={classes.patho} value={"other"}>
                            Khác
                          </MenuItem>
                        </Select>
                      </FormControl>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    {...register("gender")}
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControl
                        style={{ marginTop: "0.1em" }}
                        variant="outlined"
                        className={classes.formControl}
                        required
                        fullWidth
                      >
                        <InputLabel>Giới tính</InputLabel>
                        <Select
                          id="demo-simple-select-outlined"
                          label="Giới tính"
                          {...register("gender")}
                          value={gender}
                          disabled={props?.patient}
                          onChange={(e) => setGender(e.target.value)}
                        >
                          <MenuItem className={classes.patho} value={"male"}>
                            Nam
                          </MenuItem>
                          <MenuItem className={classes.patho} value={"female"}>
                            Nữ
                          </MenuItem>
                          <MenuItem className={classes.patho} value={"other"}>
                            Không xác định
                          </MenuItem>
                        </Select>
                      </FormControl>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="dob"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          variant="inline"
                          format="dd-MM-yyyy"
                          id="date-picker-inline"
                          fullWidth
                          required
                          label="Ngày sinh"
                          disabled={props?.patient}
                          value={value}
                          defaultValue={moment()}
                          onChange={(e) => {
                            setValue("dob", e);
                            setErr3(
                              e?.toString() === "Invalid Date" || e == null
                            );
                          }}
                          error={
                            value?.toString() === "Invalid Date" ||
                            value == null
                          }
                          helperText={
                            (value?.toString() === "Invalid Date" ||
                              value == null) &&
                            "Ngày không hợp lệ"
                          }
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    )}
                  />
                </Grid>
              </Grid>

              <Controller
                control={control}
                name="category"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <FormControl
                    style={{ marginTop: "1em" }}
                    variant="outlined"
                    className={classes.formControl}
                    required
                  >
                    <InputLabel>Khám sàng lọc</InputLabel>
                    <Select
                      id="demo-simple-select-outlined"
                      label="Khám sàng lọc"
                      {...register("category")}
                      value={selectedCate}
                      disabled={props?.patient}
                      onChange={(e) => {
                        setValue("category", e.target.value);
                        setSelectedCate(e.target.value);
                      }}
                    >
                      {categories?.map((item) => {
                        return (
                          <MenuItem
                            className={classes.patho}
                            key={item.id}
                            value={item.id}
                          >
                            {item.name}
                          </MenuItem>
                        );
                      })}
                    </Select>
                  </FormControl>
                )}
              />
              <Controller
                control={control}
                name="phone"
                render={({ field: { value } }) => (
                  <TextField
                    error={errPhone}
                    variant="outlined"
                    margin="normal"
                    disabled={props?.patient}
                    {...register("phone")}
                    required
                    label="Số điện thoại"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          ( +84 )
                        </InputAdornment>
                      ),
                    }}
                    helperText={errPhone && "Số điện thoại phải từ 9-11 ký tự"}
                    type="number"
                    onInput={(e) => {
                      e.target.value = Math.max(0, parseInt(e.target.value))
                        .toString()
                        .slice(0, 20);
                      if (e.target.value.length < 9) setErrPhone(true);
                      else setErrPhone(false);
                      setPhone(e.target.value);
                      setValue("phone", e.target.value);
                    }}
                    value={value}
                  />
                )}
              />
              <Controller
                control={control}
                name="phoneemerency1"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    error={errPhone1}
                    disabled={props?.patient}
                    margin="normal"
                    {...register("phoneemerency1")}
                    label="Số điện thoại người thân 1"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          ( +84 )
                        </InputAdornment>
                      ),
                    }}
                    helperText={errPhone1 && "Số điện thoại phải từ 9-11 ký tự"}
                    value={value}
                    onInput={(e) => {
                      if (e.target.value) {
                        e.target.value = Math.max(0, parseInt(e.target.value))
                          .toString()
                          .slice(0, 20);
                        if (e.target.value.length < 9) setErrPhone1(true);
                        else setErrPhone1(false);
                        setPhone1(e.target.value);
                      } else setErrPhone1(false);
                      setValue("phoneemerency1", e.target.value);
                    }}
                    type="number"
                  />
                )}
              />
              <Controller
                control={control}
                name="phoneemerency2"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    disabled={props?.patient}
                    error={errPhone2}
                    {...register("phoneemerency2")}
                    label="Số điện thoại người thân 2"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          ( +84 )
                        </InputAdornment>
                      ),
                    }}
                    value={value}
                    helperText={errPhone2 && "Số điện thoại phải từ 9-11 ký tự"}
                    onInput={(e) => {
                      if (e.target.value) {
                        e.target.value = Math.max(0, parseInt(e.target.value))
                          .toString()
                          .slice(0, 20);
                        if (e.target.value.length < 9) setErrPhone2(true);
                        else setErrPhone2(false);
                        setPhone2(e.target.value);
                      } else setErrPhone2(false);
                      setValue("phoneemerency2", e.target.value);
                    }}
                    type="number"
                  />
                )}
              />
              <Controller
                control={control}
                name="phoneemerency3"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    error={errPhone3}
                    {...register("phoneemerency3")}
                    disabled={props?.patient}
                    label="Số điện thoại người thân 3"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          ( +84 )
                        </InputAdornment>
                      ),
                    }}
                    helperText={errPhone3 && "Số điện thoại phải từ 9-11 ký tự"}
                    value={value}
                    onInput={(e) => {
                      if (e.target.value) {
                        e.target.value = Math.max(0, parseInt(e.target.value))
                          .toString()
                          .slice(0, 20);
                        if (e.target.value.length < 9) setErrPhone3(true);
                        else setErrPhone3(false);
                        setPhone3(e.target.value);
                      } else setErrPhone3(false);
                      setValue("phoneemerency3", e.target.value);
                    }}
                    type="number"
                  />
                )}
              />
              <Controller
                control={control}
                name="quantityfamily"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    disabled={props?.patient}
                    margin="normal"
                    {...register("quantityfamily")}
                    label="Số người ở chung nhà"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">Người</InputAdornment>
                      ),
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    error={errQty}
                    helperText={errQty && "Xin nhập số người ở chung nhà."}
                    onInput={(e) => {
                      if (e.target.value >= 0) setErrQty(false);
                      else setErrQty(true);
                      setValue("quantityfamily", e.target.value);
                      setQty(e.target.value);
                    }}
                    placeholder="0"
                    value={value}
                    type="number"
                  />
                )}
              />
              <Controller
                control={control}
                name="quantittyvacin"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    disabled={props?.patient}
                    margin="normal"
                    {...register("quantittyvacin")}
                    label="Số lần tiêm vắc xin"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">Lần</InputAdornment>
                      ),
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    error={errVx}
                    helperText={errVx && "Xin nhập số lần tiêm vắc xin."}
                    onInput={(e) => {
                      if (e.target.value >= 0) setErrVx(false);
                      else setErrVx(true);
                      setVaxcinTime(e.target.value);
                    }}
                    placeholder="0"
                    value={vaxcinTime}
                    type="number"
                  />
                )}
              />
              {pathos?.length > 0 && (
                <Autocomplete
                  multiple
                  filterSelectedOptions
                  style={{ marginTop: "0.5em" }}
                  id="multiple-limit-tags"
                  {...register("pathogenicid")}
                  options={pathos?.map((item) => {
                    return {
                      title: item.name,
                      id: item.id,
                    };
                  })}
                  value={
                    selectedYtnc.length === 0
                      ? pathos
                          ?.map((item) => {
                            return {
                              title: item.name,
                              id: item.id,
                            };
                          })
                          .filter((item) => item.title === "Không")
                      : selectedYtnc
                  }
                  onChange={(e, value) => {
                    // console.log(value);
                    setSelectedYtnc(_.uniqBy(value, "id"));
                  }}
                  getOptionLabel={(option) => option.title}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="outlined"
                      label="Yếu tố nguy cơ"
                    />
                  )}
                />
              )}
              <Grid style={{ marginTop: "0.4em" }} item xs={12} sm={12} md={12}>
                <Controller
                  control={control}
                  name="otherreason"
                  render={({ field: { value } }) => (
                    <TextField
                      variant="outlined"
                      fullWidth
                      margin="normal"
                      disabled={props?.patient}
                      multiline
                      rows={2}
                      maxRows={5}
                      error={errPhone3}
                      {...register("otherreason")}
                      label="Yếu tố nguy cơ khác"
                      value={value}
                      InputProps={{
                        startAdornment: (
                          <InputAdornment
                            style={{ marginLeft: "-0.1em" }}
                            position="end"
                          ></InputAdornment>
                        ),
                      }}
                      onInput={(e) => {
                        setValue("otherreason", e.target.value);
                      }}
                    />
                  )}
                />
              </Grid>
              <Grid style={{ marginTop: "0.1em" }} container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="pathogenicdate"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          variant="inline"
                          format="dd-MM-yyyy"
                          fullWidth
                          disabled={props?.patient}
                          margin="normal"
                          label="Ngày có triệu chứng đầu tiên"
                          onChange={onChange}
                          // error={err3}
                          // helperText={
                          //   err3 && "Vui lòng nhập ngày có triệu chứng đầu tiên"
                          // }
                          value={value}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    {...register("pathogenicname")}
                    render={({ field: { value } }) => (
                      <Autocomplete
                        multiple
                        filterSelectedOptions
                        style={{ marginTop: "0.5em" }}
                        id="multiple-limit-tags"
                        {...register("pathogenicname")}
                        // disabled
                        options={listReason}
                        value={listTc}
                        onChange={(e, value) => {
                          if (value) setErrFirstSick(false);
                          else setErrFirstSick(true);
                          setFirstSick(value);
                          // setValue("pathogenicname",value);
                          setListTc(_.uniqBy(value, "name"));
                        }}
                        getOptionLabel={(option) => option.name}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            variant="outlined"
                            label="Triệu chứng đầu tiên"
                          />
                        )}
                      />
                    )}
                  />
                </Grid>
                {/* <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="pathogenicname"
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        required
                        error={errFirstSick}
                        disabled={props?.patient}
                        margin="normal"
                        {...register("pathogenicname")}
                        label="Triệu chứng đầu tiên"
                        value={value}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        onInput={(e) => {
                          setValue("pathogenicname", e.target.value);
                          if (e.target.value) setErrFirstSick(false);
                          else setErrFirstSick(true);
                          setFirstSick(e.target.value);
                        }}
                      />
                    )}
                  />
                </Grid> */}
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="pathogenicnameother"
                    multiline
                    rows={2}
                    maxRows={5}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        multiline
                        // disabled
                        maxRows={5}
                        {...register("pathogenicnameother")}
                        rows={2}
                        label="Triệu chứng đầu tiên khác"
                        value={value}
                        onChange={(e) =>
                          setValue("pathogenicnameother", e.target.value)
                        }
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="firstdatecovid"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          variant="inline"
                          format="dd-MM-yyyy"
                          fullWidth
                          disabled={props?.patient}
                          margin="normal"
                          id="date-picker-inline"
                          label="Ngày có kết quả PCR dương tính với SARS-CoV-2"
                          value={value}
                          defaultValue={moment()}
                          onChange={onChange}
                          // error={err3}
                          // helperText={
                          //   err3 &&
                          //   "Vui lòng nhập Ngày có KQ XN Dương tính với SARS-CoV-2"
                          // }
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="firstdatequicktest"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          variant="inline"
                          format="dd-MM-yyyy"
                          fullWidth
                          disabled={props?.patient}
                          margin="normal"
                          id="date-picker-inline"
                          label="Ngày có kết quả Test nhanh dương tính với SARS-CoV-2"
                          value={value}
                          defaultValue={moment()}
                          onChange={onChange}
                          // error={err3}
                          // helperText={
                          //   err3 &&
                          //   "Vui lòng nhập Ngày có KQ XN Dương tính với SARS-CoV-2"
                          // }
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="beforesick"
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        margin="normal"
                        multiline
                        disabled={props?.patient}
                        maxRows={5}
                        rows={2}
                        error={errPhone3}
                        {...register("beforesick")}
                        label="Bệnh mạn tính trước đây"
                        value={value}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        onInput={(e) => {
                          setValue("beforesick", e.target.value);
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Controller
                    control={control}
                    name="notesatussick"
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        fullWidth
                        margin="normal"
                        disabled={props?.patient}
                        multiline
                        maxRows={5}
                        rows={2}
                        error={errPhone3}
                        {...register("notesatussick")}
                        label="Tình trạng bệnh mạn tính hiện tại"
                        value={value}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        onChange={(e) => {
                          setValue("notesatussick", e.target.value);
                        }}
                      />
                    )}
                  />
                </Grid>
              </Grid>
              <div>
                <Grid style={{ marginTop: "0.5em" }} container spacing={2}>
                  <Grid item xs={12} sm={12} md={12}>
                    <Controller
                      control={control}
                      name="province"
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <Autocomplete
                          options={getCity()}
                          {...register("province")}
                          fullWidth
                          style={{ marginTop: "1em" }}
                          value={selectedCity}
                          // disabled
                          onChange={(option, value) => {
                            setSelectedWard("");
                            setSelectedProvince("");
                            if (value !== null) {
                              setSelectedCity(value);
                              setListFoundProvince(
                                _.uniqBy(
                                  AddressData.address.map((item) => {
                                    if (item.city === value.city) {
                                      return {
                                        city: item.city,
                                        province: item.province,
                                      };
                                    }
                                  }),
                                  function (e) {
                                    if (e) return e.province;
                                  }
                                )
                              );
                            } else setSelectedCity("");
                          }}
                          getOptionLabel={(option) => option.city}
                          renderInput={(params) => (
                            <TextField
                              fullWidth
                              {...params}
                              label="Tỉnh/ Thành phố"
                              variant="outlined"
                            />
                          )}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <Controller
                      control={control}
                      name="city"
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <Autocomplete
                          options={listFoundProvince.filter(
                            (item) => item != null
                          )}
                          {...register("city")}
                          style={{ marginTop: "0.5em" }}
                          fullWidth
                          disabled={selectedCity.length === 0}
                          value={selectedProvince}
                          onChange={(option, value) => {
                            setSelectedWard("");
                            if (value !== null) {
                              setSelectedProvince(value);
                              setListFoundWard(
                                AddressData.address
                                  .filter((item) => {
                                    if (
                                      item.city === value.city &&
                                      item.province === value.province
                                    )
                                      return item;
                                  })
                                  .map((item) => {
                                    return { ward: item.ward };
                                  })
                              );
                            } else setSelectedProvince("");
                          }}
                          getOptionLabel={(option) => option.province}
                          renderInput={(params) => (
                            <TextField
                              fullWidth
                              {...params}
                              label="Quận/ Huyện/ Thị xã"
                              variant="outlined"
                            />
                          )}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <Controller
                      control={control}
                      name="ward"
                      margin="normal"
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <Autocomplete
                          options={listFoundWard.filter((item) => item != null)}
                          {...register("ward")}
                          fullWidth
                          disabled={
                            selectedCity.length === 0 ||
                            selectedProvince.length === 0
                          }
                          style={{ marginTop: "0.5em" }}
                          value={selectedWard}
                          onChange={(option, value) => {
                            if (value !== null) setSelectedWard(value);
                            else setSelectedWard("");
                          }}
                          getOptionLabel={(option) => option.ward}
                          renderInput={(params) => (
                            <TextField
                              fullWidth
                              {...params}
                              label="Xã/ Phường/ Thị trấn"
                              variant="outlined"
                            />
                          )}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <Controller
                      control={control}
                      name="name"
                      render={({ field: { value } }) => (
                        <TextField
                          variant="outlined"
                          fullWidth
                          style={{ marginTop: "0.5em" }}
                          disabled={props?.patient}
                          {...register("name")}
                          label="Địa chỉ"
                          value={value}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment
                                style={{ marginLeft: "-0.1em" }}
                                position="end"
                              ></InputAdornment>
                            ),
                          }}
                          onInput={(e) => {
                            setValue("name", e.target.value);
                          }}
                        />
                      )}
                    />
                  </Grid>
                </Grid>
              </div>
            </div>
            {mode !== "view" && (
              <div
                style={{
                  display: "flex",
                  alignSelf: "flex-end",
                  bottom: 0,
                  right: "3em",
                }}
              >
                {!props?.patient && (
                  <Button
                    onClick={() => {
                      setCancelForm(true);
                    }}
                    variant="contained"
                    className={classes.cancel}
                  >
                    HỦY
                  </Button>
                )}

                {props.isEdit ? (
                  <Button
                    type="submit"
                    variant="contained"
                    className={classes.submit}
                    disabled={source === "" || doctorName?.title == null}
                  >
                    LƯU
                  </Button>
                ) : props.patient ? (
                  ""
                ) : (
                  <Button
                    type="submit"
                    variant="contained"
                    className={classes.submit}
                    disabled={
                      firstname.length === 0 ||
                      lastname.length === 0 ||
                      phone.length === 0 ||
                      source === "" ||
                      err3 ||
                      selectedCate == null ||
                      errPhone || (doctorName?.title == null && selectedDoctor === "")
                    }
                  >
                    {isEdit ? "LƯU" : "THÊM"}
                  </Button>
                )}
              </div>
            )}
          </form>
        </div>
      </Grid>
      <ModalConfirm
        title="Thông báo"
        open={isModalOpen}
        disableBtn={resAddPatient?.loading || resUpdatePatient?.loading}
        description={
          props.isEdit ? "Xác nhận thay đổi" : "Xác nhận thêm thông tin"
        }
        handleOk={() => handleOk()}
        handleClose={() => setIsModalOpen(false)}
      ></ModalConfirm>
      <ModalConfirm
        // title={isEdit?"Xác nhận thay đổi":"Thêm bác sĩ"}
        title="Hủy"
        open={cancelForm}
        description="Bạn có muốn hủy thao tác?"
        handleOk={() => {
          if (history.location.pathname === "/admin/dashboard")
            dispatch(actionsDashboard.handleCloseDashboardForm());
          else {
            dispatch(actions.handleCloseForm());
            history.push(history.location.path);
          }
          setCancelForm(false);
        }}
        handleClose={() => setCancelForm(false)}
      ></ModalConfirm>
    </Grid>
  );
}
