import React, { useEffect, useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import * as actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import * as apis from "./store/api";
import logo from "assets/img/medx-logo.svg";
import MuiAlert from "@material-ui/lab/Alert";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useForm, Controller } from "react-hook-form";
import blank_avt from "assets/img/blank_avt.png";
import InputAdornment from "@material-ui/core/InputAdornment";
import moment from "moment";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import Autocomplete from "@material-ui/lab/Autocomplete";

// import { name } from "./store/reducer";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  DateTimePicker,
  KeyboardDateTimePicker,
} from "@material-ui/pickers";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import ClearIcon from "@material-ui/icons/Clear";
import Backdrop from "@material-ui/core/Backdrop";
import ImportContactsIcon from "@material-ui/icons/ImportContacts";
import Snackbar from "@material-ui/core/Snackbar";
import { findAllGroup } from "views/DoctorGroup/store/api";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  checkboxLable: {
    color: "#444",
    width: "100%",
    justifyContent: "space-between",
    "&:hover": {
      background: "#a2e0ad",
    },
    "@media (max-width: 600px)": {
      fontSize: "0.9em",
      marginTop: "0.3em",
    },
  },
  nextBtn: {
    marginTop: "0.3em",
    color: "#fff",
    background: "#37774B",
    border: "none",
    borderRadius: "0.3em",
    "&:hover": {
      background: "#6A995B",
    },
  },
  titleTab: {
    fontSize: "2em",
    "@media (max-width: 600px)": {
      fontSize: "1.6em",
      marginTop: "1em",
    },
  },
  reportBtn: {
    marginTop: "0.3em",
    color: "#fff",
    background: "#37774B",
    border: "none",
    borderRadius: "0.3em",
    "&:hover": {
      background: "#6A995B",
    },
  },
  backdrop: {
    zIndex: 4799,
    color: "#fff",
  },
  checkboxes: {
    alignSelf: "start",
    marginLeft: "0.2em",
    minWidth: "80vw",
    justifyContent: "space-between",
    marginTop: "0.2em",
    "&:hover": {
      background: "#b6e5b0",
    },
    "@media (min-width: 1024px)": {
      minWidth: "61vw",
      maxWidth: "66vw",
    },
  },
  formatStyle: {
    fontSize: "0.9rem",
    "& .MuiInputBase-root.Mui-disabled": {
      color: "rgba(0, 0, 0, 1)", // (default alpha is 0.38)
    },
  },
  disabled: {
    color: "black",
  },
}));
const ADD_MEDICAL = apis.addMedical;
const UPDATE_MEDICAL = apis.updateMedical;
const FIND_ONE_MEDICAL = apis.findOneMedical;
const SMS_GROUP = apis.sendSMSGroup;
const SMS_DOCTOR = apis.sendSMSDoctor;

export default function MedicalProfile(props) {
  const classes = useStyles();

  const {
    patient,
    isOpenForm,
    isOpenMed,
    medical,
    idEdit,
    message,
    mode,
    isOpenNoti,
    groups,
  } = useSelector((state) => state["Patients"]);

  const [findOneMedical, dataMed] = useMyLazyQuery(FIND_ONE_MEDICAL, {
    onCompleted: () => {
      if (dataMed) {
        dispatch(actions.handleFindOneMedSuccess(dataMed));
      } else dispatch(actions.handleFindOneMedFail());
    },
    fetchPolicy: "network-only",
  });

  const [findGroups, resFindAllGroup] = useMyLazyQuery(findAllGroup, {
    onCompleted: () => {
      if (resFindAllGroup.data) {
        dispatch(actions.handleFindGroupsSuccess(resFindAllGroup.data));
      } else dispatch(actions.handleFindGroupsFail());
    },
    fetchPolicy: "network-only",
  });

  const [dataSubmit, setDataSubmit] = useState(null);

  const { register, handleSubmit, setValue, control } = useForm();

  const theme = useTheme();

  const [reExam, setReExam] = useState(false);

  const [tab, setTab] = React.useState(0);

  const dispatch = useDispatch();
  const [addMedical, resMedical] = useMutation(ADD_MEDICAL, {
    errorPolicy: "all",
  });

  const [sendGroup, resSendGroup] = useMutation(SMS_GROUP, {
    errorPolicy: "all",
  });

  const [sendDoctor, resSendDoctor] = useMutation(SMS_DOCTOR, {
    errorPolicy: "all",
  });
  /////Validation

  const [isContact, setIsContact] = useState(false);
  const [selectedGroup, setSelectedGroup] = useState("");

  const [err1, setErr1] = useState(false);
  const [val1, setVal1] = useState("");
  const [err2, setErr2] = useState(false);
  const [val2, setVal2] = useState("");
  const [err3, setErr3] = useState(false);
  const [val3, setVal3] = useState("");
  const [err4, setErr4] = useState(false);
  const [val4, setVal4] = useState("");
  const [err5, setErr5] = useState(false);
  const [val5, setVal5] = useState("");
  const [err6, setErr6] = useState(false);
  const [val6, setVal6] = useState("");
  const [err7, setErr7] = useState(false);
  const [val7, setVal7] = useState("");
  const [err8, setErr8] = useState(false);
  const [val8, setVal8] = useState("");
  const [err9, setErr9] = useState(false);
  const [val9, setVal9] = useState("");
  const [err10, setErr10] = useState(false);
  const [val10, setVal10] = useState("");
  const [err11, setErr11] = useState(false);
  const [val11, setVal11] = useState("");
  const [err12, setErr12] = useState(false);
  const [val12, setVal12] = useState("");
  const [err13, setErr13] = useState(false);
  const [val13, setVal13] = useState("");

  const [s1, setS1] = useState(false);
  const [s2, setS2] = useState(false);
  const [s3, setS3] = useState(false);
  const [s4, setS4] = useState(false);
  const [s5, setS5] = useState(false);
  const [s6, setS6] = useState(false);
  const [s7, setS7] = useState(false);
  const [s8, setS8] = useState(false);
  const [s9, setS9] = useState(false);
  const [s10, setS10] = useState(false);
  const [s11, setS11] = useState(false);
  const [s12, setS12] = useState(false);
  const [s13, setS13] = useState(false);
  const [s14, setS14] = useState(false);
  const [s15, setS15] = useState(false);
  const [s16, setS16] = useState(false);
  const [s17, setS17] = useState(false);
  const [s18, setS18] = useState(false);
  const [s19, setS19] = useState(false);
  const [s20, setS20] = useState(false);
  const [s21, setS21] = useState(false);
  const [s22, setS22] = useState(false);
  const [s23, setS23] = useState(false);
  const [s24, setS24] = useState(false);
  const [s25, setS25] = useState(false);
  /////
  const [updateMedical, resUpdate] = useMutation(UPDATE_MEDICAL, {
    errorPolicy: "all",
  });

  useEffect(() => {
    if (idEdit !== "");
    dispatch(actions.handleFindOneMed());
    findOneMedical({ id: idEdit });
  }, [isOpenMed]);

  const onSubmit = (dataForm) => {
    delete dataForm.undefined;
    if (!idEdit) {
      dispatch(actions.handleAddMedical());
      addMedical({
        variables: {
          ...dataForm,
          displaydate: moment(dataForm.displaydate).utc(),
          ownerid: window.sessionStorage.getItem("id"),
          doctorid: patient.doctor.id,
          patientid: patient.id,
          attribute: dataForm.attribute || "codvidhide",
          beforesick: dataForm.beforesick || "",
          bloodpressure: parseFloat(dataForm.bloodpressure) || 0,
          bloodpressure2: parseFloat(dataForm.bloodpressure2) || 0,
          bmi: dataForm.bmi || "",
          color: dataForm.color || "no",
          countdateremains: parseFloat(dataForm.countdateremains) || 0,
          dateofreexamination: dataForm.reexamination
            ? dataForm.dateofreexamination
              ? dataForm.dateofreexamination
              : moment().utc().format()
            : moment().utc().format(),
          // datetest: dataForm.datetest || moment("1/1/2200").utc().format(),
          guide: dataForm.guide || "",
          height: parseFloat(dataForm.height) || 0,
          weight: parseFloat(dataForm.weight) || 0,
          medicalnote: dataForm.medicalnote || "",
          medicalpackage: dataForm.medicalpackage || "",
          medicalresutl: dataForm.medicalresutl || "",
          noteattribute: dataForm.noteattribute || "",
          medicine: dataForm.medicine || "",
          notebloodpressure: dataForm.notebloodpressure || "",
          notepluse: dataForm.notepluse || "",
          notesatussick: dataForm.notesatussick || "",
          notespo2home: dataForm.notespo2home || "",
          notetemperature: dataForm.notetemperature || "",
          other: dataForm.other || "",
          otherSignal: dataForm.otherSignal || "",
          otherid: dataForm.otherid || "",
          otherreason: dataForm.otherreason || "",
          pluse: parseFloat(dataForm.pluse) || 0,
          prcrealdate: dataForm.prcrealdate || moment().utc().format(),
          quantityofsick: parseFloat(dataForm.quantityofsick) || 0,
          resultsarscovid2: dataForm.resultsarscovid2 || "no",
          resulttest: dataForm.resulttest || "no",
          riskassessment: dataForm.riskassessment || "low",
          spo2home: parseFloat(dataForm.spo2home) || 0,
          statusquo: dataForm.statusquo || "",
          summary: dataForm.summary || "",
          temperature: parseFloat(dataForm.temperature) || 0,
          //signal
          signal1: s1 ? s1 : false,
          signal2: s2 ? s2 : false,
          signal3: s3 ? s3 : false,
          signal4: s4 ? s4 : false,
          signal5: s5 ? s5 : false,
          signal6: s6 ? s6 : false,
          signal7: s7 ? s7 : false,
          signal8: s8 ? s8 : false,
          signal9: s9 ? s9 : false,
          signal10: s10 ? s10 : false,
          signal11: s11 ? s11 : false,
          signal12: s12 ? s12 : false,
          signal13: s13 ? s13 : false,
          signal14: s14 ? s14 : false,
          signal15: s15 ? s15 : false,
          signal16: s16 ? s16 : false,
          signal17: s17 ? s17 : false,
          signal18: s18 ? s18 : false,
          signal19: s19 ? s19 : false,
          signal20: s20 ? s20 : false,
          signal21: s21 ? s21 : false,
          signal22: s22 ? s22 : false,
          signal23: s23 ? s23 : false,
          signal24: s24 ? s24 : false,
          signal25: s25 ? s25 : false,
          contacdescription: dataForm.contacdescription
            ? dataForm.contacdescription
            : false,
          reexamination: reExam,
        },
      }).then((res) => {
        if (res.data === null || res.errors) {
          dispatch(
            actions.handleAddMedicalFail(res.errors[0].message || "Phone wrong")
          );
        } else {
          dispatch(actions.handleAddMedicalSuccess(res));
          dispatch(actions.handleCloseMed());
          dispatch(actions.handleOpenNoti());
          if (isContact && selectedGroup) {
            dispatch(actions.handleSendSMSGroup());
            sendGroup({
              variables: {
                ownerid: window.sessionStorage.getItem("id"),
                groupid: selectedGroup,
                medicalid: res.data.create_medical.id,
              },
            })
              .then((resSend) => {
                if (resSend.data !== null) {
                  dispatch(actions.handleSendSMSGroupSuccess(resSend.data));
                  location.reload();
                } else dispatch(actions.handleSendSMSGroupFail());
              })
              .catch((error) => {
                dispatch(actions.handleSendSMSGroupFail());
              });
          } else {
            location.reload();
          }
        }
      });
    } else {
      dispatch(actions.handleEditMed());
      updateMedical({
        variables: {
          ...dataForm,
          id: idEdit,
          displaydate: moment(dataForm.displaydate).utc(),
          ownerid: window.sessionStorage.getItem("id"),
          doctorid: patient.doctor.id,
          patientid: patient.id,
          attribute: dataForm.attribute,
          beforesick: dataForm.beforesick || "",
          bloodpressure: parseFloat(dataForm.bloodpressure) || 0,
          bloodpressure2: parseFloat(dataForm.bloodpressure2) || 0,
          bmi: dataForm.bmi || "",
          color: dataForm.color || "normal",
          countdateremains: parseFloat(dataForm.countdateremains) || 0,
          dateofreexamination: dataForm.reexamination
            ? dataForm.dateofreexamination
            : moment().utc().format(),
          // datetest: dataForm.datetest || moment("1/1/2100").utc().format(),
          guide: dataForm.guide || "",
          height: parseFloat(dataForm.height) || 0,
          weight: parseFloat(dataForm.weight) || 0,
          // medicalnote: dataForm.medicalnote || "",
          // medicalpackage: dataForm.medicalpackage || "",
          // medicalresutl: dataForm.medicalresutl || "",
          medicine: dataForm.medicine || "",
          notebloodpressure: dataForm.notebloodpressure || "",
          notepluse: dataForm.notepluse || "",
          notesatussick: dataForm.notesatussick || "",
          noteattribute: dataForm.noteattribute || "",
          notespo2home: dataForm.notespo2home || "",
          notetemperature: dataForm.notetemperature || "",
          other: dataForm.other || "",
          otherSignal: dataForm.otherSignal || "",
          otherid: dataForm.otherid || "",
          otherreason: dataForm.otherreason || "",
          pluse: parseFloat(dataForm.pluse) || 0,
          // prcrealdate: dataForm.prcrealdate || moment().utc().format(),
          quantityofsick: parseFloat(dataForm.quantityofsick) || 0,
          resultsarscovid2: dataForm.resultsarscovid2 || "no",
          resulttest: dataForm.resulttest || "no",
          riskassessment: dataForm.riskassessment || "low",
          spo2home: parseFloat(dataForm.spo2home) || 0,
          statusquo: dataForm.statusquo || "",
          summary: dataForm.summary || "",
          temperature: parseFloat(dataForm.temperature) || 0,
          //signal
          signal1: s1 ? s1 : false,
          signal2: s2 ? s2 : false,
          signal3: s3 ? s3 : false,
          signal4: s4 ? s4 : false,
          signal5: s5 ? s5 : false,
          signal6: s6 ? s6 : false,
          signal7: s7 ? s7 : false,
          signal8: s8 ? s8 : false,
          signal9: s9 ? s9 : false,
          signal10: s10 ? s10 : false,
          signal11: s11 ? s11 : false,
          signal12: s12 ? s12 : false,
          signal13: s13 ? s13 : false,
          signal14: s14 ? s14 : false,
          signal15: s15 ? s15 : false,
          signal16: s16 ? s16 : false,
          signal17: s17 ? s17 : false,
          signal18: s18 ? s18 : false,
          signal19: s19 ? s19 : false,
          signal20: s20 ? s20 : false,
          signal21: s21 ? s21 : false,
          signal22: s22 ? s22 : false,
          signal23: s23 ? s23 : false,
          signal24: s24 ? s24 : false,
          signal25: s25 ? s25 : false,
          contacdescription: dataForm.contacdescription
            ? dataForm.contacdescription
            : false,
          reexamination: reExam,
        },
      }).then((res) => {
        if (res.data === null || res.errors) {
          dispatch(
            actions.handleEditMedFail(res.errors[0].message || "Phone wrong")
          );
          // setIsModalOpen(false);
          // dispatch(actions.handleOpenNoti());
          // showNoti();
          // dispatch(actions.handleCloseMed())
        } else {
          dispatch(actions.handleEditMedSuccess(res));
          dispatch(actions.handleCloseMed());
          dispatch(actions.handleOpenNoti());
          if (isContact && selectedGroup) {
            dispatch(actions.handleSendSMSGroup());
            sendGroup({
              variables: {
                ownerid: window.sessionStorage.getItem("id"),
                groupid: selectedGroup,
                medicalid: idEdit,
              },
            })
              .then((resSend) => {
                if (resSend.data !== null) {
                  dispatch(actions.handleSendSMSGroupSuccess(resSend.data));
                  location.reload();
                } else dispatch(actions.handleSendSMSGroupFail());
              })
              .catch((error) => {
                dispatch(actions.handleSendSMSGroupFail());
              });
          } else {
            location.reload();
          }
        }
      });
    }

    // console.log(dataSubmit);
  };
  useEffect(() => {
    if (idEdit) {
      findOneMedical({ id: idEdit });

      //set value ///////////////////////
      setValue("displaydate", medical?.find_one_medical?.displaydate);

      setValue("attribute", medical?.find_one_medical?.attribute);
      setValue("bloodpressure", medical?.find_one_medical?.bloodpressure);
      setValue("bloodpressure2", medical?.find_one_medical?.bloodpressure2);
      setValue("color", medical?.find_one_medical?.color);
      setValue(
        "contacdescription",
        medical?.find_one_medical?.contacdescription
      );
      setValue("countdateremains", medical?.find_one_medical?.countdateremains);
      setValue(
        "dateofreexamination",
        medical?.find_one_medical?.dateofreexamination
      );
      // console.log(moment(medical?.find_one_medical?.datetest).format("DD/MM/YYYY"),"1/1/2200")
      // setValue(
      //   "datetest",
      //   moment(medical?.find_one_medical?.datetest)
      //     .format("DD/MM/YYYY")
      //     .toString() === "01/01/2200"
      //     ? null
      //     : medical?.find_one_medical?.datetest
      // );
      setValue("guide", medical?.find_one_medical?.guide);
      setValue("height", medical?.find_one_medical?.height);
      setValue("id", medical?.find_one_medical?.id);
      // setValue("medicalnote", medical?.find_one_medical?.medicalnote);
      // setValue("medicalpackage", medical?.find_one_medical?.medicalpackage);
      // setValue("medicalresutl", medical?.find_one_medical?.medicalresutl);
      setValue("medicine", medical?.find_one_medical?.medicine);
      setValue("noteattribute", medical?.find_one_medical?.noteattribute);
      setValue(
        "notebloodpressure",
        medical?.find_one_medical?.notebloodpressure
      );
      setValue("notepluse", medical?.find_one_medical?.notepluse);
      setValue("notespo2home", medical?.find_one_medical?.notespo2home);
      setValue("notetemperature", medical?.find_one_medical?.notetemperature);
      setValue("other", medical?.find_one_medical?.other);
      setValue("otherSignal", medical?.find_one_medical?.otherSignal);
      setValue("pluse", medical?.find_one_medical?.pluse);
      // setValue("prcrealdate", medical?.find_one_medical?.prcrealdate);
      setValue("quantityofsick", medical?.find_one_medical?.quantityofsick);
      setValue("reexamination", medical?.find_one_medical?.reexamination);
      setValue("resultsarscovid2", medical?.find_one_medical?.resultsarscovid2);
      setValue("resulttest", medical?.find_one_medical?.resulttest);
      setValue("riskassessment", medical?.find_one_medical?.riskassessment);

      setS1(medical?.find_one_medical?.signal1);
      setS2(medical?.find_one_medical?.signal2);
      setS3(medical?.find_one_medical?.signal3);
      setS4(medical?.find_one_medical?.signal4);
      setS5(medical?.find_one_medical?.signal5);
      setS6(medical?.find_one_medical?.signal6);
      setS7(medical?.find_one_medical?.signal7);
      setS8(medical?.find_one_medical?.signal8);
      setS9(medical?.find_one_medical?.signal9);
      setS10(medical?.find_one_medical?.signal10);
      setS11(medical?.find_one_medical?.signal11);
      setS12(medical?.find_one_medical?.signal12);
      setS13(medical?.find_one_medical?.signal13);
      setS14(medical?.find_one_medical?.signal14);
      setS15(medical?.find_one_medical?.signal15);
      setS16(medical?.find_one_medical?.signal16);
      setS17(medical?.find_one_medical?.signal17);
      setS18(medical?.find_one_medical?.signal18);
      setS19(medical?.find_one_medical?.signal19);
      setS20(medical?.find_one_medical?.signal20);
      setS21(medical?.find_one_medical?.signal21);
      setS22(medical?.find_one_medical?.signal22);
      setS23(medical?.find_one_medical?.signal23);
      setS24(medical?.find_one_medical?.signal24);
      setS25(medical?.find_one_medical?.signal25);

      setValue("spo2home", medical?.find_one_medical?.spo2home);
      setValue("statusquo", medical?.find_one_medical?.statusquo);
      setValue("summary", medical?.find_one_medical?.summary);
      setValue("temperature", medical?.find_one_medical?.temperature);
      setValue("weight", medical?.find_one_medical?.weight);
      setValue(
        "bmi",
        `${(
          medical?.find_one_medical?.weight /
          ((medical?.find_one_medical?.height / 100) *
            (medical?.find_one_medical?.height / 100))
        ).toFixed(2)}`
      );
      setWeight(medical?.find_one_medical?.weight);
      setHeight(medical?.find_one_medical?.height);

      setVal1("a");
      setVal2("a");
      setVal3("a");
      setVal9("a");
      setVal10("a");
      setVal11("a");
      setVal12("a");
    }
    ///////////////////////////////////
  }, [medical]);
  const handleChange = (event, newValue) => {
    setTab(newValue);
  };
  const [height, setHeight] = useState(0);
  const [weight, setWeight] = useState(0);
  // console.log(moment(patient.pathogenicdate).format("DD/MM/YYYY").toString() === "01/01/2200")
  const handleChangeIndex = (index) => {
    setTab(index);
  };
  useEffect(() => {
    if (patient) {
      setValue("otherreason", patient.otherreason);
      setValue("beforesick", patient.beforesick);
      setValue("otherid", patient.otherid);
      setValue("weight", patient?.weight);
      setValue("height", patient?.height);
      setValue(
        "bmi",
        `${(
          patient?.weight /
          ((patient?.height / 100) * (patient?.height / 100))
        ).toFixed(2)}`
      );
      setVal1(patient?.weight);
      setVal2(patient?.height);
      setHeight(patient?.height);
      setWeight(patient?.weight);
      /// tab thăm khám hằng ngày
    }
  }, []);

  useEffect(() => {
    dispatch(actions.handleFindGroups());
    findGroups({ adminid: window.sessionStorage.getItem("id") });
  }, []);

  useEffect(() => {
    if (height > 0 && weight > 0)
      setValue(
        "bmi",
        `${(weight / ((height / 100) * (height / 100))).toFixed(2)}`
      );
    else setValue("bmi", "0");
  }, [height, weight]);
  return (
    <Container component="main" maxWidth="m">
      <Backdrop className={classes.backdrop} open={dataMed?.loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <CssBaseline />
      <div style={{ display: "flex", flexDirection: "row" }}>
        <AppBar
          position="fixed"
          color="default"
          style={{ width: "90%", left: "0" }}
        >
          <Tabs
            value={tab}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            aria-label="full width tabs example"
          >
            <Tab label="Hành chính" {...a11yProps(0)} />
            <Tab label="TIỀN CĂN" {...a11yProps(1)} />
            <Tab
              disabled={
                val1.length === 0 || val2.length === 0 || val3.length === 0
              }
              label="THEO DÕI HẰNG NGÀY"
              {...a11yProps(2)}
            />
            <Tab
              disabled={
                (s1 === false &&
                  s2 === false &&
                  s3 === false &&
                  s4 === false &&
                  s5 === false &&
                  s6 === false &&
                  s7 === false &&
                  s8 === false &&
                  s9 === false &&
                  s10 === false &&
                  s11 === false) ||
                (s12 === false &&
                  s13 === false &&
                  s14 === false &&
                  s15 === false &&
                  s16 === false &&
                  s17 === false &&
                  s18 === false &&
                  s19 === false &&
                  s20 === false &&
                  s21 === false &&
                  s22 === false &&
                  s23 === false &&
                  s24 === false &&
                  s25 === false)
              }
              label="KHÁM VÀ HƯỚNG XỬ TRÍ"
              {...a11yProps(3)}
            />
            <Tab
              disabled={
                (s1 === false &&
                  s2 === false &&
                  s3 === false &&
                  s4 === false &&
                  s5 === false &&
                  s6 === false &&
                  s7 === false &&
                  s8 === false &&
                  s9 === false &&
                  s10 === false &&
                  s11 === false) ||
                (s12 === false &&
                  s13 === false &&
                  s14 === false &&
                  s15 === false &&
                  s16 === false &&
                  s17 === false &&
                  s18 === false &&
                  s19 === false &&
                  s20 === false &&
                  s21 === false &&
                  s22 === false &&
                  s23 === false &&
                  s24 === false &&
                  s25 === false)
              }
              label="Kết luận"
              {...a11yProps(4)}
            />
          </Tabs>
        </AppBar>
        <div style={{ textAlign: "end", width: "100%" }}>
          <Button
            style={{ paddingRight: "0", marginRight: "-1em" }}
            onClick={() => dispatch(actions.handleCloseMed())}
          >
            <ClearIcon></ClearIcon>
          </Button>
        </div>
      </div>

      <form
        onSubmit={handleSubmit(onSubmit)}
        noValidate
        className={classes.form}
      >
        <TabPanel
          hidden={tab !== 0}
          value={tab}
          index={0}
          dir={theme.direction}
        >
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            {/* <Button><ArrowBackIosIcon></ArrowBackIosIcon></Button> */}
            <Button onClick={() => setTab(1)}>
              <ArrowForwardIosIcon></ArrowForwardIosIcon>
            </Button>
          </div>
          <Grid container>
            <Grid item xs={12} sm={6} md={6}>
              <div className={classes.titleTab}>HÀNH CHÍNH</div>
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="displaydate"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDateTimePicker
                      variant="inline"
                      className={classes.formatStyle}
                      fullWidth
                      format="dd-MM-yyyy HH:mm"
                      margin="normal"
                      id="displaydate"
                      label="Ngày nhận bệnh"
                      onChange={(e) => setValue("displaydate", e)}
                      value={value}
                    />
                  </MuiPickersUtilsProvider>
                )}
              />
            </Grid>
          </Grid>
          <hr />
          <div className={classes.typo}></div>
          <div className={classes.body}>
            <div className={classes.body}>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        style={{ fontSize: "4rem" }}
                        className={classes.formatStyle}
                        label="Bác sĩ"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        disabled
                        value={`${patient?.doctor?.user?.lastname} ${patient?.doctor?.user?.firstname}`}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        disabled
                        className={classes.formatStyle}
                        fullWidth
                        label="Mã bệnh nhân khác (Nếu có)"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        value={patient?.otherid}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        disabled
                        className={classes.formatStyle}
                        fullWidth
                        label="Họ và tên lót"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        value={patient.lastname}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        disabled
                        className={classes.formatStyle}
                        fullWidth
                        label="Tên"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        value={patient.firstname}
                      />
                    )}
                  />
                </Grid>

                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        label="Giới tính"
                        margin="normal"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        className={classes.formatStyle}
                        fullWidth
                        disabled
                        value={
                          patient.gender === "male"
                            ? "Nam"
                            : patient.gender === "female"
                            ? "Nữ"
                            : "Không xác định"
                        }
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          variant="inline"
                          format="dd-MM-yyyy"
                          margin="normal"
                          id="date-picker-inline"
                          className={classes.formatStyle}
                          fullWidth
                          label="Ngày sinh"
                          value={patient.dob}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                          disabled
                        />
                      </MuiPickersUtilsProvider>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        label="Số điện thoại"
                        margin="normal"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              ( +84 )
                            </InputAdornment>
                          ),
                        }}
                        className={classes.formatStyle}
                        fullWidth
                        disabled
                        type="number"
                        value={patient.phone.replace("(+84)", "").trim()}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        label="Số điện thoại người thân 1"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              ( +84 )
                            </InputAdornment>
                          ),
                        }}
                        className={classes.formatStyle}
                        fullWidth
                        disabled
                        value={patient.phoneemerency1
                          .replace("(+84)", "")
                          .trim()}
                        type="number"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        label="Số điện thoại người thân 2"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              ( +84 )
                            </InputAdornment>
                          ),
                        }}
                        className={classes.formatStyle}
                        fullWidth
                        disabled
                        value={patient.phoneemerency2
                          .replace("(+84)", "")
                          .trim()}
                        type="number"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.formatStyle}
                        margin="normal"
                        label="Số điện thoại người thân 3"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              ( +84 )
                            </InputAdornment>
                          ),
                        }}
                        value={patient.phoneemerency3
                          .replace("(+84)", "")
                          .trim()}
                        fullWidth
                        disabled
                        type="number"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.formatStyle}
                        label="Địa chỉ"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        fullWidth
                        disabled
                        value={
                          patient.name &&
                          `${patient.name}, ${patient.ward}, ${patient.province}, ${patient.city}`
                        }
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    render={({ field: { value } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.formatStyle}
                        label="Số người ở chung nhà"
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              Người
                            </InputAdornment>
                          ),
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        fullWidth
                        disabled
                        value={patient.quantityfamily}
                        type="number"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </div>
        </TabPanel>
        <TabPanel
          hidden={tab !== 1}
          value={tab}
          index={1}
          dir={theme.direction}
        >
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <Button onClick={() => setTab(0)}>
              <ArrowBackIosIcon></ArrowBackIosIcon>
            </Button>
            <Button
              disabled={
                val1.length === 0 || val2.length === 0 || val3.length === 0
              }
              onClick={() => setTab(2)}
            >
              <ArrowForwardIosIcon></ArrowForwardIosIcon>
            </Button>
          </div>
          <div className={classes.titleTab}>
            THÔNG TIN TỔNG QUÁT <hr />
          </div>
          <div className={classes.typo}></div>
          <div className={classes.body}>
            <Grid container spacing={2}>
              {/* <Grid item xs={12} sm={6} md={6}>
                <Controller
                  control={control}
                  name="prcrealdate"
                  render={({ field: { onChange, onBlur, value, ref } }) => (
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        className={classes.formatStyle}
                        fullWidth
                        format="dd-MM-yyyy"
                        margin="normal"
                        id="date-picker-inline"
                        label="Ngày thực hiện xét nghiệm"
                        value={value}
                        onChange={(e) => setValue("prcrealdate", e)}
                        defaultValue={moment().format()}
                        KeyboardButtonProps={{
                          "aria-label": "change date",
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
                <Controller
                  control={control}
                  name="medicalpackage"
                  render={({ field: { onChange, onBlur, value, ref } }) => (
                    <FormControl
                      style={{ marginTop: "1em" }}
                      variant="outlined"
                      className={classes.formControl}
                      fullWidth
                    >
                      <InputLabel>Loại xét nghiệm</InputLabel>
                      <Select
                        id="demo-simple-select-outlined"
                        label="Loại xét nghiệm"
                        {...register("medicalpackage")}
                        className={classes.formatStyle}
                        value={value}
                        onChange={onChange}
                      >
                        <MenuItem value={"prc"}>PCR</MenuItem>
                        <MenuItem value={"quicktest"}>Test nhanh</MenuItem>
                        <MenuItem value={"other"}>Khác</MenuItem>
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
                <Controller
                  control={control}
                  name="medicalresutl"
                  render={({ field: { onChange, onBlur, value, ref } }) => (
                    <FormControl
                      style={{ marginTop: "1em" }}
                      variant="outlined"
                      className={classes.formControl}
                      fullWidth
                    >
                      <InputLabel>Kết quả</InputLabel>
                      <Select
                        id="demo-simple-select-outlined"
                        className={classes.formatStyle}
                        label="Kết quả"
                        {...register("medicalresutl")}
                        onChange={onChange}
                        value={value}
                      >
                        <MenuItem value={"negative"}>Âm tính</MenuItem>
                        <MenuItem value={"positive"}>Dương tính</MenuItem>
                        <MenuItem value={"other"}>Khác</MenuItem>
                      </Select>
                    </FormControl>
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
                <Controller
                  control={control}
                  name="medicalnote"
                  render={({ field: { onChange, value } }) => (
                    <TextField
                      variant="outlined"
                      margin="normal"
                      className={classes.formatStyle}
                      fullWidth
                      onChange={(e) => setValue("medicalnote", e.target.value)}
                      {...register("medicalnote")}
                      label="Ghi chú"
                      id="medicalnote"
                      value={value}
                    />
                  )}
                />
              </Grid> */}
              <Grid item xs={12} sm={6} md={6}>
                <Controller
                  control={control}
                  name="pathogenicdate"
                  render={({ field: { onChange, onBlur, value, ref } }) => (
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        className={classes.formatStyle}
                        fullWidth
                        format="dd-MM-yyyy"
                        margin="normal"
                        id="pathogenicdate"
                        label="Ngày có triệu chứng đầu tiên"
                        value={
                          moment(patient.pathogenicdate)
                            .format("DD/MM/YYYY")
                            .toString() === "01/01/2200"
                            ? null
                            : patient.pathogenicdate
                        }
                        disabled
                      />
                    </MuiPickersUtilsProvider>
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
                <Controller
                  control={control}
                  name="pathogenicname"
                  render={({ field: { onChange, onBlur, value, ref } }) => (
                    <TextField
                      variant="outlined"
                      className={classes.formatStyle}
                      margin="normal"
                      fullWidth
                      {...register("pathogenicname")}
                      label="Triệu chứng đầu tiên"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment
                            style={{ marginLeft: "-0.1em" }}
                            position="end"
                          ></InputAdornment>
                        ),
                      }}
                      disabled
                      value={patient.pathogenicname}
                    />
                  )}
                />
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={6} sm={4} md={4}>
                <Controller
                  control={control}
                  name="weight"
                  render={({ field: { value } }) => (
                    <TextField
                      variant="outlined"
                      className={classes.formatStyle}
                      margin="normal"
                      fullWidth
                      required
                      {...register("weight")}
                      label="Cân nặng"
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">kg</InputAdornment>
                        ),
                        startAdornment: (
                          <InputAdornment
                            style={{ marginLeft: "-0.1em" }}
                            position="end"
                          ></InputAdornment>
                        ),
                      }}
                      error={err1}
                      value={value}
                      helperText={err1 && "Vui lòng nhập cân nặng"}
                      onChange={(e) => {
                        if (e.target.value) setErr1(false);
                        else setErr1(true);
                        setVal1(e.target.value);
                        setValue("weight", e.target.value);
                        setWeight(e.target.value);
                      }}
                      type="number"
                      id="weight"
                    />
                  )}
                />
              </Grid>
              <Grid item xs={6} sm={4} md={4}>
                <Controller
                  control={control}
                  name="height"
                  render={({ field: { value } }) => (
                    <TextField
                      variant="outlined"
                      margin="normal"
                      className={classes.formatStyle}
                      fullWidth
                      required
                      {...register("height")}
                      label="Chiều cao"
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">cm</InputAdornment>
                        ),
                        startAdornment: (
                          <InputAdornment
                            style={{ marginLeft: "-0.1em" }}
                            position="end"
                          ></InputAdornment>
                        ),
                      }}
                      error={err2}
                      helperText={err2 && "Vui lòng nhập chiều cao"}
                      onChange={(e) => {
                        if (e.target.value) setErr2(false);
                        else setErr2(true);
                        setVal2(e.target.value);
                        setValue("height", e.target.value);
                        setHeight(e.target.value);
                      }}
                      type="number"
                      id="height"
                      value={value}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={4} md={4}>
                <Controller
                  control={control}
                  name="bmi"
                  render={({ field: { value } }) => (
                    <TextField
                      variant="outlined"
                      margin="normal"
                      className={classes.formatStyle}
                      fullWidth
                      {...register("bmi")}
                      value={value}
                      label="Chỉ số BMI"
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            kg/m<sup>2</sup>
                          </InputAdornment>
                        ),
                        startAdornment: (
                          <InputAdornment
                            style={{ marginLeft: "-0.1em" }}
                            position="end"
                          ></InputAdornment>
                        ),
                      }}
                      disabled
                      id="bmi"
                    />
                  )}
                />
              </Grid>
            </Grid>
            <div className={classes.typo} style={{ marginTop: "2em" }}>
              <b> Yếu tố nguy cơ </b>
            </div>

            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={6}>
                <Controller
                  control={control}
                  name="pathogenic"
                  render={({ field: { value } }) => (
                    <TextField
                      variant="outlined"
                      margin="normal"
                      className={classes.formatStyle}
                      fullWidth
                      multiline
                      disabled
                      rows={2}
                      onChange={(e) => setValue("pathogenic", e.target.value)}
                      InputProps={{
                        startAdornment: (
                          <InputAdornment
                            style={{ marginLeft: "-0.1em" }}
                            position="end"
                          ></InputAdornment>
                        ),
                      }}
                      label="Yếu tố nguy cơ"
                      id="pathogenic"
                      defaultValue={patient?.pathogenic[0].name}
                      value={patient.pathogenic.map((item) => {
                        return ` ${item.name}`;
                      })}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Controller
                  control={control}
                  name="otherreason"
                  render={({ field: { value } }) => (
                    <TextField
                      variant="outlined"
                      margin="normal"
                      className={classes.formatStyle}
                      fullWidth
                      multiline
                      rows={2}
                      disabled
                      onChange={(e) => setValue("otherreason", e.target.value)}
                      {...register("otherreason")}
                      label="Yếu tố nguy cơ khác (Nếu có)"
                      id="otherreason"
                      value={value}
                    />
                  )}
                />
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={6}>
                <Controller
                  control={control}
                  name="beforesick"
                  render={({ field: { value } }) => (
                    <TextField
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      multiline
                      className={classes.formatStyle}
                      disabled
                      rows={1}
                      onChange={(e) => {
                        setValue("beforesick", e.target.value || "Không có");
                      }}
                      InputProps={{
                        startAdornment: (
                          <InputAdornment
                            style={{ marginLeft: "-0.1em" }}
                            position="end"
                          ></InputAdornment>
                        ),
                      }}
                      label="Bệnh mạn tính trước đây - Giai đoạn"
                      value={patient.beforesick}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Controller
                  control={control}
                  name="notesatussick"
                  render={({ field: { onChange, value } }) => (
                    <TextField
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      multiline
                      className={classes.formatStyle}
                      rows={1}
                      {...register("notesatussick")}
                      label="Tình trạng bệnh mạn tính"
                      value={patient.notesatussick}
                      disabled
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Controller
                  control={control}
                  name="countdateremains"
                  render={({ field: { onChange, value } }) => (
                    <TextField
                      variant="outlined"
                      fullWidth
                      {...register("countdateremains")}
                      label="Thuốc đang sử dụng còn bao nhiêu ngày"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment
                            style={{ marginLeft: "-0.1em" }}
                            position="end"
                          ></InputAdornment>
                        ),
                        endAdornment: (
                          <InputAdornment position="end">Ngày</InputAdornment>
                        ),
                      }}
                      type="number"
                      onChange={(e) => {
                        setValue("countdateremains", e.target.value);
                      }}
                      value={value}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Controller
                  control={control}
                  name="medicine"
                  render={({ field: { onChange, value } }) => (
                    <TextField
                      variant="outlined"
                      fullWidth
                      multiline
                      maxRows={6}
                      rows={3}
                      required
                      error={err3}
                      helperText={err3 && "Vui lòng nhập thuốc đang sử dụng"}
                      {...register("medicine")}
                      onChange={(e) => {
                        if (e.target.value) setErr3(false);
                        else setErr3(true);
                        setVal3(e.target.value);
                        setValue("medicine", e.target.value);
                      }}
                      label="Thuốc đang sử dụng"
                      value={value}
                    />
                  )}
                />
              </Grid>
            </Grid>
            <div className={classes.titleTab} style={{ marginTop: "1em" }}>
              THEO DÕI
              <hr />
            </div>
            <div>KẾT QUẢ XÉT NGHIỆM</div>
            {medical?.find_one_medical?.medicaltrack?.map((item, index) => {
              return (
                <Grid container spacing={2} key={index}>
                  <Grid item xs={4} sm={4} md={4}>
                    <Controller
                      control={control}
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <DateTimePicker
                            variant="outlined"
                            format="dd-MM-yyyy HH:mm"
                            margin="normal"
                            id="date-picker-inline"
                            className={classes.formatStyle}
                            fullWidth
                            label="Thời gian xét nghiệm"
                            value={item.prcrealdate}
                            KeyboardButtonProps={{
                              "aria-label": "change date",
                            }}
                            disabled
                          />
                        </MuiPickersUtilsProvider>
                      )}
                    />
                  </Grid>
                  <Grid item xs={4} sm={4} md={4}>
                    <Controller
                      control={control}
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <FormControl
                          style={{ marginTop: "1em" }}
                          variant="outlined"
                          fullWidth
                        >
                          <InputLabel>Loại xét nghiệm </InputLabel>
                          <Select
                            id="demo-simple-select-outlined"
                            label="Loại xét nghiệm"
                            className={classes.formatStyle}
                            classes={{ disabled: classes.disabled }}
                            value={item.medicalpackage}
                            disabled
                          >
                            <MenuItem value={"prc"}>PCR</MenuItem>
                            <MenuItem value={"quicktest"}>Test nhanh</MenuItem>
                            <MenuItem value={"other"}>Khác</MenuItem>
                          </Select>
                        </FormControl>
                      )}
                    />
                  </Grid>
                  <Grid item xs={4} sm={4} md={4}>
                    <Controller
                      control={control}
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <FormControl
                          style={{ marginTop: "1em" }}
                          variant="outlined"
                          fullWidth
                        >
                          <InputLabel>Kết quả</InputLabel>
                          <Select
                            id="demo-simple-select-outlined"
                            className={classes.formatStyle}
                            label="Kết quả"
                            classes={{ disabled: classes.disabled }}
                            disabled
                            value={item.medicalresutl}
                          >
                            <MenuItem value={"negative"}>Âm tính</MenuItem>
                            <MenuItem value={"positive"}>Dương tính</MenuItem>
                            <MenuItem value={"other"}>Khác</MenuItem>
                          </Select>
                        </FormControl>
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextField
                      variant="outlined"
                      disabled
                      className={classes.formatStyle}
                      fullWidth
                      label="Ghi chú"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment
                            style={{ marginLeft: "-0.1em" }}
                            position="end"
                          ></InputAdornment>
                        ),
                      }}
                      value={item.medicalnote}
                    />
                  </Grid>
                </Grid>
              );
            })}
            <hr style={{ marginTop: "2em" }} />
            <div>THUỐC ĐÃ THÊM</div>
            {medical?.find_one_medical?.medicinenow === [] && (
              <div>
                <ImportContactsIcon></ImportContactsIcon>CHƯA CÓ THÔNG TIN THUỐC
              </div>
            )}
            {medical?.find_one_medical?.medicinenow?.map((item, index) => {
              return (
                <Grid container spacing={2} key={index}>
                  <Grid item xs={4} sm={4} md={4}>
                    <TextField
                      variant="outlined"
                      disabled
                      margin="normal"
                      className={classes.formatStyle}
                      fullWidth
                      label="Tên thuốc"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment
                            style={{ marginLeft: "-0.1em" }}
                            position="end"
                          ></InputAdornment>
                        ),
                      }}
                      value={item.name}
                    />
                  </Grid>
                  <Grid item xs={3} sm={3} md={3}>
                    <Controller
                      control={control}
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          disabled
                          className={classes.formatStyle}
                          margin="normal"
                          fullWidth
                          label="Số lượng"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment
                                style={{ marginLeft: "-0.1em" }}
                                position="end"
                              ></InputAdornment>
                            ),
                          }}
                          value={item.quantity}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={5} sm={5} md={5}>
                    <Controller
                      control={control}
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          disabled
                          className={classes.formatStyle}
                          margin="normal"
                          fullWidth
                          label="Ghi chú"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment
                                style={{ marginLeft: "-0.1em" }}
                                position="end"
                              ></InputAdornment>
                            ),
                          }}
                          value={item.description}
                        />
                      )}
                    />
                  </Grid>
                </Grid>
              );
            })}
            <Grid style={{ marginTop: "1em" }} container spacing={2}>
              <Grid item xs={12} sm={6} md={6}></Grid>
            </Grid>
          </div>
          <div style={{ textAlignLast: "end" }}>
            <Button
              onClick={() => {
                setTab(2);
                window.scrollTo({ top: 10, behavior: "smooth" });
              }}
              className={classes.nextBtn}
              disabled={
                val1.length === 0 || val2.length === 0 || val3.length === 0
              }
            >
              <b>TIẾP THEO </b>
              <ArrowForwardIosIcon style={{ marginLeft: "0.2em" }} />
            </Button>
          </div>
        </TabPanel>
        <TabPanel
          hidden={tab !== 2}
          value={tab}
          index={2}
          dir={theme.direction}
        >
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <Button onClick={() => setTab(1)}>
              <ArrowBackIosIcon></ArrowBackIosIcon>
            </Button>
            <Button onClick={() => setTab(3)}>
              <ArrowForwardIosIcon></ArrowForwardIosIcon>
            </Button>
          </div>
          <div className={classes.titleTab}>
            DẤU HIỆU, TRIỆU CHỨNG
            <hr />
          </div>
          {/* <Grid style={{ marginTop: "1em" }} container spacing={2}>
            <Grid item xs={12} sm={12} md={12}>
              <Controller
                control={control}
                name="datetest"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDateTimePicker
                      variant="inline"
                      fullWidth
                      format="dd-MM-yyyy HH:mm"
                      margin="normal"
                      id="date-picker-inline"
                      label="Ngày làm PCR/Test nhanh với SARS-CoV-2 gần nhất (Nếu có)"
                      value={value}
                      defaultValue={null}
                      onChange={(e) => {
                        setValue("datetest", e);
                      }}
                      KeyboardButtonProps={{
                        "aria-label": "change date",
                      }}
                    />
                  </MuiPickersUtilsProvider>
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="resulttest"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <FormControl
                    style={{ marginTop: "1em" }}
                    variant="outlined"
                    className={classes.formControl}
                    fullWidth
                    name="resulttest"
                  >
                    <InputLabel>
                      Kết quả PCR/Test nhanh gần nhất (Nếu có)
                    </InputLabel>
                    <Select
                      id="demo-simple-select-outlined"
                      label="Kết quả PCR/Test nhanh gần nhất (Nếu có)"
                      {...register("resulttest")}
                      value={value}
                      onChange={onChange}
                    >
                      <MenuItem value={"prc"}>PCR</MenuItem>
                      <MenuItem value={"quicktest"}>Test nhanh</MenuItem>
                      <MenuItem value={"other"}>Khác</MenuItem>
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="resultsarscovid2"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <FormControl
                    style={{ marginTop: "1em" }}
                    variant="outlined"
                    className={classes.formControl}
                    fullWidth
                  >
                    <InputLabel>
                      Kết quả PCR/Test nhanh với SARS-CoV-2 gần nhất (Nếu có)
                    </InputLabel>
                    <Select
                      id="demo-simple-select-outlined"
                      label="Kết quả PCR/Test nhanh với SARS-CoV-2 gần nhất (Nếu có)"
                      {...register("resultsarscovid2")}
                      onChange={onChange}
                      value={value}
                    >
                      <MenuItem value={"negative"}>Âm tính</MenuItem>
                      <MenuItem value={"positive"}>Dương tính</MenuItem>
                      <MenuItem value={"other"}>Khác</MenuItem>
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
          </Grid> */}
          <div className={classes.paper}>
            <div className={classes.body}>
              <div className={classes.typo}>
                <b>Dấu hiệu nguy hiểm</b> (Cần nhập viện ngay lập tức)
              </div>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <div>
                  <Controller
                    control={control}
                    name="signal1"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        name="signal1"
                        onChange={
                          (onChange,
                          (e) => {
                            setS1(e.target.checked);
                          })
                        }
                        value={s1}
                        control={
                          <Checkbox
                            checked={s1}
                            color="primary"
                            name="signal1"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Mặt hay môi tím tái
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal2"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        name="signal2"
                        onChange={
                          (onChange,
                          (e) => {
                            setS2(e.target.checked);
                          })
                        }
                        defaultValue={false}
                        value={s2}
                        control={
                          <Checkbox
                            checked={s2}
                            color="primary"
                            name="signal2"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Cảm thấy đau hoặc tức ngực nhiều không giảm
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal3"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        name="signal3"
                        onChange={
                          (onChange,
                          (e) => {
                            setS3(e.target.checked);
                          })
                        }
                        value={s3}
                        control={
                          <Checkbox
                            checked={s3}
                            color="primary"
                            name="signal3"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Khó thở rất nhiều (Thở mạnh, hụt hơi khó nói chuyện,
                            khò khè nặng, cánh mũi phập phồng, cần phải sử dụng
                            các cơ bên ngoài xung quanh ngực để cố gắng thở){" "}
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal4"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS4(e.target.checked);
                          })
                        }
                        name="signal4"
                        value={s4}
                        control={
                          <Checkbox
                            checked={s4}
                            color="primary"
                            name="signal4"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Mất định hướng không gian, lú lẫn (mới xuất hiện)
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal5"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS5(e.target.checked);
                          })
                        }
                        name="signal5"
                        value={s5}
                        control={
                          <Checkbox
                            checked={s5}
                            color="primary"
                            name="signal5"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Bất tỉnh hoặc rất khó thức giấc{" "}
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal6"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS6(e.target.checked);
                          })
                        }
                        name="signal6"
                        value={s6}
                        control={
                          <Checkbox
                            checked={s6}
                            color="primary"
                            name="signal6"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Nói lắp hoặc khó nói (mới xuất hiện hoặc là nặng hơn
                            nếu đã có trước đây){" "}
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal7"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS7(e.target.checked);
                          })
                        }
                        name="signal7"
                        value={s7}
                        control={
                          <Checkbox
                            checked={s7}
                            color="primary"
                            name="signal7"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Co giật (mới xuất hiện hoặc là nặng hơn nếu đã có
                            trước đây){" "}
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal8"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS8(e.target.checked);
                          })
                        }
                        name="signal8"
                        value={s8}
                        control={
                          <Checkbox
                            checked={s8}
                            color="primary"
                            name="signal8"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Tụt huyết áp (khó đứng dậy, hoa mắt, chóng mặt,
                            choáng váng, cảm thấy da lạnh ẩm, tái nhợt){" "}
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal9"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS9(e.target.checked);
                          })
                        }
                        name="signal9"
                        value={s9}
                        control={
                          <Checkbox
                            checked={s9}
                            color="primary"
                            name="signal9"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Mất nước (miệng và môi khô, tiểu ít, mắt lõm){" "}
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal10"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        name="signal10"
                        onChange={
                          (onChange,
                          (e) => {
                            setS10(e.target.checked);
                          })
                        }
                        value={s10}
                        control={
                          <Checkbox
                            checked={s10}
                            color="primary"
                            name="signal10"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>Không có </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal11"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS11(e.target.checked);
                          })
                        }
                        name="signal11"
                        value={s11}
                        control={
                          <Checkbox
                            checked={s11}
                            color="primary"
                            name="signal11"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Hôm nay ông/bà có cảm thấy điều gì khác thường trong
                            cơ thể so với trước đây không?{" "}
                          </div>
                        }
                      />
                    )}
                  />
                </div>

                <div className={classes.typo} style={{ marginTop: "2em" }}>
                  <b> Triệu chứng hiện có </b>(Cảm thấy khác thường trong cơ thể
                  so với trước đây)
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal12"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS12(e.target.checked);
                          })
                        }
                        name="signal2"
                        value={s12}
                        control={
                          <Checkbox
                            checked={s12}
                            color="primary"
                            name="signal12"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Sốt hoặc cảm thấy muốn sốt (nóng, lạnh run, đổ mồ
                            hôi)
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal13"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        name="signal13"
                        onChange={
                          (onChange,
                          (e) => {
                            setS13(e.target.checked);
                          })
                        }
                        value={s13}
                        control={
                          <Checkbox
                            checked={s13}
                            color="primary"
                            name="signal13"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>Khó thở</div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal14"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS14(e.target.checked);
                          })
                        }
                        name="signal14"
                        value={s14}
                        control={
                          <Checkbox
                            checked={s14}
                            color="primary"
                            name="signal14"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Đau ngực nhẹ
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal15"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS15(e.target.checked);
                          })
                        }
                        name="signal15"
                        value={s15}
                        control={
                          <Checkbox
                            checked={s15}
                            color="primary"
                            name="signal15"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>Đau họng</div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal16"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS16(e.target.checked);
                          })
                        }
                        value={s16}
                        control={
                          <Checkbox
                            checked={s16}
                            color="primary"
                            name="signal16"
                          />
                        }
                        name="signal16"
                        label={
                          <div className={classes.checkboxLable}>
                            Ho: khan hay đàm
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal17"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS17(e.target.checked);
                          })
                        }
                        name="signal17"
                        value={s17}
                        control={
                          <Checkbox
                            checked={s17}
                            color="primary"
                            name="signal17"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Đau cơ hoặc đau mỏi khắp người
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal18"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS18(e.target.checked);
                          })
                        }
                        name="signal18"
                        value={value}
                        control={
                          <Checkbox
                            checked={s18}
                            color="primary"
                            name="signal18"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>Nôn ói</div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal19"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS19(e.target.checked);
                          })
                        }
                        name="signal19"
                        value={s19}
                        control={
                          <Checkbox
                            checked={s19}
                            color="primary"
                            name="signal19"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>Tiêu chảy</div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal20"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS20(e.target.checked);
                          })
                        }
                        name="signal20"
                        value={s20}
                        control={
                          <Checkbox
                            checked={s20}
                            color="primary"
                            name="signal20"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Mất vị giác hoặc khứu giác mới xảy ra
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal21"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS21(e.target.checked);
                          })
                        }
                        name="signal21"
                        value={s21}
                        control={
                          <Checkbox
                            checked={s21}
                            color="primary"
                            name="signal21"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>Nghẹt mũi</div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal22"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS22(e.target.checked);
                          })
                        }
                        name="signal22"
                        value={s22}
                        control={
                          <Checkbox
                            checked={s22}
                            color="primary"
                            name="signal22"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>Sổ mũi</div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal23"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        name="signal23"
                        onChange={
                          (onChange,
                          (e) => {
                            setS23(e.target.checked);
                          })
                        }
                        value={s23}
                        control={
                          <Checkbox
                            checked={s23}
                            color="primary"
                            name="signal23"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>Mệt mỏi</div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal24"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        onChange={
                          (onChange,
                          (e) => {
                            setS24(e.target.checked);
                          })
                        }
                        name="signal24"
                        value={s24}
                        control={
                          <Checkbox
                            checked={s24}
                            color="primary"
                            name="signal24"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>
                            Đau nhức đầu
                          </div>
                        }
                      />
                    )}
                  />
                </div>
                <div>
                  <Controller
                    control={control}
                    name="signal25"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControlLabel
                        className={classes.checkboxLable}
                        labelPlacement="start"
                        name="signal25"
                        onChange={
                          (onChange,
                          (e) => {
                            setS25(e.target.checked);
                          })
                        }
                        value={s25}
                        control={
                          <Checkbox
                            checked={s25}
                            color="primary"
                            name="signal25"
                          />
                        }
                        label={
                          <div className={classes.checkboxLable}>Không có</div>
                        }
                      />
                    )}
                  />
                </div>
              </div>
              <Grid style={{ marginTop: "1em" }} container spacing={2}>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="otherSignal"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        multiline
                        maxRows={4}
                        {...register("otherSignal")}
                        label="Triệu chứng khác (Nếu có)"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        id="otherSignal"
                        onChange={(e) =>
                          setValue("otherSignal", e.target.value)
                        }
                        value={value}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="statusquo"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        multiline
                        fullWidth
                        maxRows={4}
                        {...register("statusquo")}
                        label="Tình trạng bệnh mạn tính hiện tại"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        onChange={(e) => setValue("statusquo", e.target.value)}
                        value={value}
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </div>
          <div style={{ textAlignLast: "end" }}>
            <Button
              onClick={() => {
                setTab(3);
                window.scrollTo({ top: 0, behavior: "smooth" });
              }}
              disabled={
                (s1 === false &&
                  s2 === false &&
                  s3 === false &&
                  s4 === false &&
                  s5 === false &&
                  s6 === false &&
                  s7 === false &&
                  s8 === false &&
                  s9 === false &&
                  s10 === false &&
                  s11 === false) ||
                (s12 === false &&
                  s13 === false &&
                  s14 === false &&
                  s15 === false &&
                  s16 === false &&
                  s17 === false &&
                  s18 === false &&
                  s19 === false &&
                  s20 === false &&
                  s21 === false &&
                  s22 === false &&
                  s23 === false &&
                  s24 === false &&
                  s25 === false)
              }
              className={classes.nextBtn}
            >
              <b>TIẾP THEO </b>
              <ArrowForwardIosIcon style={{ marginLeft: "0.2em" }} />
            </Button>
          </div>
        </TabPanel>
        <TabPanel
          hidden={tab !== 3}
          value={tab}
          index={3}
          dir={theme.direction}
        >
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <Button onClick={() => setTab(2)}>
              <ArrowBackIosIcon></ArrowBackIosIcon>
            </Button>
            <Button onClick={() => setTab(4)}>
              <ArrowForwardIosIcon></ArrowForwardIosIcon>
            </Button>
          </div>
          <div className={classes.titleTab}>
            THÔNG SỐ HIỆN TẠI
            <hr />
          </div>
          <Grid style={{ marginTop: "1em" }} container spacing={2}>
            <Grid item xs={12} sm={12} md={12}>
              <Controller
                control={control}
                name="color"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <FormControl
                    style={{ marginTop: "1em" }}
                    variant="outlined"
                    className={classes.formControl}
                    fullWidth
                  >
                    <InputLabel>Màu da và môi</InputLabel>
                    <Select
                      id="demo-simple-select-outlined"
                      label="Màu da và môi"
                      {...register("color")}
                      onChange={onChange}
                      value={value}
                    >
                      <MenuItem value={"typhus"}>Da niêm hồng</MenuItem>
                      <MenuItem value={"normal"}>Da niêm nhạt</MenuItem>
                      <MenuItem value={"haggard"}>Xanh xao</MenuItem>
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
          </Grid>
          <Grid style={{ marginTop: "1em" }} container spacing={2}>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="pluse"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    {...register("pluse")}
                    label="Mạch(Nhập hoặc ghi chú vào mục Khác)"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">Nhịp</InputAdornment>
                      ),
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="start"
                        ></InputAdornment>
                      ),
                    }}
                    onChange={(e) => {
                      setValue("pluse", e.target.value);
                    }}
                    type="number"
                    id="pluse"
                    value={value}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="notepluse"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    {...register("notepluse")}
                    onChange={(e) => {
                      setValue("notepluse", e.target.value);
                    }}
                    multiline
                    maxRows={4}
                    label="Ghi chú"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    value={value}
                  />
                )}
              />
            </Grid>
          </Grid>

          <Grid style={{ marginTop: "1em" }} container spacing={2}>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="spo2home"
                render={({ field: { onChange, value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    {...register("spo2home")}
                    label="SpO2 tại nhà (nhập tên hãng  hoặc ghi chú khác vào mục Khác)"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">%</InputAdornment>
                      ),
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    onChange={(e) => {
                      setValue("spo2home", e.target.value);
                    }}
                    type="number"
                    id="spo2home"
                    value={value}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="notespo2home"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    multiline
                    maxRows={4}
                    {...register("notespo2home")}
                    label="Ghi chú"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    id="notespo2home"
                    value={value}
                    onChange={(e) => {
                      setValue("notespo2home", e.target.value);
                    }}
                  />
                )}
              />
            </Grid>
          </Grid>
          <Grid style={{ marginTop: "1em" }} container spacing={2}>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="temperature"
                render={({ field: { onChange, value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    {...register("temperature")}
                    onChange={(e) => {
                      setValue("temperature", e.target.value);
                    }}
                    label=" Nhiệt độ (nhập trị số hoặc ghi chú khác vào mục Khác)"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <sup>o</sup>C
                        </InputAdornment>
                      ),
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    type="number"
                    id="temperature"
                    value={value}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="notetemperature"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    multiline
                    maxRows={4}
                    {...register("notetemperature")}
                    onChange={(e) => {
                      setValue("notetemperature", e.target.value);
                    }}
                    label="Ghi chú"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    value={value}
                  />
                )}
              />
            </Grid>
          </Grid>
          <Grid style={{ marginTop: "1em" }} container spacing={2}>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="bloodpressure"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    {...register("bloodpressure")}
                    label="Huyết áp (Tâm thu)"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">mmHg</InputAdornment>
                      ),
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="start"
                        ></InputAdornment>
                      ),
                    }}
                    onChange={(e) => {
                      setValue("bloodpressure", e.target.value);
                    }}
                    type="number"
                    id="c"
                    value={value}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="bloodpressure2"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    {...register("bloodpressure2")}
                    label="Huyết áp (Tâm trương)"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">mmHg</InputAdornment>
                      ),
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="start"
                        ></InputAdornment>
                      ),
                    }}
                    onChange={(e) => {
                      setValue("bloodpressure2", e.target.value);
                    }}
                    type="number"
                    value={value}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <Controller
                control={control}
                name="notebloodpressure"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    multiline
                    maxRows={4}
                    {...register("notebloodpressure")}
                    onChange={(e) => {
                      setValue("notebloodpressure", e.target.value);
                    }}
                    label="Ghi chú (Huyết áp)"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    value={value}
                  />
                )}
              />
            </Grid>
          </Grid>
          <Grid style={{ marginTop: "1em" }} container spacing={2}>
            <Grid item xs={12} sm={12} md={12}>
              <Controller
                control={control}
                name="other"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    {...register("other")}
                    label="Khác"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    onChange={(e) => {
                      setValue("other", e.target.value);
                    }}
                    id="other"
                    value={value}
                  />
                )}
              />
            </Grid>
          </Grid>
          <div style={{ textAlignLast: "end" }}>
            <Button
              onClick={() => {
                setTab(4);
                window.scrollTo({ top: 0, behavior: "smooth" });
              }}
              className={classes.nextBtn}
            >
              <b>TIẾP THEO </b>
              <ArrowForwardIosIcon style={{ marginLeft: "0.2em" }} />
            </Button>
          </div>
        </TabPanel>
        <TabPanel
          hidden={tab !== 4}
          value={tab}
          index={4}
          dir={theme.direction}
        >
          <div
            style={{
              display: "flex",
              justifyContent: idEdit ? "space-between" : "",
            }}
          >
            <Button onClick={() => setTab(3)}>
              <ArrowBackIosIcon></ArrowBackIosIcon>
            </Button>
          </div>
          <div className={classes.titleTab}>
            KẾT LUẬN
            <hr />
          </div>
          <Grid style={{ marginTop: "1em" }} container spacing={2}>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="attribute"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <FormControl
                    style={{ marginTop: "1em" }}
                    variant="outlined"
                    className={classes.formControl}
                    fullWidth
                  >
                    <InputLabel>Đánh giá mức độ bệnh hiện tại</InputLabel>
                    <Select
                      id="demo-simple-select-outlined"
                      label="Đánh giá mức độ bệnh hiện tại"
                      {...register("attribute")}
                      onChange={onChange}
                      value={value}
                      defaultValue={"codvidhide"}
                    >
                      <MenuItem value={"f1hidecovid"}>
                        F1 có triệu chứng (nghi nhiễm)
                      </MenuItem>
                      <MenuItem value={"f1showcovid"}>
                        F1 không triệu chứng
                      </MenuItem>
                      <MenuItem value={"codvidhide"}>
                        COVID-19 không triệu chứng
                      </MenuItem>
                      <MenuItem value={"covidsimple"}>
                        COVID-19 mức độ nhẹ (Viêm hô hấp trên cấp)
                      </MenuItem>
                      <MenuItem value={"covidmedium"}>
                        COVID-19 mức độ vừa (Viêm phổi)
                      </MenuItem>
                      <MenuItem value={"covidhight"}>
                        COVID-19 mức độ nặng (Viêm phổi nặng) và Nguy kịch
                      </MenuItem>
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="quantityofsick"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    required
                    {...register("quantityofsick")}
                    label="Bệnh ngày thứ"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    type="number"
                    error={err9}
                    helperText={err9 && "Vui lòng nhập thông tin ngày bệnh"}
                    onChange={(e) => {
                      if (e.target.value > 0) setErr9(false);
                      else setErr9(true);
                      setVal9(e.target.value);
                      setValue("quantityofsick", e.target.value);
                    }}
                    value={value}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <Controller
                control={control}
                name="noteattribute"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    {...register("noteattribute")}
                    label="Ghi chú"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    onChange={(e) => setValue("noteattribute", e.target.value)}
                    value={value}
                  />
                )}
              />
            </Grid>

            <Grid item xs={12} sm={12} md={12}>
              <Controller
                control={control}
                name="guide"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    required
                    maxRows={4}
                    multiline
                    {...register("guide")}
                    label="Miêu tả cách xử lý triệu chứng, hướng dẫn bệnh nhân/người nhà tự chăm sóc"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    error={err10}
                    helperText={
                      err10 &&
                      "Vui lòng nhập miêu tả, hướng dẫn bệnh nhân/người nhà tự chăm sóc"
                    }
                    onChange={(e) => {
                      if (e.target.value) setErr10(false);
                      else setErr10(true);
                      setVal10(e.target.value);
                      setValue("guide", e.target.value);
                    }}
                    value={value}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <Controller
                control={control}
                name="summary"
                render={({ field: { value } }) => (
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    required
                    maxRows={4}
                    multiline
                    {...register("summary")}
                    label="Tình trạng/bệnh lý khác kèm theo (miêu tả ngắn gọn)"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    error={err11}
                    helperText={
                      err11 && "Vui lòng nhập tình trạng/bệnh lý khác kèm theo"
                    }
                    onChange={(e) => {
                      if (e.target.value) setErr11(false);
                      else setErr11(true);
                      setVal11(e.target.value);
                      setValue("summary", e.target.value);
                    }}
                    value={value}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <Controller
                control={control}
                name="riskassessment"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <FormControl
                    style={{ marginTop: "1em" }}
                    variant="outlined"
                    className={classes.formControl}
                    required
                    fullWidth
                  >
                    <InputLabel>Đánh giá phân loại nguy cơ & xử trí</InputLabel>
                    <Select
                      id="demo-simple-select-outlined"
                      label="Đánh giá phân loại nguy cơ & xử trí"
                      {...register("riskassessment")}
                      defaultValue={"low"}
                      onChange={onChange}
                      value={value}
                    >
                      <MenuItem value={"low"}>Nguy cơ thấp</MenuItem>
                      <MenuItem value={"medium"}>Nguy cơ trung bình</MenuItem>
                      <MenuItem value={"high"}>Nguy cơ cao</MenuItem>
                      <MenuItem value={"hightest"}>Nguy cơ rất cao</MenuItem>
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6} style={{ alignSelf: "center" }}>
              <div>
                <Controller
                  control={control}
                  name="reexamination"
                  render={({ field: { onChange, onBlur, value, ref } }) => (
                    <FormControlLabel
                      className={classes.checkboxLable}
                      labelPlacement="start"
                      onChange={(e) => {
                        setReExam(e.target.checked);
                        setValue("reexamination", e.target.checked);
                      }}
                      name="reexamination"
                      value={value}
                      control={
                        <Checkbox
                          checked={value}
                          color="primary"
                          name="reexamination"
                        />
                      }
                      label={
                        <div className={classes.checkboxLable}>Tái khám</div>
                      }
                    />
                  )}
                />
              </div>
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Controller
                control={control}
                name="dateofreexamination"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <DateTimePicker
                      variant="inline"
                      fullWidth
                      {...register("dateofreexamination")}
                      format="dd-MM-yyyy HH:mm"
                      margin="normal"
                      id="date-picker-inline"
                      label="Ngày hẹn tái khám, theo dõi (Nếu có)"
                      value={value}
                      disabled={!reExam}
                      defaultValue={moment().format()}
                      error={err12}
                      helperText={err12 && "Vui lòng chọn ngày tái khám"}
                      onChange={(e) => {
                        if (e) setErr12(false);
                        else setErr12(true);
                        setVal12(e);
                        setValue("dateofreexamination", e);
                      }}
                      KeyboardButtonProps={{
                        "aria-label": "change date",
                      }}
                    />
                  </MuiPickersUtilsProvider>
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6} style={{ alignSelf: "center" }}>
              <div>
                <Controller
                  control={control}
                  name="contacdescription"
                  render={({ field: { onChange, onBlur, value, ref } }) => (
                    <FormControlLabel
                      className={classes.checkboxLable}
                      labelPlacement="start"
                      onChange={(e) => {
                        setValue("contacdescription", e.target.checked);
                        setIsContact(e.target.checked);
                      }}
                      name="contacdescription"
                      value={value}
                      control={
                        <Checkbox
                          checked={value}
                          disabled={idEdit}
                          color="primary"
                          name="contacdescription"
                        />
                      }
                      label={
                        <div className={classes.checkboxLable}>
                          Liên hệ đội hiện trường
                        </div>
                      }
                    />
                  )}
                />
              </div>
            </Grid>
            <Grid item xs={12} sm={6} md={6} style={{ alignSelf: "center" }}>
              <div>
                <Autocomplete
                  fullWidth
                  disableClearable={true}
                  required={isContact}
                  disabled={isContact === false}
                  onChange={(option, value) => {
                    setSelectedGroup(value.id);
                  }}
                  options={groups?.map((item) => {
                    return { name: item.name, id: item.id };
                  })}
                  getOptionLabel={(option) => option.name}
                  renderInput={(params) => (
                    <TextField
                      fullWidth
                      style={{ background: "#eee", borderRadius: "0.4em" }}
                      {...params}
                      placeholder="Nhóm bác sĩ"
                      variant="outlined"
                    />
                  )}
                />
              </div>
            </Grid>
          </Grid>
          {mode !== "view" && (
            <div style={{ textAlignLast: "end" }}>
              {idEdit ? (
                <Button
                  type="submit"
                  className={classes.reportBtn}
                  disabled={
                    val1.length === 0 ||
                    val2.length === 0 ||
                    val3.length === 0 ||
                    // val4.length === 0 ||
                    // val5.length === 0 ||
                    // val6.length === 0 ||
                    // val7.length === 0 ||
                    // val8.length === 0 ||
                    val9.length === 0 ||
                    val10.length === 0 ||
                    val11.length === 0 ||
                    val12.length === 0 ||
                    resMedical?.loading ||
                    resUpdate?.loading ||
                    (isContact === true && selectedGroup === "")
                  }
                >
                  {(resMedical?.loading || resUpdate?.loading) && (
                    <CircularProgress
                      style={{
                        width: "1rem",
                        height: "1rem",
                        marginRight: "0.3rem",
                      }}
                    ></CircularProgress>
                  )}
                  {idEdit ? "LƯU" : "BÁO CÁO"}
                </Button>
              ) : (
                <Button
                  type="submit"
                  className={classes.reportBtn}
                  disabled={
                    val1.length === 0 ||
                    val2.length === 0 ||
                    val3.length === 0 ||
                    // val4.length === 0 ||
                    // val5.length === 0 ||
                    // val6.length === 0 ||
                    // val7.length === 0 ||
                    // val8.length === 0 ||
                    val9.length === 0 ||
                    val10.length === 0 ||
                    val11.length === 0 ||
                    resMedical?.loading ||
                    resUpdate?.loading ||
                    (isContact === true && selectedGroup === "")
                  }
                >
                  {(resMedical?.loading || resUpdate?.loading) && (
                    <CircularProgress
                      style={{
                        width: "1rem",
                        height: "1rem",
                        marginRight: "0.3rem",
                      }}
                    ></CircularProgress>
                  )}
                  {idEdit ? "LƯU" : "BÁO CÁO"}
                </Button>
              )}
            </div>
          )}
        </TabPanel>
        {/* <TabPanel
          hidden={tab !== 5}
          value={tab}
          index={5}
          dir={theme.direction}
        >
          {" "}
          <div>
            <Button onClick={() => setTab(4)}>
              <ArrowBackIosIcon></ArrowBackIosIcon>
            </Button>
          </div>
        </TabPanel> */}
      </form>
      {/* <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={isOpenNoti}
        autoHideDuration={3000}
        onClose={() => dispatch(actions.handleCloseNoti())}
      >
        <Alert severity={"error"}>{message}</Alert>
      </Snackbar> */}
    </Container>
  );
}
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
