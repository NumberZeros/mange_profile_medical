import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import * as actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import * as apis from "./store/api";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useForm, Controller } from "react-hook-form";
import moment from "moment";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";
import PropTypes from "prop-types";
import ClearIcon from "@material-ui/icons/Clear";
import Backdrop from "@material-ui/core/Backdrop";
import ModalConfirm from "components/Modal/ModalConfirm";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { DataGrid } from "@material-ui/data-grid";
import DeleteIcon from "@material-ui/icons/Delete";
import AddCircleIcon from "@material-ui/icons/AddCircle";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  checkboxLable: {
    color: "#444",
    width: "100%",
    justifyContent: "space-between",
    "&:hover": {
      background: "#a2e0ad",
    },
  },
  nextBtn: {
    color: "#fff",
    background: "#37774B",
    border: "none",
    borderRadius: "0.3em",
    "&:hover": {
      background: "#6A995B",
    },
  },
  titleTab: {
    fontSize: "2em",
  },
  reportBtn: {
    marginTop: "0.3em",
    color: "#fff",
    background: "#37774B",
    border: "none",
    borderRadius: "0.3em",
    "&:hover": {
      background: "#6A995B",
    },
  },
  backdrop: {
    zIndex: 4799,
    color: "#fff",
  },
  checkboxes: {
    alignSelf: "start",
    marginLeft: "0.2em",
    minWidth: "80vw",
    justifyContent: "space-between",
    marginTop: "0.2em",
    "&:hover": {
      background: "#b6e5b0",
    },
    "@media (min-width: 1024px)": {
      minWidth: "61vw",
      maxWidth: "66vw",
    },
  },
  formatStyle: {
    fontSize: "0.9rem",
  },
  cancel: {
    backgroundColor: "#F44336",
    color: "white",
    marginRight: "1em",
  },
}));
const ADD_RESULT = apis.addResult;
const ADD_MEDICINE = apis.addMedicine;
const ADD_MEDICINE_EMER = apis.addMedicineEmer;
const UPDATE_MEDICAL = apis.updateMedical;
const FIND_ONE_MEDICAL = apis.findOneMedical;
const FIND_ALL_MEDICINE = apis.findAllMedicine;

export default function Medicine(props) {
  const classes = useStyles();

  const { isOpenMed, medical, idMed, medType, patient, medicines } = useSelector(
    (state) => state["Patients"]
  );

  const [medName, setMedName] = useState("");
  const [medQty, setMedQty] = useState(0);
  const [medNote, setMedNote] = useState("");

  const [findOneMedical, dataMed] = useMyLazyQuery(FIND_ONE_MEDICAL, {
    onCompleted: () => {
      if (dataMed) {
        dispatch(actions.handleFindOneMedSuccess(dataMed));
      } else dispatch(actions.handleFindOneMedFail());
    },
    fetchPolicy: "network-only",
  });

  const [findAllMedicine, dataMedicine] = useMyLazyQuery(FIND_ALL_MEDICINE, {
    onCompleted: () => {
      if (dataMedicine.data) {
        dispatch(actions.handleFindMedicineSuccess(dataMedicine.data));
      } else dispatch(actions.handleFindMedicineFail());
    },
    fetchPolicy: "network-only",
  });

  const [addMedicine, addMedicineRes] = useMutation(ADD_MEDICINE, {
    errorPolicy: "all",
  });

  const [addMedicineEmer, addMedicineEmerRes] = useMutation(ADD_MEDICINE_EMER, {
    errorPolicy: "all",
  });

  const [listMedicine, setListMedicine] = useState([]);

  const [dataSubmit, setDataSubmit] = useState(null);
  const [numberRows, setNumberRows] = useState(0);
  const { register, handleSubmit, setValue, control } = useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const dispatch = useDispatch();
  const [pcrDate, setPcrdate] = useState(moment().utc().format());

  const top100Films = [
    { title: "Test thuốc" },
  ];

  const onSubmit = (dataForm) => {
    setDataSubmit({ ...dataForm, prcrealdate: pcrDate });
    setIsModalOpen(true);
  };

  useEffect(()=>{
    dispatch(actions.handleFindMedicine())
    findAllMedicine({})
  },[])

  return (
    <Container component="main" maxWidth="l" style={{ paddingTop: "1em" }}>
      <Backdrop className={classes.backdrop} open={dataMed?.loading || dataMedicine.loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <div style={{ textAlign: "end" }}></div>

      <form
        onSubmit={handleSubmit(onSubmit)}
        noValidate
        className={classes.form}
      >
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div className={classes.titleTab}>THUỐC</div>
          <Button onClick={() => dispatch(actions.handleCloseMedicine())}>
            <ClearIcon></ClearIcon>
          </Button>
        </div>

        <hr style={{ marginBottom: "1em" }} />
        <div className={classes.body}>
          <Grid container spacing={2}>
            <Grid container spacing={2}>
              <Grid item xs={8} sm={4} md={4}>
                <Controller
                  control={control}
                  name="name"
                  render={({ field: { onChange, value } }) => (
                    <Autocomplete
                      className={classes.formatStyle}
                      fullWidth
                      freeSolo
                      margin="normal"
                      onChange={(e) => {
                        setValue("name", e.target.value);
                      }}
                      required
                      label="Chọn thuốc"
                      id="name"
                      options={[...top100Films, ...medicines.map(item=>{return {title: item.name}})]}
                      onSelect={(e) => {
                        setMedName(e.target.defaultValue);
                      }}
                      getOptionLabel={(option) => option.title}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Chọn thuốc"
                          variant="outlined"
                        />
                      )}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={4} sm={2} md={2}>
                <Controller
                  control={control}
                  name="quantity"
                  render={({ field: { onChange, value } }) => (
                    <TextField
                      variant="outlined"
                      className={classes.formatStyle}
                      fullWidth
                      required
                      {...register("quantity")}
                      onChange={(e) => {
                        setValue("quantity", e.target.value);
                        setMedQty(e.target.value);
                      }}
                      label="Số lượng"
                      id="quantity"
                      type="number"
                      value={value}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={10} sm={5} md={5}>
                <Controller
                  control={control}
                  name="description"
                  render={({ field: { onChange, onBlur, value, ref } }) => (
                    <TextField
                      variant="outlined"
                      className={classes.formatStyle}
                      fullWidth
                      {...register("description")}
                      label="Ghi chú"
                      id="description"
                      onChange={(e) => setMedNote(e.target.value)}
                      value={value}
                    />
                  )}
                />
              </Grid>

              <Grid item xs={1} sm={1} md={1} style={{ alignSelf: "center" }}>
                <Button
                  style={{ alignSelf: "center" }}
                  onClick={() => {
                    if (medName && medQty > 0) {
                      setListMedicine([
                        ...listMedicine,
                        {
                          id: numberRows + 1,
                          name: medName,
                          quantity: medQty,
                          description: medNote,
                        },
                      ]);
                      setNumberRows(numberRows + 1);
                    }
                  }}
                >
                  <AddCircleIcon style={{ color: "#37774B" }} />
                </Button>
              </Grid>
            </Grid>
          </Grid>
          <div style={{ marginTop: "0.6em", marginBottom: "-0.5em" }}>
            Thuốc đã chọn
          </div>

          <div
            style={{ height: "300px", marginTop: "1em", minHeight: "120px" }}
          >
            <DataGrid
              rows={listMedicine}
              columns={[
                {
                  field: "name",
                  headerName: "Tên",
                  flex: 1,
                  minWidth: 100,
                },
                {
                  flex: 0.6,
                  field: "quantity",
                  headerName: "Số lượng",
                  minWidth: 140,
                },
                {
                  flex: 1,
                  field: "description",
                  headerName: "Ghi chú",
                  minWidth: 240,
                },
                {
                  field: "actions",
                  headerName: `${" "}`,
                  minWidth: 80,
                  headerClassName: classes.headerTable,
                  // eslint-disable-next-line react/display-name
                  renderCell: (params) => (
                    <div>
                      <Button className={classes.btnDel}>
                        <DeleteIcon
                          onClick={() => {
                            setListMedicine(
                              listMedicine.filter(
                                (item) => item.id !== params.row.id
                              )
                            );
                          }}
                          style={{ color: "#f44336" }}
                        ></DeleteIcon>
                      </Button>
                    </div>
                  ),
                },
              ]}
              pageSize={5}
              rowsPerPageOptions={[5]}
              checkboxSelection={false}
              disableSelectionOnClick
            />
          </div>
          <div
            style={{ textAlign: "end", marginBottom: "1em", marginTop: "1em" }}
          >
            <Button
              className={classes.cancel}
              onClick={() => dispatch(actions.handleCloseMedicine())}
            >
              HỦY
            </Button>
            <Button
              disabled={listMedicine.length === 0}
              type="submit"
              className={classes.nextBtn}
            >
              LƯU
            </Button>
          </div>
          <ModalConfirm
            title="Xác nhận"
            open={isModalOpen}
            disableBtn={addMedicineRes.loading || addMedicineEmerRes.loading}
            description="Bạn có muốn xác nhận thêm thuốc?"
            handleOk={() => {
              // console.log(listMedicine);
              if (medType === "normal") {
                dispatch(actions.handleAddMedicine());
                addMedicine({
                  variables: {
                    medicalid: props.idMed,
                    ownerid: window.sessionStorage.getItem("id"),
                    listMedicine: listMedicine.map(item=>{
                      return{
                        name: item.name,
                        quantity: parseFloat(item.quantity),
                        description: item.description,
                      }
                    })
                  },
                })
                  .then((res) => {
                    dispatch(actions.handleAddMedicineSuccess(res));
                    setIsModalOpen(false);
                    // setOnloading(false);
                  })
                  .catch((err) => {
                    dispatch(actions.handleAddMedicineFail(err));
                    // setOnloading(false);
                  });
              } else {
                dispatch(actions.handleAddMedicineEmer());
                addMedicineEmer({
                  variables: {
                    emergencyid: props.idMed,
                    patientid: patient.id,
                    ownerid: window.sessionStorage.getItem("id"),
                    listMedicine: listMedicine.map(item=>{
                      return{
                        name: item.name,
                        quantity: parseFloat(item.quantity),
                        description: item.description,
                      }
                    })
                  },
                })
                  .then((res) => {
                    dispatch(
                      actions.handleAddMedicineEmerSuccess({
                        ...res,
                        idEmer: props.idMed,
                      })
                    );
                    setIsModalOpen(false);
                    // setOnloading(false);
                  })
                  .catch((err) => {
                    dispatch(actions.handleAddMedicineEmerFail(err));
                    // setOnloading(false);
                  });
              }
            }}
            handleClose={() => setIsModalOpen(false)}
          ></ModalConfirm>
        </div>
      </form>
    </Container>
  );
}
