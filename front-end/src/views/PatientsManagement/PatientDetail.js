import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useForm, Controller } from "react-hook-form";
import Grid from "@material-ui/core/Grid";
import * as actions from "./store/actions";
import * as apis from "./store/api";

import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";
import Drawer from "@material-ui/core/Drawer";
import blank_avt from "assets/img/blank_avt.png";
import { Chrono } from "react-chrono";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";
import moment from "moment";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import ImportContactsIcon from "@material-ui/icons/ImportContacts";
import QueueIcon from "@material-ui/icons/Queue";
import MedicalProfile from "./MedicalProfile";
import PatientForm from "./PatientForm";
import AddressForm from "constants/Address/AddressForm";
import ModalConfirm from "components/Modal/ModalConfirm";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import ResultTest from "./Result";
import Medicine from "./Medicine";
import WarningIcon from "@material-ui/icons/Warning";
import logo from "assets/img/dhyd.png";
import PropTypes from "prop-types";
import Box from "@material-ui/core/Box";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Emergency from "./Emergency";
import HistoryIcon from "@material-ui/icons/History";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import VisibilityIcon from "@material-ui/icons/Visibility";
import EditIcon from "@material-ui/icons/Edit";
import {
  changeStatusPatient,
  findAllStatusType,
} from "views/Dashboard/store/api";
import { useMutation } from "@apollo/client";
import AutorenewIcon from "@material-ui/icons/Autorenew";
import { findAllLessDoctors } from "views/DoctorsManagement/store/api";
import Autocomplete from "@material-ui/lab/Autocomplete";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};
const useStyles = makeStyles((theme) => ({
  paper: {
    display: "flex",
    flexDirection: "column",
  },
  tab: {
    minWidth: 100, // a number of your choice
    width: 120, // a number of your choice
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#50AF50",
    color: "white",
  },
  cancel: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#F44336",
    color: "white",
    marginRight: "1em",
  },
  cardLeft: {
    borderRadius: "0.5em",
    padding: "0.4em 1.6em 0 0 !important",
  },
  left: {
    background: "#C7E5CE",
    height: "100vh",
    padding: "0 4em",
  },
  rightTimeline: {
    overflow: "auto",
    maxHeight: "70vh",
    padding: "0px !important",
  },
  copy: {
    justifyContent: "space-between",
  },
  banner: {
    textAlign: "center",
    marginTop: "20vh",
  },
  title: {
    textAlign: "center",
    marginTop: "1.2em",
    fontSize: "1.4em",
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    "@media (max-width: 1024px)": {
      flexDirection: "column",
    },
  },
  logo: {
    marginBottom: "1.2em",
  },
  p: {
    marginRight: "0.4em",
    "@media (max-width: 900px)": {
      fontSize: "1rem",
    },
  },
  typo: {
    marginBottom: "0.2em",
    alignSelf: "center",
    marginLeft: "1em",
  },
  img: {
    "@media (max-width: 950px)": {
      width: "20em",
    },
  },
  top: {
    display: "flex",
  },
  body: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    flexGrow: "1",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
  avt: {
    width: "5em",
  },
  label: {
    fontWeight: "bold",
    marginTop: "0.1em",
  },
  value: {
    marginTop: "0.1em",
    marginLeft: "0.2em",
  },
  btnAdd: {
    color: "#fff",
    background: "#37774B",
    border: "none",
    borderRadius: "0.2em",
    "&:hover": {
      background: "#6A995B",
    },
  },
  btnAddCc: {
    color: "#eee",
    background: "#F44336",
    border: "none",
    borderRadius: "0.2em",
    "&:hover": {
      background: "#F55446",
    },
  },
  inverified: {
    color: "red",
  },
  bigIndicator: {
    backgroundColor: "#3f51b5",
  },
  rssModal: {
    width: "76%",
    overflowY: "scroll",
    padding: "0 0 1em 0",
    transform: "translate(25%, 15%)",
    maxHeight: "80vh",
    background: "#eee",
    "@media (max-width: 769px)": {
      transform: "translate(15%, 15%)",
      width: "85%",
    },
    "@media (max-width: 425px)": {
      transform: "translate(6%, 15%)",
      width: "98%",
    },
  },
  rssModalAssign: {
    width: "100%",
    overflowY: "scroll",
    padding: "0 0 1em 0",
    transform: "translate(15%, 15%)",
    maxHeight: "80vh",
    background: "#eee",
    "@media (max-width: 769px)": {
      transform: "translate(12%, 15%)",
    },
    "@media (max-width: 500px)": {
      transform: "translate(32%, 15%)",
      width: "75%",
    },
    "@media (max-width: 400px)": {
      transform: "translate(58%, 15%)",
      width: "55%",
    },
  },
  btnText: {
    "@media (max-width: 600px)": {
      display: "none",
    },
  },
  btnAddMode: {
    color: "#37774B",
    border: "none",
    padding: "0 !important",
    "&:hover": {
      color: "#6A995B",
      background: "none",
    },
    marginRight: "-1em",
  },
}));
function getModalStyle() {
  return {
    top: "50%",
    left: "50%",
    transform: `translate(-50%, -50%)`,
  };
}

const FIND_ONE_PATIENT = apis.findOnePatient;
const FIND_ALL_MED = apis.findAllMedical;
const FIND_ALL_EMER = apis.findAllEmer;
const FIND_LOG = apis.findAllLogPatients;
const ASSIGN_DOCTOR = apis.assignDoctor;
const SMS_DOCTOR = apis.sendSMSDoctor;

export default function PatientDetail() {
  const [modalStyle] = React.useState(getModalStyle);
  const { isOpenAddress } = useSelector((state) => state["Addresses"]);
  const { dataUserAuth } = useSelector((state) => state["Login"]);
  const classes = useStyles();
  const dispatch = useDispatch();
  const params = useParams();
  const history = useHistory();
  const [tabs, setTabs] = React.useState(0);
  const [onlyView, setOnlyView] = useState(false);
  const [selectedStatus, setSelectedStatus] = useState("");
  const [otherReason, setOtherReason] = useState("");
  const [listDoc, setListDoc] = useState(null);
  const [confirmAssign, setConfirmAssign] = useState(false);

  const {
    patient,
    isOpenForm,
    isOpenMed,
    medicals,
    isOpenNoti,
    message,
    isSuccess,
    isLoading,
    isLoadingMed,
    isOpenMedicine,
    isOpenResult,
    isOpenEmer,
    emers,
    oneEmer,
    doctors,
    logs,
    isUpdate,
    isOpenComplete,
    statusTypes,
  } = useSelector((state) => state["Patients"]);

  const [changeStatus, resChangeStatus] = useMutation(changeStatusPatient, {
    errorPolicy: "all",
  });
  const [assignDoctor, resAssignDoctor] = useMutation(ASSIGN_DOCTOR, {
    errorPolicy: "all",
  });
  const [findStatus, resStatus] = useMyLazyQuery(findAllStatusType, {
    onCompleted: () => {
      if (resStatus.data) {
        dispatch(actions.handleFindAllStatusSuccess(resStatus.data));
      } else dispatch(actions.handleFindAllStatusFail());
    },
    fetchPolicy: "network-only",
  });

  const [findOne, dataPatient] = useMyLazyQuery(FIND_ONE_PATIENT, {
    onCompleted: () => {
      if (dataPatient.data) {
        dispatch(actions.handleFindOnePatientSuccess(dataPatient.data));
      } else dispatch(actions.handleFindOnePatientFail());
    },
    fetchPolicy: "network-only",
  });

  const [findLog, dataLog] = useMyLazyQuery(FIND_LOG, {
    onCompleted: () => {
      if (dataLog.data) {
        dispatch(actions.handleFindLogSuccess(dataLog.data));
      } else dispatch(actions.handleFindLogFail());
    },
    fetchPolicy: "network-only",
  });

  const [findAllMed, dataMed] = useMyLazyQuery(FIND_ALL_MED, {
    onCompleted: () => {
      if (dataMed.data) {
        dispatch(actions.handleFindAllMedSuccess(dataMed.data));
      } else dispatch(actions.handleFindAllMedFail());
    },
    fetchPolicy: "network-only",
  });

  const [findAllEmer, dataEmers] = useMyLazyQuery(FIND_ALL_EMER, {
    onCompleted: () => {
      if (dataEmers.data) {
        dispatch(actions.handleFindAllEmerSuccess(dataEmers.data));
      } else dispatch(actions.handleFindAllEmerFail());
    },
    fetchPolicy: "network-only",
  });

  const [findAllDoc, dataDoc] = useMyLazyQuery(findAllLessDoctors, {
    onCompleted: () => {
      if (dataDoc) {
        dispatch(actions.handleFindAllDoctorSuccess(dataDoc));
      } else dispatch(actions.handleFindAllDoctorFail());
    },
    fetchPolicy: "network-only",
  });

  const [sendDoctor, resSendDoctor] = useMutation(SMS_DOCTOR, {
    errorPolicy: "all",
  });

  const [isEdit, setIsEdit] = useState(false);
  const [isAssign, setIsAssign] = useState(false);
  const [emersList, setEmersList] = useState(null);
  const [patientInfo, setPatientInfo] = useState(null);
  const [showForm, setShowForm] = useState(false);
  const [errorPhone, setErrorPhone] = useState(false);
  const [onLoading, setOnLoading] = useState(true);
  const [confirmProceed, setConfirmProceed] = useState(false);
  // const [isEdit, etIsEdit] = useState(false);
  const handleSetEdit = (value) => setIsEdit(value);
  // let { patientId } = useParams();
  const [medicalList, setMedicalList] = useState(medicals);
  const [selectedMed, setSelectedMed] = useState("");
  const [selectedEmer, setSelectedEmer] = useState(null);
  const [isOtherReason, setIsOtherReason] = useState(null);
  const [doctorName, setDoctorName] = useState(null);
  const [openModalStatus, setOpenModalStatus] = useState(false);
  const [selectedMedical, setSelectedMedical] = useState(null);
  const [docTitle, setDocTitle] = useState("");
  const [medTitle, setMedTitle] = useState("");

  useEffect(() => {
    if (patientInfo) {
      setOnlyView(patientInfo?.status.name === "");
    }
  }, [patientInfo, isLoadingMed]);

  useEffect(() => {
    if (params.id) {
      dispatch(actions.handleFindAllMed());
      dispatch(actions.handleFindAllEmer());
      dispatch(actions.handleFindLog());
      dispatch(actions.handleFindOnePatient());
      dispatch(actions.handleFindAllStatus());
      findAllMed({
        patientid: params.id,
      });
      findOne({
        id: params.id,
      });
      findAllEmer({
        patientid: params.id,
      });
      findLog({
        patientid: params.id,
      });
      findStatus({});
    }
    setPatientInfo(patient);
  }, [patient]);

  useEffect(() => {
    if (history.location.search === "?edit" && !isOpenForm) {
      dispatch(actions.handleOpenForm());
      history.push("?edit");
    }
  }, [history.location]);
  useEffect(() => {
    if (doctors != null)
      setListDoc(
        doctors
          ?.map((item) => {
            return {
              title: `${item.user.lastname} ${item.user.firstname}`,
              id: item.id,
            };
          })
          .filter((item) => item != null)
      );
    // setDoctorName()
  }, [doctors]);

  useEffect(() => {
    if (dataUserAuth?.roles === "admin") {
      if (doctors.length === 0) {
        dispatch(actions.handleFindAllDoctor());
        findAllDoc({ adminid: window.sessionStorage.getItem("id") });
      }
    }
  }, [dataUserAuth]);

  const attributes = {
    codvidhide: "COVID-19 không triệu chứng",
    covidsimple: "COVID-19 mức độ nhẹ (Viêm hô hấp trên cấp)",
    covidmedium: "COVID-19 mức độ vừa (Viêm phổi)",
    covidhight: "COVID-19 mức độ nặng (Viêm phổi nặng) và Nguy kịch",
    f1hidecovid: "F1 có triệu chứng (nghi nhiễm)",
    f1showcovid: "F1 không triệu chứng",
  };

  const reasons = {
    treated: "Đang điều trị",
    die: "Tử vong",
    gohome: "Khỏe/ Về nhà",
    notinformation: "Chưa rõ thông tin",
  };
  const handleChange = (event, newValue) => {
    setTabs(newValue);
    // console.log(tabs);
    // if (newValue === 0) handleFindDoctors();
    // else if (newValue === 1) handleFindPatients();
  };

  const listMedicalContact = medicals?.map((item) => {
    if (item.contacdescription === true)
      return {
        title: `HS ${moment(item.createddat).valueOf()}`,
        id: item.id,
      };
    else return;
  });
  const items =
    medicals?.map((item) => {
      return {
        title: (
          <div>
            <div className={item.isverified === false && classes.inverified}>
              {moment(item.displaydate).format("DD-MM-YYYY HH:mm")}
            </div>
            <div>
              {item.isverified === false && (
                <WarningIcon style={{ color: "red" }}></WarningIcon>
              )}
            </div>
          </div>
        ),
        cardTitle: (
          <div>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                marginLeft: "1em",
                justifyContent: "space-between",
              }}
            >
              <div style={{ marginTop: "0.5em" }}>
                HS {moment(item.createddat).valueOf()}
              </div>
              <Button
                onClick={() =>
                  dispatch(actions.handleOpenMed({ id: item.id, mode: "view" }))
                }
              >
                <VisibilityIcon></VisibilityIcon>
              </Button>
            </div>

            <div
              className={classes.timelineTitle}
              style={{ alignSelf: "center", marginLeft: "1em" }}
            >
              {attributes[item.attribute]?.toUpperCase()}
            </div>
          </div>
        ),
        cardSubtitle: <div style={{ marginLeft: "1.1em" }}>{item.summary}</div>,
        cardDetailedText: (
          <div style={{ marginLeft: "0.6em", marginBottom: "0" }}>
            {item.guide}
            <p style={{ marginTop: "0.2em" }}>
              Tái khám: {item.reexamination ? "Có" : "Không"}
            </p>
            {item.reexamination ? (
              <div>
                Ngày tái khám:{" "}
                {moment(item.dateofreexamination).format("DD-MM-YYYY")}
              </div>
            ) : (
              <br />
            )}
            {onlyView && !isLoadingMed && (
              <div style={{ textAlign: "end" }}>
                <Button
                  onClick={() =>
                    dispatch(
                      actions.handleOpenMed({ id: item.id, mode: "edit" })
                    )
                  }
                >
                  Sửa
                </Button>
                <Button
                  onClick={() => {
                    setSelectedMed(item.id);
                    dispatch(actions.handleOpenResult(item.id));
                  }}
                >
                  Thêm xét nghiệm
                </Button>
                <Button
                  onClick={() => {
                    setSelectedMed(item.id);
                    dispatch(
                      actions.handleOpenMedicine({
                        idMed: item.id,
                        medType: "normal",
                      })
                    );
                  }}
                >
                  Thêm thuốc
                </Button>
              </div>
            )}
          </div>
        ),
      };
    }) || [];

  const emer =
    emers?.map((item) => {
      return {
        title: (
          <div>
            <div>{moment(item.displaydate).format("DD-MM-YYYY HH:mm")}</div>
          </div>
        ),
        cardTitle: (
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              marginLeft: "1em",
              justifyContent: "space-between",
            }}
          >
            <div style={{ marginTop: "0.5em" }}>
              CC {moment(item.createddat).valueOf()}
            </div>
            <Button
              onClick={() => {
                // setSelectedEmer(item);
                // console.log(history.location)
                dispatch(actions.handleOpenEmer({ id: item.id, mode: "view" }));
              }}
            >
              <VisibilityIcon></VisibilityIcon>
            </Button>
          </div>
        ),
        // cardSubtitle: <div style={{ marginLeft: "1.1em" }}>chưa có</div>,
        cardDetailedText: (
          <div style={{ marginLeft: "0.6em", marginBottom: "0" }}>
            {item.guide}
            <p style={{ marginTop: "0.2em" }}>
              <b>Trạng thái bệnh nhân:</b> {item.status.name}
            </p>
            <p style={{ marginTop: "0.2em" }}>
              <b>Nơi chuyển đến:</b> {item.transferfrom}
            </p>
            <p style={{ marginTop: "0.2em" }}>
              <b> Kết cục bệnh nhân:</b> {reasons[item.reason]}
            </p>
            <p style={{ marginTop: "0.2em" }}>
              <b>Ghi chú:</b> {item.notestatus}
            </p>
            {onlyView && !isLoadingMed && (
              <div style={{ textAlign: "end" }}>
                <Button
                  onClick={() => {
                    // setSelectedEmer(item);
                    // console.log(history.location)
                    dispatch(
                      actions.handleOpenEmer({ id: item.id, mode: "edit" })
                    );
                  }}
                >
                  Sửa
                </Button>
                <Button
                  onClick={() => {
                    setSelectedMed(item.id);
                    dispatch(
                      actions.handleOpenMedicine({
                        idMed: item.id,
                        medType: "emer",
                      })
                    );
                  }}
                >
                  Thêm thuốc
                </Button>
              </div>
            )}
          </div>
        ),
      };
    }) || [];

  const log =
    logs?.map((item) => {
      return {
        title: (
          <div>
            <div>{moment(item.createddat).format("DD-MM-YYYY HH:mm")}</div>
          </div>
        ),
        // cardSubtitle: <div style={{ marginLeft: "1.1em" }}>chưa có</div>,
        cardDetailedText: (
          <div style={{ marginLeft: "0.6em", marginBottom: "0" }}>
            <p style={{ marginTop: "0.2em" }}>
              <b>Thao tác:</b> {item.name.replace(" admin", "Admin")}
            </p>
          </div>
        ),
      };
    }) || [];

  return (
    <Grid container spacing={2} component="main" className={classes.root}>
      <Backdrop
        className={classes.backdrop}
        open={dataPatient?.loading || dataMed?.loading || dataDoc?.loading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Grid className={classes.right} item xs={12} sm={12} md={12} lg={4}>
        <Card className={classes.cardLeft}>
          <CardContent>
            <div style={{ textAlign: "end" }}>
              <Button
                onClick={() => {
                  setIsEdit(true);
                  dispatch(actions.handleOpenForm("view"));
                }}
                className={classes.btnAddMode}
              >
                <VisibilityIcon></VisibilityIcon>
              </Button>
            </div>
            <div style={{ textAlign: "center" }}>
              <LazyLoadImage
                src={blank_avt}
                className={classes.avt}
              ></LazyLoadImage>
            </div>

            <Typography
              style={{ display: "flex" }}
              className={classes.title}
              gutterBottom
            >
              <div>
                {`${patientInfo?.lastname || ""} ${
                  patientInfo?.firstname || ""
                } `}{" "}
                {patientInfo?.status.name === "" && (
                  <Button
                    onClick={() => {
                      setIsEdit(true);
                      dispatch(actions.handleOpenForm("edit"));
                    }}
                    className={classes.btnAddMode}
                  >
                    <EditIcon></EditIcon>
                  </Button>
                )}
              </div>
            </Typography>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6} md={6} lg={12}>
                <div style={{ display: "flex" }}>
                  <p className={classes.label}>Ngày sinh:</p>
                  <p className={classes.value}>
                    {moment(patientInfo?.dob).format("DD/MM/YYYY") || ""}
                  </p>
                </div>
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={12}
                style={{ marginTop: "-0.5em" }}
              >
                <div style={{ display: "flex" }}>
                  <p className={classes.label}>Giới tính:</p>
                  <p className={classes.value}>
                    {patientInfo?.gender === "male"
                      ? "Nam"
                      : patientInfo?.gender === "female"
                      ? "Nữ"
                      : "Không xác định" || ""}
                  </p>
                </div>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6} md={6} lg={12}>
                <div style={{ display: "flex" }}>
                  <div style={{ display: "flex" }}>
                    <p className={classes.label}>Khám sàng lọc:</p>
                    <p className={classes.value}>
                      {patientInfo?.category?.name}
                    </p>
                  </div>
                </div>
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={12}
                style={{ marginTop: "-0.5em" }}
              >
                <div style={{ display: "flex" }}>
                  <p className={classes.label}>Người đánh giá:</p>
                  <p className={classes.value}>
                    {patientInfo?.doctor?.user?.lastname}{" "}
                    {patientInfo?.doctor?.user?.firstname}
                  </p>
                </div>
              </Grid>
              <Grid
                item
                xs={12}
                sm={12}
                md={12}
                lg={12}
                style={{ marginTop: "-0.5em", display: "flex" }}
              >
                <div style={{ display: "flex" }}>
                  <p className={classes.label}>Bác sĩ phụ trách:</p>
                  <p className={classes.value}>
                    {patientInfo?.sharedoctor?.user?.lastname}{" "}
                    {patientInfo?.sharedoctor?.user?.firstname}
                  </p>
                </div>
                {dataUserAuth?.roles === "admin" && (
                  <Button
                    style={{ marginTop: "-0.8em" }}
                    onClick={() => setIsAssign(true)}
                  >
                    <AutorenewIcon></AutorenewIcon>
                  </Button>
                )}
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6} md={6} lg={12}>
                <div style={{ display: "flex" }}>
                  <p className={classes.label} style={{ whiteSpace: "nowrap" }}>
                    Địa chỉ:
                  </p>
                  {patientInfo?.name || patientInfo?.city ? (
                    <p className={classes.value}>
                      {patientInfo?.name ? patientInfo?.name + ", " : ""}{" "}
                      {patientInfo?.ward ? patientInfo?.ward + ", " : ""}
                      {patientInfo?.province
                        ? patientInfo?.province + ", "
                        : ""}{" "}
                      {patientInfo?.city ? patientInfo?.city : ""}
                    </p>
                  ) : (
                    "Không có"
                  )}
                </div>
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={12}
                style={{ marginTop: "-0.5em" }}
              >
                <div style={{ display: "flex" }}>
                  <p className={classes.label}>Số người ở chung nhà:</p>
                  <p className={classes.value}>{patientInfo?.quantityfamily}</p>
                </div>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={6} lg={12}>
                <div style={{ display: "flex" }}>
                  <p className={classes.label}>Điện thoại:</p>
                  <p className={classes.value}>{patientInfo?.phone || ""}</p>
                </div>
              </Grid>
              <Grid
                item
                xs={12}
                sm={12}
                md={6}
                lg={12}
                style={{ marginTop: "-0.5em" }}
              >
                {patientInfo?.phoneemerency1.replace("(+84)", "").trim() !==
                  "undefined" &&
                  patientInfo?.phoneemerency1.replace("(+84)", "").trim() !==
                    "" && (
                    <div style={{ display: "flex" }}>
                      <p className={classes.label}>SĐT người thân 1:</p>
                      <p className={classes.value}>
                        {patientInfo?.phoneemerency1 || ""}
                      </p>
                    </div>
                  )}
              </Grid>
              <Grid
                item
                xs={12}
                sm={12}
                md={6}
                lg={12}
                style={{ marginTop: "-0.5em" }}
              >
                {patientInfo?.phoneemerency2.replace("(+84)", "").trim() !==
                  "undefined" &&
                  patientInfo?.phoneemerency2.replace("(+84)", "").trim() !==
                    "" && (
                    <div style={{ display: "flex" }}>
                      <p className={classes.label}>SĐT người thân 2:</p>
                      <p className={classes.value}>
                        {patientInfo?.phoneemerency2 || ""}
                      </p>
                    </div>
                  )}
              </Grid>
              <Grid
                item
                xs={12}
                sm={12}
                md={6}
                lg={12}
                style={{ marginTop: "-0.5em" }}
              >
                {patientInfo?.phoneemerency3.replace("(+84)", "").trim() !==
                  "undefined" &&
                  patientInfo?.phoneemerency3.replace("(+84)", "").trim() !==
                    "" && (
                    <div style={{ display: "flex" }}>
                      <p className={classes.label}>SĐT người thân 3:</p>
                      <p className={classes.value}>
                        {patientInfo?.phoneemerency3 || ""}
                      </p>
                    </div>
                  )}
              </Grid>
              {patientInfo?.status.name !== "" && (
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={12}
                  lg={12}
                  style={{ marginTop: "-0.5em" }}
                >
                  <div style={{ display: "flex" }}>
                    <p
                      className={classes.label}
                      style={{ whiteSpace: "nowrap" }}
                    >
                      Trạng thái bệnh nhân:
                    </p>
                    <p className={classes.value}>
                      {patientInfo?.status.name || ""}
                    </p>
                  </div>
                  {dataUserAuth?.roles === "admin" && (
                    <div style={{ textAlign: "end", marginBottom: "1em" }}>
                      <Button onClick={() => setOpenModalStatus(true)}>
                        Mở lại
                      </Button>
                    </div>
                  )}
                </Grid>
              )}

              <ModalConfirm
                title="Xác nhận"
                open={openModalStatus}
                disableBtn={resChangeStatus?.loading}
                description="Xác nhận mở lại hồ sơ bệnh nhân"
                handleOk={() => {
                  dispatch(actions.handleChangeStatus());
                  changeStatus({
                    variables: {
                      id: history.location.pathname.replace(
                        "/admin/patients-management/",
                        ""
                      ),
                      otherstatus: "",
                      status: statusTypes.id,
                      ownerid:
                        dataUserAuth?.roles === "admin"
                          ? patientInfo?.doctor.user.id
                          : window.sessionStorage.getItem("id"),
                    },
                  }).then((res) => {
                    if (res.data !== null) {
                      dispatch(actions.handleChangeStatusSuccess(res.data));
                      setOpenModalStatus(false);
                      location.reload();
                    } else dispatch(actions.handleChangeStatusFail());
                  });
                }}
                handleClose={() => setOpenModalStatus(false)}
              ></ModalConfirm>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={8}>
        <Card className={classes.cardLeft}>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <Tabs
              value={tabs}
              style={{ height: "50px", overflowX: "auto" }}
              classes={{ indicator: classes.bigIndicator }}
              onChange={handleChange}
              aria-label="disabled tabs example"
            >
              <Tab classes={{ root: classes.tab }} label="Bệnh án" />
              <Tab classes={{ root: classes.tab }} label="Cấp cứu" />
              <Tab classes={{ root: classes.tab }} label="Lịch sử" />
            </Tabs>
            <div
              style={{
                marginTop: "1em",
                marginBottom: "0.8em",
              }}
            >
              {tabs === 0 && onlyView && (
                <Button
                  onClick={() => {
                    dispatch(actions.handleOpenMed({ id: "", mode: "" }));
                  }}
                  className={classes.btnAdd}
                >
                  <QueueIcon style={{ marginRight: "0.2em" }}></QueueIcon>
                  <span className={classes.btnText}> THÊM BỆNH ÁN</span>
                </Button>
              )}
              {tabs === 1 && onlyView && (
                <Button
                  onClick={() => {
                    setConfirmProceed(true);
                  }}
                  className={classes.btnAddCc}
                >
                  <QueueIcon style={{ marginRight: "0.2em" }}></QueueIcon>
                  <span className={classes.btnText}> THÊM HỒ SƠ CẤP CỨU</span>
                </Button>
              )}
            </div>
          </div>
          <TabPanel value={tabs} index={0}>
            {!isLoadingMed && (
              <CardContent className={classes.rightTimeline}>
                {items.length > 0 ? (
                  <Chrono
                    items={items}
                    useReadMore={false}
                    scrollable
                    slideShow
                    mode="VERTICAL"
                    style={{ padding: "0 !important" }}
                  >
                    <div className="chrono-icons">
                      <img src={logo} alt="image1" />
                      {/* <WarningIcon></WarningIcon> */}
                    </div>
                  </Chrono>
                ) : (
                  <div style={{ textAlign: "center" }}>
                    <div>
                      <ImportContactsIcon></ImportContactsIcon>
                    </div>
                    <p>Hiện tại bệnh nhân chưa có bệnh án</p>
                  </div>
                )}
              </CardContent>
            )}
          </TabPanel>
          <TabPanel value={tabs} index={1}>
            {!isLoadingMed && (
              <CardContent
                style={{ padding: "0 !important" }}
                className={classes.rightTimeline}
              >
                {emer.length > 0 ? (
                  <Chrono
                    items={emer.reverse()}
                    useReadMore={false}
                    scrollable
                    slideShow
                    mode="VERTICAL"
                  >
                    <div className="chrono-icons">
                      <img src={logo} alt="image1" />
                      {/* <WarningIcon></WarningIcon> */}
                    </div>
                  </Chrono>
                ) : (
                  <div style={{ textAlign: "center" }}>
                    <div>
                      <ImportContactsIcon
                        style={{ color: "#F44336" }}
                      ></ImportContactsIcon>
                    </div>
                    <p>Hiện tại bệnh nhân chưa có hồ sơ cấp cứu</p>
                  </div>
                )}
              </CardContent>
            )}
          </TabPanel>
          <TabPanel value={tabs} index={2}>
            {!isLoadingMed && (
              <CardContent className={classes.rightTimeline}>
                {log.length > 0 ? (
                  <Chrono
                    items={log.reverse()}
                    useReadMore={false}
                    scrollable
                    slideShow
                    cardHeight={50}
                    mode="VERTICAL"
                    style={{ padding: "0 !important" }}
                  >
                    <div className="chrono-icons">
                      <img src={logo} alt="image1" />
                      {/* <WarningIcon></WarningIcon> */}
                    </div>
                  </Chrono>
                ) : (
                  <div style={{ textAlign: "center", marginTop: "0.95em" }}>
                    <div>
                      <HistoryIcon></HistoryIcon>
                    </div>
                    <p>Chưa ghi nhận lịch sử</p>
                  </div>
                )}
              </CardContent>
            )}
          </TabPanel>
        </Card>
      </Grid>
      <Drawer
        anchor="right"
        open={isOpenForm}
        onClose={() => {
          dispatch(actions.handleCloseForm(true));
          history.push(history.location.path);
        }}
      >
        <PatientForm
          isEdit={isEdit}
          setEdit={() => handleSetEdit()}
        ></PatientForm>
      </Drawer>
      <Modal
        open={isAssign}
        style={{
          maxWidth: "30em",
          position: "absolute",
          left: "calc(50% - 320px)",
          top: "calc(50% - 15em)",
        }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div className={classes.rssModalAssign} style={{ padding: "1em" }}>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div style={{ fontWeight: "bold", fontSize: "1.2em" }}>
              Phân công bác sĩ
            </div>
          </div>
          <div style={{ fontSize: "1em", marginTop: "1em" }}>Chọn bác sĩ</div>
          <Autocomplete
            fullWidth
            margin="normal"
            label="Chọn bác sĩ"
            id="name"
            onChange={(event, value) => {
              if (value) {
                setDoctorName(value);
                setDocTitle(value.title);
              }
            }}
            options={listDoc}
            getOptionLabel={(option) => option?.title}
            renderInput={(params) => {
              return (
                <TextField
                  placeholder="Chọn bác sĩ"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment
                        style={{ marginLeft: "-0.1em" }}
                        position="end"
                      ></InputAdornment>
                    ),
                    classes: { input: classes.doctorSelect },
                  }}
                  {...{
                    ...params,
                    inputProps: {
                      ...params.inputProps,
                      value: docTitle || "",
                    },
                  }}
                  onChange={(e) => setDocTitle(e.target.value)}
                  variant="outlined"
                />
              );
            }}
          />
          <div style={{ fontSize: "1em", marginTop: "1em" }}>Chọn bệnh án</div>
          <Autocomplete
            fullWidth
            margin="normal"
            label="Chọn bệnh án"
            id="name"
            onChange={(event, value) => {
              if (value) {
                setSelectedMedical(value);
                setMedTitle(value.title);
              }
            }}
            onInputChange={(event, value) => {
              if (value) {
                setSelectedMedical(value);
              }
            }}
            options={listMedicalContact?.filter((item) => item != null)}
            getOptionLabel={(option) => option?.title}
            renderInput={(params) => {
              return (
                <TextField
                  placeholder="Chọn bệnh án"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment
                        style={{ marginLeft: "-0.1em" }}
                        position="end"
                      ></InputAdornment>
                    ),
                    classes: { input: classes.doctorSelect },
                  }}
                  {...{
                    ...params,
                    inputProps: {
                      ...params.inputProps,
                      value: medTitle || "",
                    },
                  }}
                  onChange={(e) => setMedTitle(e.target.value)}
                  variant="outlined"
                />
              );
            }}
          />
          <div style={{ textAlign: "end" }}>
            <Button
              className={classes.cancel}
              onClick={() => setIsAssign(false)}
            >
              Hủy
            </Button>
            <Button
              className={classes.submit}
              onClick={() => setConfirmAssign(true)}
              disabled={doctorName === null || selectedMedical === null}
            >
              Xác nhận
            </Button>
          </div>
        </div>
      </Modal>
      <Modal
        open={isOpenAddress}
        style={{
          maxWidth: "30em",
          position: "absolute",
          left: "calc(50% - 300px)",
          top: "calc(50% - 25em)",
        }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <AddressForm></AddressForm>
      </Modal>
      <Modal
        open={isOpenMed}
        style={{ maxHeight: "20vh", maxWidth: "90vw" }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          // style={{
          //   overflowY: "scroll",
          //   padding: "0 0 1em 0",
          //   transform: "translate(25%, 15%)",
          //   maxHeight: "80vh",
          //   background: "#eee",
          //   width: "76%",
          // }}
          className={classes.rssModal}
          item
          xs={12}
          sm={12}
          md={12}
        >
          <MedicalProfile></MedicalProfile>
        </Grid>
      </Modal>
      <Modal
        open={isOpenEmer}
        style={{ maxHeight: "20vh", maxWidth: "90vw" }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid className={classes.rssModal} item xs={12} sm={12} md={12}>
          <Emergency data={selectedEmer}></Emergency>
        </Grid>
      </Modal>
      <Modal
        open={isOpenResult}
        style={{ maxHeight: "20vh", maxWidth: "90vw" }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid className={classes.rssModal} item xs={12} sm={12} md={12}>
          <ResultTest idMed={selectedMed}></ResultTest>
        </Grid>
      </Modal>
      <Modal
        open={isOpenMedicine}
        style={{ maxHeight: "20vh", maxWidth: "90vw" }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid className={classes.rssModal} item xs={12} sm={12} md={12}>
          <Medicine idMed={selectedMed}></Medicine>
        </Grid>
      </Modal>

      <ModalConfirm
        title="Xác nhận phân công"
        open={confirmAssign}
        disableBtn={resAssignDoctor.loading || resSendDoctor.loading}
        handleOk={() => {
          dispatch(actions.handleAssignDoctor());
          assignDoctor({
            variables: {
              ownerid: window.sessionStorage.getItem("id"),
              id: history.location.pathname.replace(
                "/admin/patients-management/",
                ""
              ),
              doctor: doctorName.id,
            },
          }).then(async () => {
            if (resAssignDoctor.data !== null) {
              dispatch(actions.handleAssignDoctorSuccess());
              dispatch(actions.handleSendSMSDoctor());
              await sendDoctor({
                variables: {
                  ownerid: window.sessionStorage.getItem("id"),
                  doctorid: doctorName.id,
                  medicalid: selectedMedical.id,
                },
              })
                .then((resSend) => {
                  if (resSend.data !== null) {
                    dispatch(actions.handleSendSMSDoctorSuccess(resSend.data));
                    setConfirmAssign(false);
                    location.reload();
                  } else dispatch(actions.handleSendSMSDoctorFail());
                })
                .catch((error) => {
                  dispatch(actions.handleSendSMSDoctorFail());
                });
            } else dispatch(actions.handleAssignDoctorFail());
          });
          // setConfirmAssign(false);
        }}
        description={<p>Bạn có chắc chắn muốn phân công cho bác sĩ này?</p>}
        handleClose={() => setConfirmAssign(false)}
      ></ModalConfirm>
      <ModalConfirm
        title="Cảnh báo"
        open={confirmProceed}
        handleOk={() => {
          dispatch(actions.handleOpenEmer({ id: "", mode: "add" }));
          setConfirmProceed(false);
        }}
        description={
          <p>
            Chức năng chỉ dành cho đội hỗ trợ hiện trường! <br /> Bạn có chắc
            chắc muốn thêm bệnh án cấp cứu?
          </p>
        }
        handleClose={() => setConfirmProceed(false)}
      ></ModalConfirm>
      {isSuccess !== null && (
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={isOpenNoti}
          autoHideDuration={3000}
          onClose={() => dispatch(actions.handleCloseNoti())}
        >
          <Alert
            severity={
              isSuccess === true
                ? "success"
                : isSuccess === false
                ? "error"
                : ""
            }
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </Grid>
  );
}
