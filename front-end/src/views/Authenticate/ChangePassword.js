import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useForm } from "react-hook-form";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import * as actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import * as apis from "./store/api";
import logo from "assets/img/medx-logo-auth.png";
import banner from "assets/img/banner-login.svg";
import { useHistory } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Cookies from "js-cookie";
import jwt_decode from "jwt-decode";
import getAuthorize from "constants/authen/authen";
import ModalConfirm from "components/Modal/ModalConfirm";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import dhyd from "assets/img/dhyd.png";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#50AF50",
    color: "white",
  },
  root: {
    height: "100vh",
  },
  left: {
    background: "#C7E5CE",
    height: "100vh",
    padding: "0 3em",
    display: "flex",
    placeContent: "space-between",
    flexDirection: "column",
  },
  right: {
    padding: "4em 4em 0 4em",
  },
  copy: {
    justifyContent: "space-between",
  },
  banner: {
    textAlign: "center",
    marginTop: "20vh",
  },
  title: {
    textAlign: "center",
    marginTop: "1.2em",
    color: "#50AF50",
    fontSize: "2em",
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    "@media (max-width: 1024px)": {
      flexDirection: "column",
    },
  },
  login: {
    textAlign: "center",
    marginTop: "1.2em",
    color: "#50AF50",
    fontSize: "2em",
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    "@media (max-width: 1024px)": {
      flexDirection: "column",
    },
  },
  subScript: {
    marginTop: "0",
    textAlign: "center",
    color: "#50AF50",
    fontSize: "1.2em",
    display: "flex",
    justifyContent: "center",
  },
  logo: {
    marginBottom: "1.2em",
  },
  p: {
    marginRight: "0.4em",
    "@media (max-width: 900px)": {
      fontSize: "1rem",
    },
  },
  plogin: {
    marginTop: "0",
    "@media (max-width: 900px)": {
      fontSize: "1rem",
    },
  },
  psub: {
    marginTop: "0",
    "@media (max-width: 900px)": {
      fontSize: "1rem",
    },
  },
  typo: {
    marginBottom: "0.2em",
  },
  img: {
    "@media (max-width: 950px)": {
      width: "20em",
    },
  },
  ptit: {
    fontSize: "1.3rem",
    fontWeight: "bold",
  },
  psubtit: {
    marginTop: "0",
    fontSize: "1.9rem",
    fontWeight: "bold",
  },
}));
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://medx.vn">
        <b>medx.vn</b>
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}
const CHANGE_PASSWORD = apis.changePassword;

export default function ChangePassword() {
  const classes = useStyles();
  const dispatch = useDispatch();
  getAuthorize();
  const history = useHistory();
  const { register, handleSubmit } = useForm();

  const [oldPass, setOldPass] = useState(false);
  const [validPass, setValidPass] = useState(false);
  const [hasConfirmPass, setHasConfirmPass] = useState(false);

  const [confirmPass, setConfirmPass] = useState("");
  const [f1, setF1] = useState("");
  const [f2, setF2] = useState("");
  const [f3, setF3] = useState("");
  const [confirmInput, setConfirmInput] = useState("");
  const { dataUserAuth, isLoading, isSuccess, message } = useSelector(
    (state) => state["Login"]
  );

  const [dis, setDis] = useState(true);
  // const [success, setSuccess] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [dataForm, setDataForm] = useState(null);

  const [id, setId] = useState("");
  const [ChangePassword, resChangePass] = useMutation(CHANGE_PASSWORD, {
    errorPolicy: "all",
  });

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [openNoti, setOpenNoti] = useState(false);

  useEffect(() => {
    if (Cookies.get("access_token")) {
      setId(jwt_decode(Cookies.get("access_token")));
    }
    // if (window.sessionStorage.getItem("access_token")) {
    //   setId(jwt_decode(window.sessionStorage.getItem("access_token")));
    // }
  }, [dataUserAuth]);

  const handleOk = () => {
    dispatch(actions.handleChangePassword(resChangePass.data));
    ChangePassword({
      variables: {
        id: id.id,
        email: dataUserAuth.email,
        password: dataForm.password,
        oldpassword: dataForm.oldpassword,
      },
    })
      .then((res) => {
        if (res.data === null) {
          dispatch(actions.handleChangePasswordFail(res));
          setIsModalOpen(false);
          setOpenNoti(true);
        } else if (res.data !== null) {
          dispatch(actions.handleChangePasswordSuccess(res));
          setIsModalOpen(false);
          setOpenNoti(true);
          history.push("/admin/dashboard");
        }
      })
      .catch((err) => {
        dispatch(actions.handleChangePasswordFail(err));
        setIsModalOpen(false);
        setOpenNoti(true);
      });
  };
  const onSubmit = (form) => {
    setDataForm(form);
    setIsModalOpen(true);
    // console.log(form)
  };
  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={6} md={7} className={classes.left}>
        <div>
          <div className={classes.banner}>
            <LazyLoadImage alt="medx.vn" src={banner} className={classes.img} />
          </div>
          <div className={classes.subScript}>
            <p className={classes.ptit}>Chào mừng bạn đến với </p>
          </div>
          <div className={classes.subScript}>
            <p className={classes.psubtit}>
              HỆ THỐNG QUẢN LÝ BỆNH NHÂN COVID-19 CÁCH LY TẠI NHÀ
            </p>
          </div>
        </div>

        <div className={classes.subScript}>
          <p className={classes.psub}>
            Một sản phẩm của <b>Công ty TNHH Dịch Vụ Giáo Dục & Y Tế MedX </b>{" "}
            <br />
            Với sự hợp tác phát triển từ{" "}
            <b>Đại học Y Dược Thành phố Hồ Chí Minh</b>
          </p>
        </div>
      </Grid>
      <Grid
        className={classes.right}
        item
        xs={12}
        sm={6}
        md={5}
        elevation={6}
        square
        style={{
          display: "flex",
          flexDirection: "column",
          maxHeight: "100vh",
          placeContent: "space-between",
        }}
      >
        <div>
          <Button onClick={() => history.push("/admin/dashboard")}>
            <ArrowBackIosIcon></ArrowBackIosIcon>Trở về
          </Button>
        </div>
        <div className={classes.paper}>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <Link href="https://medx.vn" target="_blank">
              <LazyLoadImage
                width="190"
                className={classes.logo}
                alt="medx.vn"
                src={logo}
                style={{ marginRight: "5em",marginTop:"1em" }}
              />
            </Link>
            <Link href="https://ump.edu.vn/" target="_blank">
              <LazyLoadImage
                href="https://ump.edu.vn/"
                width="90em"
                alt="dhyd.vn"
                src={dhyd}
                className={classes.logo}
              />
            </Link>
          </div>
          <div className={classes.login}>
            <p className={classes.plogin}>Thay đổi mật khẩu</p>
          </div>
          <form
            noValidate
            onSubmit={handleSubmit(onSubmit)}
            className={classes.form}
          >
            <TextField
              variant="outlined"
              margin="normal"
              error={oldPass}
              required
              autoFocus
              value={f2}
              defaultValue=""
              helperText={oldPass ? "Xin nhập mật khẩu hiện tại" : ""}
              fullWidth
              onInput={(e) => {
                if (e.target.value.length > 0) setOldPass(false);
                else setOldPass(true);
                setF2(e.target.value);
              }}
              {...register("oldpassword")}
              label="Mật khẩu hiện tại"
              type="password"
              id="password"
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              error={validPass}
              fullWidth
              value={f1}
              maxLength={16}
              onInput={(e) => {
                const pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
                if (
                  pattern.test(String(e.target.value)) &&
                  e.target.value.length >= 8 &&
                  e.target.value.length <= 16
                )
                  setValidPass(false);
                else setValidPass(true);
                setF1(e.target.value);
                setConfirmInput("");
                setConfirmPass(e.target.value);
              }}
              helperText={
                validPass
                  ? "Mật khẩu từ 8-16 ký tự, bao gồm chữ in hoa, chữ thường và số"
                  : ""
              }
              {...register("password")}
              label="Mật khẩu mới"
              type="password"
              id="password"
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              error={hasConfirmPass}
              helperText={
                hasConfirmPass ? "Mật khẩu xác nhận không trùng khớp" : ""
              }
              fullWidth
              value={confirmInput}
              {...register("confirmpass")}
              onInput={(e) => {
                if (e.target.value === confirmPass) setHasConfirmPass(false);
                else setHasConfirmPass(true);
                setConfirmInput(e.target.value);
                setF3(e.target.value);
              }}
              label="Xác nhận khẩu mới"
              type="password"
              id="password"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              className={classes.submit}
              disabled={
                resChangePass.loading ||
                f1.length === 0 ||
                f2.length === 0 ||
                f3.length === 0 ||
                hasConfirmPass ||
                oldPass ||
                validPass ||
                confirmInput.length === 0
              }
            >
              Xác nhận
            </Button>
          </form>
        </div>
        <div></div>
        <div></div>
        <Box mb={2}>
          <Copyright />
        </Box>
      </Grid>
      <ModalConfirm
        title="Đổi mật khẩu"
        open={isModalOpen}
        disableBtn={resChangePass.loading}
        description="Bạn có muốn xác nhận thay đổi mật khẩu?"
        handleOk={() => {
          handleOk();
        }}
        handleClose={() => {
          setIsModalOpen(false);
        }}
      ></ModalConfirm>
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={openNoti}
        autoHideDuration={3000}
        onClose={() => setOpenNoti(false)}
      >
        <Alert severity={isSuccess ? "success" : "error"}>{message}</Alert>
      </Snackbar>
    </Grid>
  );
}
