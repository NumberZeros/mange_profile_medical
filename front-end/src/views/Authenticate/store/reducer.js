/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "Login";

const initialState = freeze({
  dataUser: null,
  dataUserAuth: null,
  userInfo: null,
  isAuthenticated: false,
  error: null,
  isWrong: null,
  isSuccessLogin: null,
  isLoading: null,
  isOpenNoti: null,
  titleNoti: "",
  isSuccess:null,
  message:"",
  isNotiChangePass:"",
});

export default handleActions(
  {
    [actions.closeNoti]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: false,
        isNotiChangePass: false,
      });
    },
    [actions.handleLogin]: (state) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleLoginSuccess]: (state, actions) => {
      return freeze({
        ...state,
        dataUser: actions.payload.data.login_user,
        isSuccess: true,
        isLoading: false,
        isOpenNoti: true,
        isSuccessLogin: true,
        titleNoti: "Đăng nhập thành công",
      });
    },
    [actions.handleLoginFail]: (state) => {
      return freeze({
        ...state,
        isSuccess: false,
        isLoading: false,
        isSuccessLogin: false,
      });
    },
    [actions.handleAuth]: (state) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleAuthSuccess]: (state, actions) => {
      return freeze({
        ...state,
        dataUserAuth: actions.payload,
        isSuccess: true,
        isLoading: false,
      });
    },
    [actions.handleAuthFail]: (state) => {
      return freeze({
        ...state,
        isSuccess: false,
        isLoading: false,
      });
    },
    [actions.handleChangePassword]: (state) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleChangePasswordSuccess]: (state, actions) => {
      return freeze({
        ...state,
        dataUserAuth: actions.payload,
        isSuccess: true,
        isLoading: false,
        isNotiChangePass: true,
        message: "Đổi mật khẩu thành công",
      });
    },
    [actions.handleChangePasswordFail]: (state) => {
      return freeze({
        ...state,
        isSuccess: false,
        isLoading: false,
        message: "Đổi mật khẩu thất bại, xin kiểm tra lại.",
      });
    },
    [actions.handleUpdateInfo]: (state) => {
      return freeze({
        ...state,
        // isLoading: true,
      });
    },
    [actions.handleUpdateInfoSuccess]: (state, actions) => {
      return freeze({
        ...state,
        dataUserAuth: {
          dob: actions.payload.data.update_info.dob,
          email: actions.payload.data.update_info.email,
          firstname: actions.payload.data.update_info.firstname,
          lastname: actions.payload.data.update_info.lastname,
          gender: actions.payload.data.update_info.gender,
          id: actions.payload.data.update_info.id,
          phone: actions.payload.data.update_info.phone,
          roles: actions.payload.data.update_info.roles,
        },
        isSuccess: true,
        message:"Cập nhật thông tin thành công",
        // isLoading: false,
      });
    },
    [actions.handleUpdateInfoFail]: (state) => {
      return freeze({
        ...state,
        isSuccess: false,
        message:"Cập nhật thông tin thất bại",
        // isLoading: false,
      });
    },
    [actions.handleGetRefresh]: (state) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleGetRefreshSuccess]: (state, actions) => {
      return freeze({
        ...state,
        dataUserAuth: actions.payload,
        isSuccess: true,
        isLoading: false,
      });
    },
    [actions.handleGetRefreshFail]: (state) => {
      return freeze({
        ...state,
        isSuccess: false,
        isLoading: false,
      });
    },
  },
  initialState
);
