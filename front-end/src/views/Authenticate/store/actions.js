import { createAction } from "redux-actions";

export const handleLogin = createAction("AUTHENTICATE/LOGIN");
export const handleLoginSuccess = createAction("AUTHENTICATE/LOGIN_SUCCESS");
export const handleLoginFail = createAction("AUTHENTICATE/LOGIN_FAIL");

export const handleAuth = createAction("AUTHENTICATE/AUTH");
export const handleAuthSuccess = createAction("AUTHENTICATE/AUTH_SUCCESS");
export const handleAuthFail = createAction("AUTHENTICATE/AUTH_FAIL");

export const handleChangePassword = createAction("AUTHENTICATE/CHANGE_PASSWORD");
export const handleChangePasswordSuccess = createAction("AUTHENTICATE/CHANGE_PASSWORD_SUCCESS");
export const handleChangePasswordFail = createAction("AUTHENTICATE/CHANGE_PASSWORD_FAIL");

export const handleUpdateInfo = createAction("AUTHENTICATE/UPDATE_INFO");
export const handleUpdateInfoSuccess = createAction("AUTHENTICATE/UPDATE_INFO_SUCCESS");
export const handleUpdateInfoFail = createAction("AUTHENTICATE/UPDATE_INFO_FAIL");

export const handleGetRefresh = createAction("AUTHENTICATE/GET_REFRESH");
export const handleGetRefreshSuccess = createAction("AUTHENTICATE/GET_REFRESH_SUCCESS");
export const handleGetRefreshFail = createAction("AUTHENTICATE/GET_REFRESH_FAIL");

export const closeNoti = createAction("AUTHENTICATE/CLOSE_NOTI");