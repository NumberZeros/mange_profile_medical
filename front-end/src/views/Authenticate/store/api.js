import { gql } from "@apollo/client";

export const login = gql`
  mutation Login($email: String!, $password: String!) {
    login_user(input: { email: $email, password: $password }) {
      access_token
      refresh_token
      id
    }
  }
`;

export const getRefreshToken = gql`
  query GGetRefreshToken($refresh_token: String!) {
    get_refresh_token(refresh_token: $refresh_token) {
      id
      dob
      firstname
      lastname
      email
      inactive
      phone
      roles
      gender
      access_token
      refresh_token
    }
  }
`;

export const authen = gql`
  query Auth($id: String!) {
    author(id: $id) {
      id
      dob
      firstname
      lastname
      email
      inactive
      phone
      roles
      gender
    }
  }
`;

export const changePassword = gql`
  mutation ChangePassword(
    $id: String!
    $email: String!
    $password: String!
    $oldpassword: String!
  ) {
    change_password(
      input: {
        id: $id
        email: $email
        oldpassword: $oldpassword
        password: $password
      }
    ) {
      id
      dob
      firstname
      lastname
      email
      inactive
      phone
      roles
      gender
    }
  }
`;

export const updateInfo = gql`
  mutation UpdateInfo(
    $id: String!
    $firstname: String!
    $lastname: String!
    $email: String!
    $phone: String!
    $dob: DateTime!
    $gender: String!
  ) {
    update_info(
      input: {
        id: $id
        firstname: $firstname
        lastname: $lastname
        email: $email
        phone: $phone
        dob: $dob
        gender: $gender
      }
    ) {
      id
      dob
      firstname
      lastname
      email
      inactive
      phone
      roles
      gender
    }
  }
`;

export const authLink = gql`
  mutation AuthLink($authid: String!, $token: String!) {
    redirect(input: { authid: $authid, token: $token }) {
      access_token
      sms {
        doctor{
          id
          user{
            firstname
            lastname
            id
          }
        }
        medical {
          id
          inactive
          createddat
          updatedat
          patient {
            id
            beforesick
            notesatussick
            name
            city
            province
            ward
            phone
            gender
            lastname
            firstname
          }
          issupport
          weight
          height
          riskassessment
          medicine
          countdateremains
          signal1
          signal2
          signal3
          signal4
          signal5
          signal6
          signal7
          signal8
          signal9
          signal10
          signal11
          signal12
          signal13
          signal14
          signal15
          signal16
          signal17
          signal18
          signal19
          signal20
          signal21
          signal22
          signal23
          signal24
          signal25
          otherSignal
          statusquo
          color
          pluse
          notepluse
          bloodpressure
          bloodpressure2
          notebloodpressure
          spo2home
          notespo2home
          temperature
          notetemperature
          other
          attribute
          noteattribute
          quantityofsick
          guide
          summary
          contacdescription
          reexamination
          dateofreexamination
          displaydate
        }
      }
    }
  }
`;
