import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useForm } from "react-hook-form";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import * as actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import * as apis from "./store/api";
import logo from "assets/img/medx-logo-auth.png";
import banner from "assets/img/banner-login.svg";
import { useHistory } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Danger from "components/Typography/Danger.js";
import MuiAlert from "@material-ui/lab/Alert";
import CircularProgress from "@material-ui/core/CircularProgress";
import Cookies from "js-cookie";
import dhyd from "assets/img/dhyd.png";
// import { name } from "./store/reducer";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(0),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#50AF50",
    color: "white",
  },
  root: {
    height: "100vh",
  },
  left: {
    background: "#C7E5CE",
    height: "100vh",
    padding: "0 3em",
    display: "flex",
    placeContent: "space-between",
    flexDirection: "column",
    "@media (max-width: 610px)": {
      display: "none",
    },
  },
  right: {
    padding: "4em 2em 0 2em",
  },
  copy: {
    justifyContent: "space-between",
  },
  banner: {
    textAlign: "center",
    marginTop: "20vh",
  },
  title: {
    textAlign: "center",
    marginTop: "1.2em",
    color: "#50AF50",
    fontSize: "2em",
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    "@media (max-width: 1024px)": {
      flexDirection: "column",
    },
  },
  login: {
    textAlign: "center",
    marginTop: "1.2em",
    color: "#50AF50",
    fontSize: "2em",
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    "@media (max-width: 1024px)": {
      flexDirection: "column",
    },
  },
  subScript: {
    marginTop: "0",
    textAlign: "center",
    color: "#50AF50",
    fontSize: "1.2em",
    display: "flex",
    justifyContent: "center",
  },
  subScriptPhone: {
    marginTop: "0",
    textAlign: "center",
    color: "#50AF50",
    fontSize: "1.2em",
    display: "flex",
    justifyContent: "center",
    "@media (min-width: 610px)": {
      fontSize: "0.7rem",
      display: "none",
    },
  },
  logo: {
    marginBottom: "1.2em",
    width: "190px",
    "@media (max-width: 380px)": {
      width: "150px",
    },
  },
  logo1: {
    marginBottom: "1.2em",
    width: "90px",
    "@media (max-width: 380px)": {
      width: "70px",
    },
  },
  p: {
    marginRight: "0.4em",
    "@media (max-width: 900px)": {
      fontSize: "1rem",
    },
  },
  plogin: {
    marginTop: "0",
    "@media (max-width: 1100px)": {
      fontSize: "1.4rem",
    },
    "@media (max-width: 900px)": {
      fontSize: "1.3rem",
    },
  },
  ptit: {
    fontSize: "1.3rem",
    fontWeight: "bold",
  },
  psubtit: {
    marginTop: "0",
    fontSize: "1.9rem",
    fontWeight: "bold",
  },
  psub: {
    marginTop: "0",
    "@media (max-width: 900px)": {
      fontSize: "1rem",
    },
    marginRight: "0",
  },
  psubPhone: {
    marginTop: "0",
    "@media (min-width: 610px)": {
      fontSize: "0.7rem",
      display: "none",
    },
    marginRight: "0",
  },
  typo: {
    marginBottom: "0.2em",
  },
  img: {
    "@media (max-width: 950px)": {
      width: "20em",
    },
  },
}));
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://medx.vn">
        <b>medx.vn</b>
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}
const LOGIN = apis.login;
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
export default function PhoneSignIn() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [Login, { data, error, loading }] = useMutation(LOGIN, {
    errorPolicy: "all",
  });
  const { register, handleSubmit } = useForm();
  const { dataUser } = useSelector((state) => state["Login"]);
  const [success, setSuccess] = useState(false);
  const [errMail, setErrMail] = useState(false);
  const [email, setEmail] = useState("");
  const [errPassword, setErrPassword] = useState(false);
  const [choosen, setChoosen] = useState("");

  // useEffect(() => {
  //   if (Cookies.get("sv")) {
  //     setChoosen(Cookies.get("sv").toString());
  //   } else history.push("/detect");
  // }, []);

  const onSubmit = (dataForm) => {
    dispatch(actions.handleLogin(data));
    // console.log(dataForm)
    Login({ variables: { email: email, password: dataForm.password } })
      .then((res) => {
        setSuccess(true);
        // window.sessionStorage.setItem(
        //   "access_token",
        //   res.data.login_user.access_token
        // );
        Cookies.set("access_token", res.data.login_user.access_token, {
          expires: 1 / 2,
        });
        // window.sessionStorage.setItem(
        //   "refresh_token",
        //   res.data.login_user.access_token
        // );

        Cookies.set("refresh_token", res.data.login_user.refresh_token, {
          expires: 1 / 2,
        });
        window.sessionStorage.setItem("id", res.data.login_user.id);
        dispatch(actions.handleLoginSuccess(res));
        history.push("/admin/dashboard");
      })
      .catch((err) => dispatch(actions.handleLoginFail(err)));
  };

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={0} sm={6} md={7} className={classes.left}>
        <div>
          <div className={classes.banner}>
            <LazyLoadImage alt="medx.vn" src={banner} className={classes.img} />
          </div>
          <div className={classes.subScript}>
            <p className={classes.ptit}>Chào mừng bạn đến với </p>
          </div>
          <div className={classes.subScript}>
            <p className={classes.psubtit}>
              HỆ THỐNG QUẢN LÝ BỆNH NHÂN COVID-19 CÁCH LY TẠI NHÀ
            </p>
          </div>
        </div>

        <div className={classes.subScript}>
          <p className={classes.psub}>
            Một sản phẩm của <b>Công ty TNHH Dịch Vụ Giáo Dục & Y Tế MedX </b>{" "}
            <br />
            Với sự hợp tác phát triển từ{" "}
            <b>Đại học Y Dược Thành phố Hồ Chí Minh</b>
          </p>
        </div>
      </Grid>
      <Grid
        className={classes.right}
        item
        xs={12}
        sm={6}
        md={5}
        elevation={6}
        square
        style={{
          display: "flex",
          flexDirection: "column",
          maxHeight: "100vh",
          placeContent: "space-between",
        }}
      >
        <div className={classes.paper}>
          <div style={{ textAlign: "center" }}>
            <p className={classes.subScript}>Đơn vị phát triển</p>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Link href="https://medx.vn" target="_blank">
                <LazyLoadImage
                  className={classes.logo}
                  alt="medx.vn"
                  src={logo}
                  style={{ marginRight: "5em", marginTop: "1em" }}
                />
              </Link>
              <Link href="https://ump.edu.vn/" target="_blank">
                <LazyLoadImage
                  href="https://ump.edu.vn/"
                  alt="dhyd.vn"
                  src={dhyd}
                  className={classes.logo1}
                />
              </Link>
            </div>
          </div>
          <div className={classes.login}>
            <p className={classes.plogin}>
              Đăng nhập
            </p>
          </div>
          <div className={classes.typo}>
            <Danger>
              {!loading && error ? "Sai địa chỉ Email hoặc mật khẩu!" : <br />}
            </Danger>
          </div>
          {/* <Grid container style={{ padding: "1em" }}>
            <Grid style={{alignSelf:"center"}} item xs={2} sm={2} lg={2}>
              <Button onClick={()=>{Cookies.remove("sv"); history.push("/detect")}}><ArrowBackIos></ArrowBackIos></Button>
            </Grid>
            <Grid item xs={10} sm={10} lg={10}>
              <FormControl
                style={{ marginTop: "1em" }}
                variant="outlined"
                fullWidth
              >
                <InputLabel>Chọn khu vực</InputLabel>
                <Select
                  label="Chọn khu vực"
                  name="server"
                  disabled
                  id="demo-simple-select-outlined"
                  value={choosen}
                >
                  <MenuItem value={"1"}>Quận Bình Tân</MenuItem>
                  <MenuItem value={"2"}>Quận 8</MenuItem>
                  <MenuItem value={"3"}>Quận 10</MenuItem>
                  <MenuItem value={"4"}>Khác</MenuItem>
                </Select>
              </FormControl>
            </Grid>
          </Grid> */}
            <form
              onSubmit={handleSubmit(onSubmit)}
              className={classes.form}
              style={{ padding: "0 1em 0 1em" }}
            >
              <TextField
                variant="outlined"
                error={errMail}
                helperText={errMail && "Email sai định dạng."}
                margin="normal"
                fullWidth
                id="email"
                {...register("email")}
                label="Số điện thoại"
                autoComplete="email"
                disabled={loading}
                autoFocus
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
              <TextField
                variant="outlined"
                margin="normal"
                error={errPassword}
                helperText={errPassword && "Vui lòng nhập mật khẩu."}
                fullWidth
                {...register("password")}
                label="Mật khẩu"
                type="password"
                id="password"
                disabled={loading}
                onInput={(e) => {
                  if (e.target.value.length > 0) setErrPassword(false);
                  else setErrPassword(true);
                }}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                className={classes.submit}
                disabled={loading || errMail || errPassword}
              >
                {loading && (
                  <CircularProgress
                    style={{
                      width: "1.6em",
                      height: "1.6em",
                      marginRight: "1em",
                    }}
                  />
                )}
                Đăng nhập
              </Button>
            </form>
          
        </div>

        <Box mb={2}>
          <div className={classes.subScriptPhone}>
            <p className={classes.psubPhone}>
              Một sản phẩm của <b>Công ty TNHH Dịch Vụ Giáo Dục & Y Tế MedX </b>{" "}
              <br />
              Với sự hợp tác phát triển từ{" "}
              <b>Đại học Y Dược Thành phố Hồ Chí Minh</b>
            </p>
          </div>
          <Copyright />
        </Box>
      </Grid>
    </Grid>
  );
}
