import React, { useEffect, useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import { useForm, Controller } from "react-hook-form";
import InputAdornment from "@material-ui/core/InputAdornment";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import blank_avt from "assets/img/blank_avt.png";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import getAuthorize from "constants/authen/authen";
import * as apis from "./store/api";
import * as actions from "./store/actions";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import ModalConfirm from "components/Modal/ModalConfirm";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0",
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
  },
  topBar: {
    background: "#50AF50 !important",
  },
  submit: {
    marginTop: "2em",
    backgroundColor: "#50AF50",
    color: "white",
    "&:hover": {
      backgroundColor: "#60BF50",
      boxShadow: "none",
    },
  },
  cancel: {
    marginTop: "2em",
    backgroundColor: "#F44336",
    color: "white",
    marginRight: "1em",
    "&:hover": {
      backgroundColor: "#F66646",
      boxShadow: "none",
      color: "white",
    },
  },
  backdrop: {
    zIndex: 9999,
    color: "#fff",
  },
  title: {
    textAlign: "center",
    marginTop: "1.2em",
    color: "#50AF50",
    fontSize: "1.5em",
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    "@media (max-width: 1024px)": {
      flexDirection: "column",
    },
  },
  label: {
    fontWeight: "bold",
  },
  value: {
    marginLeft: "0.2em",
  },
  info: {
    marginLeft: "3em",
    fontSize: "1.1em",
    "@media (max-width: 1024px)": {
      marginLeft: "1.8em",
    },
    "@media (max-width: 450px)": {
      marginLeft: "1.2em",
    },
  },
  formatStyle: {
    fontSize: "0.9rem",
    "& .MuiInputBase-root.Mui-disabled": {
      color: "rgba(0, 0, 0, 1)" // (default alpha is 0.38)
    }
  },
  form:{
    padding:"0.3em 3em 3em 3em",
    "@media (max-width: 850px)": {
      padding:"0.3em 2em 2em 2em",
    },
    "@media (max-width: 450px)": {
      padding: "1em !important",
    },
  },
  typo:{
    "@media (max-width: 450px)": {
      fontSize:"1.2em !important"
    },
  }
};

const useStyles = makeStyles(styles);
const UPDATE_INFO = apis.updateInfo;
export default function UserProfile() {
  const [updateInfo, resEdit] = useMutation(UPDATE_INFO, {
    errorPolicy: "all",
  });
  const dispatch = useDispatch();
  const data = getAuthorize();
  const { dataUserAuth, isLoading, isSuccess, message } = useSelector(
    (state) => state["Login"]
  );
  const [selectedGender, setSelectedGender] = useState("");
  const [dataUser, setDataUser] = useState(null);
  const { register, handleSubmit, setValue, control } = useForm();
  const [openNoti, setOpenNoti] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [dataOk, setDataOk] = useState(null);
  const [err1, setErr1] = useState(false);
  const [err2, setErr2] = useState(false);
  const [err3, setErr3] = useState(false);
  const [err4, setErr4] = useState(false);
  const [errorPhone, setErrorPhone] = useState(false);

  const [modalCancel, setModalCancel] = useState(false);
  const [ipMail, setIpMail] = useState("");
  const [ipFirstname, setIpFirstname] = useState("");
  const [ipLastname, setIpLastname] = useState("");
  const [ipDob, setIpDob] = useState("");

  const onSubmitForm = (dataForm) => {
    setDataOk(dataForm);
    setIsModalOpen(true);
  };
  const classes = useStyles();
  const [isEdit, setIsEdit] = useState(false);

  useEffect(() => {
    if (dataUserAuth) {
      setDataUser(dataUserAuth);
      setSelectedGender(dataUserAuth.gender);
      setValue("firstname", dataUserAuth.firstname);
      setValue("lastname", dataUserAuth.lastname);
      setValue("email", dataUserAuth.email);
      setValue("gender", dataUserAuth.gender);
      setValue("dob", dataUserAuth.dob);
      setValue("phone", dataUserAuth?.phone?.replace("(+84)", "").trim());
    }
  }, dataUserAuth);

  const cancelEdit = () => {
    setDataUser(dataUserAuth);
    setSelectedGender(dataUserAuth.gender);
    setValue("firstname", dataUserAuth.firstname);
    setValue("lastname", dataUserAuth.lastname);
    setValue("email", dataUserAuth.email);
    // setValue("gender", dataUserAuth.gender);
    setValue("dob", dataUser.dob);
    setValue("phone", dataUserAuth?.phone?.replace("(+84)", "").trim());
    
    setErr1(false);
    setErr2(false);
    setErr3(false);
    setErr4(false);
    setIsEdit(false);
  };
  return (
    <div>
      <GridContainer>
        <Backdrop className={classes.backdrop} open={isLoading}>
          <CircularProgress color="inherit" />
        </Backdrop>
        <GridItem xs={12} sm={12} md={5}>
          <Card profile>
            <CardAvatar profile>
              <a href="#pablo" onClick={(e) => e.preventDefault()}>
                <img src={blank_avt} alt="..." />
              </a>
            </CardAvatar>
            <Typography className={classes.title} gutterBottom>
              {`${dataUser?.lastname?.toUpperCase() || ""} ${
                dataUser?.firstname?.toUpperCase() || ""
              } `}
            </Typography>
            <div className={classes.info}>
              <div style={{ display: "flex" }}>
                <p className={classes.label}>Email:</p>
                <p className={classes.value}>{dataUser?.email || ""}</p>
              </div>
              <div style={{ display: "flex" }}>
                <p className={classes.label}>Điện thoại:</p>
                <p className={classes.value}>{dataUser?.phone || ""}</p>
              </div>
              <div style={{ display: "flex" }}>
                <p className={classes.label}>Ngày sinh:</p>
                <p className={classes.value}>
                  {moment(dataUser?.dob).format("DD/MM/YYYY") || ""}
                </p>
              </div>
              <div style={{ display: "flex" }}>
                <p className={classes.label}>Giới tính:</p>
                <p className={classes.value}>
                  {dataUser?.gender === "male"
                    ? "Nam"
                    : dataUser?.gender === "female"
                    ? "Nữ"
                    : "Không xác định" || ""}
                </p>
              </div>
            </div>
            <CardBody profile>
              <Button
                onClick={() => {
                  setIsEdit(true);
                  setIpMail();
                }}
                style={{ background: "#50AF50" }}
                round
              >
                CẬP NHẬT
              </Button>
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={7}>
          <Card>
            <CardHeader className={classes.topBar}>
              <h4 className={classes.cardTitleWhite}>Thông tin cá nhân</h4>
            </CardHeader>
            <form
              className={classes.form}
              noValidate
              style={{
                borderRadius: "0.4em",
              }}
              onSubmit={handleSubmit(onSubmitForm)}
            >
              <Grid style={{ marginTop: "1em" }} container spacing={2}>
                <Grid item xs={12} sm={8} md={8}>
                  <Controller
                    control={control}
                    name="email"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        required
                        className={classes.formatStyle}
                        {...register("email")}
                        label="Email"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        onInput={(e) => {
                          const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                          if (re.test(String(e.target.value).toLowerCase()))
                            setErr1(false);
                          else setErr1(true);
                          setIpMail(e.target.value);
                        }}
                        helperText={err1 && "Email không đúng định dạng"}
                        error={err1}
                        disabled={!isEdit}
                        id="email"
                        onChange={(e) => setValue("email", e.target.value)}
                        value={value}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={4} md={4}>
                  <Controller
                    control={control}
                    name="gender"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <FormControl
                        style={{ marginTop: "1em" }}
                        variant="outlined"
                        fullWidth
                          className={classes.formatStyle}
                          required
                        // className={classes.formControl}
                      >
                        <InputLabel>Giới tính</InputLabel>
                        <Select
                          disabled={!isEdit}
                          id="demo-simple-select-outlined"
                          label="Giới tính"
                          className={classes.formatStyle}
                          {...register("gender")}
                          value={selectedGender}
                          onChange={(e) => {
                            // setValue("gender", e.target.value);
                            setSelectedGender(e.target.value);
                          }}
                        >
                          <MenuItem value={"male"}>Nam</MenuItem>
                          <MenuItem value={"female"}>Nữ</MenuItem>
                          <MenuItem value={"other"}>Không xác định</MenuItem>
                        </Select>
                      </FormControl>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="lastname"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        disabled={!isEdit}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        required
                        className={classes.formatStyle}
                        {...register("lastname")}
                        label="Họ"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        error={err2}
                        onInput={(e) => {
                          if (e.target.value.length > 0) setErr2(false);
                          else setErr2(true);
                          setIpLastname(e.target.value);
                        }}
                        helperText={err2 && "Xin nhập họ"}
                        id="lastname"
                        onChange={(e) => setValue("lastname", e.target.value)}
                        value={value}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="firstname"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        disabled={!isEdit}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        className={classes.formatStyle}
                        required
                        {...register("firstname")}
                        label="Tên"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment
                              style={{ marginLeft: "-0.1em" }}
                              position="end"
                            ></InputAdornment>
                          ),
                        }}
                        error={err3}
                        onInput={(e) => {
                          if (e.target.value.length > 0) setErr3(false);
                          else setErr3(true);
                          setIpFirstname(e.target.value);
                        }}
                        helperText={err3 && "Xin nhập tên"}
                        id="firstname"
                        onChange={(e) => setValue("firstname", e.target.value)}
                        value={value}
                      />
                    )}
                  />
                </Grid>
              </Grid>
              <Grid style={{ marginTop: "1em" }} container spacing={2}>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="phone"
                    render={({ field: { value } }) => (
                      <TextField
                        error={errorPhone}
                        variant="outlined"
                        margin="normal"
                        required
                        className={classes.formatStyle}
                        fullWidth
                        disabled={!isEdit}
                        {...register("phone")}
                        label="Số điện thoại"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              ( +84 )
                            </InputAdornment>
                          ),
                        }}
                        type="number"
                        onInput={(e) => {
                          e.target.value = Math.max(0, parseInt(e.target.value))
                            .toString()
                            .slice(0, 20);
                          if (e.target.value.length < 9) setErrorPhone(true);
                          else setErrorPhone(false);
                          setValue("phone", e.target.value);
                        }}
                        id="phone"
                        helperText={
                          errorPhone
                            ? "Số điện thoại phải có ít nhất 9 chữ số."
                            : ""
                        }
                        value={value}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <Controller
                    control={control}
                    name="dob"
                    {...register("dob")}
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          disableToolbar
                          disabled={!isEdit}
                          fullWidth
                          required
                          className={classes.formatStyle}
                          variant="inline"
                          format="dd-MM-yyyy"
                          {...register("dob")}
                          margin="normal"
                          id="date-picker-inline"
                          label="Ngày sinh"
                          value={value}
                          onChange={(e) => {
                            setValue("dob", e);
                            if (e) setErr4(false);
                            else setErr4(true);
                            setIpDob(e);
                          }}
                          error={err4}
                          helperText={err4 && "Xin nhập ngày sinh"}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    )}
                  />
                </Grid>
              </Grid>
              {isEdit && (
                <div style={{ textAlign: "end" }}>
                  <Button
                    variant="contained"
                    className={classes.cancel}
                    onClick={() => {

                      setModalCancel(true);
                    }}
                  >
                    HỦY
                  </Button>
                  <Button
                    type="submit"
                    variant="contained"
                    className={classes.submit}
                    disabled={err1 || err2 || err3 || err4}
                  >
                    LƯU
                  </Button>
                </div>
              )}
            </form>
          </Card>
        </GridItem>
      </GridContainer>
      <ModalConfirm
        title="Xác nhận thay đổi"
        open={isModalOpen}
        disableBtn={resEdit.loading}
        description="Bạn có muốn xác nhận thay đổi?"
        handleOk={() => {
          dispatch(actions.handleUpdateInfo());
          updateInfo({
            variables: {
              id: window.sessionStorage.getItem("id"),
              email: dataOk.email,
              phone: `(+84) ${dataOk.phone}`,
              gender: selectedGender,
              firstname: dataOk.firstname,
              lastname: dataOk.lastname,
              dob: moment.utc(dataOk.dob).format(),
            },
          })
            .then((res) => {
              dispatch(actions.handleUpdateInfoSuccess(res));
              setDataUser(res.data.update_info);
              setIsEdit(false);
              setIsModalOpen(false);
              setOpenNoti(true);
            })
            .catch((err) => {
              dispatch(actions.handleUpdateInfoFail(err));
              setIsModalOpen(false);
              setOpenNoti(true);
            });
        }}
        handleClose={() => {
          setIsModalOpen(false);
        }}
      ></ModalConfirm>
      <ModalConfirm
        title="Hủy thay đổi"
        open={modalCancel}
        description="Bạn có muốn xác nhận hủy thay đổi?"
        handleOk={() => {
          cancelEdit();
          setModalCancel(false);
        }}
        handleClose={() => {
          setModalCancel(false);
        }}
      ></ModalConfirm>
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={openNoti}
        autoHideDuration={3000}
        onClose={() => setOpenNoti(false)}
      >
        <Alert severity={isSuccess ? "success" : "error"}>{message}</Alert>
      </Snackbar>
    </div>
  );
}
