import React, { useEffect, useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import { Grid } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import FooterOutSide from "components/Footer/Footer-v1";
import dhyd from "assets/img/dhyd.png";
import logo from "assets/img/medx-logo.svg";
import ClearIcon from "@material-ui/icons/Clear";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      marginTop: "0",
      marginBottom: "0",
      fontSize: "1rem !important",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    fontSize: "1.5rem !important",
    "& small": {
      color: "#777",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  drawer: {
    width: "50vw",
  },
  backdrop: {
    zIndex: 4799,
    color: "#fff",
  },
  topBar: {
    background: "#50AF50 !important",
    display: "flex",
    justifyContent: "space-between",
  },
  searchBar: {
    background: "#ddd",
    borderRadius: "0.4em",
    border: "none !important",
    height: "fit-content",
    width: "-webkit-fill-available",
  },
  headerTable: {
    color: "#0A3A34",
    fontWeight: "bold",
    fontSize: "1rem",
  },
  bodyCell: {
    fontSize: "0.9rem",
    paddingLeft: "1em !important",
    "&:nth-child(1)": {
      paddingLeft: "0 !important",
    },
  },
  buttonAdd: {
    whiteSpace: "nowrap",
    background: "#50AF50",
    color: "#eee",
    height: "3em",
    padding: "0.2em 0.3em 0.2em 0.3em",
    marginRight: "0.5em",
    marginTop: "1em",
    marginLeft: "1em",
    "&:hover": {
      background: "#59996D",
    },
  },
  buttonGroup: {
    whiteSpace: "nowrap",
    background: "#37774B",
    color: "#eee",
    height: "3.9em",
    // padding: "0.2em 1em 0.2em 1em",
    marginRight: "0.5em",
    marginLeft: "1em",
  },
  buttonUp: {
    background: "#37774B",
    marginBottom: "0.1em",
    color: "#eee",
    padding: "0.2em 1em 0.2em 1em",
    marginRight: "1em",
    marginTop: "0.4em",
    marginLeft: "1em",
    "&:hover": {
      background: "#59996D",
    },
  },
  btnDel: {
    color: "red",
    minWidth: "auto",
  },
  btnEdit: {
    color: "#37774B",
    minWidth: "auto",
  },
  bigIndicator: {
    backgroundColor: "white",
  },
  tab: {
    minWidth: 100, // a number of your choice
    width: 120, // a number of your choice
  },
  hoverico: {
    whiteSpace: "nowrap",
    background: "#37774B",
    padding: "0",
    color: "#eee",
    // height: "3.9em",
    "&:hover": {
      background: "#59996D",
    },
  },
  hoverselect: {
    whiteSpace: "nowrap",
    background: "#37774B",
    paddingLeft: "1em",
    paddingRight: "0.6em",
    borderRight: "none !important",
    width: "4.3rem",
    color: "#eee",
    // height: "3.9em",
    "&:hover": {
      background: "#59996D",
    },
  },
  logoLink: {
    textTransform: "uppercase",
    padding: "5px 0",
    display: "block",
    fontSize: "18px",
    textAlign: "left",
    fontWeight: "400",
    lineHeight: "30px",
    textDecoration: "none",
    backgroundColor: "transparent",
  },
  img: {
    width: "45px",
    top: "22px",
    verticalAlign: "middle",
    border: "0",
  },
};

const useStyles = makeStyles(styles);

export default function NotFound() {
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();
  const { isOpenRemoteForm } = useSelector((state) => state["Remote"]);
  const dataPatients = [{ title: "Họ tên", content: "" }];
  const [isAccepted, setIsAccepted] = useState(false);
  const [modalAccept, setModalAccept] = useState(false);

  return (
    <div>
      <div className={classes.logo} style={{ display: "flex", padding: "1em" }}>
        <a
          rel="noopener noreferrer"
          href="https://www.medx.vn"
          className={classes.logoLink}
          target="_blank"
          style={{ display: "flex" }}
        >
          <div className={classes.logoImage}>
            <img src={logo} alt="logo" className={classes.img} />
          </div>{" "}
          <a
            rel="noopener noreferrer"
            href="https://www.medx.vn"
            target="_blank"
            style={{
              alignSelf: "center",
              color: "#50AF50",
              fontWeight: "bold",
              marginLeft: "0.7em",
            }}
          >
            MEDX
          </a>
        </a>
        <div style={{ width: "80%", textAlign: "end", alignSelf: "center" }}>
          <a
            rel="noopener noreferrer"
            href="https://ump.edu.vn"
            target="_blank"
          >
            <img src={dhyd} alt="logo" className={classes.img} />
          </a>
        </div>
      </div>
      <Grid container spacing={2} style={{ padding: "1em" }}>
        NOT FOUNDDDDDDDDDDD
      </Grid>

      <div style={{ position: "fixed", width: "100%" }}>
        <FooterOutSide></FooterOutSide>
      </div>
    </div>
  );
}
