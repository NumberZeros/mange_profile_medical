import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import * as actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
import { useForm, Controller } from "react-hook-form";
import InputAdornment from "@material-ui/core/InputAdornment";
import moment from "moment";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
// import { name } from "./store/reducer";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  DateTimePicker,
} from "@material-ui/pickers";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import PropTypes from "prop-types";
import FormControl from "@material-ui/core/FormControl";
import ClearIcon from "@material-ui/icons/Clear";
import ModalConfirm from "components/Modal/ModalConfirm";
import { DataGrid } from "@material-ui/data-grid";
import { Line } from "react-chartjs-2";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import PermContactCalendarIcon from "@material-ui/icons/PermContactCalendar";
import { Modal } from "@material-ui/core";
import * as apis from "./store/api";
import { useMutation } from "@apollo/client";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#50AF50",
    color: "white",
  },
  cancel: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#F44336",
    color: "white",
    marginRight: "1em",
  },
  checkboxLable: {
    color: "#444",
    width: "100%",
    justifyContent: "space-between",
    margin: "0 !important",
    "&:hover": {
      background: "#a2e0ad",
    },
  },
  nextBtn: {
    color: "#fff",
    background: "#37774B",
    border: "none",
    borderRadius: "0.3em",
    "&:hover": {
      background: "#6A995B",
    },
  },
  titleTab: {
    fontSize: "1.1em",
    fontWeight: "400",
  },
  reportBtn: {
    marginTop: "0.3em",
    color: "#fff",
    background: "#37774B",
    border: "none",
    borderRadius: "0.3em",
    "&:hover": {
      background: "#6A995B",
    },
  },
  backdrop: {
    zIndex: 4799,
    color: "#fff",
  },
  checkboxes: {
    alignSelf: "start",
    marginLeft: "0.2em",
    minWidth: "80vw",
    justifyContent: "space-between",
    marginTop: "0.2em",
    "&:hover": {
      background: "#b6e5b0",
    },
    "@media (min-width: 1024px)": {
      minWidth: "61vw",
      maxWidth: "66vw",
    },
  },
  formatStyle: {
    fontSize: "0.9rem",
    "& .MuiInputBase-root.Mui-disabled": {
      color: "rgba(0, 0, 0, 1)", // (default alpha is 0.38)
    },
  },
}));
const CHANGE_STATUS = apis.changeStatusPatient;
const GET_STATUS = apis.findAllStatusType;

export default function Summary(props) {
  const classes = useStyles();
  const { register, handleSubmit, setValue, control } = useForm();
  const dispatch = useDispatch();
  const [pcrDate, setPcrdate] = useState(moment().utc().format());
  const [openModal, setOpenModal] = useState(false);

  const [selectedStatus, setSelectedStatus] = useState("");
  const [otherReason, setOtherReason] = useState("");
  const [openModalStatus, setOpenModalStatus] = useState(false);
  const [isOtherReason, setIsOtherReason] = useState(false);
  const [findStatus, resStatus] = useMyLazyQuery(GET_STATUS, {
    onCompleted: () => {
      if (resStatus.data) {
        dispatch(actions.handleFindStatusSuccess(resStatus.data));
      } else dispatch(actions.handleFindStatusFail());
      // dataDoctor.refetch();
    },
    fetchPolicy: "network-only",
  });
  const [changeStatus, resChangeStatus] = useMutation(CHANGE_STATUS, {
    errorPolicy: "all",
  });
  const { dataUserAuth } = useSelector((state) => state["Login"]);
  const { isOpenComplete, statusTypes } = useSelector(
    (state) => state["Dashboard"]
  );

  const [blood1, setBlood1] = useState(
    JSON.parse(props.data.summary_patients.dataTableBloodpressure)
  );
  const [blood2, setBlood2] = useState(
    JSON.parse(props.data.summary_patients.dataTableBloodpressure2)
  );
  const [tablePluse, setTablePluse] = useState(
    JSON.parse(props.data.summary_patients.dataTablePluse)
  );
  const [resultTest, setResultTest] = useState(
    JSON.parse(props.data.summary_patients.dataTableResultTest)
  );
  const [spo2, setSpo2] = useState(
    JSON.parse(props.data.summary_patients.dataTableSpo2home)
  );
  const [statsQuo, setStatusQuo] = useState(
    JSON.parse(props.data.summary_patients.dataTableStatusQuo).map(
      (item, index) => {
        if (item.data) return { ...item, id: index };
        else return { ...item, data: "Không có dữ liệu", id: index };
      }
    )
  );
  const [tempature, setTempature] = useState(
    JSON.parse(props.data.summary_patients.dataTableTemperature)
  );
  const listLabel = JSON.parse(
    props.data.summary_patients.dataTableBloodpressure
  ).map((item) => {
    return Object.values(item)[0].replace("/2021", "");
  });

  useEffect(() => {
    dispatch(actions.handleFindStatus());
    findStatus({});
  }, [statusTypes]);

  useEffect(() => {
    setSelectedStatus(
      props.data.find_one_patient.status.name === ""
        ? ""
        : props.data.find_one_patient.status.id
    );
  }, []);

  return (
    <Container component="main" maxWidth="l" style={{ paddingTop: "1em" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div
          style={{
            fontSize: "1.4rem",
            padding: "0.4em 0 0 0.1em",
            fontWeight: "450",
          }}
        >
          TÓM TẮT BỆNH ÁN
        </div>
        <Button onClick={() => dispatch(actions.handleCloseSummary())}>
          <ClearIcon></ClearIcon>
        </Button>
      </div>
      <form noValidate className={classes.form}>
        <Accordion defaultExpanded>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <div
              className={classes.titleTab}
              style={{
                justifyContent: "space-between",
                width: "100%",
                display: "flex",
              }}
            >
              <span style={{ alignSelf: "center" }}>HÀNH CHÍNH</span>
              <Link
                target="_blank"
                href={`/admin/patients-management/${props?.data.find_one_patient.id}`}
              >
                <Button>
                  <PermContactCalendarIcon
                    style={{ color: "#1c8821" }}
                  ></PermContactCalendarIcon>
                </Button>
              </Link>
            </div>
          </AccordionSummary>
          <AccordionDetails
            style={{ display: "flex", flexDirection: "column" }}
          >
            <div className={classes.typo}></div>
            <div className={classes.body}>
              <div className={classes.body}>
                <Grid container spacing={2}>
                  {/* <Grid item xs={6} sm={6} md={6}>
                    <Controller
                      control={control}
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          margin="normal"
                          fullWidth
                          style={{ fontSize: "4rem" }}
                          className={classes.formatStyle}
                          label="Bác sĩ"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment
                                style={{ marginLeft: "-0.1em" }}
                                position="end"
                              ></InputAdornment>
                            ),
                          }}
                          disabled
                          value={`${props?.data.find_one_patient.doctor.user.lastname} ${props?.data.find_one_patient.doctor.user.firstname}`}
                        />
                      )}
                    />
                  </Grid> */}

                  <Grid item xs={6} sm={6} md={6}>
                    <Controller
                      control={control}
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          margin="normal"
                          disabled
                          className={classes.formatStyle}
                          fullWidth
                          label="Họ và tên lót"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment
                                style={{ marginLeft: "-0.1em" }}
                                position="end"
                              ></InputAdornment>
                            ),
                          }}
                          value={props?.data?.find_one_patient?.lastname}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <Controller
                      control={control}
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          margin="normal"
                          disabled
                          className={classes.formatStyle}
                          fullWidth
                          label="Tên"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment
                                style={{ marginLeft: "-0.1em" }}
                                position="end"
                              ></InputAdornment>
                            ),
                          }}
                          value={props?.data?.find_one_patient?.firstname}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <Controller
                      control={control}
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          margin="normal"
                          disabled
                          className={classes.formatStyle}
                          fullWidth
                          label="Mã bệnh nhân khác (Nếu có)"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment
                                style={{ marginLeft: "-0.1em" }}
                                position="end"
                              ></InputAdornment>
                            ),
                          }}
                          value={props?.data?.find_one_patient?.otherid}
                        />
                      )}
                    />
                  </Grid>

                  <Grid item xs={12} sm={6} md={6}>
                    <Controller
                      control={control}
                      render={({ field: { value } }) => (
                        <TextField
                          variant="outlined"
                          label="Giới tính"
                          margin="normal"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment
                                style={{ marginLeft: "-0.1em" }}
                                position="end"
                              ></InputAdornment>
                            ),
                          }}
                          className={classes.formatStyle}
                          fullWidth
                          disabled
                          value={
                            props?.data?.find_one_patient?.gender === "male"
                              ? "Nam"
                              : props?.data?.find_one_patient?.gender ===
                                "female"
                              ? "Nữ"
                              : "Không xác định"
                          }
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <Controller
                      control={control}
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardDatePicker
                            variant="inline"
                            format="dd-MM-yyyy"
                            margin="normal"
                            id="date-picker-inline"
                            className={classes.formatStyle}
                            fullWidth
                            label="Ngày sinh"
                            value={props?.data?.find_one_patient?.dob}
                            KeyboardButtonProps={{
                              "aria-label": "change date",
                            }}
                            disabled
                          />
                        </MuiPickersUtilsProvider>
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <Controller
                      control={control}
                      render={({ field: { value } }) => (
                        <TextField
                          variant="outlined"
                          label="Số điện thoại"
                          margin="normal"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                ( +84 )
                              </InputAdornment>
                            ),
                          }}
                          className={classes.formatStyle}
                          fullWidth
                          disabled
                          type="number"
                          value={props?.data?.find_one_patient?.phone
                            .replace("(+84)", "")
                            .trim()}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <Controller
                      control={control}
                      render={({ field: { value } }) => (
                        <TextField
                          variant="outlined"
                          required
                          className={classes.formatStyle}
                          label="Địa chỉ"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment
                                style={{ marginLeft: "-0.1em" }}
                                position="end"
                              ></InputAdornment>
                            ),
                          }}
                          fullWidth
                          disabled
                          value={
                            `${props?.data?.find_one_patient?.name ? `${props?.data?.find_one_patient?.name},`:""} ${props?.data?.find_one_patient?.ward ? `${props?.data?.find_one_patient?.ward},`:""} ${props?.data?.find_one_patient?.province ? `${props?.data?.find_one_patient?.province},`:""} ${props?.data?.find_one_patient?.city ? `${props?.data?.find_one_patient?.city}`:""}`
                          }
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <Controller
                      control={control}
                      render={({ field: { value } }) => (
                        <TextField
                          variant="outlined"
                          className={classes.formatStyle}
                          required
                          label="Số người ở chung nhà"
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                Người
                              </InputAdornment>
                            ),
                            startAdornment: (
                              <InputAdornment
                                style={{ marginLeft: "-0.1em" }}
                                position="end"
                              ></InputAdornment>
                            ),
                          }}
                          fullWidth
                          disabled
                          value={props?.data?.find_one_patient?.quantityfamily}
                          type="number"
                        />
                      )}
                    />
                  </Grid>
                </Grid>
              </div>
            </div>
          </AccordionDetails>
        </Accordion>
        <Accordion style={{ display: "flex", flexDirection: "column" }}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <div
              style={{ marginTop: "0.2em", marginBottom: "0.1em" }}
              className={classes.titleTab}
            >
              KẾT QUẢ XÉT NGHIỆM
            </div>
          </AccordionSummary>
          <AccordionDetails>
            {resultTest.map((item, index) => {
              return (
                item.data.length > 0 &&
                item.data.map((res, indexres) => {
                  return (
                    <Grid container spacing={2} key={indexres}>
                      <Grid item xs={12} sm={6} md={6}>
                        <Controller
                          control={control}
                          render={({
                            field: { onChange, onBlur, value, ref },
                          }) => (
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                              <DateTimePicker
                                variant="outlined"
                                format="dd-MM-yyyy HH:mm"
                                margin="normal"
                                id="date-picker-inline"
                                className={classes.formatStyle}
                                fullWidth
                                label="Thời gian xét nghiệm"
                                value={res.time}
                                KeyboardButtonProps={{
                                  "aria-label": "change date",
                                }}
                                disabled
                              />
                            </MuiPickersUtilsProvider>
                          )}
                        />
                      </Grid>
                      <Grid item xs={12} sm={6} md={6}>
                        <Controller
                          control={control}
                          render={({
                            field: { onChange, onBlur, value, ref },
                          }) => (
                            <FormControl
                              style={{ marginTop: "1em" }}
                              variant="outlined"
                              className={classes.formControl}
                              fullWidth
                            >
                              <InputLabel>Kết quả</InputLabel>
                              <Select
                                id="demo-simple-select-outlined"
                                className={classes.formatStyle}
                                label="Kết quả"
                                disabled
                                value={res.result}
                              >
                                <MenuItem value={"negative"}>Âm tính</MenuItem>
                                <MenuItem value={"positive"}>
                                  Dương tính
                                </MenuItem>
                                <MenuItem value={"other"}>Khác</MenuItem>
                              </Select>
                            </FormControl>
                          )}
                        />
                      </Grid>
                    </Grid>
                  );
                })
              );
            })}
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <div
              style={{ marginTop: "0.2em", marginBottom: "0.1em" }}
              className={classes.titleTab}
            >
              SINH HIỆU
            </div>
          </AccordionSummary>
          <AccordionDetails>
            <GridContainer>
              <Grid item xs={12} sm={12} md={6}>
                <Card chart>
                  <CardHeader>
                    <Line
                      height={100}
                      data={{
                        labels: listLabel,
                        datasets: [
                          {
                            label: "Huyết áp tâm thu",
                            data: blood1.map((item) => {
                              return Object.values(item)[1];
                            }),
                            backgroundColor: "#50AF50",
                            borderColor: "#50AF50",
                            borderWidth: 2,
                            barPercentage: 0.4,
                          },
                          {
                            label: "Huyết áp tâm trương",
                            data: blood2.map((item) => {
                              return Object.values(item)[1];
                            }),
                            backgroundColor: "#f44336",
                            borderColor: "#f44336",
                            borderWidth: 2,
                            barPercentage: 0.4,
                          },
                        ],
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                      options={{
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                    ></Line>
                  </CardHeader>
                  <CardBody>
                    <h4 className={classes.cardTitle}>Huyết áp tâm thu</h4>
                  </CardBody>
                </Card>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Card chart>
                  <CardHeader>
                    <Line
                      height={100}
                      data={{
                        labels: listLabel,
                        datasets: [
                          {
                            label: "Mạch",
                            data: tablePluse.map((item) => {
                              return Object.values(item)[1];
                            }),
                            backgroundColor: "#039be5",
                            borderColor: "#039be5",
                            borderWidth: 2,
                            barPercentage: 0.4,
                          },
                        ],
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                      options={{
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                    ></Line>
                  </CardHeader>
                  <CardBody>
                    <h4 className={classes.cardTitle}>Mạch</h4>
                  </CardBody>
                </Card>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Card chart>
                  <CardHeader>
                    <Line
                      height={100}
                      data={{
                        labels: listLabel,
                        datasets: [
                          {
                            label: "Nhiệt độ",
                            data: tempature.map((item) => {
                              return Object.values(item)[1];
                            }),
                            backgroundColor: "#f57c00",
                            borderColor: "#f57c00",
                            borderWidth: 2,
                            barPercentage: 0.4,
                          },
                        ],
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                      options={{
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                    ></Line>
                  </CardHeader>
                  <CardBody>
                    <h4 className={classes.cardTitle}>Nhiệt độ</h4>
                  </CardBody>
                </Card>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Card chart>
                  <CardHeader>
                    <Line
                      height={100}
                      data={{
                        labels: listLabel,
                        datasets: [
                          {
                            label: "Chỉ số SpO2",
                            data: spo2.map((item) => {
                              return Object.values(item)[1];
                            }),
                            backgroundColor: "#9c27b0",
                            borderColor: "#9c27b0",
                            borderWidth: 2,
                            barPercentage: 0.4,
                          },
                        ],
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                      options={{
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                    ></Line>
                  </CardHeader>
                  <CardBody>
                    <h4 className={classes.cardTitle}>Chỉ số SpO2</h4>
                  </CardBody>
                </Card>
              </Grid>
            </GridContainer>
          </AccordionDetails>
        </Accordion>
        <Accordion style={{ marginBottom: "1em" }}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <div
              className={classes.titleTab}
              style={{ margin: "0.2em 0 0.1em 0" }}
            >
              TÌNH TRẠNG BỆNH MÃN TÍNH
            </div>
          </AccordionSummary>
          <AccordionDetails
            style={{ display: "flex", flexDirection: "column" }}
          >
            <div
              style={{ height: "500px", marginTop: "1em", minHeight: "120px" }}
            >
              <DataGrid
                rows={statsQuo}
                columns={[
                  {
                    field: "date",
                    headerName: "Ngày",
                    flex: 0.5,
                    minWidth: 100,
                  },
                  {
                    flex: 1,
                    field: "data",
                    headerName: "Tình trạng",
                    minWidth: 140,
                  },
                ]}
                hideFooterRowCount
                hideFooterPagination
                checkboxSelection={false}
                disableSelectionOnClick
              />
            </div>
          </AccordionDetails>
        </Accordion>
      </form>
      {props.data.find_one_patient.status.name === "" && (
        <div style={{ textAlign: "end", marginBottom: "1em" }}>
          <Button onClick={() => dispatch(actions.handleOpenComplete())}>
            KẾT THÚC BỆNH ÁN
          </Button>
        </div>
      )}

      <Modal
        open={isOpenComplete}
        style={{ maxHeight: "20vh", maxWidth: "90vw" }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          style={{
            overflow: "auto",
            transform: "translate(30%, 50%)",
            maxHeight: "65vh",
            background: "#eee",
            width: "70%",
            padding: "2em",
          }}
          item
          xs={12}
          sm={12}
          md={12}
        >
          <div
            style={{
              fontSize: "1.4rem",
              fontWeight: "450",
            }}
          >
            KẾT THÚC BỆNH ÁN <hr></hr>
          </div>
          <div style={{ margin: "1em 0 1em 0" }}>
            Vui lòng chọn lý do kết thúc bệnh án.
          </div>
          <FormControl
            style={{ marginTop: "1em" }}
            variant="outlined"
            className={classes.formControl}
            fullWidth
          >
            <InputLabel>Chọn lý do</InputLabel>
            <Select
              id="demo-simple-select-outlined"
              className={classes.formatStyle}
              label="Chọn lý do "
              value={selectedStatus}
              onChange={(e, value) => {
                setSelectedStatus(e.target.value);
                setOtherReason("");
                if (value.props.children === "Khác") {
                  setIsOtherReason(true);
                } else setIsOtherReason(false);
              }}
            >
              {statusTypes
                ?.filter((item) => item.name !== "")
                .map((item) => {
                  return (
                    <MenuItem key={item.id} value={item.id}>
                      {item.name}
                    </MenuItem>
                  );
                })}
            </Select>
          </FormControl>
          <TextField
            multiline
            rows={2}
            maxRows={4}
            margin="normal"
            style={{ display: !isOtherReason && "none" }}
            variant="outlined"
            className={classes.formatStyle}
            required
            label="Lý do khác"
            value={otherReason}
            onChange={(e) => setOtherReason(e.target.value)}
            InputProps={{
              startAdornment: (
                <InputAdornment
                  style={{ marginLeft: "-0.1em" }}
                  position="end"
                ></InputAdornment>
              ),
            }}
            fullWidth
          />
          <div style={{ textAlign: "end" }}>
            <Button
              className={classes.cancel}
              onClick={() => dispatch(actions.handleCloseComplete())}
            >
              HỦY
            </Button>
            <Button
              disabled={isOtherReason && otherReason === ""}
              className={classes.submit}
              onClick={() => setOpenModalStatus(true)}
            >
              XÁC NHẬN
            </Button>
          </div>
        </Grid>
      </Modal>
      <ModalConfirm
        title="Xác nhận"
        open={openModalStatus}
        disableBtn={resChangeStatus?.loading}
        description="Xác nhận kết thúc hồ sơ bệnh nhân"
        handleOk={() => {
          dispatch(actions.handleChangePatientStatus());
          changeStatus({
            variables: {
              id: props?.data.find_one_patient.id,
              otherstatus: otherReason,
              status: selectedStatus,
              ownerid:
                dataUserAuth?.roles === "admin"
                  ? props?.data.find_one_patient.doctor.user.id
                  : window.sessionStorage.getItem("id"),
            },
          }).then((res) => {
            if (res.data !== null) {
              dispatch(
                actions.handleChangePatientStatusSuccess({
                  ...res.data,
                  patientId: props?.data.find_one_patient.id,
                })
              );
              setOpenModalStatus(false);
              dispatch(actions.handleCloseComplete());
              location.reload();
            } else dispatch(actions.handleChangePatientStatusFail());
          });
        }}
        handleClose={() => setOpenModalStatus(false)}
      ></ModalConfirm>
    </Container>
  );
}
