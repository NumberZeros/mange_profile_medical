import React, { useEffect, useState } from "react";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import DateRange from "@material-ui/icons/DateRange";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
// core components
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import "assets/css/material-dashboard-react.css?v=1.10.0";
import { useMutation } from "@apollo/client";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import * as actions from "views/Authenticate/store/actions";
import { useDispatch, useSelector } from "react-redux";
import PeopleAltIcon from "@material-ui/icons/PeopleAlt";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import WarningIcon from "@material-ui/icons/Warning";
import RemoveRedEyeIcon from "@material-ui/icons/RemoveRedEye";
import { Button } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import { DataGrid } from "@material-ui/data-grid";
import moment from "moment";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";
import { Modal } from "@material-ui/core";
import * as actionCharts from "./store/actions";
import { Line } from "react-chartjs-2";
import { Bar } from "react-chartjs-2";
import ClearIcon from "@material-ui/icons/Clear";
import { Pie } from "react-chartjs-2";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { findAllLessDoctors } from "views/DoctorsManagement/store/api";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Summary from "./Summary";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import { useForm, Controller } from "react-hook-form";
import PatientForm from "views/PatientsManagement/PatientForm";
import Drawer from "@material-ui/core/Drawer";
import { findAdvancePatients } from "views/PatientsManagement/store/api";
import PaginationCustom from "components/Pagination/Pagination";
import Board from "react-trello";
import GroupPatientForm from "./GroupPatientForm";
import ModalConfirm from "components/Modal/ModalConfirm";
import { AddCircleOutlineOutlined } from "@material-ui/icons";
import Tooltip from "@material-ui/core/Tooltip";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";

import * as apiChart from "./store/api";

const FETCH_CHART = apiChart.chart;
const FETCH_DOCTOR_CHART = apiChart.chartDoctor;
const SUMMARY = apiChart.summaryPateints;
const FETCH_PATIENT = apiChart.findAllPatienss;
const FETCH_ONE_MED = apiChart.findAllMedical;
const FIND_ONE = apiChart.findOnePatient;
const ADVANCE = apiChart.advanceSearch;
const DANGER = apiChart.chartDangerous;
const FIND_ALL_GROUP = apiChart.findAllPatientGroup;
const HANDLE_PATIENT = apiChart.handlePatientForGroup;
const MOVE_LANE = apiChart.moveLane;

const priorityGroups = {
  3: "#880e4f",
  4: "#6d4c41",
  5: "#7986cb",
  6: "#d500f9",
  7: "#3f51b5",
  8: "#50AF50",
  9: "#ff6d00",
  10: "#dd2c00",
};

export default function Dashboard() {
  const [pageSize, setPageSize] = React.useState(5);
  const [modalPatient, setModalPatient] = useState(false);

  const useStyles = makeStyles(styles);
  const dispatch = useDispatch();
  const classes = useStyles();
  const { isNotiChangePass, message, dataUserAuth } = useSelector(
    (state) => state["Login"]
  );
  const { register, handleSubmit, setValue, control } = useForm();

  const [listLabel, setListLabel] = useState(null);
  const [listContact, setListContact] = useState(null);
  const [listNewPatient, setListNewPatient] = useState(null);
  const [listNegative, setNegative] = useState(null);
  const [idDoc, setIdDoc] = useState("");
  const [listDocContact, setListDocContact] = useState(null);
  const [listDocNewPatient, setListDocNewPatient] = useState(null);
  const [listDocNegative, setDocNegative] = useState(null);
  const [listDoc, setListDoc] = useState(null);
  const [listPat, setListPat] = useState(null);
  const [nowTime, setNowTime] = useState(moment().utc().format());
  const [subValue, setSubValue] = useState(0);
  const [subValueDoc, setSubValueDoc] = useState(0);
  const [page, setPage] = useState(0);
  const [chartDangerMap, setChartDangerMap] = useState(null);
  const [chartDangerDoc, setChartDangerDoc] = useState(null);
  const [listGroups, setListGroups] = useState([]);
  const [openListPatient, setOpenListPatient] = useState(false);
  const [listSelectedPatient, setListSelectedPatient] = useState([]);
  const [confirmHandlePatient, setConfirmHandlePatient] = useState(false);
  const [selectedGroup, setSelectedGroup] = useState("");
  const [selectedLane, setSelectedLane] = useState("");
  const [moveTo, setMoveTo] = useState("");
  const [groupLength, setGroupLength] = useState(0);
  const [query, setQuery] = useState({
    firstname: "",
    lastname: "",
    dob: "",
    updatedat: "DESC",
    quantityfamily: "",
    phone: "",
    otherid: "",
  });

  const {
    chartList,
    listDoctors,
    listPatients,
    chartDocList,
    isOpenSummary,
    patient,
    summary,
    isOpenFormDashboard,
    messageDash,
    successAdd,
    isOpenNoti,
    chartDanger,
    chartDangerDoctor,
    isOpenPatientGroup,
    listPatientGroup,
    pagination,
    isLoadingMove,
  } = useSelector((state) => state["Dashboard"]);

  const [fetchChart, dataChart] = useMyLazyQuery(FETCH_CHART, {
    onCompleted: () => {
      if (dataChart.data) {
        dispatch(actionCharts.handleFindAllChartSuccess(dataChart.data));
      } else dispatch(actionCharts.handleFindAllChartFail());
    },
  });

  const [fetchGroups, dataGroups] = useMyLazyQuery(FIND_ALL_GROUP, {
    onCompleted: () => {
      if (dataGroups.data) {
        dispatch(
          actionCharts.handleFindAllPatientGroupSuccess(dataGroups.data)
        );
      } else dispatch(actionCharts.handleFindAllPatientGroupFail());
    },
  });

  const [fetchDangerChart, dataDangerChart] = useMyLazyQuery(DANGER, {
    onCompleted: () => {
      if (dataDangerChart.data) {
        dispatch(
          actionCharts.handleFetchChartDangerSuccess(dataDangerChart.data)
        );
      } else dispatch(actionCharts.handleFetchChartDangerFail());
    },
  });

  const [fetchDangerChartDoctor, dataDangerChartDoctor] = useMyLazyQuery(
    DANGER,
    {
      onCompleted: () => {
        if (dataDangerChartDoctor.data) {
          dispatch(
            actionCharts.handleFetchChartDangerDoctorSuccess(
              dataDangerChartDoctor.data
            )
          );
        } else dispatch(actionCharts.handleFetchChartDangerDoctorFail());
      },
    }
  );

  const [fetchSummary, dataSummary] = useMyLazyQuery(SUMMARY, {
    onCompleted: () => {
      if (dataSummary.data) {
        dispatch(actionCharts.handleFindSummarySuccess(dataSummary.data));
      } else dispatch(actionCharts.handleFindSummaryFail());
    },
  });

  const [findOneMed, dataOneMed] = useMyLazyQuery(FETCH_ONE_MED, {
    onCompleted: () => {
      if (dataOneMed.data) {
        dispatch(actionCharts.handleFindSummaryMedSuccess(dataOneMed.data));
      } else dispatch(actionCharts.handleFindSummaryMedFail());
    },
  });

  const [findOne, dataOne] = useMyLazyQuery(FIND_ONE, {
    onCompleted: () => {
      if (dataOne.data) {
        dispatch(actionCharts.handleFindSummaryPatientSuccess(dataOne.data));
      } else dispatch(actionCharts.handleFindSummaryPatientFail());
    },
  });

  const [findAllDoc, dataDoc] = useMyLazyQuery(findAllLessDoctors, {
    onCompleted: () => {
      if (dataDoc) {
        dispatch(actionCharts.handleFindDashboardDoctorsSuccess(dataDoc));
      } else dispatch(actionCharts.handleFindDashboardDoctorsFail());
    },
  });

  const [findAllPat, dataPat] = useMyLazyQuery(FETCH_PATIENT, {
    onCompleted: () => {
      if (dataPat) {
        dispatch(actionCharts.handleFindDashboardPatientsSuccess(dataPat));
      } else dispatch(actionCharts.handleFindDashboardPatientsFail());
    },
  });

  const [findDocChart, dataDocChart] = useMyLazyQuery(FETCH_DOCTOR_CHART, {
    onCompleted: () => {
      if (dataDocChart) {
        dispatch(actionCharts.handleFindAllDocChartSuccess(dataDocChart));
      } else dispatch(actionCharts.handleFindAllDocChartFail());
    },
  });

  const [advancePatients, dataAdvance] = useMyLazyQuery(findAdvancePatients, {
    onCompleted: () => {
      if (dataAdvance) {
        dispatch(
          actionCharts.handleFindDashboardPatientsSuccess(dataAdvance.data)
        );
      } else dispatch(actionCharts.handleFindDashboardPatientsFail());
      // dataDoctor.refetch();
    },
  });

  const [advance, resAdvance] = useMyLazyQuery(ADVANCE, {
    onCompleted: () => {
      console.log(resAdvance);
    },
  });

  const onAdvanceSearch = (dataForm) => {
    setQuery({
      ...query,
      adminid: "",
      doctorid:
        dataUserAuth?.roles === "admin"
          ? idDoc
          : window.sessionStorage.getItem("id"),
      lastname: dataForm.lastname || "",
      firstname: dataForm.firstname || "",
      phone: dataForm.phone || "",
      otherid: dataForm.otherid || "",
      dob: dataForm.dob || "",
    });
  };

  const [handlePatient, resHandlePatient] = useMutation(HANDLE_PATIENT, {
    errorPolicy: "all",
  });

  const [moveGroup, resMoveGroup] = useMutation(MOVE_LANE, {
    errorPolicy: "all",
  });

  const onHandlePatient = () => {
    console.log(listSelectedPatient);
    handlePatient({
      variables: {
        id: selectedGroup,
        adminid:
          dataUserAuth?.roles === "admin"
            ? window.sessionStorage.getItem("id")
            : "",
        doctorid:
          dataUserAuth?.roles === "admin"
            ? idDoc
            : window.sessionStorage.getItem("id"),

        listPatient: listSelectedPatient,
        isRemove: false,
      },
    }).then((res) => {
      if (res.data === null || res.errors) {
        dispatch(actionCharts.handlePatientForGroupFail(res.errors[0].message));
        // dispatch(actions.handleCloseGroupNoti);
      } else {
        dispatch(actionCharts.handlePatientForGroupSuccess(res));
        setOpenListPatient(false);
        setConfirmHandlePatient(false);
        // showNoti();
      }
    });
  };

  const onHandleRemovePatient = (value) => {
    handlePatient({
      variables: {
        id: value.groupid,
        adminid:
          dataUserAuth?.roles === "admin"
            ? window.sessionStorage.getItem("id")
            : "",
        doctorid:
          dataUserAuth?.roles === "admin"
            ? idDoc
            : window.sessionStorage.getItem("id"),

        listPatient: [value.patientid],
        isRemove: true,
      },
    }).then((res) => {
      if (res.data === null || res.errors) {
        dispatch(actionCharts.handlePatientForGroupFail(res.errors[0].message));
        // dispatch(actions.handleCloseGroupNoti);
      } else {
        dispatch(actionCharts.handlePatientForGroupSuccess(res));
        setOpenListPatient(false);
        setConfirmHandlePatient(false);
        // showNoti();
      }
    });
  };

  useEffect(() => {
    advance();
  }, []);

  useEffect(() => {
    if (dataUserAuth && dataUserAuth?.roles !== "admin") {
      // dispatch(actionCharts.handleFindDashboardPatients());
      dispatch(actionCharts.handleFindAllDocChart());
      // advancePatients({
      //   adminid: "",
      //   doctorid: window.sessionStorage.getItem("id"),
      //   take: 5,
      //   skip: 0,
      //   firstname: "",
      //   lastname: "",
      //   phone: "",
      //   otherid: "",
      //   dob: "",
      //   updatedat: "",
      // });
      findDocChart({
        doctorid: window.sessionStorage.getItem("id"),
        now: moment().subtract(subValueDoc, "days").utc().format(),
      });
    }
  }, [dataUserAuth, successAdd]);

  useEffect(() => {
    dispatch(actionCharts.handleFindAllChart());
    fetchChart({
      adminid: window.sessionStorage.getItem("id"),
      now: moment().subtract(subValue, "days").utc().format(),
    });
    const dataChartContact =
      chartList?.analysis_patients?.dataTablePatientContacDescription;
    const dataChartNew = chartList?.analysis_patients?.dataTablePatientCreated;
    const dataChartNegative =
      chartList?.analysis_patients?.dataTablePatientNegative;
    if (dataChartContact) {
      setListLabel(
        JSON.parse(dataChartContact).map((item) => {
          return Object.values(item)[0].replace("/2021", "");
        })
      );
      setListContact(
        JSON.parse(dataChartContact).map((item) => {
          return Object.values(item)[1];
        })
      );
    }
    if (dataChartContact) {
      setListNewPatient(
        JSON.parse(dataChartNew).map((item) => {
          return Object.values(item)[1];
        })
      );
    }
    if (dataChartNegative) {
      setNegative(
        JSON.parse(dataChartNegative).map((item) => {
          return Object.values(item)[1];
        })
      );
    }
  }, [chartList, subValue]);

  useEffect(() => {
    if (chartDanger) {
      setChartDangerMap(Object.values(chartDanger));
    }
  }, [chartDanger]);
  useEffect(() => {
    if (chartDangerDoctor) {
      setChartDangerDoc(Object.values(chartDangerDoctor));
    }
  }, [chartDangerDoctor]);

  useEffect(() => {
    const dataChartDocContact =
      chartDocList?.analysis_patients_for_doctor
        ?.dataTablePatientContacDescription;
    const dataChartDocNew =
      chartDocList?.analysis_patients_for_doctor?.dataTablePatientCreated;
    const dataChartDocNegative =
      chartDocList?.analysis_patients_for_doctor?.dataTablePatientNegative;
    if (dataChartDocContact) {
      setListLabel(
        JSON.parse(dataChartDocContact).map((item) => {
          return Object.values(item)[0].replace("/2021", "");
        })
      );

      setListDocContact(
        JSON.parse(dataChartDocContact).map((item) => {
          return Object.values(item)[1];
        })
      );
    }
    if (dataChartDocContact) {
      setListDocNewPatient(
        JSON.parse(dataChartDocNew).map((item) => {
          return Object.values(item)[1];
        })
      );
    }
    if (dataChartDocNegative) {
      setDocNegative(
        JSON.parse(dataChartDocNegative).map((item) => {
          return Object.values(item)[1];
        })
      );
    }
  }, [chartDocList]);

  useEffect(() => {
    if (dataUserAuth) {
      if (dataUserAuth?.roles === "admin") {
        dispatch(actionCharts.handleFindDashboardDoctors());
        findAllDoc({ adminid: dataUserAuth?.id });
        dispatch(actionCharts.handleFetchChartDanger());
        fetchDangerChart({
          doctorid: "",
          adminid: window.sessionStorage.getItem("id"),
        });
      } else {
        dispatch(actionCharts.handleFetchChartDangerDoctor());
        fetchDangerChartDoctor({
          doctorid: window.sessionStorage.getItem("id"),
          adminid: "",
        });
        dispatch(actionCharts.handleFindAllPatientGroup());
        fetchGroups({
          doctorId: window.sessionStorage.getItem("id"),
        });
      }
    }
  }, [dataUserAuth]);

  const handleChangePage = (e) => {
    setPage(e);
  };

  const handleChangeSize = (e) => {
    setPageSize(e);
  };

  useEffect(() => {
    advancePatients({
      adminid: "",
      doctorid:
        dataUserAuth?.roles === "admin"
          ? idDoc
          : window.sessionStorage.getItem("id"),
      take: pageSize,
      skip: 0,
      firstname: query.firstname,
      lastname: query.lastname,
      phone: query.phone,
      otherid: query.otherid,
      dob: query.dob,
      quantityfamily: parseFloat(query.quantityfamily),
      updatedat: query.updatedat,
    });
    setPage(0);
  }, [pageSize]);

  useEffect(() => {
    advancePatients({
      adminid: "",
      doctorid:
        dataUserAuth?.roles === "admin"
          ? idDoc
          : window.sessionStorage.getItem("id"),
      take: pageSize,
      skip: page,
      firstname: query.firstname,
      lastname: query.lastname,
      phone: query.phone,
      otherid: query.otherid,
      dob: query.dob,
      updatedat: "DESC",
    });
  }, [page]);

  useEffect(() => {
    advancePatients({
      adminid: "",
      doctorid:
        dataUserAuth?.roles === "admin"
          ? idDoc
          : window.sessionStorage.getItem("id"),
      take: pageSize,
      skip: 0,
      firstname: query.firstname,
      lastname: query.lastname,
      phone: query.phone,
      otherid: query.otherid,
      quantityfamily: parseFloat(query.quantityfamily),
      dob: query.dob,
      updatedat: "DESC",
    });
    setPage(0);
  }, [query]);

  // useEffect(() => {
  //   advancePatients({
  //     adminid:
  //       dataUserAuth?.roles === "admin"
  //         ? window.sessionStorage.getItem("id")
  //         : "",
  //     doctorid:
  //       dataUserAuth?.roles === "admin"
  //         ? idDoc
  //         : window.sessionStorage.getItem("id"),
  //     take: 5,
  //     skip: 0,
  //     firstname: "",
  //     lastname: "",
  //     phone: "",
  //     otherid: "",
  //     dob: "",
  //     updatedat: "",
  //     quantityfamily: parseFloat(0),
  //   });
  // }, []);
  useEffect(() => {
    if (idDoc) {
      dispatch(actionCharts.handleFindDashboardPatients());
      advancePatients({
        adminid: "",
        doctorid:
          dataUserAuth?.roles === "admin"
            ? idDoc
            : window.sessionStorage.getItem("id"),
        take: 5,
        skip: 0,
        firstname: "",
        lastname: "",
        phone: "",
        otherid: "",
        dob: "",
        updatedat: "DESC",
        quantityfamily: parseFloat(0),
      });
      dispatch(actionCharts.handleFindAllDocChart());
      findDocChart({
        doctorid: idDoc,
        now: moment().subtract(subValueDoc, "days").utc().format(),
      });
      dispatch(actionCharts.handleFetchChartDangerDoctor());
      fetchDangerChartDoctor({
        doctorid: idDoc,
        adminid:
          dataUserAuth?.roles === "admin"
            ? window.sessionStorage.getItem("id")
            : "",
      });
      dispatch(actionCharts.handleFindAllPatientGroup());
      fetchGroups({
        doctorid: idDoc,
      });
    }
  }, [idDoc]);

  const [propGroup, setPropGroup] = useState(null);

  const onOpenForm = async (data) => {
    await setPropGroup(data);
    console.log(data);
    dispatch(actionCharts.handleOpenPatientGroup());
  };

  const onMoveGroup = (s, e, p) => {
    dispatch(actionCharts.handleMoveLane());
    // console.log(groupLength - s - 1,groupLength - e - 1)
    moveGroup({
      variables: {
        id: p.id,
        doctorid: window.sessionStorage.getItem("id"),
        index: parseFloat(listPatientGroup[e].index),
      },
    })
      .then((res) => {
        if (res.data !== null) {
          dispatch(actionCharts.handleMoveLaneSuccess({ to: e, from: s }));
        } else if (res.data === null) {
          dispatch(actionCharts.handleMoveLaneFail());
        }
      })
      .catch((error) => {
        dispatch(actionCharts.handleMoveLaneFail(error));
      });
  };
  useEffect(() => {
    if (listPatientGroup !== []) {
      setGroupLength(listPatientGroup.length);
      setListGroups(
        listPatientGroup?.map((item) => {
          return {
            laneStyle:{
              height:"80vh",
              overflowY:"auto"
            },
            id: item.id,
            title: (
              <Tooltip title={item.name}>
                <div
                  style={{
                    padding: "10px 10px",
                    cursor: "pointer",
                    alignItems: "center",
                    backgroundColor: priorityGroups[item.priority],
                    borderRadius: "0.3em 0 0 0.3em",
                    color: "white",
                    width: "12.4em",
                    marginLeft: "-0.12em",
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    whiteSpace: "nowrap",
                  }}
                  className={classes.hoverBtn}
                  onClick={() => onOpenForm(item)}
                >
                  {item.name}
                </div>
              </Tooltip>
            ),
            label: (
              <Tooltip title="Thêm bệnh nhân vào nhóm">
                <Button
                  style={{
                    padding: "7px",
                    borderRadius: " 0 0.3em 0.3em 0",
                    backgroundColor: priorityGroups[item.priority],
                    marginRight: "-0.8em",
                  }}
                  onClick={() => {
                    setOpenListPatient(true);
                    setSelectedGroup(item.id);
                  }}
                  disabled={dataPat.loading}
                  className={classes.hoverBtn}
                >
                  <AddCircleOutlineOutlined
                    style={{
                      color: "#fff",
                    }}
                  ></AddCircleOutlineOutlined>
                </Button>
              </Tooltip>
            ),
            style: {
              flex: 1,
              height:"75vh"
            },
            cards: [
              ...item.patient.map((pat) => {
                return {
                  id: pat.id,
                  title: (
                    <Tooltip title={`${pat.lastname} ${pat.firstname}`}>
                      <span
                        style={{ cursor: "pointer" }}
                        onClick={() => {
                          dispatch(actionCharts.handleOpenSummary());
                          dispatch(actionCharts.handleFindSummary());
                          dispatch(actionCharts.handleFindSummaryPatient());
                          fetchSummary({
                            patientid: pat.id,
                            now: moment()
                              .subtract(subValueDoc, "days")
                              .format(),
                          });
                          findOne({ id: pat.id });
                        }}
                      >
                        {`${pat.lastname} ${pat.firstname}`}
                      </span>
                    </Tooltip>
                  ),
                  label: (
                    <Tooltip title={`${pat.lastname} ${pat.firstname}`}>
                      <div style={{ fontSize: "1.2em" }}>
                        {moment(pat.dob).format("DD/MM/YYYY")}
                      </div>
                    </Tooltip>
                  ),
                  description: (
                    <Tooltip title={`${pat.lastname} ${pat.firstname}`}>
                      <div>
                        <div>
                          SĐT: <b>{pat.phone}</b>
                        </div>
                        <div>
                          Giới tính:{" "}
                          <b>
                            {pat.gender === "male"
                              ? "Nam"
                              : pat.gender === "female"
                              ? "Nữ"
                              : "Không xác định"}
                          </b>
                        </div>
                        <div>
                          Địa chỉ:{" "}
                          <b>
                            {pat.ward ? `${pat.ward}, ` : ""}
                            {pat.province ? `${pat.province} ` : ""}
                          </b>
                        </div>
                        <div style={{ textAlign: "end" }}>
                          <Button
                            loading={resHandlePatient.loading}
                            style={{ fontSize: "0.9em" }}
                            onClick={() =>
                              onHandleRemovePatient({
                                groupid: item.id,
                                patientid: pat.id,
                              })
                            }
                          >
                            XÓA
                          </Button>
                        </div>
                      </div>
                    </Tooltip>
                  ),
                };
              }),
            ],
          };
        })
      );
    }
  }, [listPatientGroup]);

  const columns = [
    {
      field: "updatedat",
      headerName: "Ngày khám gần nhất",
      minWidth: 150,
      flex: 0.5,
      headerClassName: classes.headerTable,
    },
    {
      field: "name",
      headerName: "Họ và tên",
      minWidth: 200,
      flex: 0.5,
      headerClassName: classes.headerTable,
      // eslint-disable-next-line react/display-name
      renderCell: (params) => (
        <span
          style={{ cursor: "pointer" }}
          onClick={() => {
            dispatch(actionCharts.handleOpenSummary());
            dispatch(actionCharts.handleFindSummary());
            dispatch(actionCharts.handleFindSummaryPatient());
            fetchSummary({
              patientid: params.id,
              now: moment().subtract(subValueDoc, "days").format(),
            });
            findOne({ id: params.id });
          }}
        >
          {params.row.name}
        </span>
      ),
    },
    {
      field: "status",
      headerName: "Trạng thái",
      minWidth: 250,
      flex: 0.5,
      headerClassName: classes.headerTable,
    },
    {
      field: "gender",
      headerName: "Giới tính",
      width: 140,
      headerClassName: classes.headerTable,
    },
    {
      field: "dob",
      headerName: "Ngày sinh",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "phone",
      headerName: "Số điện thoại",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "address",
      headerName: "Địa chỉ",
      flex: 0.6,
      minWidth: 350,
      headerClassName: classes.headerTable,
    },
    {
      field: "actions",
      headerName: `${" "}`,
      width: 120,
      headerClassName: classes.headerTable,
      // eslint-disable-next-line react/display-name
      renderCell: (params) => (
        <div>
          <Button
            className={classes.btnEdit}
            onClick={() => {
              dispatch(actionCharts.handleOpenSummary());
              dispatch(actionCharts.handleFindSummary());
              dispatch(actionCharts.handleFindSummaryPatient());
              fetchSummary({
                patientid: params.id,
                now: moment().subtract(subValueDoc, "days").format(),
              });
              findOne({ id: params.id });
            }}
          >
            <RemoveRedEyeIcon></RemoveRedEyeIcon>
          </Button>
        </div>
      ),
    },
  ];

  useEffect(() => {
    setListDoc(
      listDoctors?.map((item) => {
        return {
          title: `${item.user.lastname} ${item.user.firstname}`,
          id: item.user.id,
        };
      })
    );
  }, [listDoctors]);

  useEffect(() => {
    setListPat(listPatients);
  }, [listPatients]);

  return (
    <div>
      <Backdrop
        style={{ zIndex: "4488" }}
        className={classes.backdrop}
        open={
          dataOne.loading ||
          dataSummary.loading ||
          dataChart.loading ||
          dataDoc.loading ||
          dataDocChart.loading ||
          resHandlePatient.loading ||
          isLoadingMove
        }
      >
        <CircularProgress color="inherit" />
      </Backdrop>

      {dataUserAuth?.roles === "admin" && (
        <Accordion defaultExpanded>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <GridItem xs={12} sm={12} md={12} style={{ margin: "1em 0 1em 0" }}>
              <div
                style={{
                  color: "#0A3A34",
                  fontWeight: "bold",
                  fontSize: "1.7em",
                }}
              >
                TỔNG QUAN HỆ THỐNG
              </div>
            </GridItem>
          </AccordionSummary>
          <AccordionDetails
            style={{ display: "flex", flexDirection: "column" }}
          >
            <GridContainer>
              <GridItem xs={12} sm={6} md={6} lg={3}>
                <Card>
                  <CardHeader color="warning" stats icon>
                    <CardIcon color="warning">
                      <PeopleAltIcon></PeopleAltIcon>
                    </CardIcon>
                    <p className={classes.cardCategory}>Tổng bệnh nhân</p>
                    <h3 className={classes.cardTitle}>
                      <b>{chartList?.analysis_patients?.totalPatient}</b>{" "}
                      <span className={classes.cardCategory}>Người</span>
                    </h3>
                  </CardHeader>
                  <CardFooter stats>
                    <div className={classes.stats}>
                      <DateRange />
                      Từ ngày 1/1/2021 đến nay
                    </div>
                  </CardFooter>
                </Card>
              </GridItem>
              <GridItem xs={12} sm={6} md={6} lg={3}>
                <Card>
                  <CardHeader color="success" stats icon>
                    <CardIcon color="success">
                      <PersonAddIcon></PersonAddIcon>
                    </CardIcon>
                    <p className={classes.cardCategory}>Bệnh nhân mới</p>
                    <h3 className={classes.cardTitle}>
                      <b>{chartList?.analysis_patients?.totalPatientNow}</b>{" "}
                      <span className={classes.cardCategory}>Người</span>
                    </h3>
                  </CardHeader>
                  <CardFooter stats>
                    <div className={classes.stats}>
                      <DateRange />
                      Từ ngày 1/1/2021 đến nay
                    </div>
                  </CardFooter>
                </Card>
              </GridItem>
              <GridItem xs={12} sm={6} md={6} lg={3}>
                <Card>
                  <CardHeader color="danger" stats icon>
                    <CardIcon color="danger">
                      <WarningIcon></WarningIcon>
                    </CardIcon>
                    <p className={classes.cardCategory}>Nguy cơ cao</p>
                    <h3 className={classes.cardTitle}>
                      <b>
                        {
                          chartList?.analysis_patients
                            ?.totalPatientRiskAssessment
                        }
                      </b>{" "}
                      <span className={classes.cardCategory}>Người</span>
                    </h3>
                  </CardHeader>
                  <CardFooter stats>
                    <div className={classes.stats}>
                      <DateRange />
                      Từ ngày 1/1/2021 đến nay
                    </div>
                  </CardFooter>
                </Card>
              </GridItem>
              {/* <GridItem xs={12} sm={6} md={6} lg={3}>
                <Card>
                  <CardHeader color="info" stats icon>
                    <CardIcon color="info">
                      <AddIcon style={{ fontSize: "40px" }} fontSize="large" />
                    </CardIcon>
                    <p className={classes.cardCategory}>Tử vong</p>
                    <h3 className={classes.cardTitle}>
                      <b>0</b>{" "}
                      <span className={classes.cardCategory}>Người</span>
                    </h3>
                  </CardHeader>
                  <CardFooter stats>
                    <div className={classes.stats}>
                      <DateRange />
                      Từ ngày 1/1/2021 đến nay
                    </div>
                  </CardFooter>
                </Card>
              </GridItem> */}
            </GridContainer>
            <GridContainer style={{ justifyContent: "flex-end" }}>
              <GridItem
                xs={12}
                sm={6}
                md={6}
                lg={4}
                style={{ display: "flex", justifyContent: "flex-end" }}
              >
                <Button onClick={() => setSubValue(subValue + 7)}>
                  <ArrowBackIosIcon></ArrowBackIosIcon>
                </Button>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    format="dd/MM/yyyy"
                    value={moment().subtract(subValue, "days").format()}
                    margin="normal"
                    // onChange={(e)=>set}
                    disabled
                    id="date-picker-inline"
                    label="Chọn mốc thời gian"
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                  />
                </MuiPickersUtilsProvider>
                <Button onClick={() => setSubValue(subValue - 7)}>
                  <ArrowForwardIosIcon></ArrowForwardIosIcon>
                </Button>
              </GridItem>
            </GridContainer>
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                <Card chart>
                  <CardHeader>
                    <Line
                      height={100}
                      data={{
                        labels: listLabel,
                        datasets: [
                          {
                            label: "Bệnh nhân mới",
                            data: listNewPatient,
                            backgroundColor: "#50AF50",
                            borderColor: "#50AF50",
                            borderWidth: 2,
                            barPercentage: 0.4,
                          },
                          {
                            label: "Bệnh nhân âm tính",
                            data: listNegative,
                            backgroundColor: "#1e88e5",
                            borderColor: "#1e88e5",
                            borderWidth: 2,
                            barPercentage: 0.4,
                          },
                          {
                            label: "Bệnh nhân cần hỗ trợ hiện trường",
                            data: listContact,
                            backgroundColor: "#fb8c00",
                            borderColor: "#fb8c00",
                            borderWidth: 2,
                            barPercentage: 0.4,
                          },
                          // {
                          //   label: "Bệnh nhân tử vong",
                          //   data: [0, 0, 0, 0, 0, 0, 0],
                          //   backgroundColor: "#ef5350",
                          //   borderColor: "#ef5350",
                          //   borderWidth: 2,
                          //   barPercentage: 0.4,
                          // },
                        ],
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                      options={{
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                    ></Line>
                  </CardHeader>
                  <CardBody>
                    <h4 className={classes.cardTitle}>Số bệnh nhân mới</h4>
                  </CardBody>
                  <CardFooter chart>
                    <div className={classes.stats}>
                      <DateRange /> Từ 14/08/2021 đến 21/08/2021
                    </div>
                  </CardFooter>
                </Card>
              </GridItem>
              <GridItem xs={12} sm={12} md={6}>
                <Card chart>
                  <CardHeader>
                    <Bar
                      height={100}
                      data={{
                        labels: listLabel,
                        datasets: [
                          {
                            label: "Bệnh nhân mới",
                            data: listNewPatient,
                            backgroundColor: "#50AF50",
                            borderColor: "#50AF50",
                            borderWidth: 2,
                            barPercentage: 0.4,
                          },
                        ],
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                      options={{
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                    ></Bar>
                  </CardHeader>
                  <CardBody>
                    <h4 className={classes.cardTitle}>Số bệnh nhân mới</h4>
                    <br />
                  </CardBody>
                  <CardFooter chart>
                    <div className={classes.stats}>
                      <DateRange /> Từ 14/08/2021 đến 21/08/2021
                    </div>
                  </CardFooter>
                </Card>
              </GridItem>
              <GridItem xs={12} sm={12} md={6}>
                <Card chart>
                  <CardHeader>
                    <Bar
                      height={100}
                      data={{
                        labels: listLabel,
                        datasets: [
                          {
                            label: "Bệnh nhân âm tính",
                            data: listNegative,
                            backgroundColor: "#1e88e5",
                            borderColor: "#1e88e5",
                            borderWidth: 2,
                            barPercentage: 0.4,
                          },
                        ],
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                      options={{
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                    ></Bar>
                  </CardHeader>
                  <CardBody>
                    <h4 className={classes.cardTitle}>Số bệnh nhân âm tính</h4>
                    <p className={classes.cardCategory}>
                      Xét nghiệm PCR/Test nhanh SARS-CoV-2
                    </p>
                  </CardBody>
                  <CardFooter chart>
                    <div className={classes.stats}>
                      <DateRange /> Từ 14/08/2021 đến 21/08/2021
                    </div>
                  </CardFooter>
                </Card>
              </GridItem>
              <GridItem xs={12} sm={12} md={6}>
                <Card chart>
                  <CardHeader>
                    <Bar
                      height={100}
                      data={{
                        labels: listLabel,
                        datasets: [
                          {
                            label: "Bệnh nhân cần hỗ trợ hiện trường",
                            data: listContact,
                            backgroundColor: "#fb8c00",
                            borderColor: "#fb8c00",
                            borderWidth: 2,
                            barPercentage: 0.4,
                          },
                        ],
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                      options={{
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                    ></Bar>
                  </CardHeader>
                  <CardBody>
                    <h4 className={classes.cardTitle}>
                      Số bệnh nhân cần hỗ trợ hiện trường
                    </h4>
                  </CardBody>
                  <CardFooter chart>
                    <div className={classes.stats}>
                      <DateRange /> Từ 14/08/2021 đến 21/08/2021
                    </div>
                  </CardFooter>
                </Card>
              </GridItem>
              <GridItem xs={0} sm={0} md={6}></GridItem>
              <GridItem xs={12} sm={12} md={6}>
                <Card chart>
                  <CardHeader>
                    <Pie
                      height={100}
                      data={{
                        labels: ["1-4 ngày", "5-10 ngày", "11-28 ngày"],
                        datasets: [
                          {
                            data: Object.values(chartDanger),
                            backgroundColor: ["#ffcd56", "#ef5350", "#50af50"],
                            borderColor: ["#ffcd56", "#ef5350", "#50af50"],
                            borderWidth: 2,
                            barPercentage: 0.4,
                          },
                        ],
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                      options={{
                        scales: {
                          xAxes: [
                            {
                              gridLines: {
                                borderColor: "rgba(0, 0, 0, 0)",
                              },
                            },
                          ],
                        },
                      }}
                    ></Pie>
                  </CardHeader>
                  <CardBody>
                    <h4 className={classes.cardTitle}>
                      Phân loại nhóm ngày bệnh
                    </h4>
                  </CardBody>
                  <CardFooter chart>
                    <div className={classes.stats}>
                      <DateRange /> Từ 1/1/2021 đến nay
                    </div>
                  </CardFooter>
                </Card>
              </GridItem>
            </GridContainer>
          </AccordionDetails>
        </Accordion>
      )}
      {dataUserAuth?.roles !== "admin" && (
        <div style={{ backgroundColor: "white", padding: "1em" }}>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div
              style={{
                color: "#0A3A34",
                fontWeight: "bold",
                fontSize: "1.7em",
                padding: "0.5em"
              }}
            >
              QUẢN LÝ NHÓM BỆNH NHÂN
            </div>
            <Button
              onClick={() => {
                setPropGroup(null);
                dispatch(actionCharts.handleOpenPatientGroup());
              }}
              className={classes.btnAddGroup}
            >
              Thêm nhóm
            </Button>
          </div>
          {listGroups && (
            <Tooltip title="">
              <Board
                cardDraggable={false}
                laneDraggable
                draggable
                // collapsibleLanes={true}
                hideCardDeleteIcon={true}
                style={{
                  backgroundColor: "#fff",
                  height: "fit-content",
                  minHeight: "8em",
                  maxHeight: "80vh",
                  overflow: "auto",
                }} // Style of BoardWrapper
                data={{ lanes: listGroups }}
                laneStyle={{ backgroundColor: "red" }}
                handleLaneDragEnd={(s, e, p) => {
                  onMoveGroup(s, e, p);
                }}
              />
            </Tooltip>
          )}
        </div>
      )}

      <Accordion defaultExpanded>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <GridItem xs={12} sm={12} md={12} style={{ margin: "1em 0 1em 0" }}>
            <div
              style={{
                color: "#0A3A34",
                fontWeight: "bold",
                fontSize: "1.7em",
              }}
            >
              TỔNG QUAN DỮ LIỆU NGƯỜI DÙNG
            </div>
          </GridItem>{" "}
        </AccordionSummary>
        <AccordionDetails style={{ display: "flex", flexDirection: "column" }}>
          {dataUserAuth?.roles === "admin" && (
            <Autocomplete
              className={classes.formatStyle}
              fullWidth
              margin="normal"
              label="Chọn bác sĩ"
              disabled={dataDoc.loading}
              id="name"
              options={listDoc}
              onChange={(event, value) => {
                console.log(event, value);
                setIdDoc(value !== null ? value.id : idDoc);
              }}
              getOptionLabel={(option) => option.title}
              renderInput={(params) => (
                <TextField {...params} label="Chọn bác sĩ" variant="outlined" />
              )}
            />
          )}
          <GridContainer>
            <GridItem xs={12} sm={6} md={6} lg={3}>
              <Card>
                <CardHeader color="warning" stats icon>
                  <CardIcon color="warning">
                    <PeopleAltIcon></PeopleAltIcon>
                  </CardIcon>
                  <p className={classes.cardCategory}>Tổng bệnh nhân</p>
                  <h3 className={classes.cardTitle}>
                    <b>
                      {chartDocList?.analysis_patients_for_doctor?.totalPatient}
                    </b>{" "}
                    <span className={classes.cardCategory}>Người</span>
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <div className={classes.stats}>
                    <DateRange />
                    Từ ngày 1/1/2021 đến nay
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={6} md={6} lg={3}>
              <Card>
                <CardHeader color="success" stats icon>
                  <CardIcon color="success">
                    <PersonAddIcon></PersonAddIcon>
                  </CardIcon>
                  <p className={classes.cardCategory}>Bệnh nhân mới</p>
                  <h3 className={classes.cardTitle}>
                    <b>
                      {
                        chartDocList?.analysis_patients_for_doctor
                          ?.totalPatientNow
                      }
                    </b>{" "}
                    <span className={classes.cardCategory}>Người</span>
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <div className={classes.stats}>
                    <DateRange />
                    Từ ngày 1/1/2021 đến nay
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={6} md={6} lg={3}>
              <Card>
                <CardHeader color="danger" stats icon>
                  <CardIcon color="danger">
                    <WarningIcon></WarningIcon>
                  </CardIcon>
                  <p className={classes.cardCategory}>Nguy cơ cao</p>
                  <h3 className={classes.cardTitle}>
                    <b>
                      {
                        chartDocList?.analysis_patients_for_doctor
                          ?.totalPatientRiskAssessment
                      }
                    </b>{" "}
                    <span className={classes.cardCategory}>Người</span>
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <div className={classes.stats}>
                    <DateRange />
                    Từ ngày 1/1/2021 đến nay
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            {/* <GridItem xs={12} sm={6} md={6} lg={3}>
              <Card>
                <CardHeader color="info" stats icon>
                  <CardIcon color="info">
                    <AddIcon style={{ fontSize: "40px" }} fontSize="large" />
                  </CardIcon>
                  <p className={classes.cardCategory}>Tử vong</p>
                  <h3 className={classes.cardTitle}>
                    <b>0</b> <span className={classes.cardCategory}>Người</span>
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <div className={classes.stats}>
                    <DateRange />
                    Từ ngày 1/1/2021 đến nay
                  </div>
                </CardFooter>
              </Card>
            </GridItem> */}
          </GridContainer>

          <GridContainer style={{ justifyContent: "flex-end" }}>
            <GridItem
              xs={12}
              sm={6}
              md={6}
              lg={4}
              style={{ display: "flex", justifyContent: "flex-end" }}
            >
              <Button
                onClick={() => {
                  if (dataUserAuth?.roles === "admin") {
                    dispatch(actionCharts.handleFindDashboardPatients());
                    dispatch(actionCharts.handleFindAllDocChart());
                    advancePatients({
                      adminid: "",
                      doctorid:
                        dataUserAuth?.roles === "admin"
                          ? idDoc
                          : window.sessionStorage.getItem("id"),
                      take: 5,
                      skip: 0,
                      firstname: "",
                      lastname: "",
                      phone: "",
                      otherid: "",
                      dob: "",
                      updatedat: "DESC",
                      quantityfamily: parseFloat(0),
                    });
                    findDocChart({
                      doctorid: idDoc,
                      now: moment()
                        .subtract(subValueDoc + 7, "days")
                        .utc()
                        .format(),
                    });
                  } else {
                    dispatch(actionCharts.handleFindDashboardPatients());
                    dispatch(actionCharts.handleFindAllDocChart());
                    advancePatients({
                      adminid: "",
                      doctorid:
                        dataUserAuth?.roles === "admin"
                          ? idDoc
                          : window.sessionStorage.getItem("id"),
                      take: 5,
                      skip: 0,
                      firstname: "",
                      lastname: "",
                      phone: "",
                      otherid: "",
                      dob: "",
                      updatedat: "DESC",
                      quantityfamily: parseFloat(0),
                    });
                    findDocChart({
                      doctorid: dataUserAuth.id,
                      now: moment()
                        .subtract(subValueDoc, "days")
                        .utc()
                        .format(),
                    });
                  }
                  setSubValueDoc(subValueDoc + 7);
                }}
              >
                <ArrowBackIosIcon></ArrowBackIosIcon>
              </Button>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  disableToolbar
                  variant="inline"
                  format="dd/MM/yyyy"
                  value={moment().subtract(subValueDoc, "days").format()}
                  margin="normal"
                  // onChange={(e)=>set}
                  disabled
                  id="date-picker-inline"
                  label="Chọn mốc thời gian"
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                />
              </MuiPickersUtilsProvider>
              <Button
                onClick={() => {
                  if (dataUserAuth?.roles === "admin") {
                    dispatch(actionCharts.handleFindDashboardPatients());
                    dispatch(actionCharts.handleFindAllDocChart());
                    advancePatients({
                      adminid: "",
                      doctorid:
                        dataUserAuth?.roles === "admin"
                          ? idDoc
                          : window.sessionStorage.getItem("id"),
                      take: 5,
                      skip: 0,
                      firstname: "",
                      lastname: "",
                      phone: "",
                      otherid: "",
                      dob: "",
                      updatedat: "DESC",
                      quantityfamily: parseFloat(0),
                    });
                    findDocChart({
                      doctorid: idDoc,
                      now: moment()
                        .subtract(subValueDoc - 7, "days")
                        .utc()
                        .format(),
                    });
                  } else {
                    dispatch(actionCharts.handleFindDashboardPatients());
                    dispatch(actionCharts.handleFindAllDocChart());
                    advancePatients({
                      adminid: "",
                      doctorid:
                        dataUserAuth?.roles === "admin"
                          ? idDoc
                          : window.sessionStorage.getItem("id"),
                      take: 5,
                      skip: 0,
                      firstname: "",
                      lastname: "",
                      phone: "",
                      otherid: "",
                      dob: "",
                      updatedat: "DESC",
                      quantityfamily: parseFloat(0),
                    });
                    findDocChart({
                      doctorid: dataUserAuth.id,
                      now: moment()
                        .subtract(subValueDoc, "days")
                        .utc()
                        .format(),
                    });
                  }
                  setSubValueDoc(subValueDoc - 7);
                }}
              >
                <ArrowForwardIosIcon></ArrowForwardIosIcon>
              </Button>
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <Card chart>
                <CardHeader>
                  <Line
                    height={100}
                    data={{
                      labels: listLabel,
                      datasets: [
                        {
                          label: "Bệnh nhân mới",
                          data: listDocNewPatient,
                          backgroundColor: "#50AF50",
                          borderColor: "#50AF50",
                          borderWidth: 2,
                          barPercentage: 0.4,
                        },
                        {
                          label: "Bệnh nhân âm tính",
                          data: listDocNegative,
                          backgroundColor: "#1e88e5",
                          borderColor: "#1e88e5",
                          borderWidth: 2,
                          barPercentage: 0.4,
                        },
                        {
                          label: "Bệnh nhân cần hỗ trợ hiện trường",
                          data: listDocContact,
                          backgroundColor: "#fb8c00",
                          borderColor: "#fb8c00",
                          borderWidth: 2,
                          barPercentage: 0.4,
                        },
                        // {
                        //   label: "Bệnh nhân tử vong",
                        //   data: [0, 0, 0, 0, 0, 0, 0],
                        //   backgroundColor: "#ef5350",
                        //   borderColor: "#ef5350",
                        //   borderWidth: 2,
                        //   barPercentage: 0.4,
                        // },
                      ],
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              borderColor: "rgba(0, 0, 0, 0)",
                            },
                          },
                        ],
                      },
                    }}
                    options={{
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              borderColor: "rgba(0, 0, 0, 0)",
                            },
                          },
                        ],
                      },
                    }}
                  ></Line>
                </CardHeader>
                <CardBody>
                  <h4 className={classes.cardTitle}>Số bệnh nhân mới</h4>
                </CardBody>
                <CardFooter chart>
                  <div className={classes.stats}>
                    <DateRange /> Từ 14/08/2021 đến 21/08/2021
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <Card chart>
                <CardHeader>
                  <Bar
                    height={100}
                    data={{
                      labels: listLabel,
                      datasets: [
                        {
                          label: "Bệnh nhân mới",
                          data: listDocNewPatient,
                          backgroundColor: "#50AF50",
                          borderColor: "#50AF50",
                          borderWidth: 2,
                          barPercentage: 0.4,
                        },
                      ],
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              borderColor: "rgba(0, 0, 0, 0)",
                            },
                          },
                        ],
                      },
                    }}
                    options={{
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              borderColor: "rgba(0, 0, 0, 0)",
                            },
                          },
                        ],
                      },
                    }}
                  ></Bar>
                </CardHeader>
                <CardBody>
                  <h4 className={classes.cardTitle}>Số bệnh nhân mới</h4>
                  <br />
                </CardBody>
                <CardFooter chart>
                  <div className={classes.stats}>
                    <DateRange /> Từ 14/08/2021 đến 21/08/2021
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <Card chart>
                <CardHeader>
                  <Bar
                    height={100}
                    data={{
                      labels: listLabel,
                      datasets: [
                        {
                          label: "Bệnh nhân âm tính",
                          data: listDocNegative,
                          backgroundColor: "#1e88e5",
                          borderColor: "#1e88e5",
                          borderWidth: 2,
                          barPercentage: 0.4,
                        },
                      ],
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              borderColor: "rgba(0, 0, 0, 0)",
                            },
                          },
                        ],
                      },
                    }}
                    options={{
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              borderColor: "rgba(0, 0, 0, 0)",
                            },
                          },
                        ],
                      },
                    }}
                  ></Bar>
                </CardHeader>
                <CardBody>
                  <h4 className={classes.cardTitle}>Số bệnh nhân âm tính</h4>
                  <p className={classes.cardCategory}>
                    Xét nghiệm PCR/Test nhanh SARS-CoV-2
                  </p>
                </CardBody>
                <CardFooter chart>
                  <div className={classes.stats}>
                    <DateRange /> Từ 14/08/2021 đến 21/08/2021
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <Card chart>
                <CardHeader>
                  <Bar
                    height={100}
                    data={{
                      labels: listLabel,
                      datasets: [
                        {
                          label: "Bệnh nhân cần hỗ trợ hiện trường",
                          data: listDocContact,
                          backgroundColor: "#fb8c00",
                          borderColor: "#fb8c00",
                          borderWidth: 2,
                          barPercentage: 0.4,
                        },
                      ],
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              borderColor: "rgba(0, 0, 0, 0)",
                            },
                          },
                        ],
                      },
                    }}
                    options={{
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              borderColor: "rgba(0, 0, 0, 0)",
                            },
                          },
                        ],
                      },
                    }}
                  ></Bar>
                </CardHeader>
                <CardBody>
                  <h4 className={classes.cardTitle}>
                    Số bệnh nhân cần hỗ trợ hiện trường
                  </h4>
                </CardBody>
                <CardFooter chart>
                  <div className={classes.stats}>
                    <DateRange /> Từ 14/08/2021 đến 21/08/2021
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            <GridItem xs={0} sm={0} md={6}></GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <Card chart>
                <CardHeader>
                  <Pie
                    height={100}
                    data={{
                      labels: ["1-4 ngày", "5-10 ngày", "11-28 ngày"],
                      datasets: [
                        {
                          data: Object.values(chartDangerDoctor),
                          backgroundColor: ["#ffcd56", "#ef5350", "#50af50"],
                          borderColor: ["#ffcd56", "#ef5350", "#50af50"],
                          borderWidth: 2,
                          barPercentage: 0.4,
                        },
                      ],
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              borderColor: "rgba(0, 0, 0, 0)",
                            },
                          },
                        ],
                      },
                    }}
                    options={{
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              borderColor: "rgba(0, 0, 0, 0)",
                            },
                          },
                        ],
                      },
                    }}
                  ></Pie>
                </CardHeader>
                <CardBody>
                  <h4 className={classes.cardTitle}>
                    Phân loại nhóm ngày bệnh
                  </h4>
                </CardBody>
                <CardFooter chart>
                  <div className={classes.stats}>
                    <DateRange /> Từ 1/1/2021 đến nay
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            {/* <GridItem xs={12} sm={12} md={6}>
              <Card chart>
                <CardHeader>
                  <Bar
                    height={100}
                    data={{
                      labels: listLabel,
                      datasets: [
                        {
                          label: "Bệnh nhân tử vong",
                          data: [0, 0, 0, 0, 0, 0, 0],
                          backgroundColor: "#ef5350",
                          borderColor: "#ef5350",
                          borderWidth: 2,
                          barPercentage: 0.4,
                        },
                      ],
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              borderColor: "rgba(0, 0, 0, 0)",
                            },
                          },
                        ],
                      },
                    }}
                    options={{
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              borderColor: "rgba(0, 0, 0, 0)",
                            },
                          },
                        ],
                      },
                    }}
                  ></Bar>
                </CardHeader>
                <CardBody>
                  <h4 className={classes.cardTitle}>Số bệnh nhân tử vong</h4>
                </CardBody>
                <CardFooter chart>
                  <div className={classes.stats}>
                    <DateRange /> Từ 14/08/2021 đến 21/08/2021
                  </div>
                </CardFooter>
              </Card>
            </GridItem> */}
          </GridContainer>
        </AccordionDetails>
      </Accordion>
      <Drawer
        anchor="right"
        open={isOpenFormDashboard}
        style={{ width: "500px" }}
      >
        <PatientForm></PatientForm>
      </Drawer>
      <Drawer
        anchor="right"
        open={isOpenPatientGroup}
        style={{ width: "500px" }}
      >
        <GroupPatientForm group={propGroup}></GroupPatientForm>
      </Drawer>
      <Modal
        open={isOpenSummary && !dataOne.loading && !dataSummary.loading}
        style={{ maxHeight: "20vh", maxWidth: "90vw" }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid className={classes.rssModal} item xs={12} sm={12} md={12}>
          <Summary data={{ ...patient, ...summary }}></Summary>
        </Grid>
      </Modal>
      <Modal
        open={openListPatient}
        style={{ maxHeight: "20vh", maxWidth: "90vw" }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid className={classes.rssModal} item xs={12} sm={12} md={12}>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div
              style={{ fontSize: "1.2em", fontWeight: "bold", padding: "1em" }}
            >
              THÊM BỆNH NHÂN VÀO NHÓM
            </div>
            <Button onClick={() => setOpenListPatient(false)}>
              <ClearIcon></ClearIcon>
            </Button>
          </div>
          <Grid
            item
            xs={12}
            sm={12}
            md={12}
            style={{ padding: "0px !important" }}
          >
            <form
              key="address_form"
              className={classes.form}
              style={{
                padding: "0",
                background: "#eee",
                borderRadius: "0.4em",
              }}
              onSubmit={handleSubmit(onAdvanceSearch)}
              noValidate
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  padding: "0.5em",
                  whiteSpace: "nowrap",
                }}
              >
                <div
                  style={{
                    maxWidth: "fit-content",
                    overflow: "auto",
                    marginRight: "1em",
                  }}
                >
                  <Controller
                    control={control}
                    name="lastname"
                    margin="normal"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.textAdvance}
                        margin="normal"
                        {...register("lastname")}
                        label="Họ và tên lót"
                        value={value}
                        onChange={onChange}
                      />
                    )}
                  />
                  <Controller
                    control={control}
                    name="firstname"
                    margin="normal"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.textAdvance}
                        margin="normal"
                        {...register("firstname")}
                        label="Tên"
                        value={value}
                        onChange={onChange}
                      />
                    )}
                  />
                  <Controller
                    control={control}
                    name="phone"
                    margin="normal"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.textAdvance}
                        margin="normal"
                        {...register("phone")}
                        label="Số điện thoại"
                        value={value}
                        onChange={onChange}
                      />
                    )}
                  />
                  <Controller
                    control={control}
                    name="otherid"
                    margin="normal"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <TextField
                        variant="outlined"
                        className={classes.textAdvance}
                        margin="normal"
                        {...register("otherid")}
                        label="Mã bệnh nhân (Khác)"
                        value={value}
                        onChange={onChange}
                      />
                    )}
                  />
                </div>
                <div style={{ display: "flex", alignItems: "center" }}>
                  <Button
                    type="submit"
                    variant="contained"
                    className={classes.buttonClear}
                  >
                    <SearchIcon></SearchIcon>
                  </Button>
                </div>
              </div>
            </form>
            <div style={{ textAlign: "end" }}>
              <Button
                className={classes.buttonAdd}
                onClick={() => setConfirmHandlePatient(true)}
              >
                THÊM
              </Button>
            </div>
          </Grid>
          <div style={{ overflowY: "auto", width: "100%" }}>
            <div
              style={{
                display: "flex",
                height: "100%",
                overflow: "auto",
              }}
            >
              <DataGrid
                loading={dataPat.loading || dataAdvance.loading}
                checkboxSelection={true}
                hideFooterPagination={true}
                onSelectionModelChange={(e) => {
                  setListSelectedPatient(e);
                }}
                rows={
                  !(dataPat.loading || dataAdvance.loading)
                    ? listPat
                      ? listPat?.map((item) => {
                          return {
                            ...item,
                            status: item.status.name,
                            name: `${item.lastname} ${item.firstname} `,
                            dob: moment(item.dob).format("DD-MM-YYYY"),
                            updatedat: moment(item.updatedat).format(
                              "DD-MM-YYYY"
                            ),
                            phone: item.phone,
                            createddat: moment(item.createddat).format(
                              "DD-MM-YYYY"
                            ),
                            address: `${item?.name ? item.name + "," : ""} ${
                              item?.ward ? item.ward + "," : ""
                            } ${item?.province ? item.province + "," : ""} ${
                              item?.city ? item.city : ""
                            }`,
                            gender:
                              item.gender === "male"
                                ? "Nam"
                                : item.gender === "female"
                                ? "Nữ"
                                : "Không xác định",
                          };
                        })
                      : []
                    : []
                }
                getCellClassName={() => {
                  return classes.bodyCell;
                }}
                autoHeight={true}
                columns={columns}
                pageSize={pageSize}
                disableSelectionOnClick
              />
            </div>
            <PaginationCustom
              handleChangeSize={(e) => handleChangeSize(e)}
              handleChangePage={(e) => handleChangePage(e)}
              count={pagination?.totalPage}
              page={page}
            ></PaginationCustom>
          </div>
        </Grid>
      </Modal>
      <ModalConfirm
        title="Thông báo"
        open={confirmHandlePatient}
        disableBtn={resHandlePatient.loading}
        description="Xác nhận thêm thông tin"
        handleOk={() => onHandlePatient()}
        handleClose={() => setConfirmHandlePatient(false)}
      ></ModalConfirm>
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={isNotiChangePass}
        autoHideDuration={3000}
        onClose={() => dispatch(actions.closeNoti())}
      >
        <Alert severity="success">{message}</Alert>
      </Snackbar>
      {successAdd !== null && (
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={isOpenNoti}
          autoHideDuration={3000}
          onClose={() => dispatch(actionCharts.handleCloseDashboardNoti())}
        >
          <Alert
            severity={
              successAdd === true
                ? "success"
                : successAdd === false
                ? "error"
                : ""
            }
          >
            {messageDash}
          </Alert>
        </Snackbar>
      )}
    </div>
  );
}
