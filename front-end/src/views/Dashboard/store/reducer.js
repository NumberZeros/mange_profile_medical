/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "Dashboard";

const initialState = freeze({
  listDoctors: [],
  listPatients: [],
  medical: null,
  patient: null,
  doctor: null,
  isAuthenticated: false,
  error: null,
  chartList: null,
  chartDocList: null,
  titleNoti: "",
  idLocation: "",
  idUser: "",
  messageDash: "",
  isOpenSummary: null,
  summary: null,
  successAdd: null,
  isOpenNoti: null,
  isLoading: null,
  isLoadingMove: null,
  isOpenFormDashboard: null,
  isOpenComplete: null,
  pagination: null,
  statusTypes: null,
  chartDanger: {
    1: 0,
    2: 0,
    3: 0,
  },
  chartDangerDoctor: {
    1: 0,
    2: 0,
    3: 0,
  },
  isOpenPatientGroup: false,
  listPatientGroup: [],
});

export default handleActions(
  {
    [actions.handleCloseDashboardNoti]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: false,
      });
    },
    [actions.handleOpenComplete]: (state, actions) => {
      return freeze({
        ...state,
        isOpenComplete: true,
      });
    },
    [actions.handleCloseComplete]: (state, actions) => {
      return freeze({
        ...state,
        isOpenComplete: false,
      });
    },
    [actions.handleOpenPatientGroup]: (state, actions) => {
      return freeze({
        ...state,
        isOpenPatientGroup: true,
      });
    },
    [actions.handleClosePatientGroup]: (state, actions) => {
      return freeze({
        ...state,
        isOpenPatientGroup: false,
      });
    },
    [actions.handleOpenDashboardForm]: (state, actions) => {
      return freeze({
        ...state,
        isOpenFormDashboard: true,
      });
    },
    [actions.handleCloseDashboardForm]: (state, actions) => {
      return freeze({
        ...state,
        isOpenFormDashboard: false,
      });
    },
    [actions.handleOpenSummary]: (state, actions) => {
      return freeze({
        ...state,
        isOpenSummary: true,
      });
    },
    [actions.handleCloseSummary]: (state, actions) => {
      return freeze({
        ...state,
        isOpenSummary: false,
      });
    },
    [actions.handleFindAllChart]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindAllChartSuccess]: (state, actions) => {
      return freeze({
        ...state,
        chartList: actions.payload,
        isLoading: false,
      });
    },
    [actions.handleFindAllChartFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleFindAllDocChart]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindAllDocChartSuccess]: (state, actions) => {
      return freeze({
        ...state,
        chartDocList: actions.payload.data,
        isLoading: false,
      });
    },
    [actions.handleFindAllDocChartFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleFindDashboardDoctors]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindDashboardDoctorsSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listDoctors: actions.payload.data.find_all_doctor.filter(
          (item) => item.isverified
        ),
        isLoading: false,
      });
    },
    [actions.handleFindDashboardDoctorsFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleFindDashboardPatients]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindDashboardPatientsSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listPatients: actions.payload.find_all_patient_use_pagination.patients,
        pagination: {
          totalPage: Math.ceil(
            actions.payload.find_all_patient_use_pagination.total /
              actions.payload.find_all_patient_use_pagination.take
          ),
        },
        isLoading: false,
      });
    },
    [actions.handleFindDashboardPatientsFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleFindSummary]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindSummarySuccess]: (state, actions) => {
      return freeze({
        ...state,
        summary: actions.payload,
        isLoading: false,
      });
    },
    [actions.handleFindSummaryFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleFindSummaryMed]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindSummaryMedSuccess]: (state, actions) => {
      return freeze({
        ...state,
        medical: actions.payload,
        isLoading: false,
      });
    },
    [actions.handleFindSummaryMedFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleFindSummaryPatient]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindSummaryPatientSuccess]: (state, actions) => {
      return freeze({
        ...state,
        patient: actions.payload,
        isLoading: false,
      });
    },
    [actions.handleFindSummaryPatientFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleAddPatientDashboard]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleAddPatientDashboardSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listPatients: [
          {
            ...actions.payload.data.create_patient,
            status: { name: "", id: "" },
          },
          ...state.listPatients,
        ],
        successAdd: true,
        isOpenNoti: true,
        messageDash: "Thêm thông tin bệnh nhân thành công.",
        isLoading: false,
        isOpenFormDashboard: false,
      });
    },
    [actions.handleAddPatientDashboardFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        messageDash: "Thêm thông tin thất bại! Vui lòng kiểm tra lại.",
        isSuccess: false,
        successAdd: false,
      });
    },
    [actions.handleFindStatus]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindStatusSuccess]: (state, actions) => {
      return freeze({
        ...state,
        statusTypes: actions.payload.find_all_patient_status,
        isLoading: false,
      });
    },
    [actions.handleFindStatusFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleChangePatientStatus]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleChangePatientStatusSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        successAdd: true,
        isOpenNoti: true,
        listPatients: state.listPatients.map((item) => {
          if (item.id === actions.payload.patientId) {
            return {
              ...item,
              status: actions.payload.change_status_patient.status,
            };
          } else return item;
        }),
        patient: {
          ...state.patient,
          find_one_patient: {
            ...state.patient.find_one_patient,
            status: actions.payload.change_status_patient.status,
          },
        },
        messageDash: "Kết thúc hồ sơ bệnh nhân thành công.",
      });
    },
    [actions.handleChangePatientStatusFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        messageDash: "Kết thúc hồ sơ bệnh nhân thất bại, vui lòng thử lại.",
        isOpenNoti: true,
        successAdd: false,
      });
    },
    [actions.handleFetchChartDanger]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFetchChartDangerSuccess]: (state, actions) => {
      return freeze({
        ...state,
        chartDanger: actions.payload.analaysis_group_dangerous_patient,
        isLoading: false,
      });
    },
    [actions.handleFetchChartDangerFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleFetchChartDangerDoctor]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFetchChartDangerDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        chartDangerDoctor: actions.payload.analaysis_group_dangerous_patient,
        isLoading: false,
      });
    },
    [actions.handleFetchChartDangerDoctorFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleCreateGroupPatient]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleCreateGroupPatientSuccess]: (state, actions) => {
      // let arr =[
      //   { ...actions.payload.data.create_group_patient, patient: [] },
      //   ...state.listPatientGroup,
      // ];
      // let length = arr.length;
      // return freeze({
      //   ...state,
      //   successAdd: true,
      //   isOpenNoti: true,
      //   listPatientGroup: arr.map((item, index)=>{
      //     return{
      //       ...item,
      //       index: length-index-1
      //     }
      //   }),
      //   messageDash: "Thêm thông tin nhóm bệnh nhân thành công.",
      //   isLoading: false,
      //   isOpenPatientGroup: false,
      // });
      return freeze({
        ...state,
        successAdd: true,
        isOpenNoti: true,
        listPatientGroup: [
          { ...actions.payload.data.create_group_patient, patient: [] },
          ...state.listPatientGroup,
        ],
        messageDash: "Thêm thông tin nhóm bệnh nhân thành công.",
        isLoading: false,
        isOpenPatientGroup: false,
      });
    },
    [actions.handleCreateGroupPatientFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        messageDash: "Thêm thông tin nhóm thất bại! Vui lòng kiểm tra lại.",
        isSuccess: false,
        successAdd: false,
      });
    },
    [actions.handleEditGroupPatient]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleEditGroupPatientSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listPatientGroup: state.listPatientGroup.map((item) => {
          if (item.id === actions.payload.data.put_group_patient.id) {
            return {
              ...item,
              name: actions.payload.data.put_group_patient.name,
              description: actions.payload.data.put_group_patient.description,
              priority: actions.payload.data.put_group_patient.priority,
            };
          } else return item;
        }),
        successAdd: true,
        isOpenNoti: true,
        messageDash: "Sửa thông tin nhóm bệnh nhân thành công.",
        isLoading: false,
        isOpenPatientGroup: false,
      });
    },
    [actions.handleEditGroupPatientFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        messageDash: "Sửa thông tin nhóm thất bại! Vui lòng kiểm tra lại.",
        isSuccess: false,
        successAdd: false,
      });
    },
    [actions.handleFindAllPatientGroup]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindAllPatientGroupSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listPatientGroup: actions.payload.find_all_group_patient,
        isLoading: false,
      });
    },
    [actions.handleFindAllPatientGroupFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleMoveLane]: (state, actions) => {
      return freeze({
        ...state,
        isLoadingMove: true,
      });
    },
    [actions.handleMoveLaneSuccess]: (state, actions) => {
      const from = parseFloat(actions.payload.from);
      const to = parseFloat(actions.payload.to);
      // let tempFrom = state.listPatientGroup[`${from}`];
      // let tempTo = state.listPatientGroup[`${to}`];
      function array_move(arr, old_index, new_index) {
        if (new_index >= arr.length) {
          var k = new_index - arr.length + 1;
          while (k--) {
            arr.push(undefined);
          }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr; // for testing
      }
      let arr = [...state.listPatientGroup];
      let swapped = array_move(arr, from, to);
      return freeze({
        ...state,
        listPatientGroup: swapped.map((item, index)=>{
          return{
            ...item,
            index: state.listPatientGroup.length - index - 1
          }
        }),
        isLoadingMove: false,
      });
    },
    [actions.handleMoveLaneFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoadingMove: false,
      });
    },
    [actions.handlePatientForGroup]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handlePatientForGroupSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listPatientGroup: state.listPatientGroup.map((item) => {
          if (item.id === actions.payload.data.handle_group_patient.id) {
            return {
              ...item,
              patient: actions.payload.data.handle_group_patient.patient,
            };
          } else return item;
        }),
        isLoading: false,
        successAdd: true,
        isOpenNoti: true,
        messageDash: "Thao tác thành công.",
      });
    },
    [actions.handlePatientForGroupFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        successAdd: true,
        isOpenNoti: true,
        messageDash: "Thao tác thất bại. Vui lòng thử lại.",
      });
    },
  },
  initialState
);
