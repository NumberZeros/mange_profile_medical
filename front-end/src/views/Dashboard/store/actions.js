import { createAction } from "redux-actions";

export const handleFindAllChart = createAction("DASHBOARD/FIND_ALL_CHART");
export const handleFindAllChartSuccess = createAction("DASHBOARD/FIND_ALL_CHART_SUCCESS");
export const handleFindAllChartFail = createAction("DASHBOARD/FIND_ALL_CHART_FAIL");

export const handleFindDashboardPatients = createAction("DASHBOARD/FIND_DASHBOARD_PATIENTS");
export const handleFindDashboardPatientsSuccess = createAction("DASHBOARD/FIND_DASHBOARD_PATIENTS_SUCCESS");
export const handleFindDashboardPatientsFail = createAction("DASHBOARD/FIND_DASHBOARD_PATIENTS_FAIL");

export const handleFindDashboardDoctors = createAction("DASHBOARD/FIND_DASHBOARD_DOCTORS");
export const handleFindDashboardDoctorsSuccess = createAction("DASHBOARD/FIND_DASHBOARD_DOCTORS_SUCCESS");
export const handleFindDashboardDoctorsFail = createAction("DASHBOARD/FIND_DASHBOARD_DOCTORS_FAIL");

export const handleFindAllDocChart = createAction("DASHBOARD/FIND_ALL_DOCTOR_CHART");
export const handleFindAllDocChartSuccess = createAction("DASHBOARD/FIND_ALL_DOCTOR_CHART_SUCCESS");
export const handleFindAllDocChartFail = createAction("DASHBOARD/FIND_ALL_DOCTOR_CHART_FAIL");

export const handleOpenSummary = createAction("DASHBOARD/OPEN_SUMMARY");
export const handleCloseSummary = createAction("DASHBOARD/CLOSE_SUMMARY");

export const handleOpenComplete = createAction("DASHBOARD/OPEN_COMPLETE");
export const handleCloseComplete = createAction("DASHBOARD/CLOSE_COMPLETE");

export const handleOpenDashboardNoti = createAction("DASHBOARD/OPEN_NOTI");
export const handleCloseDashboardNoti = createAction("DASHBOARD/CLOSE_NOTI");

export const handleFindSummary = createAction("DASHBOARD/FIND_SUMMARY");
export const handleFindSummarySuccess = createAction("DASHBOARD/FIND_SUMMARY_SUCCESS");
export const handleFindSummaryFail = createAction("DASHBOARD/FIND_SUMMARY_FAIL");

export const handleFindSummaryMed = createAction("DASHBOARD/FIND_MEDICAL");
export const handleFindSummaryMedSuccess = createAction("DASHBOARD/FIND_MEDICAL_SUCCESS");
export const handleFindSummaryMedFail = createAction("DASHBOARD/FIND_MEDICAL_FAIL");

export const handleFindSummaryPatient = createAction("DASHBOARD/FIND_ONE_PATIENT");
export const handleFindSummaryPatientSuccess = createAction("DASHBOARD/FIND_ONE_PATIENT_SUCCESS");
export const handleFindSummaryPatientFail = createAction("DASHBOARD/FIND_ONE_PATIENT_FAIL");

export const handleAddPatientDashboard = createAction("DASHBOARD/ADD_PATIENT");
export const handleAddPatientDashboardSuccess = createAction("DASHBOARD/ADD_PATIENT_SUCCESS");
export const handleAddPatientDashboardFail = createAction("DASHBOARD/ADD_PATIENT_FAIL");

export const handleOpenDashboardForm = createAction("DASHBOARD/OPEN_FORM");
export const handleCloseDashboardForm = createAction("DASHBOARD/CLOSE_FORM");

export const handleFindStatus = createAction("DASHBOARD/FIND_ALL_STATUS");
export const handleFindStatusSuccess = createAction("DASHBOARD/FIND_ALL_STATUS_SUCCESS");
export const handleFindStatusFail = createAction("DASHBOARD/FIND_ALL_STATUS_FAIL");

export const handleChangePatientStatus = createAction("DASHBOARD/CHANGE_PATIENT_STATUS");
export const handleChangePatientStatusSuccess = createAction("DASHBOARD/CHANGE_PATIENT_STATUS_SUCCESS");
export const handleChangePatientStatusFail = createAction("DASHBOARD/CHANGE_PATIENT_STATUS_FAIL");

export const handleFetchChartDanger = createAction("DASHBOARD/FETCH_CHART_DANGER");
export const handleFetchChartDangerSuccess = createAction("DASHBOARD/FETCH_CHART_DANGER_SUCCESS");
export const handleFetchChartDangerFail = createAction("DASHBOARD/FETCH_CHART_DANGER_FAIL");

export const handleFetchChartDangerDoctor = createAction("DASHBOARD/FETCH_CHART_DANGER_DOCTOR");
export const handleFetchChartDangerDoctorSuccess = createAction("DASHBOARD/FETCH_CHART_DANGER_DOCTOR_SUCCESS");
export const handleFetchChartDangerDoctorFail = createAction("DASHBOARD/FETCH_CHART_DANGER_DOCTOR_FAIL");

export const handleOpenPatientGroup = createAction("DASHBOARD/OPEN_PATIENT_GROUP");
export const handleClosePatientGroup = createAction("DASHBOARD/CLOSE_PATIENT_GROUP");

export const handleCreateGroupPatient = createAction("DASHBOARD/CREATE_GROUP_PATIENT");
export const handleCreateGroupPatientSuccess = createAction("DASHBOARD/CREATE_GROUP_PATIENT_SUCCESS");
export const handleCreateGroupPatientFail = createAction("DASHBOARD/CREATE_GROUP_PATIENT_FAIL");

export const handleEditGroupPatient = createAction("DASHBOARD/EDIT_GROUP_PATIENT");
export const handleEditGroupPatientSuccess = createAction("DASHBOARD/EDIT_GROUP_PATIENT_SUCCESS");
export const handleEditGroupPatientFail = createAction("DASHBOARD/EDIT_GROUP_PATIENT_FAIL");

export const handleFindAllPatientGroup = createAction("DASHBOARD/FIND_ALL_PATIENT_GROUP");
export const handleFindAllPatientGroupSuccess = createAction("DASHBOARD/FIND_ALL_PATIENT_GROUP_SUCCESS");
export const handleFindAllPatientGroupFail = createAction("DASHBOARD/FIND_ALL_PATIENT_GROUP_FAIL");

export const handlePatientForGroup = createAction("DASHBOARD/HANDLE_PATIENT_GROUP");
export const handlePatientForGroupSuccess = createAction("DASHBOARD/HANDLE_PATIENT_GROUP_SUCCESS");
export const handlePatientForGroupFail = createAction("DASHBOARD/HANDLE_PATIENT_GROUP_FAIL");

export const handleMoveLane = createAction("DASHBOARD/HANDLE_MOVE_LANE");
export const handleMoveLaneSuccess = createAction("DASHBOARD/HANDLE_MOVE_LANE_SUCCESS");
export const handleMoveLaneFail = createAction("DASHBOARD/HANDLE_MOVE_LANE_FAIL");
