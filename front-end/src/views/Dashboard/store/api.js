import { gql } from "@apollo/client";

export const chart = gql`
  query find($now: DateTime!, $adminid: String!) {
    analysis_patients(now: $now, adminid: $adminid) {
      totalPatient
      totalPatientNow
      totalPatientRiskAssessment
      dataTablePatientCreated
      dataTablePatientNegative
      dataTablePatientContacDescription
    }
  }
`;

export const chartDoctor = gql`
  query findDocChart($doctorid: String!, $now: DateTime!) {
    analysis_patients_for_doctor(now: $now, doctorid: $doctorid) {
      totalPatient
      totalPatientNow
      totalPatientRiskAssessment
      dataTablePatientCreated
      dataTablePatientNegative
      dataTablePatientContacDescription
    }
  }
`;

export const findAllPatienss = gql`
  query FindAllPatient($id: String!) {
    find_all_patient(id: $id) {
      id
      inactive
      createddat
      isverified
      updatedat
      category {
        name
      }
      firstname
      lastname
      phone
      updatedat
      dob
      gender
      quantityfamily
      phone
      category {
        id
        name
      }
      phoneemerency1
      phoneemerency2
      phoneemerency3
    }
  }
`;

export const summaryPateints = gql`
  query summaryPatients($patientid: String!, $now: DateTime!) {
    summary_patients(patientid: $patientid, now: $now) {
      dataTableResultTest
      dataTableStatusQuo
      dataTablePluse
      dataTableTemperature
      dataTableSpo2home
      dataTableBloodpressure
      dataTableBloodpressure2
    }
  }
`;

export const chartDangerous = gql`
  query ChartDangerous($doctorid: String!, $adminid: String!) {
    analaysis_group_dangerous_patient(doctorid: $doctorid, adminid: $adminid) {
      totalPatient4Date
      totalPatient10Date
      totalPatient28Date
    }
  }
`;

export const findOnePatient = gql`
  query FindOnePatient($id: String!) {
    find_one_patient(id: $id) {
      id
      updatedat
      otherid
      firstname
      lastname
      phone
      dob
      gender
      quantityfamily
      phone
      phoneemerency1
      phoneemerency2
      phoneemerency3
      name
      ward
      province
      city
      doctor {
        id
        user {
          id
          firstname
          lastname
        }
      }
      status {
        id
        name
      }
    }
  }
`;

export const findAllMedical = gql`
  query findAllMedical($patientid: String!) {
    find_all_medical(patientid: $patientid) {
      summary
      weight
      height
      riskassessment
      medicine
      countdateremains
      datetest
      resulttest
      resultsarscovid2
      otherSignal
      statusquo
      color
      pluse
      notepluse
      bloodpressure
      bloodpressure2
      notebloodpressure
      spo2home
      notespo2home
      temperature
      notetemperature
      other
      attribute
      noteattribute
      quantityofsick
      guide
      summary
      contacdescription
      reexamination
      dateofreexamination
      medicaltrack {
        id
        medicalnote
        prcrealdate
        medicalpackage
        medicalresutl
      }
      medicinenow {
        id
        name
        quantity
        createddat
      }
    }
  }
`;

export const advanceSearch = gql`
  query AdvanceSearch($pagination: PaginationPatientInput!) {
    find_all_patient_use_pagination_admin(pagination: $pagination) {
      take
      skip
      total
      nextPage
      patients {
        id
        inactive
        createddat
        isverified
        updatedat
        category {
          name
        }
        firstname
        lastname
        phone
        updatedat
        dob
        gender
        quantityfamily
        phone
        category {
          id
          name
        }
        phoneemerency1
        phoneemerency2
        phoneemerency3
        status {
          id
          name
        }
      }
    }
  }
`;

export const findAllStatusType = gql`
  query FindAllStatusType {
    find_all_patient_status {
      id
      name
      description
    }
  }
`;

export const changeStatusPatient = gql`
  mutation ChangeStatusPatient(
    $id: String!
    $status: String!
    $ownerid: String!
    $otherstatus: String!
  ) {
    change_status_patient(
      input: {
        id: $id
        ownerid: $ownerid
        status: $status
        otherstatus: $otherstatus
      }
    ) {
      status {
        id
        name
      }
    }
  }
`;

export const createGroupPatient = gql`
  mutation CreateGroupPatient(
    $doctorid: String!
    $name: String!
    $description: String!
    $priority: Float!
  ) {
    create_group_patient(
      input: {
        doctorid: $doctorid
        name: $name
        description: $description
        priority: $priority
      }
    ) {
      id
      name
      description
      index
      priority
      doctor {
        id
      }
    }
  }
`;

export const editGroupPatient = gql`
  mutation EditGroupPatient(
    $id: String!
    $doctorid: String!
    $name: String!
    $description: String!
    $priority: Float!
  ) {
    put_group_patient(
      input: {
        id: $id
        doctorid: $doctorid
        name: $name
        description: $description
        priority: $priority
      }
    ) {
      name
      id
      priority
      description
      doctor {
        id
      }
    }
  }
`;

export const findAllPatientGroup = gql`
  query FindAllPatientGroup($doctorId: String!) {
    find_all_group_patient(doctorId: $doctorId) {
      index
      name
      id
      priority
      description
      doctor {
        id
      }
      index
      patient {
        id
        firstname
        lastname
        dob
        gender
        phone
        ward
        province
      }
    }
  }
`;

export const handlePatientForGroup = gql`
  mutation HandlePatientForGroup(
    $id: String!
    $doctorid: String!
    $listPatient: [String!]!
    $isRemove: Boolean!
  ) {
    handle_group_patient(
      input: {
        id: $id
        doctorid: $doctorid
        isRemove: $isRemove
        listPatient: $listPatient
      }
    ) {
      name
      id
      priority
      description
      doctor {
        id
      }
      patient {
        id
        firstname
        lastname
        dob
        gender
        phone
        ward
        province
      }
    }
  }
`;

export const moveLane = gql`
  mutation MoveLane($id: String!, $doctorid: String!, $index: Float!) {
    update_index_group_patient(
      input: { id: $id, doctorid: $doctorid, index: $index }
    ) {
      name
      id
      priority
      description
      doctor {
        id
      }
      index
      patient {
        id
        firstname
        lastname
        dob
        gender
        phone
        ward
        province
      }
    }
  }
`;
