import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useForm, Controller } from "react-hook-form";
import * as actions from "./store/actions";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import * as apis from "./store/api";
import { useHistory } from "react-router-dom";
import InputAdornment from "@material-ui/core/InputAdornment";
import moment from "moment";
// import { name } from "./store/reducer";
import ClearIcon from "@material-ui/icons/Clear";
import ModalConfirm from "components/Modal/ModalConfirm";
import MuiAlert from "@material-ui/lab/Alert";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Brightness1Icon from "@material-ui/icons/Brightness1";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    display: "flex",
    flexDirection: "column",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#50AF50",
    color: "white",
    "&:hover": {
      background: "#55d555",
    },
  },
  cancel: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#F44336",
    color: "white",
    marginRight: "1em",
  },
  root: {
    height: "100vh",
  },
  left: {
    background: "#C7E5CE",
    height: "100vh",
    padding: "0 4em",
  },
  right: {
    padding: "3em 3em 0 3em",
  },
  copy: {
    justifyContent: "space-between",
  },
  banner: {
    textAlign: "center",
    marginTop: "20vh",
  },
  title: {
    textAlign: "center",
    marginTop: "1.2em",
    color: "#50AF50",
    fontSize: "2em",
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    "@media (max-width: 1024px)": {
      flexDirection: "column",
    },
  },
  logo: {
    marginBottom: "1.2em",
  },
  p: {
    marginRight: "0.4em",
    "@media (max-width: 900px)": {
      fontSize: "1rem",
    },
  },
  typo: {
    marginRight: "0.5em",
    marginTop: "0.1em",
  },
  img: {
    "@media (max-width: 950px)": {
      width: "20em",
    },
  },
  top: {
    display: "flex",
  },
  body: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    flexGrow: "1",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 100,
    color: "#fff",
  },
  label: {
    fontWeight: "bold",
  },
  value: {
    marginLeft: "0.2em",
  },
  disabled: {
    color: "black",
  },
  tfdisable: {
    "& .MuiInputBase-root.Mui-disabled": {
      color: "rgba(0, 0, 0, 1)", // (default alpha is 0.38)
    },
  },
}));

const ADD_GROUP = apis.createGroupPatient;
const EDIT_GROUP = apis.editGroupPatient;

const priorityGroups = {
  3: "#880e4f",
  4: "#6d4c41",
  5: "#7986cb",
  6: "#d500f9",
  7: "#3f51b5",
  8: "#50AF50",
  9: "#ff6d00",
  10: "#dd2c00",
};
const listTag = [
  {
    value: 3,
    title: "#880e4f",
  },
  {
    value: 4,
    title: "#6d4c41",
  },
  {
    value: 5,
    title: "#7986cb",
  },
  {
    value: 6,
    title: "#d500f9",
  },
  {
    value: 7,
    title: "#3f51b5",
  },
  {
    value: 8,
    title: "#50AF50",
  },
  {
    value: 9,
    title: "#ff6d00",
  },
  {
    value: 10,
    title: "#dd2c00",
  },
];
export default function GroupPatientForm(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const { isOpenGroup } = useSelector((state) => state["Group"]);
  const { dataUserAuth } = useSelector((state) => state["Login"]);

  const [addGroup, resAdd] = useMutation(ADD_GROUP, {
    errorPolicy: "all",
  });

  const [editGroup, resEdit] = useMutation(EDIT_GROUP, {
    errorPolicy: "all",
  });

  const [isModalOpen, setIsModalOpen] = useState(false);
  const { register, handleSubmit, setValue, control } = useForm();

  const [dataOk, setDataOk] = useState(null);
  const [errName, setErrName] = useState(false);
  const [name, setName] = useState("");
  const [errStart, setErrStart] = useState(false);
  const [start, setStart] = useState("");
  const [errEnd, setErrEnd] = useState(false);
  const [end, setEnd] = useState("");
  const [priority, setPriority] = useState(8);

  const handleOk = () => {
    if (props.group) {
      dispatch(actions.handleEditGroupPatient());
      editGroup({
        variables: {
          adminid:
            dataUserAuth?.roles === "admin"
              ? window.sessionStorage.getItem("id")
              : "",
          id: props.group.id,
          doctorid: props.group.doctor.id,
          name: dataOk.name,
          description: dataOk.description || "",
          priority: priority,
        },
      }).then((res) => {
        if (res.data === null || res.errors) {
          dispatch(actions.handleEditGroupPatientFail(res.errors[0].message));
          setIsModalOpen(false);
          // dispatch(actions.handleCloseGroupNoti);
        } else {
          dispatch(actions.handleEditGroupPatientSuccess(res));
          dispatch(actions.handleClosePatientGroup());
          setIsModalOpen(false);
          // location.reload();
          // showNoti();
        }
      });
    } else {
      dispatch(actions.handleCreateGroupPatient());
      addGroup({
        variables: {
          adminid:
            dataUserAuth?.roles === "admin"
              ? window.sessionStorage.getItem("id")
              : "",
          doctorid:
            dataUserAuth?.roles === "admin"
              ? window.sessionStorage.getItem("id")
              : window.sessionStorage.getItem("id"),
          description: dataOk.description || "",
          priority: parseFloat(priority),
          name: dataOk.name,
        },
      }).then((res) => {
        if (res.data === null || res.errors) {
          dispatch(actions.handleCreateGroupPatientFail(res.errors[0].message));
          setIsModalOpen(false);
          // dispatch(actions.handleCloseGroupNoti);
        } else {
          dispatch(actions.handleCreateGroupPatientSuccess(res));
          dispatch(actions.handleClosePatientGroup());
          setIsModalOpen(false);
          // location.reload();
          // showNoti();
        }
      });
    }

    // .catch((err) => {
    //   console.log(err.message)
    //   dispatch(actions.handleAddDoctorFail(err));
    //   setIsModalOpen(false);
    //   showNoti();
    // });
  };

  const onSubmit = (dataForm) => {
    setIsModalOpen(true);
    setDataOk(dataForm);
  };

  useEffect(() => {
    if (props.group) {
      setValue("name", props.group.name);
      setValue("description", props.group.description);
      setValue("priority", props.group.priority);
      setName(props.group.name);
      setPriority(props.group.priority);
    } else {
      setValue("name", "");
    }
  }, [isOpenGroup]);

  return (
    <GridContainer component="main" style={{ padding: "2em" }}>
      <GridItem className={classes.right} item xs={12} sm={12} md={12}>
        <div className={classes.paper}>
          <div style={{ textAlign: "end", display: "flex" }}>
            <Typography className={classes.typo} component="h1" variant="h5">
              {props.group ? "SỬA THÔNG TIN NHÓM" : "THÊM NHÓM BỆNH NHÂN"}
            </Typography>
            <Button onClick={() => dispatch(actions.handleClosePatientGroup())}>
              <ClearIcon></ClearIcon>
            </Button>
          </div>
          <form
            key="doctor_form"
            onSubmit={handleSubmit(onSubmit)}
            className={classes.form}
            noValidate
          >
            <div className={classes.body}>
              <Controller
                control={control}
                name="name"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <TextField
                    style={{ marginTop: "1.4em" }}
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    {...register("name")}
                    label="Tên nhóm"
                    error={errName}
                    helperText={errName && "Vui lòng nhập tên nhóm"}
                    onChange={(e) => {
                      if (e.target.value) setErrName(false);
                      else setErrName(true);
                      setValue("name", e.target.value);
                      setName(e.target.value);
                    }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    value={value}
                  />
                )}
              />
              <Controller
                control={control}
                name="description"
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <TextField
                    style={{ marginTop: "1.2em" }}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    {...register("description")}
                    label="Mô tả"
                    onChange={(e) =>
                      setValue("description", e.target.value || "")
                    }
                    InputProps={{
                      startAdornment: (
                        <InputAdornment
                          style={{ marginLeft: "-0.1em" }}
                          position="end"
                        ></InputAdornment>
                      ),
                    }}
                    value={value}
                  />
                )}
              />
              <Controller
                control={control}
                {...register("priority")}
                render={({ field: { onChange, onBlur, value, ref } }) => (
                  <FormControl
                    style={{ marginTop: "1em" }}
                    variant="outlined"
                    className={classes.formControl}
                    required
                    fullWidth
                  >
                    <InputLabel>Thẻ</InputLabel>
                    <Select
                      id="demo-simple-select-outlined"
                      label="Thẻ"
                      {...register("priority")}
                      value={priority}
                      onChange={(e) => setPriority(e.target.value)}
                    >
                      {listTag.map(item=>{
                        return <MenuItem style={{backgroundColor: item.title, borderRadius:"1em", margin: "1em"}} key={item.value} value={item.value}>
                          <div style={{color: item.title, backgroundColor: item.title, borderRadius:"1em"}}>a</div>
                        </MenuItem>
                      })}
                    </Select>
                  </FormControl>
                )}
              />
              <div style={{ textAlign: "end" }}>
                <Button
                  variant="contained"
                  onClick={() => dispatch(actions.handleClosePatientGroup())}
                  className={classes.cancel}
                >
                  HỦY
                </Button>
                <Button
                  type="submit"
                  variant="contained"
                  className={classes.submit}
                  disabled={errName || name === ""}
                >
                  {props.group ? "LƯU" : "THÊM"}
                </Button>
              </div>
            </div>
          </form>
        </div>
      </GridItem>
      <ModalConfirm
        title="Thông báo"
        open={isModalOpen}
        disableBtn={resAdd.loading || resEdit.loading}
        description={
          props.group ? "Xác nhận sửa thông tin" : "Xác nhận thêm thông tin"
        }
        handleOk={() => handleOk()}
        handleClose={() => setIsModalOpen(false)}
      ></ModalConfirm>
    </GridContainer>
  );
}
