import { createAction } from "redux-actions";

export const handleOpenRemoteForm = createAction("REMOTE/OPEN_FORM");
export const handleCloseRemoteForm = createAction("REMOTE/CLOSE_FORM");

export const handleOpenRemoteNoti = createAction("REMOTE/OPEN_NOTI");
export const handleCloseRemoteNoti = createAction("REMOTE/CLOSE_NOTI");

export const handleAcceptPatient = createAction("REMOTE/ACCEPT_PATIENT");
export const handleAcceptPatientSuccess = createAction("REMOTE/ACCEPT_PATIENT_SUCCESS");
export const handleAcceptPatientFail = createAction("REMOTE/ACCEPT_PATIENT_FAIL");

export const handleFindLatestEmer = createAction("REMOTE/FIND_LATEST_EMER");
export const handleFindLatestEmerSuccess = createAction("REMOTE/FIND_LATEST_EMER_SUCCESS");
export const handleFindLatestEmerFail = createAction("REMOTE/FIND_LATEST_EMER_FAIL");

export const handleUpdateRemoteEmer = createAction("REMOTE/UPDATE_EMER");
export const handleUpdateRemoteEmerSuccess = createAction("REMOTE/UPDATE_EMER_SUCCESS");
export const handleUpdateRemoteEmerFail = createAction("REMOTE/UPDATE_EMER_FAIL");