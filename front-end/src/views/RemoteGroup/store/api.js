import { gql } from "@apollo/client";

export const acceptPatient = gql`
  mutation AcceptPatient(
    $patientid: String!
    $type: String!
    $doctorid: String!
    $medicalid: String!
  ) {
    tracking_patient_deposit_doctor(
      input: {
        patientid: $patientid
        type: $type
        doctorid: $doctorid
        medicalid: $medicalid
      }
    ) {
      id
    }
  }
`;

export const findLatestEmer = gql`
  query FindLatestEmer($patientid: String!) {
    find_latest_emergency(patientid: $patientid) {
      id
      createddat
      status {
        id
        name
      }
      notestatus
      notebackgroup
      iscontact
      clinical {
        id
        name
      }
      note
      sense
      pluse
      bloodpressure
      bloodpressure2
      spo2home
      temperature
      medicalpackage
      resultdate
      methodbreath {
        name
        id
      }
      po2
      medicines {
        name
        quantity
        description
      }
      notestatus
      transferfrom
      reason
      reasondate
      medicalpackagenote
      displaydate
    }
  }
`;
