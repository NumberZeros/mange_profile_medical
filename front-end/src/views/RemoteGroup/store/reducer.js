/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "Remote";

const initialState = freeze({
  listDoctors: [],
  listPatients: [],
  isOpenModal: null,
  isOpenRemoteForm: null,
  isOpenNoti: null,
  isSuccess: null,
  message: "",
  oneEmer: null,
});

export default handleActions(
  {
    [actions.handleOpenRemoteForm]: (state, actions) => {
      return freeze({
        ...state,
        isOpenRemoteForm: true,
        idUser: actions.payload,
      });
    },
    [actions.handleCloseRemoteForm]: (state, actions) => {
      return freeze({
        ...state,
        isOpenRemoteForm: false,
        idUser: "",
      });
    },
    [actions.handleOpenRemoteNoti]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: true,
      });
    },
    [actions.handleCloseRemoteNoti]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: false,
      });
    },
    [actions.handleFindLatestEmer]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleFindLatestEmerSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        oneEmer: actions.payload.find_latest_emergency,
      });
    },
    [actions.handleFindLatestEmerFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleUpdateRemoteEmer]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleUpdateRemoteEmerSuccess]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        isSuccess: true,
        message: "Cập nhật thành công",
      });
    },
    [actions.handleUpdateRemoteEmerFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isOpenNoti: true,
        isSuccess: false,
        message: "Cập nhật thất bại, vui lòng kiểm tra lại.",
      });
    },
  },
  initialState
);
