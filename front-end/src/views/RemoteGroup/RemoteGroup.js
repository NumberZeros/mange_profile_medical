import React, { useEffect, useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import { Grid } from "@material-ui/core";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Drawer from "@material-ui/core/Drawer";
import { Button } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import queryString from "query-string";
import * as apis from "./store/api";
import getAuthorize from "constants/authen/authen";
import moment from "moment";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useQuery } from "@apollo/client";
import { useMutation } from "@apollo/client";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { DataGrid } from "@material-ui/data-grid";
import Switch from "@material-ui/core/Switch";
import Modal from "@material-ui/core/Modal";
import AddressForm from "constants/Address/AddressForm";
import ModalConfirm from "components/Modal/ModalConfirm";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import axios from "axios";
import Cookies from "js-cookie";
import RefreshIcon from "@material-ui/icons/Refresh";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Popper from "@material-ui/core/Popper";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import MenuList from "@material-ui/core/MenuList";
import PropTypes from "prop-types";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import LibraryAddCheckIcon from "@material-ui/icons/LibraryAddCheck";
import DoctorForm from "views/DoctorsManagement/DoctorForm";
import RemoveRedEyeIcon from "@material-ui/icons/RemoveRedEye";
import PatientForm from "views/PatientsManagement/PatientForm";
import { select } from "react-cookies";
import { UPLOAD_AXIOS } from "../../config-url";
import FooterOutSide from "components/Footer/Footer-v1";
import dhyd from "assets/img/dhyd.png";
import logo from "assets/img/medx-logo.svg";
import ClearIcon from "@material-ui/icons/Clear";
import MedicalRemote from "./MedicalRemote";
import { authLink } from "views/Authenticate/store/api";
import * as apiPatient from "views/PatientsManagement/store/api";
import * as actionsPatient from "views/PatientsManagement/store/actions";
import * as actions from "views/RemoteGroup/store/actions";
import { acceptPatient, findLatestEmer } from "./store/api";
import {
  addEmerProfile,
  updateEmerProfile,
  findAllEmerStatus,
  findAllBreath,
  findAllClinical,
} from "views/PatientsManagement/store/api";
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
function getModalStyle() {
  return {
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`,
  };
}

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      marginTop: "0",
      marginBottom: "0",
      fontSize: "1rem !important",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    fontSize: "1.5rem !important",
    "& small": {
      color: "#777",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  drawer: {
    width: "50vw",
  },
  backdrop: {
    zIndex: 4799,
    color: "#fff",
  },
  topBar: {
    background: "#50AF50 !important",
    display: "flex",
    justifyContent: "space-between",
  },
  searchBar: {
    background: "#ddd",
    borderRadius: "0.4em",
    border: "none !important",
    height: "fit-content",
    width: "-webkit-fill-available",
  },
  headerTable: {
    color: "#0A3A34",
    fontWeight: "bold",
    fontSize: "1rem",
  },
  bodyCell: {
    fontSize: "0.9rem",
    paddingLeft: "1em !important",
    "&:nth-child(1)": {
      paddingLeft: "0 !important",
    },
  },
  buttonAdd: {
    whiteSpace: "nowrap",
    background: "#50AF50",
    color: "#eee",
    height: "3em",
    padding: "0.2em 0.3em 0.2em 0.3em",
    marginRight: "0.5em",
    marginTop: "1em",
    marginLeft: "1em",
    "&:hover": {
      background: "#59996D",
    },
  },
  buttonGroup: {
    whiteSpace: "nowrap",
    background: "#37774B",
    color: "#eee",
    height: "3.9em",
    // padding: "0.2em 1em 0.2em 1em",
    marginRight: "0.5em",
    marginLeft: "1em",
  },
  buttonUp: {
    background: "#37774B",
    marginBottom: "0.1em",
    color: "#eee",
    padding: "0.2em 1em 0.2em 1em",
    marginRight: "1em",
    marginTop: "0.4em",
    marginLeft: "1em",
    "&:hover": {
      background: "#59996D",
    },
  },
  btnDel: {
    color: "red",
    minWidth: "auto",
  },
  btnEdit: {
    color: "#37774B",
    minWidth: "auto",
  },
  bigIndicator: {
    backgroundColor: "white",
  },
  tab: {
    minWidth: 100, // a number of your choice
    width: 120, // a number of your choice
  },
  hoverico: {
    whiteSpace: "nowrap",
    background: "#37774B",
    padding: "0",
    color: "#eee",
    // height: "3.9em",
    "&:hover": {
      background: "#59996D",
    },
  },
  hoverselect: {
    whiteSpace: "nowrap",
    background: "#37774B",
    paddingLeft: "1em",
    paddingRight: "0.6em",
    borderRight: "none !important",
    width: "4.3rem",
    color: "#eee",
    // height: "3.9em",
    "&:hover": {
      background: "#59996D",
    },
  },
  logoLink: {
    textTransform: "uppercase",
    padding: "5px 0",
    display: "block",
    fontSize: "18px",
    textAlign: "left",
    fontWeight: "400",
    lineHeight: "30px",
    textDecoration: "none",
    backgroundColor: "transparent",
  },
  img: {
    width: "45px",
    top: "22px",
    verticalAlign: "middle",
    border: "0",
  },
  medprofile: {
    marginTop: "0.3em",
  },
  rssModal: {
    width: "76%",
    overflowY: "scroll",
    padding: "0 0 1em 0",
    transform: "translate(20%, 15%)",
    maxHeight: "80vh",
    background: "#eee",
    "@media (max-width: 769px)": {
      transform: "translate(11%, 15%)",
      width: "85%",
    },
    "@media (max-width: 550px)": {
      transform: "translate(4%, 15%)",
      width: "95%",
    },
    "@media (max-width: 425px)": {
      transform: "translate(2%, 15%)",
      width: "98%",
    },
  },
};

const useStyles = makeStyles(styles) ;
const dgmdht = {
  codvidhide: "COVID-19 không triệu chứng",
  codvidsimple: "COVID-19 mức độ nhẹ (Viêm hô hấp trên cấp)",
  covidmedium: "COVID-19 mức độ vừa (Viêm phổi)",
  covidhight: "COVID-19 mức độ nặng (Viêm phổi nặng) và nguy kịch",
  f1hidecovid: "F1 có triệu chứng (nghi nhiễm)",
  f1showcovid: "F1 không triệu chứng",
};

const dgpl = {
  low: "Nguy cơ thấp",
  medium: "Nguy cơ trung bình",
  high: "Nguy cơ cao",
  hightest: "Nguy cơ rất cao",
};

const mdvm = {
  haggard: "Xanh xao",
  typhus: "Da niêm nhạt",
  normal: "Da niêm hồng",
};

const ACCEPT_PATIENT = acceptPatient;
const FIND_LATEST = findLatestEmer;

export default function RemoteGroup(props) {
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();

  const { listBreath, listEmerStatus, listClinical } = useSelector(
    (state) => state["Patients"]
  );

  const { isOpenRemoteForm, isOpenNoti, isSuccess, message } = useSelector((state) => state["Remote"]);

  const [accept, acceptRes] = useMutation(ACCEPT_PATIENT, {
    errorPolicy: "all",
  });

  const [addEmer, addEmerRes] = useMutation(addEmerProfile, {
    errorPolicy: "all",
  });
  const [updateEmer, updateEmerRes] = useMutation(updateEmerProfile, {
    errorPolicy: "all",
  });

  const [findLatest, resFindLatest] = useMyLazyQuery(FIND_LATEST, {
    onCompleted: () => {
      if (resFindLatest.data) {
        dispatch(actions.handleFindLatestEmerSuccess(resFindLatest.data));
      } else dispatch(actions.handleFindLatestEmerFail());
    },
    fetchPolicy: "network-only",
  });

  const [findClinical, resFindAllClinical] = useMyLazyQuery(findAllClinical, {
    onCompleted: () => {
      if (resFindAllClinical.data) {
        dispatch(
          actionsPatient.handleFindClinicalSuccess(resFindAllClinical.data)
        );
      } else dispatch(actionsPatient.handleFindClinicalFail());
    },
    fetchPolicy: "network-only",
  });

  const [findBreath, resFindAllBreath] = useMyLazyQuery(findAllBreath, {
    onCompleted: () => {
      if (resFindAllBreath.data) {
        dispatch(
          actionsPatient.handleFindBreathStatusSuccess(resFindAllBreath.data)
        );
      } else dispatch(actionsPatient.handleFindBreathStatusFail());
    },
    fetchPolicy: "network-only",
  });

  const [findEmerStatus, resFindAllEmerStatus] = useMyLazyQuery(
    findAllEmerStatus,
    {
      onCompleted: () => {
        if (resFindAllEmerStatus.data) {
          dispatch(
            actionsPatient.handleFindEmerStatusSuccess(
              resFindAllEmerStatus.data
            )
          );
        } else dispatch(actionsPatient.handleFindEmerStatusFail());
      },
      fetchPolicy: "network-only",
    }
  );

  const [dataPatient, setDataPatient] = useState(null);
  const [dataDoctor, setDataDoctor] = useState(null);
  const [isAccepted, setIsAccepted] = useState(false);
  const [modalAccept, setModalAccept] = useState(false);

  const onAcceptPatient = () => {
    dispatch(actions.handleAcceptPatient());
    accept({
      variables: {
        patientid: dataPatient.patient.id,
        type: "đã tiếp nhận bệnh nhân",
        doctorid: dataDoctor.id,
        medicalid: dataPatient.id,
      },
    }).then((res) => {
      if (res.data !== null) {
        dispatch(actions.handleAcceptPatientSuccess(res.data));
        addEmer({
          variables: {
            ownerid: dataDoctor.user.id,
            patientid: dataPatient.patient.id,
            displaydate: moment().utc(),
            isverified: true,
            doctorid: dataDoctor.id,
            notebackgroup: "",
            iscontact: "",
            note: "",
            sense: "",
            pluse: parseFloat(0),
            bloodpressure: parseFloat(0),
            bloodpressure2: parseFloat(0),
            spo2home: parseFloat(0),
            temperature: parseFloat(0),
            medicalpackage: "other",
            medicalpackagenote: "",
            resultdate: moment().utc(),
            methodbreathid: listBreath.find((item) => item.name === "Khí trời")
              .id,
            po2: parseFloat(0),
            other: "",
            listclinical: listClinical.find((item) => item.name === "").id,
            statusid: listEmerStatus.find((item) => item.name === "").id,
            notestatus: "",
            transferfrom: "",
            reason: "",
            reasondate: moment().utc(),
          },
        }).then((res) => {
          if (res.data !== null) {
            location.reload();
          }
        });
        setModalAccept(false);
      } else dispatch(actions.handleAcceptPatientFail());
    });
  };

  const [authenLink, authenLinkRes] = useMutation(authLink, {
    errorPolicy: "all",
  });

  useEffect(() => {
    if (props.location.state != null) {
      setDataPatient(props.location.state.sms.medical);
      setDataDoctor(props.location.state.sms.doctor);
    } else history.push("/dashboard");
  }, [props]);

  useEffect(() => {
    authenLink({
      variables: {
        authid: window.localStorage.getItem("idDoctor"),
        token: window.localStorage.getItem("idPatient"),
      },
    }).then((res) => {
      if (res.data !== null) {
        Cookies.set("access_token", res.data.redirect.access_token);
        dispatch(actions.handleFindLatestEmer());
        findLatest({ patientid: props.location.state.sms.medical.patient.id });
        history.push({ pathname: "/hot-contact", state: res.data.redirect });
      } else history.push("/404");
    });
  }, []);

  useEffect(() => {
    if (listClinical?.length === 0) {
      dispatch(actionsPatient.handleFindClinical());
      findClinical({});
    }
    if (listEmerStatus?.length === 0) {
      dispatch(actionsPatient.handleFindEmerStatus());
      findEmerStatus({});
    }
    if (listBreath?.length === 0) {
      dispatch(actionsPatient.handleFindBreathStatus());
      findBreath({});
    }
  }, []);

  return (
    <div>
      <Backdrop
        style={{ zIndex: "4488" }}
        className={classes.backdrop}
        open={
          resFindLatest.loading ||
          resFindAllClinical.loading ||
          resFindAllBreath.loading ||
          resFindAllEmerStatus.loading 
        }
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <div className={classes.logo} style={{ display: "flex", padding: "1em" }}>
        <a
          rel="noopener noreferrer"
          href="https://www.medx.vn"
          className={classes.logoLink}
          target="_blank"
          style={{ display: "flex" }}
        >
          <div className={classes.logoImage}>
            <img src={logo} alt="logo" className={classes.img} />
          </div>{" "}
          <a
            rel="noopener noreferrer"
            href="https://www.medx.vn"
            target="_blank"
            style={{
              alignSelf: "center",
              color: "#50AF50",
              fontWeight: "bold",
              marginLeft: "0.7em",
            }}
          >
            MEDX
          </a>
        </a>
        <div style={{ width: "80%", textAlign: "end", alignSelf: "center" }}>
          <a
            rel="noopener noreferrer"
            href="https://ump.edu.vn"
            target="_blank"
          >
            <img src={dhyd} alt="logo" className={classes.img} />
          </a>
        </div>
      </div>
      <Grid
        container
        spacing={2}
        style={{ padding: "1em", marginBottom: "15em" }}
      >
        <Grid item xs={12} sm={12} md={5} lg={5}>
          <div
            style={{
              color: "#0A3A34",
              textAlign: "center",
              fontSize: "1.2em",
              fontWeight: "bold",
              marginBottom: "1em",
            }}
          >
            THÔNG TIN BỆNH NHÂN
          </div>
          <div
            style={{ background: "#fff", borderRadius: "1em", padding: "1em" }}
          >
            <div
              style={{
                color: "#0A3A34",
                textAlign: "center",
                fontSize: "1.1em",
                fontWeight: "bold",
                marginBottom: "1em",
              }}
            >
              HÀNH CHÍNH
            </div>
            <Grid container spacing={1}>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                style={{ color: "#0A3A34" }}
              >
                <b>Họ tên: </b>
                {`${dataPatient?.patient?.lastname} ${dataPatient?.patient?.firstname}`}
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                style={{ color: "#0A3A34" }}
              >
                <b>Giới tính:</b>{" "}
                {dataPatient?.patient?.gender === "male"
                  ? "Nam"
                  : dataPatient?.patient?.gender === "female"
                  ? "Nữ"
                  : "Không xác định"}
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                style={{ color: "#0A3A34" }}
              >
                <b>Số điện thoại: </b> {dataPatient?.patient?.phone}
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                style={{ color: "#0A3A34" }}
              >
                <b>Địa chỉ: </b>
                {`${
                  dataPatient?.patient?.name
                    ? dataPatient?.patient.name + ","
                    : ""
                } ${
                  dataPatient?.patient?.ward
                    ? dataPatient?.patient.ward + ","
                    : ""
                } ${
                  dataPatient?.patient?.province
                    ? dataPatient?.patient.province + ","
                    : ""
                } ${
                  dataPatient?.patient?.city ? dataPatient?.patient.city : ""
                }`}
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                style={{ color: "#0A3A34" }}
              >
                <b>Bệnh mạn tính: </b> {dataPatient?.patient.beforesick}
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                style={{ color: "#0A3A34" }}
              >
                <b>Ghi chú về bệnh lý nền:</b>{" "}
                {dataPatient?.patient.notesatussick}
              </Grid>
              <div
                style={{
                  color: "#0A3A34",
                  textAlign: "center",
                  fontSize: "1.1em",
                  fontWeight: "bold",
                  margin: "0.5em 0",
                  width: "100%",
                }}
              >
                BỆNH ÁN
              </div>
              <Grid
                item
                xs={12}
                sm={12}
                md={12}
                lg={12}
                className={classes.medprofile}
              >
                <b>Đánh giá mức độ hiện tại: </b>{" "}
                {dgmdht[dataPatient?.attribute]}
              </Grid>
              <Grid
                item
                xs={12}
                sm={12}
                md={12}
                lg={12}
                className={classes.medprofile}
              >
                <b>Cách xử lý tại nhà: </b> {dataPatient?.guide}
              </Grid>
              <Grid
                item
                xs={12}
                sm={12}
                md={12}
                lg={12}
                className={classes.medprofile}
              >
                <b>Tình trạng bệnh lý khác kèm theo:</b> {dataPatient?.summary}
              </Grid>
              <Grid
                item
                xs={12}
                sm={12}
                md={12}
                lg={12}
                className={classes.medprofile}
              >
                <b>Đánh giá phân loại nguy cơ:</b>{" "}
                {dgpl[dataPatient?.riskassessment]}
              </Grid>
              <Grid
                item
                xs={12}
                sm={12}
                md={6}
                lg={6}
                className={classes.medprofile}
              >
                <b>Màu da và môi:</b> {mdvm[dataPatient?.color] || "Không rõ"}
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                className={classes.medprofile}
              >
                <b>Mạch:</b> {dataPatient?.pluse}
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                className={classes.medprofile}
              >
                <b>Chỉ số SpO2:</b> {dataPatient?.spo2home}
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                className={classes.medprofile}
              >
                <b>Huyết áp tâm thu:</b> {dataPatient?.bloodpressure}
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                className={classes.medprofile}
              >
                <b>Huyết áp tâm trương:</b> {dataPatient?.bloodpressure2}
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                className={classes.medprofile}
              >
                <b>Nhiệt độ:</b> {dataPatient?.temperature}
              </Grid>
            </Grid>
          </div>
        </Grid>
        <div style={{ textAlign: "end", width: "100%" }}>
          {!dataPatient?.issupport && (
            <Button
              className={classes.buttonAdd}
              onClick={() => setModalAccept(true)}
            >
              TIẾP NHẬN
            </Button>
          )}
          {dataPatient?.issupport && (
            <Button
              className={classes.buttonAdd}
              onClick={() => dispatch(actions.handleOpenRemoteForm())}
            >
              CẬP NHẬT BỆNH ÁN CẤP CỨU
            </Button>
          )}
        </div>
      </Grid>
      <Modal
        open={isOpenRemoteForm}
        style={{ maxHeight: "80vh", maxWidth: "95vw" }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          className={classes.rssModal}
          item
          xs={12}
          sm={12}
          md={12}
        >
          <MedicalRemote data={{dataDoctor: dataDoctor, dataPatient: dataPatient}}></MedicalRemote>
        </Grid>
      </Modal>
      <ModalConfirm
        title="Thông báo"
        open={modalAccept}
        description="Xác nhận tiếp nhận bệnh nhân"
        disableBtn={acceptRes.loading}
        handleOk={() => {
          onAcceptPatient();
        }}
        handleClose={() => {
          setModalAccept(false);
        }}
      ></ModalConfirm>
      <div style={{ position: "fixed", width: "100%" }}>
        <FooterOutSide></FooterOutSide>
      </div>
      {isSuccess !== null && (
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={isOpenNoti}
          autoHideDuration={3000}
          onClose={() => dispatch(actions.handleCloseRemoteNoti())}
        >
          <Alert
            severity={
              isSuccess === true
                ? "success"
                : isSuccess === false
                ? "error"
                : ""
            }
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </div>
  );
}
