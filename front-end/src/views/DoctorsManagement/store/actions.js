import { createAction } from "redux-actions";

export const handleOpenForm = createAction("DOCTOR/OPEN_FORM");
export const handleCloseForm = createAction("DOCTOR/CLOSE_FORM");

export const handleOpenDoctorNoti = createAction("DOCTOR/OPEN_NOTI");
export const handleCloseDoctorNoti = createAction("DOCTOR/CLOSE_NOTI");

export const handleAddDoctor = createAction("DOCTOR/ADD_DOCTOR");
export const handleAddDoctorSuccess = createAction("DOCTOR/ADD_DOCTOR_SUCCESS");
export const handleAddDoctorFail = createAction("DOCTOR/ADD_DOCTOR_FAIL");

export const handleFindAllDoctor = createAction("DOCTOR/FIND_ALL_DOCTOR");
export const handleFindAllDoctorSuccess = createAction("DOCTOR/FIND_ALL_DOCTOR_SUCCESS");
export const handleFindAllDoctorFail = createAction("DOCTOR/FIND_ALL_DOCTOR_FAIL");

export const handleFindOneDoctor = createAction("DOCTOR/FIND_ONE_DOCTOR");
export const handleFindOneDoctorSuccess = createAction("DOCTOR/FIND_ONE_DOCTOR_SUCCESS");
export const handleFindOneDoctorFail = createAction("DOCTOR/FIND_ONE_DOCTOR_FAIL");

export const handleEditDoctor = createAction("DOCTOR/EDIT_DOCTOR");
export const handleEditDoctorSuccess = createAction("DOCTOR/EDIT_DOCTOR_SUCCESS");
export const handleEditDoctorFail = createAction("DOCTOR/EDIT_DOCTOR_FAIL");

export const handleOpenAddress = createAction("DOCTOR/OPEN_ADDRESS");
export const handleCloseAddress = createAction("DOCTOR/CLOSE_ADDRESS");

export const handleAddAddress = createAction("ADDRESS/ADD_ADDRESS");
export const handleAddAddressSuccess = createAction("ADDRESS/ADD_ADDRESS_SUCCESS");
export const handleAddAddressFail = createAction("ADDRESS/ADD_ADDRESS_FAIL");

export const handleUpdateAddress = createAction("ADDRESS/UPDATE_ADDRESS");
export const handleUpdateAddressSuccess = createAction("ADDRESS/UPDATE_ADDRESS_SUCCESS");
export const handleUpdateAddressFail = createAction("ADDRESS/UPDATE_ADDRESS_FAIL");

export const handleBlockDoctor = createAction("DOCTOR/BLOCK_DOCTOR");
export const handleBlockDoctorSuccess = createAction("DOCTOR/BLOCK_DOCTOR_SUCCESS");
export const handleBlockDoctorFail = createAction("DOCTOR/BLOCK_DOCTOR_FAIL");