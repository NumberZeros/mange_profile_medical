/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "Doctors";

const initialState = freeze({
  listDoctors: [],
  doctor: null,
  isAuthenticated: false,
  error: null,
  isWrong: null,
  isSuccess: null,
  isLoading: null,
  isOpenNoti: true,
  isOpenForm: false,
  isOpenAddress: false,
  titleNoti: "",
  idLocation: "",
  idUser: "",
  message: "",
  isSuccessAddress: null,
  pagination: null,
});

export default handleActions(
  {
    // [actions.handleOpenDoctorNoti]: (state, actions) => {
    //   return freeze({
    //     ...state,
    //     isOpenNoti: true,
    //   });
    // },
    [actions.handleCloseDoctorNoti]: (state, actions) => {
      return freeze({
        ...state,
        isOpenNoti: false,
      });
    },
    [actions.handleOpenAddress]: (state, actions) => {
      return freeze({
        ...state,
        isOpenAddress: true,
        idUser: actions.payload,
      });
    },
    [actions.handleCloseAddress]: (state, actions) => {
      return freeze({
        ...state,
        isOpenAddress: false,
      });
    },
    [actions.handleOpenForm]: (state, actions) => {
      return freeze({
        ...state,
        isOpenForm: true,
        idLocation: actions.payload,
      });
    },
    [actions.handleCloseForm]: (state, actions) => {
      return freeze({
        ...state,
        isOpenForm: false,
        idLocation: "",
        idUser: "",
      });
    },
    [actions.handleAddDoctor]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleAddDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listDoctors: [
          { ...actions.payload.data.create_doctor, addresses: [] },
          ...state.listDoctors,
        ],
        isLoading: false,
        isSuccess: true,
        message: "Thêm thông tin thành công",
      });
    },
    [actions.handleAddDoctorFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
        message: actions.payload.includes("Email")
          ? "Thêm thất bại, email đã tồn tại!"
          : actions.payload.includes("Certificate")
          ? "Thêm thất bại, chứng chỉ đã tồn tại"
          : "Thêm thất bại, số điện thoại không hợp lệ hoặc đã tồn tại",
      });
    },
    [actions.handleFindAllDoctor]: (state, actions) => {
      return freeze({
        ...state,
        listDoctors: [],
        isLoading: true,
      });
    },
    [actions.handleFindAllDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        listDoctors: actions.payload.find_all_doctor_use_pagination.doctors,
        isLoading: false,
        pagination: {
          totalPage: Math.ceil(
            actions.payload.find_all_doctor_use_pagination.total /
              actions.payload.find_all_doctor_use_pagination.take
          ),
        },
      });
    },
    [actions.handleFindAllDoctorFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
      });
    },
    [actions.handleEditDoctor]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleEditDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        doctor: {
          ...state.doctor,
          find_one_doctor: actions.payload.data.update_doctor,
        },
        listDoctors: state.listDoctors.map((item) => {
          if (item.id === actions.payload.data.update_doctor.id) {
            return {
              ...actions.payload.data.update_doctor,
            };
          } else return item;
        }),
        isLoading: false,
        isSuccess: true,
        message: "Cập nhật thông tin thành công",
      });
    },
    [actions.handleEditDoctorFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
        message: actions.payload.includes("Email")
          ? "Cập nhật thất bại, email đã tồn tại!"
          : actions.payload.includes("Certificate")
          ? "Cập nhật thất bại, chứng chỉ đã tồn tại"
          : "Cập nhật thất bại, số điện thoại không hợp lệ hoặc đã tồn tại",
      });
    },
    [actions.handleFindOneDoctor]: (state, actions) => {
      return freeze({
        ...state,
        isLoading:
          actions.payload === state.doctor?.find_one_doctor.id ? false : true,
      });
    },
    [actions.handleFindOneDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        doctor: actions.payload,
        isLoading: false,
      });
    },
    [actions.handleFindOneDoctorFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    [actions.handleAddAddress]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleAddAddressSuccess]: (state, actions) => {
      const newAddress = [
        {
          city: actions.payload.data.add_address.city,
          id: actions.payload.data.add_address.id,
          name: actions.payload.data.add_address.name,
          province: actions.payload.data.add_address.province,
          ward: actions.payload.data.add_address.ward,
        },
      ];
      return freeze({
        ...state,
        doctor: {
          ...state.doctor,
          find_one_doctor: {
            ...state.doctor.find_one_doctor,
            user: {
              ...state.doctor.find_one_doctor.user,
              addresses: [
                ...newAddress,
                ...state.doctor.find_one_doctor.user.addresses,
              ],
            },
          },
        },
        isOpenAddress: false,
        isSuccessAddress: true,
        isLoading: false,
        message: "Thêm địa chỉ thành công",
        isOpenNoti: true,
      });
    },
    [actions.handleAddAddressFail]: (state, actions) => {
      return freeze({
        ...state,
        isOpenAddress: false,
        isLoading: false,
        isSuccessAddress: false,
        message: "Thêm địa chỉ thất bại",
        isOpenNoti: true,
      });
    },
    [actions.handleUpdateAddress]: (state, actions) => {
      return freeze({
        ...state,
      });
    },
    [actions.handleUpdateAddressSuccess]: (state, actions) => {
      return freeze({
        ...state,
        doctor: {
          ...state.doctor,
          find_one_doctor: {
            ...state.doctor.find_one_doctor,
            user: {
              ...state.doctor.find_one_doctor.user,
              addresses: state.doctor.find_one_doctor.user.addresses.map(item=>{
                if(item.id === actions.payload.data.put_address.id){
                  return{
                    ...item,
                    city: actions.payload.data.put_address.city,
                    id: actions.payload.data.put_address.id,
                    name: actions.payload.data.put_address.name,
                    streetline1: actions.payload.data.put_address.streetline1,
                    streetline2: actions.payload.data.put_address.streetline2,
                    province: actions.payload.data.put_address.province,
                    ward: actions.payload.data.put_address.ward,
                  }
                }
                else return item
              })
            },
          },
        },
        // isOpenAddress: false,
        isSuccess: true,
        message: "Cập nhật địa chỉ thành công",
      });
    },
    [actions.handleUpdateAddressFail]: (state, actions) => {
      return freeze({
        ...state,
        // isOpenAddress: false,
        isSuccess: false,
        message: "Cập nhật địa chỉ thất bại",
      });
    },
    [actions.handleBlockDoctor]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleBlockDoctorSuccess]: (state, actions) => {
      return freeze({
        ...state,
        doctor: {
          ...state.doctor,
          find_one_doctor: actions.payload.data.handle_block_doctor,
        },
        listDoctors: state.listDoctors.map((item) => {
          if (item.id === actions.payload.data.handle_block_doctor.id) {
            return {
              ...actions.payload.data.handle_block_doctor,
            };
          } else return item;
        }),
        isLoading: false,
        isSuccess: true,
        message: "Cập nhật thông tin thành công",
      });
    },
    [actions.handleBlockDoctorFail]: (state, actions) => {
      return freeze({
        ...state,
        isLoading: false,
        isSuccess: false,
        message: actions.payload.includes("Email")
          ? "Cập nhật thất bại, email đã tồn tại!"
          : actions.payload.includes("Certificate")
          ? "Cập nhật thất bại, chứng chỉ đã tồn tại"
          : "Cập nhật thất bại, số điện thoại không hợp lệ hoặc đã tồn tại",
      });
    },
  },
  initialState
);
