import { gql } from "@apollo/client";

export const addDoctor = gql`
  mutation CreateDoctor(
    $adminid: String!
    $firstname: String!
    $lastname: String!
    $email: String!
    $phone: String!
    $dob: DateTime!
    $certificate: String!
    $gender: String!
  ) {
    create_doctor(
      input: {
        adminid: $adminid
        firstname: $firstname
        lastname: $lastname
        email: $email
        certificate: $certificate
        phone: $phone
        dob: $dob
        gender: $gender
      }
    ) {
      id
      inactive
      certificate
      user {
        firstname
        lastname
        email
        phone
        dob
        gender
      }
    }
  }
`;

export const updateAddress = gql`
  mutation UpdateAddress(
    $id: String!
    $name: String!
    $streetline1: String!
    $streetline2: String!
    $ward: String!
    $province: String!
    $city: String!
    $user: String!
  ) {
    put_address(
      input: {
        name: $name
        streetline1: $streetline1
        streetline2: $streetline2
        ward: $ward
        province: $province
        city: $city
        id: $id
        user: $user
      }
    ) {
      id
      name
      streetline1
      streetline2
      ward
      province
      city
    }
  }
`;

export const editDoctor = gql`
  mutation EditDoctor(
    $adminid: String!
    $id: String!
    $firstname: String!
    $lastname: String!
    $email: String!
    $phone: String!
    $isverified: Boolean!
    $dob: DateTime!
    $inactive: Boolean!
    $certificate: String!
    $gender: String!
  ) {
    update_doctor(
      input: {
        id: $id
        adminid: $adminid
        firstname: $firstname
        lastname: $lastname
        email: $email
        isverified: $isverified
        phone: $phone
        dob: $dob
        inactive: $inactive
        certificate: $certificate
        gender: $gender
      }
    ) {
      id
      inactive
      isverified
      certificate
      user {
        firstname
        id
        lastname
        email
        phone
        dob
        gender
        addresses {
          id
          name
          ward
          province
          city
        }
      }
    }
  }
`;

export const changeStatus = gql`
  mutation EditDoctor(
    $adminid: String!
    $id: String!
    $inactive: Boolean!
    $firstname: String!
    $lastname: String!
    $email: String!
    $phone: String!
    $dob: DateTime!
    $certificate: String!
    $gender: String!
  ) {
    update_doctor(
      input: {
        id: $id
        adminid: $adminid
        inactive: $inactive
        firstname: $firstname
        lastname: $lastname
        email: $email
        certificate: $certificate
        phone: $phone
        dob: $dob
        gender: $gender
      }
    ) {
      id
      inactive
      certificate
      isverified
      user {
        firstname
        lastname
        email
        phone
        dob
        gender
        addresses {
          id
          name
          ward
          province
          city
        }
      }
    }
  }
`;

export const findAllDoctors = gql`
  query FindAllDoctors($adminid: String!) {
    find_all_doctor(adminid: $adminid) {
      id
      user {
        id
        firstname
        lastname
        email
        phone
        dob
        gender
      }
      inactive
      isverified
      certificate
    }
  }
`;


export const findAllLessDoctors = gql`
query FindAllLessDoctors($adminid: String!) {
  find_all_doctor(adminid: $adminid) {
    id
    user {
      id
      firstname
      lastname
      email
      phone
    }
    isverified
  }
}
`;


export const findAdvanceDoctor = gql`
  query FindAdvanceDoctor(
    $adminid: String!
    $take: Float!
    $skip: Float!
    $firstname: String!
    $lastname: String!
    $phone: String!
  ) {
    find_all_doctor_use_pagination(
      pagination: {
        adminid: $adminid
        take: $take
        skip: $skip
        firstname: $firstname
        lastname: $lastname
        phone: $phone
      }
    ) {
      take
      skip
      total
      doctors {
        id
        user {
          id
          firstname
          lastname
          email
          phone
          dob
          gender
        }
        inactive
        isverified
        certificate
      }
    }
  }
`;

export const findOneDoctor = gql`
  query FindOneDoctor($adminid: String!, $id: String!) {
    find_one_doctor(adminid: $adminid, id: $id) {
      id
      user {
        id
        firstname
        lastname
        email
        phone
        dob
        gender
        addresses {
          id
          name
          ward
          province
          city
        }
      }
      isverified
      inactive
      certificate
    }
  }
`;

export const addAddress = gql`
  mutation AddAddress(
    $name: String!
    $ward: String!
    $province: String!
    $city: String!
    $user: String!
  ) {
    add_address(
      input: {
        name: $name
        ward: $ward
        province: $province
        city: $city
        user: $user
      }
    ) {
      id
      name
      ward
      province
      city
    }
  }
`;

export const blockDoctor = gql`
  mutation BlockDoctor($adminid: String!, $id: String!, $inactive: Boolean!) {
    handle_block_doctor(
      input: { id: $id, adminid: $adminid, inactive: $inactive }
    ) {
      id
      inactive
      isverified
      certificate
      user {
        firstname
        id
        lastname
        email
        phone
        dob
        gender
        addresses {
          id
          name
          ward
          province
          city
        }
      }
    }
  }
`;
