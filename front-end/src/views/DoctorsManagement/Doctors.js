import React, { useEffect, useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Drawer from "@material-ui/core/Drawer";
import DoctorForm from "./DoctorForm";
import { Button } from "@material-ui/core";
import * as actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import queryString from "query-string";
import * as apis from "./store/api";
import getAuthorize from "constants/authen/authen";
import moment from "moment";
import { useMutation } from "@apollo/client";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import { useMyLazyQuery } from "hooks/useMyLazyQuery";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import EditIcon from "@material-ui/icons/Edit";
import { DataGrid } from "@material-ui/data-grid";
import Switch from "@material-ui/core/Switch";
import Modal from "@material-ui/core/Modal";
import AddressForm from "constants/Address/AddressForm";
import ModalConfirm from "components/Modal/ModalConfirm";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import PaginationCustom from "components/Pagination/Pagination";
import { useForm, Controller } from "react-hook-form";
import Grid from "@material-ui/core/Grid";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
function getModalStyle() {
  return {
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`,
  };
}
const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      marginTop: "0",
      marginBottom: "0",
      fontSize: "1rem !important",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    fontSize: "1.5rem !important",
    "& small": {
      color: "#777",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  drawer: {
    width: "50vw",
  },
  backdrop: {
    zIndex: 4799,
    color: "#fff",
  },
  topBar: {
    background: "#50AF50 !important",
    display: "flex",
    justifyContent: "space-between",
  },
  searchBar: {
    background: "#ddd",
    borderRadius: "0.4em",
    border: "none !important",
    height: "fit-content",
    width: "-webkit-fill-available",
    marginTop: "0.3em",
  },
  headerTable: {
    color: "#0A3A34",
    fontWeight: "bold",
    fontSize: "1rem",
  },
  bodyCell: {
    fontSize: "0.9rem",
    paddingLeft: "1em !important",
    "&:nth-child(1)": {
      paddingLeft: "0 !important",
    },
  },
  buttonAdd: {
    background: "#37774B",
    marginBottom: "0.1em",
    color: "#eee",
    padding: "1em",
    marginRight: "1em",
    marginTop: "0.4em",
    marginLeft: "1em",
  },
  btnDel: {
    color: "red",
    minWidth: "auto",
  },
  btnEdit: {
    color: "#37774B",
    minWidth: "auto",
  },
  buttonClear: {
    background: "#37774B",
    marginBottom: "0.1em",
    color: "#eee",
    padding: "1em",
    marginLeft: "0.2em",
    height: "max-content",
    "&:hover": {
      background: "#50AF50",
    },
  },
  textAdvance: {
    marginRight: "0.3em",
  },
};

const useStyles = makeStyles(styles);
const FECTH_ALL_DOCTOR = apis.findAdvanceDoctor;
const FIND_ONE_DOCTOR = apis.findOneDoctor;
const EDIT_DOCTOR = apis.blockDoctor;

export default function Doctors() {
  getAuthorize();
  const [modalStyle] = React.useState(getModalStyle);

  const { isOpenAddress } = useSelector((state) => state["Doctors"]);
  const {
    listDoctors,
    isOpenForm,
    idLocation,
    doctor,
    isSuccess,
    message,
    pagination
  } = useSelector((state) => state["Doctors"]);
  const { register, handleSubmit, setValue, control } = useForm();

  const [doctorId, setDoctorId] = useState("");
  const [listDoctorMapped, setListDoctorMapped] = useState([]);
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [status, setStatus] = useState(null);

  const [selectedUser, setSelectedUser] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [openNoti, setOpenNoti] = useState(false);
  const [onLoading, setOnloading] = useState(false);
  // const { loading, error, data, refetch } = useQuery(FECTH_ALL_DOCTOR, {
  //   variables: { adminid: window.sessionStorage.getItem("id") },
  // });
  const [pageSize, setPageSize] = useState(5);
  const [page, setPage] = useState(0);
  const handleChangePage = (e) => {
    setPage(e);
  };

  const handleChangeSize = (e) => {
    setPageSize(e);
  };
  const [query, setQuery] = useState({
    firstname: "",
    lastname: "",
    dob: "",
    updatedat: "DESC",
    phone: "",
  });
  const [findDoctors, dataDoctors] = useMyLazyQuery(FECTH_ALL_DOCTOR, {
    onCompleted: () => {
      if (dataDoctors.data) {
        dispatch(actions.handleFindAllDoctorSuccess(dataDoctors.data));
      } else dispatch(actions.handleFindAllDoctorFail());
    },
    fetchPolicy: "network-only",
  });
  const [changeStatus, resEdit] = useMutation(EDIT_DOCTOR, {
    errorPolicy: "all",
  });
  // const [findOne, dataDoctor] = useLazyQuery(FIND_ONE_DOCTOR);
  const [findOne, dataDoctor] = useMyLazyQuery(FIND_ONE_DOCTOR, {
    onCompleted: () => {
      if (dataDoctor.data) {
        dispatch(actions.handleFindOneDoctorSuccess(dataDoctor.data));
      } else dispatch(actions.handleFindOneDoctorFail());
      dataDoctor.refetch();
    },
    fetchPolicy: "network-only",
  });

  useEffect(()=>{
    window.scrollTo({ top: 0, behavior: "smooth" });
  },[])
  
  useEffect(() => {
    if (history.location.search && !isOpenForm) {
      if (history.location.search === "?form=a")
        dispatch(actions.handleOpenForm(true));
      else {
        history.push(
          `/admin/doctors-management?form=${
            queryString.parse(history.location.search).form
          }`
        );
        dispatch(
          actions.handleFindOneDoctor(
            queryString.parse(history.location.search).form
          )
        );
        findOne({
          adminid: window.sessionStorage.getItem("id"),
          id: queryString.parse(history.location.search).form,
        });
        dispatch(actions.handleOpenForm(true));
      }
    } else if (history.location.search === "")
      dispatch(actions.handleCloseForm(true));
  }, [history.location]);

  const onAdvanceSearch = (dataForm) => {
    setQuery({
      ...query,
      lastname: dataForm.lastname || "",
      firstname: dataForm.firstname || "",
      phone: dataForm.phone || "",
    });
  };

  useEffect(() => {
    dispatch(actions.handleFindAllDoctor())
    findDoctors({
      ...query,
      adminid: window.sessionStorage.getItem("id"),
      take: 5,
      skip: 0,
      firstname: "",
      lastname: "",
      phone: "",
    })
  }, []);
  useEffect(() => {
    findDoctors({
      ...query,
      adminid: window.sessionStorage.getItem("id"),
      take: pageSize,
      skip: 0,
      firstname: query.firstname,
      lastname: query.lastname,
      phone: query.phone,
    });
    setPage(0);
  }, [pageSize]);

  useEffect(() => {
    findDoctors({
      ...query,
      adminid: window.sessionStorage.getItem("id"),
      take: pageSize,
      skip: page,
      firstname: query.firstname,
      lastname: query.lastname,
      phone: query.phone,
    });
  }, [page]);

  useEffect(() => {
    findDoctors({
      ...query,
      adminid: window.sessionStorage.getItem("id"),
      take: pageSize,
      skip: 0,
      firstname: query.firstname,
      lastname: query.lastname,
      phone: query.phone,
    });
    setPage(0);
  }, [query]);
  
  const columns = [
    {
      field: "name",
      headerName: "Họ và tên",
      flex: 0.5,
      minWidth: 200,
      headerClassName: classes.headerTable,
      // eslint-disable-next-line react/display-name
      renderCell: (params) => (
        <div>
          <span
            style={{cursor:"pointer"}}
            onClick={() => {
              window.sessionStorage.setItem("idLocation", params.id);
              dispatch(actions.handleFindOneDoctor(params.id));
              setDoctorId(params.id);
              dispatch(actions.handleOpenForm(doctorId));
              history.push(
                `/admin/doctors-management?form=${window.sessionStorage.getItem(
                  "idLocation"
                )}`
              );
              findOne({
                adminid: window.sessionStorage.getItem("id"),
                id: params.id,
              });
            }}
          >
            {params.row.name}
          </span>
        </div>
      ),
    },
    {
      field: "dob",
      headerName: "Ngày sinh",
      minWidth: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "email",
      headerName: "Email",
      minWidth: 250,
      flex: 0.5,
      headerClassName: classes.headerTable,
    },
    {
      field: "phone",
      headerName: "Số điện thoại",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "inactive",
      headerName: "Trạng thái",
      minWidth: 150,
      headerClassName: classes.headerTable,
      // eslint-disable-next-line react/display-name
      renderCell: (params) => (
        <Switch
          color="primary"
          checked={!params.row.inactive}
          onChange={(e) => {
            setIsModalOpen(true);
            setStatus(!e.target.checked);
            setSelectedUser(params);
          }}
        ></Switch>
      ),
    },
    {
      field: "certificate",
      headerName: "Chứng chỉ",
      width: 200,
      headerClassName: classes.headerTable,
    },
    {
      field: "actions",
      headerName: `${" "}`,
      width: 80,
      headerClassName: classes.headerTable,
      // eslint-disable-next-line react/display-name
      renderCell: (params) => (
        <div>
          <Button
            className={classes.btnEdit}
            onClick={() => {
              window.sessionStorage.setItem("idLocation", params.id);
              dispatch(actions.handleFindOneDoctor(params.id));
              setDoctorId(params.id);
              dispatch(actions.handleOpenForm(doctorId));
              history.push(
                `/admin/doctors-management?form=${window.sessionStorage.getItem(
                  "idLocation"
                )}`
              );
              findOne({
                adminid: window.sessionStorage.getItem("id"),
                id: params.id,
              });
            }}
          >
            <EditIcon></EditIcon>
          </Button>
          {/* <Button className={classes.btnDel}>
            <DeleteIcon></DeleteIcon>
          </Button> */}
        </div>
      ),
    },
  ];
  return (
    <GridContainer>
      {/* <Backdrop className={classes.backdrop} open={loading || onLoading}>
        <CircularProgress color="inherit" />
      </Backdrop> */}
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary" className={classes.topBar}>
            <div style={{ flex: 0.5 }}>
              <h4 className={classes.cardTitleWhite}>Danh sách bác sĩ</h4>
              <p className={classes.cardCategoryWhite}>Medx</p>
            </div>
            <div
              style={{
                alignSelf: "center",
                flex: "1",
                textAlign: "end",
                display: "flex",
              }}
            >
              <TextField
                placeholder="Tìm kiếm bác sĩ"
                id="outlined-start-adornment"
                className={classes.searchBar}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <SearchIcon></SearchIcon>
                    </InputAdornment>
                  ),
                }}
                variant="outlined"
              />
              <Button
                className={classes.buttonAdd}
                onClick={() => {
                  dispatch(actions.handleOpenForm(true));
                  history.push("/admin/doctors-management?form=a");
                }}
              >
                <PersonAddIcon></PersonAddIcon>
              </Button>
            </div>
          </CardHeader>
          <CardBody>
            <Grid item xs={12} sm={12} md={12}>
              <form
                key="address_form"
                className={classes.form}
                style={{
                  padding: "0",
                  background: "#eee",
                  borderRadius: "0.4em",
                }}
                onSubmit={handleSubmit(onAdvanceSearch)}
                noValidate
              >
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0.5em",
                    whiteSpace: "nowrap",
                  }}
                >
                  <div
                    style={{
                      maxWidth: "fit-content",
                      overflow: "auto",
                      marginRight: "1em",
                    }}
                  >
                    <Controller
                      control={control}
                      name="lastname"
                      margin="normal"
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          className={classes.textAdvance}
                          margin="normal"
                          {...register("lastname")}
                          label="Họ và tên lót"
                          value={value}
                          onChange={onChange}
                        />
                      )}
                    />
                    <Controller
                      control={control}
                      name="firstname"
                      margin="normal"
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          className={classes.textAdvance}
                          margin="normal"
                          {...register("firstname")}
                          label="Tên"
                          value={value}
                          onChange={onChange}
                        />
                      )}
                    />
                    <Controller
                      control={control}
                      name="phone"
                      margin="normal"
                      render={({ field: { onChange, onBlur, value, ref } }) => (
                        <TextField
                          variant="outlined"
                          className={classes.textAdvance}
                          margin="normal"
                          {...register("phone")}
                          label="Số điện thoại"
                          value={value}
                          onChange={onChange}
                        />
                      )}
                    />
                  </div>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <Button
                      type="submit"
                      variant="contained"
                      className={classes.buttonClear}
                    >
                      <SearchIcon></SearchIcon>
                    </Button>
                  </div>
                </div>
              </form>
            </Grid>
            <div
              style={{overflowY: "auto", width: "100%" }}
            >
              <div style={{ display: "flex", height: "100%" }}>
                <DataGrid
                  loading={dataDoctors.loading || onLoading}
                  rows={listDoctors?.map((item) => {
                    return {
                      ...item,
                      id: item.id,
                      name: `${item.user.lastname} ${item.user.firstname} `,
                      dob: moment(item.user.dob).format("DD-MM-YYYY"),
                      email: item.user.email,
                      phone: item.user.phone,
                    };
                  })}
                  getCellClassName={() => {
                    return classes.bodyCell;
                  }}
                  style={{overflow:"auto"}}
                  autoHeight={true}
                  columns={columns}
                  checkboxSelection
                  hideFooterPagination={true}
                  disableSelectionOnClick
                />
              </div>
            </div>
            <PaginationCustom
                handleChangeSize={(e) => handleChangeSize(e)}
                handleChangePage={(e) => handleChangePage(e)}
                count={pagination?.totalPage}
                page={page}
              ></PaginationCustom>
          </CardBody>
        </Card>
      </GridItem>
      <Drawer
        anchor="right"
        open={isOpenForm}
        // onClose={() => {
        //   dispatch(actions.handleCloseForm(true));
        //   history.push("/admin/doctors-management");
        // }}
      >
        <DoctorForm showNoti={() => setOpenNoti(true)}></DoctorForm>
      </Drawer>
      <Modal
        open={isOpenAddress}
        style={{
          maxWidth: "30em",
          position: "absolute",
          left: "calc(50% - 300px)",
          top: "calc(50% - 25em)",
        }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <AddressForm></AddressForm>
      </Modal>
      <ModalConfirm
        title="Xác nhận thay đổi"
        open={isModalOpen}
        disableBtn={resEdit.loading}
        description="Bạn có muốn xác nhận thay đổi?"
        handleOk={() => {
          dispatch(actions.handleBlockDoctor());
          changeStatus({
            variables: {
              id: selectedUser.id,
              adminid: window.sessionStorage.getItem("id"),
              inactive: status,
            },
          })
            .then((res) => {
              dispatch(actions.handleBlockDoctorSuccess(res));
              dispatch(actions.handleCloseForm(false));
              setIsModalOpen(false);
              setOpenNoti(true);
              // setOnloading(false);
            })
            .catch((err) => {
              dispatch(actions.handleBlockDoctorFail(err));
              setIsModalOpen(false);
              setOpenNoti(true);
            });
        }}
        handleClose={() => {
          setIsModalOpen(false);
        }}
      ></ModalConfirm>
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={openNoti}
        autoHideDuration={3000}
        onClose={() => setOpenNoti(false)}
      >
        <Alert severity={isSuccess ? "success" : "error"}>{message}</Alert>
      </Snackbar>
    </GridContainer>
  );
}
