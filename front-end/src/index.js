import React from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import { Provider } from "react-redux";

// core components
import Admin from "layouts/Admin.js";
import SignIn from "./views/Authenticate/Authenticate";
import Patients from "./views/PatientsManagement/Patients";
import ChangePassword from "./views/Authenticate/ChangePassword";
//apollo
import "assets/css/material-dashboard-react.css?v=1.10.0";
import { ApolloProvider } from "@apollo/client";
import { client } from "./redux/config";
import { ConnectedRouter } from "connected-react-router";
import configureStore from "./redux/store";
import RemoteGroup from "views/RemoteGroup/RemoteGroup";
import ChooseServer from "views/Authenticate/ChooseServer";
import PhoneSignIn from "views/Authenticate/AuthenticatePhone";
import NotFound from "views/NotFound/NotFound";
import AuthenLink from "views/RemoteGroup/AuthenLink";

function App() {
  const { store, history } = configureStore([]);

  const idDoctor = window.location.pathname.replace("/", "").trim();
  const idPatient = window.location.search.replace("?id=", "").trim();

  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <BrowserRouter>
            <Switch>
              <Route path="/admin" component={Admin} />
              <Route path="/detect" component={ChooseServer} />
              <Route path="/login" component={SignIn} />
              <Route path="/authen-link">
                <AuthenLink auth={{idDoctor: idDoctor, idPatient:idPatient}}></AuthenLink>
              </Route>
              <Route path="/hot-contact" component={RemoteGroup} />
              <Route path="/admin/patients" component={Patients} />
              <Route path="/change-password" component={ChangePassword} />
              <Route path="/404" component={NotFound} />
              <Redirect
                from="/:id"
                to={{ pathname: "/authen-link"}}
              />
              <Redirect
                from="/"
                to="/admin/dashboard"
              />
            </Switch>
          </BrowserRouter>
        </ConnectedRouter>
      </Provider>
    </ApolloProvider>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));
