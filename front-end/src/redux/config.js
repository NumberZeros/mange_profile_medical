import {
  ApolloClient,
  HttpLink,
  ApolloLink,
  InMemoryCache,
  concat,
} from "@apollo/client";
import Cookies from "js-cookie";
import cookie from "react-cookies";
import { Q8, QBT, Q10, QO } from "../config-url";

const svid = Cookies.get("sv")?.toString();

const httpLink = new HttpLink({
  uri:
    svid === "1"
      ? QBT.GRAPHQL_URL
      : svid === "2"
      ? Q8.GRAPHQL_URL
      : svid === "3"
      ? Q10.GRAPHQL_URL
      : svid === "4"
      ? QO.GRAPHQL_URL
      : "",
});

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  let token = Cookies.get("access_token")
  // let token = window.sessionStorage.getItem("access_token");
  operation.setContext(({ headers = {} }) => ({
    headers: {
      ...headers,
      authorization: token ? "Bearer " + `${token}` : null,
    },
  }));
  return forward(operation);
});

export const client = new ApolloClient({
  link: concat(authMiddleware, httpLink),
  cache: new InMemoryCache()
});
