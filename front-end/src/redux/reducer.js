import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import Login, { name as nameOfLogin } from "../views/Authenticate/store/reducer";
import Doctors, { name as nameOfDoctors } from "../views/DoctorsManagement/store/reducer";
import Patients, { name as nameOfPatients } from "../views/PatientsManagement/store/reducer";
import Addresses, { name as nameOfAddresses } from "../constants/Address/store/reducer";
import Intergrate, { name as nameOfIntergrate } from "../views//ImportCsv/store/reducer";
import Dashboard, { name as nameOfDashboard } from "../views/Dashboard/store/reducer";
import Group, { name as nameOfGroup } from "../views/DoctorGroup/store/reducer";
import Remote, { name as nameOfRemote } from "../views/RemoteGroup/store/reducer";

export const staticReducers = {
  [nameOfLogin]: Login,
  [nameOfDoctors]: Doctors,
  [nameOfPatients]: Patients,
  [nameOfAddresses]: Addresses,
  [nameOfIntergrate]: Intergrate,
  [nameOfDashboard]: Dashboard,
  [nameOfGroup]: Group,
  [nameOfRemote]: Remote,
};

export default (history) =>
  combineReducers({
    ...staticReducers,
    router: connectRouter(history),
  });


  //Group