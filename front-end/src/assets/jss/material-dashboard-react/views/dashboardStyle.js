import {
  successColor,
  whiteColor,
  grayColor,
  hexToRgb,
} from "assets/jss/material-dashboard-react.js";

const dashboardStyle = {
  successText: {
    color: successColor[0],
  },
  upArrowCardCategory: {
    width: "16px",
    height: "16px",
  },
  stats: {
    color: grayColor[0],
    display: "inline-flex",
    fontSize: "12px",
    lineHeight: "22px",
    "& svg": {
      top: "4px",
      width: "16px",
      height: "16px",
      position: "relative",
      marginRight: "3px",
      marginLeft: "3px",
    },
    "& .fab,& .fas,& .far,& .fal,& .material-icons": {
      top: "4px",
      fontSize: "16px",
      position: "relative",
      marginRight: "3px",
      marginLeft: "3px",
    },
  },
  cardCategory: {
    color: grayColor[0],
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    paddingTop: "10px",
    marginBottom: "0",
  },
  cardCategoryWhite: {
    color: "rgba(" + hexToRgb(whiteColor) + ",.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0",
  },
  cardTitle: {
    color: grayColor[2],
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: grayColor[1],
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  cardTitleWhite: {
    color: whiteColor,
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: grayColor[1],
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  searchBar: {
    background: "#ddd",
    borderRadius: "0.4em",
    border: "none !important",
    height: "fit-content",
    width: "-webkit-fill-available",
    marginTop: "0.3em",
  },
  topBar: {
    background: "#50AF50 !important",
    display: "flex",
    justifyContent: "space-between",
  },
  btnEdit: {
    color: "#37774B",
    minWidth: "auto",
  },  
  bodyCell: {
    fontSize: "0.9rem",
    paddingLeft: "1em !important",
    "&:nth-child(1)": {
      paddingLeft: "0em !important",
    },
    "@media (max-width: 450px)": {
      fontSize: "0.8rem",
    },
  },
  headerTable: {
    color: "#0A3A34",
    fontWeight: "bold",
    fontSize: "1rem",
    "@media (max-width: 450px)": {
      fontSize: "0.9rem",
    },
  },
  buttonAdd: {
    background: "#37774B",
    marginBottom: "0.2em",
    color: "#eee",
    marginRight: "1em",
    marginTop: "0.4em",
    marginLeft: "1em",
    "@media (max-width: 600px)": {
      // width:"1.5em",
      height:"3em"
    },
    "&:hover": {
      background: "#50AF50",
    },
  },
  buttonClear: {
    background: "#37774B",
    marginBottom: "0.1em",
    color: "#eee",
    padding: "1em",
    marginLeft: "0.2em",
    height: "max-content",
    "&:hover": {
      background: "#50AF50",
    },
  },
  textAdvance: {
    background: "#fff",
    marginRight: "0.3em",
  },
  rssModal: {
    width: "76%",
    overflowY: "scroll",
    padding: "0 0 1em 0",
    transform: "translate(25%, 15%)",
    maxHeight: "80vh",
    background: "#eee",
    "@media (max-width: 769px)": {
      transform: "translate(15%, 15%)",
      width: "85%",
    },
    "@media (max-width: 425px)": {
      transform: "translate(6%, 15%)",
      width: "98%",
    },
  },
  btnAddGroup:{
    background: "#50AF50",
    marginBottom: "0.4em",
    color: "#eee",
    marginTop: "0.4em",
    marginLeft: "1em",
    "@media (max-width: 600px)": {
      // width:"1.5em",
      height:"3em"
    },
    "&:hover": {
      background: "#55d555",
    },
  },
  hoverBtn:{
    "&:hover": {
      background: "transparent.100",
    },
  }
};

export default dashboardStyle;
