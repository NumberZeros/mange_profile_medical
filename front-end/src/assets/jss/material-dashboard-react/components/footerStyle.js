import {
  defaultFont,
  container,
  primaryColor,
  grayColor,
} from "assets/jss/material-dashboard-react.js";

const footerStyle = {
  block: {
    color: "inherit",
    padding: "15px",
    textTransform: "uppercase",
    borderRadius: "3px",
    textDecoration: "none",
    position: "relative",
    display: "block",
    ...defaultFont,
    fontWeight: "500",
    fontSize: "12px",
  },
  left: {
    float: "left!important",
    display: "block",
    fontSize:"0.9rem"
  },
  leftOut: {
    fontSize:"0.9rem",
    margin:"0 auto",
  },
  right: {
    padding: "0 0.5em 0 0 ",
    margin: "0 1em 0 0",
    fontSize: "14px",
    float: "right",
    "@media (max-width: 600px)": {
      float: "none",
      justifyContent:"center",
      margin: "0 0 0 0",
    },
  },
  rightOut: {
    padding: "0 0.5em 0 0 ",
    margin: "0 auto",
    fontSize: "14px",
  },
  footer: {
    bottom: "0",
    borderTop: "1px solid " + grayColor[11],
    padding: "15px 0 0 0",
    ...defaultFont,
  },
  container,
  a: {
    color: primaryColor,
    textDecoration: "none",
    backgroundColor: "transparent",
  },
  list: {
    marginBottom: "0",
    padding: "0",
    marginTop: "0",
  },
  inlineBlock: {
    display: "inline-block",
    padding: "0px",
    width: "auto",
  },
  footerInfo: {
    display: "flex",
  },
  footerLeftLabel:{
    margin:"0.1em 0 0 2em",
    "@media (max-width: 600px)": {
      fontSize:"0.9em"
    },
  },
  infoSpan:{
    color:"#50AF50",
    fontWeight:"bold",
    "@media (max-width: 600px)": {
      fontSize:"0.9em"
    },
  },
  contact:{
    color:"black"
  },
  cpr:{
    placeSelf:"flex-end",
    whiteSpace:"nowrap",
    "@media (max-width: 1024px)": {
      textAlign:"center"
    },
  },
  bottomTitle:{
    "@media (max-width: 600px)": {
      fontSize:"0.7em"
    },
  }
};
export default footerStyle;
